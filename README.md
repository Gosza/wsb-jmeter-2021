

## Projekt i wykonanie testów wydajnościowych aplikacji webowej przy pomocy narzędzia jmeter 

W repozytorium znajdują się scenariusze i przypadki testów wydajnościowych aplikacji WordPress stworzone na potrzeby pracy dyplomowej. 
Testy wykonywane będą za pomącą narzędzia jmeter.

## Struktura folderów

W katalogu głównym znajdują się foldery odpowiadające poszczególnym przypadkom testowym. W każdym folderze znajduje się scenariusz testowy oraz gotowy test który można uruchomić za pomocą narzędzia jmeter.

## Uruchomienie testu

Tutaj będzie znajdowała się instrukcja uruchomienia przypadku testowego.