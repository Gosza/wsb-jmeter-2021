/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 1609.0, "minX": 0.0, "maxY": 20300.0, "series": [{"data": [[0.0, 1609.0], [0.1, 1609.0], [0.2, 1609.0], [0.3, 1609.0], [0.4, 1609.0], [0.5, 1609.0], [0.6, 1609.0], [0.7, 1609.0], [0.8, 1609.0], [0.9, 1609.0], [1.0, 1661.0], [1.1, 1661.0], [1.2, 1661.0], [1.3, 1661.0], [1.4, 1661.0], [1.5, 1661.0], [1.6, 1661.0], [1.7, 1661.0], [1.8, 1661.0], [1.9, 1661.0], [2.0, 1703.0], [2.1, 1703.0], [2.2, 1703.0], [2.3, 1703.0], [2.4, 1703.0], [2.5, 1703.0], [2.6, 1703.0], [2.7, 1703.0], [2.8, 1703.0], [2.9, 1703.0], [3.0, 2139.0], [3.1, 2139.0], [3.2, 2139.0], [3.3, 2139.0], [3.4, 2139.0], [3.5, 2139.0], [3.6, 2139.0], [3.7, 2139.0], [3.8, 2139.0], [3.9, 2139.0], [4.0, 2251.0], [4.1, 2251.0], [4.2, 2251.0], [4.3, 2251.0], [4.4, 2251.0], [4.5, 2251.0], [4.6, 2251.0], [4.7, 2251.0], [4.8, 2251.0], [4.9, 2251.0], [5.0, 2525.0], [5.1, 2525.0], [5.2, 2525.0], [5.3, 2525.0], [5.4, 2525.0], [5.5, 2525.0], [5.6, 2525.0], [5.7, 2525.0], [5.8, 2525.0], [5.9, 2525.0], [6.0, 2778.0], [6.1, 2778.0], [6.2, 2778.0], [6.3, 2778.0], [6.4, 2778.0], [6.5, 2778.0], [6.6, 2778.0], [6.7, 2778.0], [6.8, 2778.0], [6.9, 2778.0], [7.0, 2836.0], [7.1, 2836.0], [7.2, 2836.0], [7.3, 2836.0], [7.4, 2836.0], [7.5, 2836.0], [7.6, 2836.0], [7.7, 2836.0], [7.8, 2836.0], [7.9, 2836.0], [8.0, 2926.0], [8.1, 2926.0], [8.2, 2926.0], [8.3, 2926.0], [8.4, 2926.0], [8.5, 2926.0], [8.6, 2926.0], [8.7, 2926.0], [8.8, 2926.0], [8.9, 2926.0], [9.0, 3046.0], [9.1, 3046.0], [9.2, 3046.0], [9.3, 3046.0], [9.4, 3046.0], [9.5, 3046.0], [9.6, 3046.0], [9.7, 3046.0], [9.8, 3046.0], [9.9, 3046.0], [10.0, 3556.0], [10.1, 3556.0], [10.2, 3556.0], [10.3, 3556.0], [10.4, 3556.0], [10.5, 3556.0], [10.6, 3556.0], [10.7, 3556.0], [10.8, 3556.0], [10.9, 3556.0], [11.0, 3576.0], [11.1, 3576.0], [11.2, 3576.0], [11.3, 3576.0], [11.4, 3576.0], [11.5, 3576.0], [11.6, 3576.0], [11.7, 3576.0], [11.8, 3576.0], [11.9, 3576.0], [12.0, 3847.0], [12.1, 3847.0], [12.2, 3847.0], [12.3, 3847.0], [12.4, 3847.0], [12.5, 3847.0], [12.6, 3847.0], [12.7, 3847.0], [12.8, 3847.0], [12.9, 3847.0], [13.0, 4099.0], [13.1, 4099.0], [13.2, 4099.0], [13.3, 4099.0], [13.4, 4099.0], [13.5, 4099.0], [13.6, 4099.0], [13.7, 4099.0], [13.8, 4099.0], [13.9, 4099.0], [14.0, 4140.0], [14.1, 4140.0], [14.2, 4140.0], [14.3, 4140.0], [14.4, 4140.0], [14.5, 4140.0], [14.6, 4140.0], [14.7, 4140.0], [14.8, 4140.0], [14.9, 4140.0], [15.0, 4356.0], [15.1, 4356.0], [15.2, 4356.0], [15.3, 4356.0], [15.4, 4356.0], [15.5, 4356.0], [15.6, 4356.0], [15.7, 4356.0], [15.8, 4356.0], [15.9, 4356.0], [16.0, 4410.0], [16.1, 4410.0], [16.2, 4410.0], [16.3, 4410.0], [16.4, 4410.0], [16.5, 4410.0], [16.6, 4410.0], [16.7, 4410.0], [16.8, 4410.0], [16.9, 4410.0], [17.0, 4558.0], [17.1, 4558.0], [17.2, 4558.0], [17.3, 4558.0], [17.4, 4558.0], [17.5, 4558.0], [17.6, 4558.0], [17.7, 4558.0], [17.8, 4558.0], [17.9, 4558.0], [18.0, 4900.0], [18.1, 4900.0], [18.2, 4900.0], [18.3, 4900.0], [18.4, 4900.0], [18.5, 4900.0], [18.6, 4900.0], [18.7, 4900.0], [18.8, 4900.0], [18.9, 4900.0], [19.0, 5222.0], [19.1, 5222.0], [19.2, 5222.0], [19.3, 5222.0], [19.4, 5222.0], [19.5, 5222.0], [19.6, 5222.0], [19.7, 5222.0], [19.8, 5222.0], [19.9, 5222.0], [20.0, 5304.0], [20.1, 5304.0], [20.2, 5304.0], [20.3, 5304.0], [20.4, 5304.0], [20.5, 5304.0], [20.6, 5304.0], [20.7, 5304.0], [20.8, 5304.0], [20.9, 5304.0], [21.0, 5362.0], [21.1, 5362.0], [21.2, 5362.0], [21.3, 5362.0], [21.4, 5362.0], [21.5, 5362.0], [21.6, 5362.0], [21.7, 5362.0], [21.8, 5362.0], [21.9, 5362.0], [22.0, 5469.0], [22.1, 5469.0], [22.2, 5469.0], [22.3, 5469.0], [22.4, 5469.0], [22.5, 5469.0], [22.6, 5469.0], [22.7, 5469.0], [22.8, 5469.0], [22.9, 5469.0], [23.0, 5695.0], [23.1, 5695.0], [23.2, 5695.0], [23.3, 5695.0], [23.4, 5695.0], [23.5, 5695.0], [23.6, 5695.0], [23.7, 5695.0], [23.8, 5695.0], [23.9, 5695.0], [24.0, 5814.0], [24.1, 5814.0], [24.2, 5814.0], [24.3, 5814.0], [24.4, 5814.0], [24.5, 5814.0], [24.6, 5814.0], [24.7, 5814.0], [24.8, 5814.0], [24.9, 5814.0], [25.0, 6315.0], [25.1, 6315.0], [25.2, 6315.0], [25.3, 6315.0], [25.4, 6315.0], [25.5, 6315.0], [25.6, 6315.0], [25.7, 6315.0], [25.8, 6315.0], [25.9, 6315.0], [26.0, 6399.0], [26.1, 6399.0], [26.2, 6399.0], [26.3, 6399.0], [26.4, 6399.0], [26.5, 6399.0], [26.6, 6399.0], [26.7, 6399.0], [26.8, 6399.0], [26.9, 6399.0], [27.0, 6488.0], [27.1, 6488.0], [27.2, 6488.0], [27.3, 6488.0], [27.4, 6488.0], [27.5, 6488.0], [27.6, 6488.0], [27.7, 6488.0], [27.8, 6488.0], [27.9, 6488.0], [28.0, 6742.0], [28.1, 6742.0], [28.2, 6742.0], [28.3, 6742.0], [28.4, 6742.0], [28.5, 6742.0], [28.6, 6742.0], [28.7, 6742.0], [28.8, 6742.0], [28.9, 6742.0], [29.0, 6897.0], [29.1, 6897.0], [29.2, 6897.0], [29.3, 6897.0], [29.4, 6897.0], [29.5, 6897.0], [29.6, 6897.0], [29.7, 6897.0], [29.8, 6897.0], [29.9, 6897.0], [30.0, 7093.0], [30.1, 7093.0], [30.2, 7093.0], [30.3, 7093.0], [30.4, 7093.0], [30.5, 7093.0], [30.6, 7093.0], [30.7, 7093.0], [30.8, 7093.0], [30.9, 7093.0], [31.0, 7097.0], [31.1, 7097.0], [31.2, 7097.0], [31.3, 7097.0], [31.4, 7097.0], [31.5, 7097.0], [31.6, 7097.0], [31.7, 7097.0], [31.8, 7097.0], [31.9, 7097.0], [32.0, 7594.0], [32.1, 7594.0], [32.2, 7594.0], [32.3, 7594.0], [32.4, 7594.0], [32.5, 7594.0], [32.6, 7594.0], [32.7, 7594.0], [32.8, 7594.0], [32.9, 7594.0], [33.0, 7800.0], [33.1, 7800.0], [33.2, 7800.0], [33.3, 7800.0], [33.4, 7800.0], [33.5, 7800.0], [33.6, 7800.0], [33.7, 7800.0], [33.8, 7800.0], [33.9, 7800.0], [34.0, 7820.0], [34.1, 7820.0], [34.2, 7820.0], [34.3, 7820.0], [34.4, 7820.0], [34.5, 7820.0], [34.6, 7820.0], [34.7, 7820.0], [34.8, 7820.0], [34.9, 7820.0], [35.0, 7960.0], [35.1, 7960.0], [35.2, 7960.0], [35.3, 7960.0], [35.4, 7960.0], [35.5, 7960.0], [35.6, 7960.0], [35.7, 7960.0], [35.8, 7960.0], [35.9, 7960.0], [36.0, 8416.0], [36.1, 8416.0], [36.2, 8416.0], [36.3, 8416.0], [36.4, 8416.0], [36.5, 8416.0], [36.6, 8416.0], [36.7, 8416.0], [36.8, 8416.0], [36.9, 8416.0], [37.0, 8476.0], [37.1, 8476.0], [37.2, 8476.0], [37.3, 8476.0], [37.4, 8476.0], [37.5, 8476.0], [37.6, 8476.0], [37.7, 8476.0], [37.8, 8476.0], [37.9, 8476.0], [38.0, 8701.0], [38.1, 8701.0], [38.2, 8701.0], [38.3, 8701.0], [38.4, 8701.0], [38.5, 8701.0], [38.6, 8701.0], [38.7, 8701.0], [38.8, 8701.0], [38.9, 8701.0], [39.0, 8825.0], [39.1, 8825.0], [39.2, 8825.0], [39.3, 8825.0], [39.4, 8825.0], [39.5, 8825.0], [39.6, 8825.0], [39.7, 8825.0], [39.8, 8825.0], [39.9, 8825.0], [40.0, 9114.0], [40.1, 9114.0], [40.2, 9114.0], [40.3, 9114.0], [40.4, 9114.0], [40.5, 9114.0], [40.6, 9114.0], [40.7, 9114.0], [40.8, 9114.0], [40.9, 9114.0], [41.0, 9321.0], [41.1, 9321.0], [41.2, 9321.0], [41.3, 9321.0], [41.4, 9321.0], [41.5, 9321.0], [41.6, 9321.0], [41.7, 9321.0], [41.8, 9321.0], [41.9, 9321.0], [42.0, 9699.0], [42.1, 9699.0], [42.2, 9699.0], [42.3, 9699.0], [42.4, 9699.0], [42.5, 9699.0], [42.6, 9699.0], [42.7, 9699.0], [42.8, 9699.0], [42.9, 9699.0], [43.0, 9705.0], [43.1, 9705.0], [43.2, 9705.0], [43.3, 9705.0], [43.4, 9705.0], [43.5, 9705.0], [43.6, 9705.0], [43.7, 9705.0], [43.8, 9705.0], [43.9, 9705.0], [44.0, 9962.0], [44.1, 9962.0], [44.2, 9962.0], [44.3, 9962.0], [44.4, 9962.0], [44.5, 9962.0], [44.6, 9962.0], [44.7, 9962.0], [44.8, 9962.0], [44.9, 9962.0], [45.0, 10285.0], [45.1, 10285.0], [45.2, 10285.0], [45.3, 10285.0], [45.4, 10285.0], [45.5, 10285.0], [45.6, 10285.0], [45.7, 10285.0], [45.8, 10285.0], [45.9, 10285.0], [46.0, 10329.0], [46.1, 10329.0], [46.2, 10329.0], [46.3, 10329.0], [46.4, 10329.0], [46.5, 10329.0], [46.6, 10329.0], [46.7, 10329.0], [46.8, 10329.0], [46.9, 10329.0], [47.0, 10549.0], [47.1, 10549.0], [47.2, 10549.0], [47.3, 10549.0], [47.4, 10549.0], [47.5, 10549.0], [47.6, 10549.0], [47.7, 10549.0], [47.8, 10549.0], [47.9, 10549.0], [48.0, 10695.0], [48.1, 10695.0], [48.2, 10695.0], [48.3, 10695.0], [48.4, 10695.0], [48.5, 10695.0], [48.6, 10695.0], [48.7, 10695.0], [48.8, 10695.0], [48.9, 10695.0], [49.0, 11221.0], [49.1, 11221.0], [49.2, 11221.0], [49.3, 11221.0], [49.4, 11221.0], [49.5, 11221.0], [49.6, 11221.0], [49.7, 11221.0], [49.8, 11221.0], [49.9, 11221.0], [50.0, 11287.0], [50.1, 11287.0], [50.2, 11287.0], [50.3, 11287.0], [50.4, 11287.0], [50.5, 11287.0], [50.6, 11287.0], [50.7, 11287.0], [50.8, 11287.0], [50.9, 11287.0], [51.0, 11413.0], [51.1, 11413.0], [51.2, 11413.0], [51.3, 11413.0], [51.4, 11413.0], [51.5, 11413.0], [51.6, 11413.0], [51.7, 11413.0], [51.8, 11413.0], [51.9, 11413.0], [52.0, 11546.0], [52.1, 11546.0], [52.2, 11546.0], [52.3, 11546.0], [52.4, 11546.0], [52.5, 11546.0], [52.6, 11546.0], [52.7, 11546.0], [52.8, 11546.0], [52.9, 11546.0], [53.0, 11672.0], [53.1, 11672.0], [53.2, 11672.0], [53.3, 11672.0], [53.4, 11672.0], [53.5, 11672.0], [53.6, 11672.0], [53.7, 11672.0], [53.8, 11672.0], [53.9, 11672.0], [54.0, 11990.0], [54.1, 11990.0], [54.2, 11990.0], [54.3, 11990.0], [54.4, 11990.0], [54.5, 11990.0], [54.6, 11990.0], [54.7, 11990.0], [54.8, 11990.0], [54.9, 11990.0], [55.0, 12013.0], [55.1, 12013.0], [55.2, 12013.0], [55.3, 12013.0], [55.4, 12013.0], [55.5, 12013.0], [55.6, 12013.0], [55.7, 12013.0], [55.8, 12013.0], [55.9, 12013.0], [56.0, 12068.0], [56.1, 12068.0], [56.2, 12068.0], [56.3, 12068.0], [56.4, 12068.0], [56.5, 12068.0], [56.6, 12068.0], [56.7, 12068.0], [56.8, 12068.0], [56.9, 12068.0], [57.0, 12292.0], [57.1, 12292.0], [57.2, 12292.0], [57.3, 12292.0], [57.4, 12292.0], [57.5, 12292.0], [57.6, 12292.0], [57.7, 12292.0], [57.8, 12292.0], [57.9, 12292.0], [58.0, 12576.0], [58.1, 12576.0], [58.2, 12576.0], [58.3, 12576.0], [58.4, 12576.0], [58.5, 12576.0], [58.6, 12576.0], [58.7, 12576.0], [58.8, 12576.0], [58.9, 12576.0], [59.0, 12794.0], [59.1, 12794.0], [59.2, 12794.0], [59.3, 12794.0], [59.4, 12794.0], [59.5, 12794.0], [59.6, 12794.0], [59.7, 12794.0], [59.8, 12794.0], [59.9, 12794.0], [60.0, 12873.0], [60.1, 12873.0], [60.2, 12873.0], [60.3, 12873.0], [60.4, 12873.0], [60.5, 12873.0], [60.6, 12873.0], [60.7, 12873.0], [60.8, 12873.0], [60.9, 12873.0], [61.0, 12943.0], [61.1, 12943.0], [61.2, 12943.0], [61.3, 12943.0], [61.4, 12943.0], [61.5, 12943.0], [61.6, 12943.0], [61.7, 12943.0], [61.8, 12943.0], [61.9, 12943.0], [62.0, 13209.0], [62.1, 13209.0], [62.2, 13209.0], [62.3, 13209.0], [62.4, 13209.0], [62.5, 13209.0], [62.6, 13209.0], [62.7, 13209.0], [62.8, 13209.0], [62.9, 13209.0], [63.0, 13452.0], [63.1, 13452.0], [63.2, 13452.0], [63.3, 13452.0], [63.4, 13452.0], [63.5, 13452.0], [63.6, 13452.0], [63.7, 13452.0], [63.8, 13452.0], [63.9, 13452.0], [64.0, 13530.0], [64.1, 13530.0], [64.2, 13530.0], [64.3, 13530.0], [64.4, 13530.0], [64.5, 13530.0], [64.6, 13530.0], [64.7, 13530.0], [64.8, 13530.0], [64.9, 13530.0], [65.0, 13877.0], [65.1, 13877.0], [65.2, 13877.0], [65.3, 13877.0], [65.4, 13877.0], [65.5, 13877.0], [65.6, 13877.0], [65.7, 13877.0], [65.8, 13877.0], [65.9, 13877.0], [66.0, 13941.0], [66.1, 13941.0], [66.2, 13941.0], [66.3, 13941.0], [66.4, 13941.0], [66.5, 13941.0], [66.6, 13941.0], [66.7, 13941.0], [66.8, 13941.0], [66.9, 13941.0], [67.0, 14267.0], [67.1, 14267.0], [67.2, 14267.0], [67.3, 14267.0], [67.4, 14267.0], [67.5, 14267.0], [67.6, 14267.0], [67.7, 14267.0], [67.8, 14267.0], [67.9, 14267.0], [68.0, 14413.0], [68.1, 14413.0], [68.2, 14413.0], [68.3, 14413.0], [68.4, 14413.0], [68.5, 14413.0], [68.6, 14413.0], [68.7, 14413.0], [68.8, 14413.0], [68.9, 14413.0], [69.0, 14574.0], [69.1, 14574.0], [69.2, 14574.0], [69.3, 14574.0], [69.4, 14574.0], [69.5, 14574.0], [69.6, 14574.0], [69.7, 14574.0], [69.8, 14574.0], [69.9, 14574.0], [70.0, 15072.0], [70.1, 15072.0], [70.2, 15072.0], [70.3, 15072.0], [70.4, 15072.0], [70.5, 15072.0], [70.6, 15072.0], [70.7, 15072.0], [70.8, 15072.0], [70.9, 15072.0], [71.0, 15152.0], [71.1, 15152.0], [71.2, 15152.0], [71.3, 15152.0], [71.4, 15152.0], [71.5, 15152.0], [71.6, 15152.0], [71.7, 15152.0], [71.8, 15152.0], [71.9, 15152.0], [72.0, 15190.0], [72.1, 15190.0], [72.2, 15190.0], [72.3, 15190.0], [72.4, 15190.0], [72.5, 15190.0], [72.6, 15190.0], [72.7, 15190.0], [72.8, 15190.0], [72.9, 15190.0], [73.0, 15217.0], [73.1, 15217.0], [73.2, 15217.0], [73.3, 15217.0], [73.4, 15217.0], [73.5, 15217.0], [73.6, 15217.0], [73.7, 15217.0], [73.8, 15217.0], [73.9, 15217.0], [74.0, 15732.0], [74.1, 15732.0], [74.2, 15732.0], [74.3, 15732.0], [74.4, 15732.0], [74.5, 15732.0], [74.6, 15732.0], [74.7, 15732.0], [74.8, 15732.0], [74.9, 15732.0], [75.0, 15754.0], [75.1, 15754.0], [75.2, 15754.0], [75.3, 15754.0], [75.4, 15754.0], [75.5, 15754.0], [75.6, 15754.0], [75.7, 15754.0], [75.8, 15754.0], [75.9, 15754.0], [76.0, 15896.0], [76.1, 15896.0], [76.2, 15896.0], [76.3, 15896.0], [76.4, 15896.0], [76.5, 15896.0], [76.6, 15896.0], [76.7, 15896.0], [76.8, 15896.0], [76.9, 15896.0], [77.0, 16275.0], [77.1, 16275.0], [77.2, 16275.0], [77.3, 16275.0], [77.4, 16275.0], [77.5, 16275.0], [77.6, 16275.0], [77.7, 16275.0], [77.8, 16275.0], [77.9, 16275.0], [78.0, 16423.0], [78.1, 16423.0], [78.2, 16423.0], [78.3, 16423.0], [78.4, 16423.0], [78.5, 16423.0], [78.6, 16423.0], [78.7, 16423.0], [78.8, 16423.0], [78.9, 16423.0], [79.0, 16580.0], [79.1, 16580.0], [79.2, 16580.0], [79.3, 16580.0], [79.4, 16580.0], [79.5, 16580.0], [79.6, 16580.0], [79.7, 16580.0], [79.8, 16580.0], [79.9, 16580.0], [80.0, 16951.0], [80.1, 16951.0], [80.2, 16951.0], [80.3, 16951.0], [80.4, 16951.0], [80.5, 16951.0], [80.6, 16951.0], [80.7, 16951.0], [80.8, 16951.0], [80.9, 16951.0], [81.0, 17236.0], [81.1, 17236.0], [81.2, 17236.0], [81.3, 17236.0], [81.4, 17236.0], [81.5, 17236.0], [81.6, 17236.0], [81.7, 17236.0], [81.8, 17236.0], [81.9, 17236.0], [82.0, 17304.0], [82.1, 17304.0], [82.2, 17304.0], [82.3, 17304.0], [82.4, 17304.0], [82.5, 17304.0], [82.6, 17304.0], [82.7, 17304.0], [82.8, 17304.0], [82.9, 17304.0], [83.0, 17569.0], [83.1, 17569.0], [83.2, 17569.0], [83.3, 17569.0], [83.4, 17569.0], [83.5, 17569.0], [83.6, 17569.0], [83.7, 17569.0], [83.8, 17569.0], [83.9, 17569.0], [84.0, 17705.0], [84.1, 17705.0], [84.2, 17705.0], [84.3, 17705.0], [84.4, 17705.0], [84.5, 17705.0], [84.6, 17705.0], [84.7, 17705.0], [84.8, 17705.0], [84.9, 17705.0], [85.0, 17818.0], [85.1, 17818.0], [85.2, 17818.0], [85.3, 17818.0], [85.4, 17818.0], [85.5, 17818.0], [85.6, 17818.0], [85.7, 17818.0], [85.8, 17818.0], [85.9, 17818.0], [86.0, 17859.0], [86.1, 17859.0], [86.2, 17859.0], [86.3, 17859.0], [86.4, 17859.0], [86.5, 17859.0], [86.6, 17859.0], [86.7, 17859.0], [86.8, 17859.0], [86.9, 17859.0], [87.0, 18234.0], [87.1, 18234.0], [87.2, 18234.0], [87.3, 18234.0], [87.4, 18234.0], [87.5, 18234.0], [87.6, 18234.0], [87.7, 18234.0], [87.8, 18234.0], [87.9, 18234.0], [88.0, 18504.0], [88.1, 18504.0], [88.2, 18504.0], [88.3, 18504.0], [88.4, 18504.0], [88.5, 18504.0], [88.6, 18504.0], [88.7, 18504.0], [88.8, 18504.0], [88.9, 18504.0], [89.0, 18738.0], [89.1, 18738.0], [89.2, 18738.0], [89.3, 18738.0], [89.4, 18738.0], [89.5, 18738.0], [89.6, 18738.0], [89.7, 18738.0], [89.8, 18738.0], [89.9, 18738.0], [90.0, 18764.0], [90.1, 18764.0], [90.2, 18764.0], [90.3, 18764.0], [90.4, 18764.0], [90.5, 18764.0], [90.6, 18764.0], [90.7, 18764.0], [90.8, 18764.0], [90.9, 18764.0], [91.0, 18824.0], [91.1, 18824.0], [91.2, 18824.0], [91.3, 18824.0], [91.4, 18824.0], [91.5, 18824.0], [91.6, 18824.0], [91.7, 18824.0], [91.8, 18824.0], [91.9, 18824.0], [92.0, 19321.0], [92.1, 19321.0], [92.2, 19321.0], [92.3, 19321.0], [92.4, 19321.0], [92.5, 19321.0], [92.6, 19321.0], [92.7, 19321.0], [92.8, 19321.0], [92.9, 19321.0], [93.0, 19357.0], [93.1, 19357.0], [93.2, 19357.0], [93.3, 19357.0], [93.4, 19357.0], [93.5, 19357.0], [93.6, 19357.0], [93.7, 19357.0], [93.8, 19357.0], [93.9, 19357.0], [94.0, 19773.0], [94.1, 19773.0], [94.2, 19773.0], [94.3, 19773.0], [94.4, 19773.0], [94.5, 19773.0], [94.6, 19773.0], [94.7, 19773.0], [94.8, 19773.0], [94.9, 19773.0], [95.0, 19998.0], [95.1, 19998.0], [95.2, 19998.0], [95.3, 19998.0], [95.4, 19998.0], [95.5, 19998.0], [95.6, 19998.0], [95.7, 19998.0], [95.8, 19998.0], [95.9, 19998.0], [96.0, 20149.0], [96.1, 20149.0], [96.2, 20149.0], [96.3, 20149.0], [96.4, 20149.0], [96.5, 20149.0], [96.6, 20149.0], [96.7, 20149.0], [96.8, 20149.0], [96.9, 20149.0], [97.0, 20151.0], [97.1, 20151.0], [97.2, 20151.0], [97.3, 20151.0], [97.4, 20151.0], [97.5, 20151.0], [97.6, 20151.0], [97.7, 20151.0], [97.8, 20151.0], [97.9, 20151.0], [98.0, 20221.0], [98.1, 20221.0], [98.2, 20221.0], [98.3, 20221.0], [98.4, 20221.0], [98.5, 20221.0], [98.6, 20221.0], [98.7, 20221.0], [98.8, 20221.0], [98.9, 20221.0], [99.0, 20300.0], [99.1, 20300.0], [99.2, 20300.0], [99.3, 20300.0], [99.4, 20300.0], [99.5, 20300.0], [99.6, 20300.0], [99.7, 20300.0], [99.8, 20300.0], [99.9, 20300.0]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 1600.0, "maxY": 2.0, "series": [{"data": [[1600.0, 2.0], [1700.0, 1.0], [2100.0, 1.0], [2200.0, 1.0], [2500.0, 1.0], [2700.0, 1.0], [2800.0, 1.0], [2900.0, 1.0], [3000.0, 1.0], [3500.0, 2.0], [3800.0, 1.0], [4000.0, 1.0], [4100.0, 1.0], [4300.0, 1.0], [4400.0, 1.0], [4500.0, 1.0], [4900.0, 1.0], [5200.0, 1.0], [5300.0, 2.0], [5400.0, 1.0], [5600.0, 1.0], [5800.0, 1.0], [6300.0, 2.0], [6400.0, 1.0], [6700.0, 1.0], [6800.0, 1.0], [7000.0, 2.0], [7500.0, 1.0], [7800.0, 2.0], [7900.0, 1.0], [8400.0, 2.0], [8700.0, 1.0], [8800.0, 1.0], [9100.0, 1.0], [9300.0, 1.0], [9600.0, 1.0], [9700.0, 1.0], [9900.0, 1.0], [10200.0, 1.0], [10300.0, 1.0], [10500.0, 1.0], [10600.0, 1.0], [11200.0, 2.0], [11400.0, 1.0], [11600.0, 1.0], [11500.0, 1.0], [12000.0, 2.0], [11900.0, 1.0], [12200.0, 1.0], [12500.0, 1.0], [12700.0, 1.0], [12800.0, 1.0], [12900.0, 1.0], [13200.0, 1.0], [13400.0, 1.0], [13500.0, 1.0], [13800.0, 1.0], [13900.0, 1.0], [14200.0, 1.0], [14400.0, 1.0], [14500.0, 1.0], [15000.0, 1.0], [15100.0, 2.0], [15200.0, 1.0], [15700.0, 2.0], [15800.0, 1.0], [16200.0, 1.0], [16400.0, 1.0], [16500.0, 1.0], [16900.0, 1.0], [17200.0, 1.0], [17300.0, 1.0], [17500.0, 1.0], [17700.0, 1.0], [17800.0, 2.0], [18200.0, 1.0], [18500.0, 1.0], [18700.0, 2.0], [18800.0, 1.0], [19300.0, 2.0], [19700.0, 1.0], [19900.0, 1.0], [20100.0, 2.0], [20200.0, 1.0], [20300.0, 1.0]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 20300.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 100.0, "minX": 2.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 100.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 100.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 50.5, "minX": 1.62298452E12, "maxY": 50.5, "series": [{"data": [[1.62298452E12, 50.5]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298452E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 1609.0, "minX": 1.0, "maxY": 20300.0, "series": [{"data": [[2.0, 20221.0], [3.0, 20151.0], [4.0, 20149.0], [5.0, 19998.0], [6.0, 19773.0], [7.0, 19357.0], [8.0, 19321.0], [9.0, 18824.0], [10.0, 18764.0], [11.0, 18738.0], [12.0, 18504.0], [13.0, 18234.0], [14.0, 17859.0], [15.0, 17818.0], [16.0, 17705.0], [17.0, 17569.0], [18.0, 17304.0], [19.0, 17236.0], [20.0, 16951.0], [21.0, 16580.0], [22.0, 16423.0], [23.0, 16275.0], [24.0, 15896.0], [25.0, 15754.0], [26.0, 15732.0], [27.0, 15217.0], [28.0, 15190.0], [29.0, 15152.0], [30.0, 15072.0], [31.0, 14574.0], [33.0, 14267.0], [32.0, 14413.0], [35.0, 13877.0], [34.0, 13941.0], [37.0, 13452.0], [36.0, 13530.0], [39.0, 12943.0], [38.0, 13209.0], [41.0, 12794.0], [40.0, 12873.0], [43.0, 12292.0], [42.0, 12576.0], [45.0, 12068.0], [44.0, 11990.0], [47.0, 11546.0], [46.0, 12013.0], [49.0, 11413.0], [48.0, 11672.0], [51.0, 11221.0], [50.0, 11287.0], [53.0, 10549.0], [52.0, 10695.0], [55.0, 10285.0], [54.0, 10329.0], [57.0, 9705.0], [56.0, 9962.0], [59.0, 9321.0], [58.0, 9699.0], [61.0, 8825.0], [60.0, 9114.0], [63.0, 8476.0], [62.0, 8701.0], [67.0, 7800.0], [66.0, 7820.0], [65.0, 7960.0], [64.0, 8416.0], [71.0, 6897.0], [70.0, 7093.0], [69.0, 7097.0], [68.0, 7594.0], [75.0, 6315.0], [74.0, 6399.0], [73.0, 6488.0], [72.0, 6742.0], [79.0, 5362.0], [78.0, 5469.0], [77.0, 5695.0], [76.0, 5814.0], [83.0, 4558.0], [82.0, 4900.0], [81.0, 5222.0], [80.0, 5304.0], [87.0, 4099.0], [86.0, 4140.0], [85.0, 4410.0], [84.0, 4356.0], [91.0, 3046.0], [90.0, 3556.0], [89.0, 3576.0], [88.0, 3847.0], [95.0, 2525.0], [94.0, 2778.0], [93.0, 2836.0], [92.0, 2926.0], [99.0, 1661.0], [98.0, 1703.0], [97.0, 2139.0], [96.0, 2251.0], [100.0, 1609.0], [1.0, 20300.0]], "isOverall": false, "label": " View Pages", "isController": false}, {"data": [[50.5, 10960.820000000002]], "isOverall": false, "label": " View Pages-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 386.6666666666667, "minX": 1.62298452E12, "maxY": 24143.333333333332, "series": [{"data": [[1.62298452E12, 24143.333333333332]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62298452E12, 386.6666666666667]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298452E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 10960.820000000002, "minX": 1.62298452E12, "maxY": 10960.820000000002, "series": [{"data": [[1.62298452E12, 10960.820000000002]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298452E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 10958.669999999998, "minX": 1.62298452E12, "maxY": 10958.669999999998, "series": [{"data": [[1.62298452E12, 10958.669999999998]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298452E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 0.9099999999999998, "minX": 1.62298452E12, "maxY": 0.9099999999999998, "series": [{"data": [[1.62298452E12, 0.9099999999999998]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298452E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 1609.0, "minX": 1.62298452E12, "maxY": 20300.0, "series": [{"data": [[1.62298452E12, 20300.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62298452E12, 1609.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62298452E12, 18761.4]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62298452E12, 20299.21]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62298452E12, 19986.749999999996]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298452E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 6996.5, "minX": 3.0, "maxY": 16423.5, "series": [{"data": [[4.0, 6996.5], [5.0, 11310.5], [3.0, 15424.5], [6.0, 8763.0], [7.0, 16423.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 7.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 6994.5, "minX": 3.0, "maxY": 16420.5, "series": [{"data": [[4.0, 6994.5], [5.0, 11308.5], [3.0, 15423.0], [6.0, 8761.0], [7.0, 16420.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 7.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298452E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298452E12, 1.6666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298452E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298452E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298452E12, 1.6666666666666667]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298452E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298452E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298452E12, 1.6666666666666667]], "isOverall": false, "label": " View Pages-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298452E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298452E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298452E12, 1.6666666666666667]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298452E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

