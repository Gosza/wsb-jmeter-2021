/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 1040.0, "minX": 0.0, "maxY": 6352.0, "series": [{"data": [[0.0, 1040.0], [0.1, 1040.0], [0.2, 1040.0], [0.3, 1040.0], [0.4, 1040.0], [0.5, 1040.0], [0.6, 1040.0], [0.7, 1040.0], [0.8, 1040.0], [0.9, 1040.0], [1.0, 1056.0], [1.1, 1056.0], [1.2, 1056.0], [1.3, 1056.0], [1.4, 1056.0], [1.5, 1056.0], [1.6, 1056.0], [1.7, 1056.0], [1.8, 1056.0], [1.9, 1056.0], [2.0, 1189.0], [2.1, 1189.0], [2.2, 1189.0], [2.3, 1189.0], [2.4, 1189.0], [2.5, 1189.0], [2.6, 1189.0], [2.7, 1189.0], [2.8, 1189.0], [2.9, 1189.0], [3.0, 1199.0], [3.1, 1199.0], [3.2, 1199.0], [3.3, 1199.0], [3.4, 1199.0], [3.5, 1199.0], [3.6, 1199.0], [3.7, 1199.0], [3.8, 1199.0], [3.9, 1199.0], [4.0, 1269.0], [4.1, 1269.0], [4.2, 1269.0], [4.3, 1269.0], [4.4, 1269.0], [4.5, 1269.0], [4.6, 1269.0], [4.7, 1269.0], [4.8, 1269.0], [4.9, 1269.0], [5.0, 1334.0], [5.1, 1334.0], [5.2, 1334.0], [5.3, 1334.0], [5.4, 1334.0], [5.5, 1334.0], [5.6, 1334.0], [5.7, 1334.0], [5.8, 1334.0], [5.9, 1334.0], [6.0, 1353.0], [6.1, 1353.0], [6.2, 1353.0], [6.3, 1353.0], [6.4, 1353.0], [6.5, 1353.0], [6.6, 1353.0], [6.7, 1353.0], [6.8, 1353.0], [6.9, 1353.0], [7.0, 1389.0], [7.1, 1389.0], [7.2, 1389.0], [7.3, 1389.0], [7.4, 1389.0], [7.5, 1389.0], [7.6, 1389.0], [7.7, 1389.0], [7.8, 1389.0], [7.9, 1389.0], [8.0, 1556.0], [8.1, 1556.0], [8.2, 1556.0], [8.3, 1556.0], [8.4, 1556.0], [8.5, 1556.0], [8.6, 1556.0], [8.7, 1556.0], [8.8, 1556.0], [8.9, 1556.0], [9.0, 2000.0], [9.1, 2000.0], [9.2, 2000.0], [9.3, 2000.0], [9.4, 2000.0], [9.5, 2000.0], [9.6, 2000.0], [9.7, 2000.0], [9.8, 2000.0], [9.9, 2000.0], [10.0, 2371.0], [10.1, 2371.0], [10.2, 2371.0], [10.3, 2371.0], [10.4, 2371.0], [10.5, 2371.0], [10.6, 2371.0], [10.7, 2371.0], [10.8, 2371.0], [10.9, 2371.0], [11.0, 2479.0], [11.1, 2479.0], [11.2, 2479.0], [11.3, 2479.0], [11.4, 2479.0], [11.5, 2479.0], [11.6, 2479.0], [11.7, 2479.0], [11.8, 2479.0], [11.9, 2479.0], [12.0, 3088.0], [12.1, 3088.0], [12.2, 3088.0], [12.3, 3088.0], [12.4, 3088.0], [12.5, 3088.0], [12.6, 3088.0], [12.7, 3088.0], [12.8, 3088.0], [12.9, 3088.0], [13.0, 3286.0], [13.1, 3286.0], [13.2, 3286.0], [13.3, 3286.0], [13.4, 3286.0], [13.5, 3286.0], [13.6, 3286.0], [13.7, 3286.0], [13.8, 3286.0], [13.9, 3286.0], [14.0, 3291.0], [14.1, 3291.0], [14.2, 3291.0], [14.3, 3291.0], [14.4, 3291.0], [14.5, 3291.0], [14.6, 3291.0], [14.7, 3291.0], [14.8, 3291.0], [14.9, 3291.0], [15.0, 3300.0], [15.1, 3300.0], [15.2, 3300.0], [15.3, 3300.0], [15.4, 3300.0], [15.5, 3300.0], [15.6, 3300.0], [15.7, 3300.0], [15.8, 3300.0], [15.9, 3300.0], [16.0, 3592.0], [16.1, 3592.0], [16.2, 3592.0], [16.3, 3592.0], [16.4, 3592.0], [16.5, 3592.0], [16.6, 3592.0], [16.7, 3592.0], [16.8, 3592.0], [16.9, 3592.0], [17.0, 3692.0], [17.1, 3692.0], [17.2, 3692.0], [17.3, 3692.0], [17.4, 3692.0], [17.5, 3692.0], [17.6, 3692.0], [17.7, 3692.0], [17.8, 3692.0], [17.9, 3692.0], [18.0, 3749.0], [18.1, 3749.0], [18.2, 3749.0], [18.3, 3749.0], [18.4, 3749.0], [18.5, 3749.0], [18.6, 3749.0], [18.7, 3749.0], [18.8, 3749.0], [18.9, 3749.0], [19.0, 3775.0], [19.1, 3775.0], [19.2, 3775.0], [19.3, 3775.0], [19.4, 3775.0], [19.5, 3775.0], [19.6, 3775.0], [19.7, 3775.0], [19.8, 3775.0], [19.9, 3775.0], [20.0, 3777.0], [20.1, 3777.0], [20.2, 3777.0], [20.3, 3777.0], [20.4, 3777.0], [20.5, 3777.0], [20.6, 3777.0], [20.7, 3777.0], [20.8, 3777.0], [20.9, 3777.0], [21.0, 3781.0], [21.1, 3781.0], [21.2, 3781.0], [21.3, 3781.0], [21.4, 3781.0], [21.5, 3781.0], [21.6, 3781.0], [21.7, 3781.0], [21.8, 3781.0], [21.9, 3781.0], [22.0, 3817.0], [22.1, 3817.0], [22.2, 3817.0], [22.3, 3817.0], [22.4, 3817.0], [22.5, 3817.0], [22.6, 3817.0], [22.7, 3817.0], [22.8, 3817.0], [22.9, 3817.0], [23.0, 3819.0], [23.1, 3819.0], [23.2, 3819.0], [23.3, 3819.0], [23.4, 3819.0], [23.5, 3819.0], [23.6, 3819.0], [23.7, 3819.0], [23.8, 3819.0], [23.9, 3819.0], [24.0, 3877.0], [24.1, 3877.0], [24.2, 3877.0], [24.3, 3877.0], [24.4, 3877.0], [24.5, 3877.0], [24.6, 3877.0], [24.7, 3877.0], [24.8, 3877.0], [24.9, 3877.0], [25.0, 3884.0], [25.1, 3884.0], [25.2, 3884.0], [25.3, 3884.0], [25.4, 3884.0], [25.5, 3884.0], [25.6, 3884.0], [25.7, 3884.0], [25.8, 3884.0], [25.9, 3884.0], [26.0, 3921.0], [26.1, 3921.0], [26.2, 3921.0], [26.3, 3921.0], [26.4, 3921.0], [26.5, 3921.0], [26.6, 3921.0], [26.7, 3921.0], [26.8, 3921.0], [26.9, 3921.0], [27.0, 3930.0], [27.1, 3930.0], [27.2, 3930.0], [27.3, 3930.0], [27.4, 3930.0], [27.5, 3930.0], [27.6, 3930.0], [27.7, 3930.0], [27.8, 3930.0], [27.9, 3930.0], [28.0, 3935.0], [28.1, 3935.0], [28.2, 3935.0], [28.3, 3935.0], [28.4, 3935.0], [28.5, 3935.0], [28.6, 3935.0], [28.7, 3935.0], [28.8, 3935.0], [28.9, 3935.0], [29.0, 4034.0], [29.1, 4034.0], [29.2, 4034.0], [29.3, 4034.0], [29.4, 4034.0], [29.5, 4034.0], [29.6, 4034.0], [29.7, 4034.0], [29.8, 4034.0], [29.9, 4034.0], [30.0, 4092.0], [30.1, 4092.0], [30.2, 4092.0], [30.3, 4092.0], [30.4, 4092.0], [30.5, 4092.0], [30.6, 4092.0], [30.7, 4092.0], [30.8, 4092.0], [30.9, 4092.0], [31.0, 4126.0], [31.1, 4126.0], [31.2, 4126.0], [31.3, 4126.0], [31.4, 4126.0], [31.5, 4126.0], [31.6, 4126.0], [31.7, 4126.0], [31.8, 4126.0], [31.9, 4126.0], [32.0, 4179.0], [32.1, 4179.0], [32.2, 4179.0], [32.3, 4179.0], [32.4, 4179.0], [32.5, 4179.0], [32.6, 4179.0], [32.7, 4179.0], [32.8, 4179.0], [32.9, 4179.0], [33.0, 4228.0], [33.1, 4228.0], [33.2, 4228.0], [33.3, 4228.0], [33.4, 4228.0], [33.5, 4228.0], [33.6, 4228.0], [33.7, 4228.0], [33.8, 4228.0], [33.9, 4228.0], [34.0, 4228.0], [34.1, 4228.0], [34.2, 4228.0], [34.3, 4228.0], [34.4, 4228.0], [34.5, 4228.0], [34.6, 4228.0], [34.7, 4228.0], [34.8, 4228.0], [34.9, 4228.0], [35.0, 4232.0], [35.1, 4232.0], [35.2, 4232.0], [35.3, 4232.0], [35.4, 4232.0], [35.5, 4232.0], [35.6, 4232.0], [35.7, 4232.0], [35.8, 4232.0], [35.9, 4232.0], [36.0, 4256.0], [36.1, 4256.0], [36.2, 4256.0], [36.3, 4256.0], [36.4, 4256.0], [36.5, 4256.0], [36.6, 4256.0], [36.7, 4256.0], [36.8, 4256.0], [36.9, 4256.0], [37.0, 4262.0], [37.1, 4262.0], [37.2, 4262.0], [37.3, 4262.0], [37.4, 4262.0], [37.5, 4262.0], [37.6, 4262.0], [37.7, 4262.0], [37.8, 4262.0], [37.9, 4262.0], [38.0, 4296.0], [38.1, 4296.0], [38.2, 4296.0], [38.3, 4296.0], [38.4, 4296.0], [38.5, 4296.0], [38.6, 4296.0], [38.7, 4296.0], [38.8, 4296.0], [38.9, 4296.0], [39.0, 4404.0], [39.1, 4404.0], [39.2, 4404.0], [39.3, 4404.0], [39.4, 4404.0], [39.5, 4404.0], [39.6, 4404.0], [39.7, 4404.0], [39.8, 4404.0], [39.9, 4404.0], [40.0, 4426.0], [40.1, 4426.0], [40.2, 4426.0], [40.3, 4426.0], [40.4, 4426.0], [40.5, 4426.0], [40.6, 4426.0], [40.7, 4426.0], [40.8, 4426.0], [40.9, 4426.0], [41.0, 4453.0], [41.1, 4453.0], [41.2, 4453.0], [41.3, 4453.0], [41.4, 4453.0], [41.5, 4453.0], [41.6, 4453.0], [41.7, 4453.0], [41.8, 4453.0], [41.9, 4453.0], [42.0, 4463.0], [42.1, 4463.0], [42.2, 4463.0], [42.3, 4463.0], [42.4, 4463.0], [42.5, 4463.0], [42.6, 4463.0], [42.7, 4463.0], [42.8, 4463.0], [42.9, 4463.0], [43.0, 4533.0], [43.1, 4533.0], [43.2, 4533.0], [43.3, 4533.0], [43.4, 4533.0], [43.5, 4533.0], [43.6, 4533.0], [43.7, 4533.0], [43.8, 4533.0], [43.9, 4533.0], [44.0, 4537.0], [44.1, 4537.0], [44.2, 4537.0], [44.3, 4537.0], [44.4, 4537.0], [44.5, 4537.0], [44.6, 4537.0], [44.7, 4537.0], [44.8, 4537.0], [44.9, 4537.0], [45.0, 4591.0], [45.1, 4591.0], [45.2, 4591.0], [45.3, 4591.0], [45.4, 4591.0], [45.5, 4591.0], [45.6, 4591.0], [45.7, 4591.0], [45.8, 4591.0], [45.9, 4591.0], [46.0, 4608.0], [46.1, 4608.0], [46.2, 4608.0], [46.3, 4608.0], [46.4, 4608.0], [46.5, 4608.0], [46.6, 4608.0], [46.7, 4608.0], [46.8, 4608.0], [46.9, 4608.0], [47.0, 4616.0], [47.1, 4616.0], [47.2, 4616.0], [47.3, 4616.0], [47.4, 4616.0], [47.5, 4616.0], [47.6, 4616.0], [47.7, 4616.0], [47.8, 4616.0], [47.9, 4616.0], [48.0, 4656.0], [48.1, 4656.0], [48.2, 4656.0], [48.3, 4656.0], [48.4, 4656.0], [48.5, 4656.0], [48.6, 4656.0], [48.7, 4656.0], [48.8, 4656.0], [48.9, 4656.0], [49.0, 4699.0], [49.1, 4699.0], [49.2, 4699.0], [49.3, 4699.0], [49.4, 4699.0], [49.5, 4699.0], [49.6, 4699.0], [49.7, 4699.0], [49.8, 4699.0], [49.9, 4699.0], [50.0, 4701.0], [50.1, 4701.0], [50.2, 4701.0], [50.3, 4701.0], [50.4, 4701.0], [50.5, 4701.0], [50.6, 4701.0], [50.7, 4701.0], [50.8, 4701.0], [50.9, 4701.0], [51.0, 4732.0], [51.1, 4732.0], [51.2, 4732.0], [51.3, 4732.0], [51.4, 4732.0], [51.5, 4732.0], [51.6, 4732.0], [51.7, 4732.0], [51.8, 4732.0], [51.9, 4732.0], [52.0, 4753.0], [52.1, 4753.0], [52.2, 4753.0], [52.3, 4753.0], [52.4, 4753.0], [52.5, 4753.0], [52.6, 4753.0], [52.7, 4753.0], [52.8, 4753.0], [52.9, 4753.0], [53.0, 4765.0], [53.1, 4765.0], [53.2, 4765.0], [53.3, 4765.0], [53.4, 4765.0], [53.5, 4765.0], [53.6, 4765.0], [53.7, 4765.0], [53.8, 4765.0], [53.9, 4765.0], [54.0, 4778.0], [54.1, 4778.0], [54.2, 4778.0], [54.3, 4778.0], [54.4, 4778.0], [54.5, 4778.0], [54.6, 4778.0], [54.7, 4778.0], [54.8, 4778.0], [54.9, 4778.0], [55.0, 4781.0], [55.1, 4781.0], [55.2, 4781.0], [55.3, 4781.0], [55.4, 4781.0], [55.5, 4781.0], [55.6, 4781.0], [55.7, 4781.0], [55.8, 4781.0], [55.9, 4781.0], [56.0, 4788.0], [56.1, 4788.0], [56.2, 4788.0], [56.3, 4788.0], [56.4, 4788.0], [56.5, 4788.0], [56.6, 4788.0], [56.7, 4788.0], [56.8, 4788.0], [56.9, 4788.0], [57.0, 4812.0], [57.1, 4812.0], [57.2, 4812.0], [57.3, 4812.0], [57.4, 4812.0], [57.5, 4812.0], [57.6, 4812.0], [57.7, 4812.0], [57.8, 4812.0], [57.9, 4812.0], [58.0, 4816.0], [58.1, 4816.0], [58.2, 4816.0], [58.3, 4816.0], [58.4, 4816.0], [58.5, 4816.0], [58.6, 4816.0], [58.7, 4816.0], [58.8, 4816.0], [58.9, 4816.0], [59.0, 4842.0], [59.1, 4842.0], [59.2, 4842.0], [59.3, 4842.0], [59.4, 4842.0], [59.5, 4842.0], [59.6, 4842.0], [59.7, 4842.0], [59.8, 4842.0], [59.9, 4842.0], [60.0, 4844.0], [60.1, 4844.0], [60.2, 4844.0], [60.3, 4844.0], [60.4, 4844.0], [60.5, 4844.0], [60.6, 4844.0], [60.7, 4844.0], [60.8, 4844.0], [60.9, 4844.0], [61.0, 4861.0], [61.1, 4861.0], [61.2, 4861.0], [61.3, 4861.0], [61.4, 4861.0], [61.5, 4861.0], [61.6, 4861.0], [61.7, 4861.0], [61.8, 4861.0], [61.9, 4861.0], [62.0, 4861.0], [62.1, 4861.0], [62.2, 4861.0], [62.3, 4861.0], [62.4, 4861.0], [62.5, 4861.0], [62.6, 4861.0], [62.7, 4861.0], [62.8, 4861.0], [62.9, 4861.0], [63.0, 4862.0], [63.1, 4862.0], [63.2, 4862.0], [63.3, 4862.0], [63.4, 4862.0], [63.5, 4862.0], [63.6, 4862.0], [63.7, 4862.0], [63.8, 4862.0], [63.9, 4862.0], [64.0, 4868.0], [64.1, 4868.0], [64.2, 4868.0], [64.3, 4868.0], [64.4, 4868.0], [64.5, 4868.0], [64.6, 4868.0], [64.7, 4868.0], [64.8, 4868.0], [64.9, 4868.0], [65.0, 4876.0], [65.1, 4876.0], [65.2, 4876.0], [65.3, 4876.0], [65.4, 4876.0], [65.5, 4876.0], [65.6, 4876.0], [65.7, 4876.0], [65.8, 4876.0], [65.9, 4876.0], [66.0, 4895.0], [66.1, 4895.0], [66.2, 4895.0], [66.3, 4895.0], [66.4, 4895.0], [66.5, 4895.0], [66.6, 4895.0], [66.7, 4895.0], [66.8, 4895.0], [66.9, 4895.0], [67.0, 4895.0], [67.1, 4895.0], [67.2, 4895.0], [67.3, 4895.0], [67.4, 4895.0], [67.5, 4895.0], [67.6, 4895.0], [67.7, 4895.0], [67.8, 4895.0], [67.9, 4895.0], [68.0, 4912.0], [68.1, 4912.0], [68.2, 4912.0], [68.3, 4912.0], [68.4, 4912.0], [68.5, 4912.0], [68.6, 4912.0], [68.7, 4912.0], [68.8, 4912.0], [68.9, 4912.0], [69.0, 4917.0], [69.1, 4917.0], [69.2, 4917.0], [69.3, 4917.0], [69.4, 4917.0], [69.5, 4917.0], [69.6, 4917.0], [69.7, 4917.0], [69.8, 4917.0], [69.9, 4917.0], [70.0, 4931.0], [70.1, 4931.0], [70.2, 4931.0], [70.3, 4931.0], [70.4, 4931.0], [70.5, 4931.0], [70.6, 4931.0], [70.7, 4931.0], [70.8, 4931.0], [70.9, 4931.0], [71.0, 4931.0], [71.1, 4931.0], [71.2, 4931.0], [71.3, 4931.0], [71.4, 4931.0], [71.5, 4931.0], [71.6, 4931.0], [71.7, 4931.0], [71.8, 4931.0], [71.9, 4931.0], [72.0, 4947.0], [72.1, 4947.0], [72.2, 4947.0], [72.3, 4947.0], [72.4, 4947.0], [72.5, 4947.0], [72.6, 4947.0], [72.7, 4947.0], [72.8, 4947.0], [72.9, 4947.0], [73.0, 4966.0], [73.1, 4966.0], [73.2, 4966.0], [73.3, 4966.0], [73.4, 4966.0], [73.5, 4966.0], [73.6, 4966.0], [73.7, 4966.0], [73.8, 4966.0], [73.9, 4966.0], [74.0, 4971.0], [74.1, 4971.0], [74.2, 4971.0], [74.3, 4971.0], [74.4, 4971.0], [74.5, 4971.0], [74.6, 4971.0], [74.7, 4971.0], [74.8, 4971.0], [74.9, 4971.0], [75.0, 5010.0], [75.1, 5010.0], [75.2, 5010.0], [75.3, 5010.0], [75.4, 5010.0], [75.5, 5010.0], [75.6, 5010.0], [75.7, 5010.0], [75.8, 5010.0], [75.9, 5010.0], [76.0, 5011.0], [76.1, 5011.0], [76.2, 5011.0], [76.3, 5011.0], [76.4, 5011.0], [76.5, 5011.0], [76.6, 5011.0], [76.7, 5011.0], [76.8, 5011.0], [76.9, 5011.0], [77.0, 5027.0], [77.1, 5027.0], [77.2, 5027.0], [77.3, 5027.0], [77.4, 5027.0], [77.5, 5027.0], [77.6, 5027.0], [77.7, 5027.0], [77.8, 5027.0], [77.9, 5027.0], [78.0, 5038.0], [78.1, 5038.0], [78.2, 5038.0], [78.3, 5038.0], [78.4, 5038.0], [78.5, 5038.0], [78.6, 5038.0], [78.7, 5038.0], [78.8, 5038.0], [78.9, 5038.0], [79.0, 5041.0], [79.1, 5041.0], [79.2, 5041.0], [79.3, 5041.0], [79.4, 5041.0], [79.5, 5041.0], [79.6, 5041.0], [79.7, 5041.0], [79.8, 5041.0], [79.9, 5041.0], [80.0, 5061.0], [80.1, 5061.0], [80.2, 5061.0], [80.3, 5061.0], [80.4, 5061.0], [80.5, 5061.0], [80.6, 5061.0], [80.7, 5061.0], [80.8, 5061.0], [80.9, 5061.0], [81.0, 5085.0], [81.1, 5085.0], [81.2, 5085.0], [81.3, 5085.0], [81.4, 5085.0], [81.5, 5085.0], [81.6, 5085.0], [81.7, 5085.0], [81.8, 5085.0], [81.9, 5085.0], [82.0, 5099.0], [82.1, 5099.0], [82.2, 5099.0], [82.3, 5099.0], [82.4, 5099.0], [82.5, 5099.0], [82.6, 5099.0], [82.7, 5099.0], [82.8, 5099.0], [82.9, 5099.0], [83.0, 5117.0], [83.1, 5117.0], [83.2, 5117.0], [83.3, 5117.0], [83.4, 5117.0], [83.5, 5117.0], [83.6, 5117.0], [83.7, 5117.0], [83.8, 5117.0], [83.9, 5117.0], [84.0, 5119.0], [84.1, 5119.0], [84.2, 5119.0], [84.3, 5119.0], [84.4, 5119.0], [84.5, 5119.0], [84.6, 5119.0], [84.7, 5119.0], [84.8, 5119.0], [84.9, 5119.0], [85.0, 5141.0], [85.1, 5141.0], [85.2, 5141.0], [85.3, 5141.0], [85.4, 5141.0], [85.5, 5141.0], [85.6, 5141.0], [85.7, 5141.0], [85.8, 5141.0], [85.9, 5141.0], [86.0, 5149.0], [86.1, 5149.0], [86.2, 5149.0], [86.3, 5149.0], [86.4, 5149.0], [86.5, 5149.0], [86.6, 5149.0], [86.7, 5149.0], [86.8, 5149.0], [86.9, 5149.0], [87.0, 5172.0], [87.1, 5172.0], [87.2, 5172.0], [87.3, 5172.0], [87.4, 5172.0], [87.5, 5172.0], [87.6, 5172.0], [87.7, 5172.0], [87.8, 5172.0], [87.9, 5172.0], [88.0, 5216.0], [88.1, 5216.0], [88.2, 5216.0], [88.3, 5216.0], [88.4, 5216.0], [88.5, 5216.0], [88.6, 5216.0], [88.7, 5216.0], [88.8, 5216.0], [88.9, 5216.0], [89.0, 5223.0], [89.1, 5223.0], [89.2, 5223.0], [89.3, 5223.0], [89.4, 5223.0], [89.5, 5223.0], [89.6, 5223.0], [89.7, 5223.0], [89.8, 5223.0], [89.9, 5223.0], [90.0, 5229.0], [90.1, 5229.0], [90.2, 5229.0], [90.3, 5229.0], [90.4, 5229.0], [90.5, 5229.0], [90.6, 5229.0], [90.7, 5229.0], [90.8, 5229.0], [90.9, 5229.0], [91.0, 5232.0], [91.1, 5232.0], [91.2, 5232.0], [91.3, 5232.0], [91.4, 5232.0], [91.5, 5232.0], [91.6, 5232.0], [91.7, 5232.0], [91.8, 5232.0], [91.9, 5232.0], [92.0, 5239.0], [92.1, 5239.0], [92.2, 5239.0], [92.3, 5239.0], [92.4, 5239.0], [92.5, 5239.0], [92.6, 5239.0], [92.7, 5239.0], [92.8, 5239.0], [92.9, 5239.0], [93.0, 5288.0], [93.1, 5288.0], [93.2, 5288.0], [93.3, 5288.0], [93.4, 5288.0], [93.5, 5288.0], [93.6, 5288.0], [93.7, 5288.0], [93.8, 5288.0], [93.9, 5288.0], [94.0, 5304.0], [94.1, 5304.0], [94.2, 5304.0], [94.3, 5304.0], [94.4, 5304.0], [94.5, 5304.0], [94.6, 5304.0], [94.7, 5304.0], [94.8, 5304.0], [94.9, 5304.0], [95.0, 5339.0], [95.1, 5339.0], [95.2, 5339.0], [95.3, 5339.0], [95.4, 5339.0], [95.5, 5339.0], [95.6, 5339.0], [95.7, 5339.0], [95.8, 5339.0], [95.9, 5339.0], [96.0, 5376.0], [96.1, 5376.0], [96.2, 5376.0], [96.3, 5376.0], [96.4, 5376.0], [96.5, 5376.0], [96.6, 5376.0], [96.7, 5376.0], [96.8, 5376.0], [96.9, 5376.0], [97.0, 5388.0], [97.1, 5388.0], [97.2, 5388.0], [97.3, 5388.0], [97.4, 5388.0], [97.5, 5388.0], [97.6, 5388.0], [97.7, 5388.0], [97.8, 5388.0], [97.9, 5388.0], [98.0, 5478.0], [98.1, 5478.0], [98.2, 5478.0], [98.3, 5478.0], [98.4, 5478.0], [98.5, 5478.0], [98.6, 5478.0], [98.7, 5478.0], [98.8, 5478.0], [98.9, 5478.0], [99.0, 6352.0], [99.1, 6352.0], [99.2, 6352.0], [99.3, 6352.0], [99.4, 6352.0], [99.5, 6352.0], [99.6, 6352.0], [99.7, 6352.0], [99.8, 6352.0], [99.9, 6352.0]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 1000.0, "maxY": 11.0, "series": [{"data": [[1000.0, 2.0], [1100.0, 2.0], [1200.0, 1.0], [1300.0, 3.0], [1500.0, 1.0], [2000.0, 1.0], [2300.0, 1.0], [2400.0, 1.0], [3000.0, 1.0], [3200.0, 2.0], [3300.0, 1.0], [3500.0, 1.0], [3700.0, 4.0], [3600.0, 1.0], [3800.0, 4.0], [3900.0, 3.0], [4000.0, 2.0], [4200.0, 6.0], [4100.0, 2.0], [4400.0, 4.0], [4500.0, 3.0], [4600.0, 4.0], [4800.0, 11.0], [4700.0, 7.0], [5000.0, 8.0], [4900.0, 7.0], [5100.0, 5.0], [5200.0, 6.0], [5300.0, 4.0], [5400.0, 1.0], [6300.0, 1.0]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 6300.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 8.0, "minX": 1.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 92.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 8.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 92.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 21.020000000000003, "minX": 1.62298458E12, "maxY": 21.020000000000003, "series": [{"data": [[1.62298458E12, 21.020000000000003]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298458E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 3286.0, "minX": 1.0, "maxY": 5077.0, "series": [{"data": [[2.0, 3935.0], [3.0, 3930.0], [4.0, 4228.0], [5.0, 4262.0], [6.0, 3777.0], [7.0, 4228.0], [8.0, 3921.0], [9.0, 3692.0], [10.0, 4126.0], [11.0, 3749.0], [12.0, 3884.0], [13.0, 4179.0], [14.0, 4034.0], [15.0, 3819.0], [16.0, 4296.0], [1.0, 3286.0], [17.0, 4404.0], [18.0, 4009.0], [19.0, 4931.0], [20.0, 4982.944444444445], [21.0, 4765.0], [22.0, 5077.0], [23.0, 4942.5], [24.0, 4716.0], [25.0, 4001.4363636363637]], "isOverall": false, "label": " View Pages", "isController": false}, {"data": [[21.020000000000003, 4247.070000000002]], "isOverall": false, "label": " View Pages-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 25.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 386.6666666666667, "minX": 1.62298458E12, "maxY": 24143.25, "series": [{"data": [[1.62298458E12, 24143.25]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62298458E12, 386.6666666666667]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298458E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 4247.070000000002, "minX": 1.62298458E12, "maxY": 4247.070000000002, "series": [{"data": [[1.62298458E12, 4247.070000000002]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298458E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 4244.99, "minX": 1.62298458E12, "maxY": 4244.99, "series": [{"data": [[1.62298458E12, 4244.99]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298458E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 0.8300000000000003, "minX": 1.62298458E12, "maxY": 0.8300000000000003, "series": [{"data": [[1.62298458E12, 0.8300000000000003]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298458E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 1040.0, "minX": 1.62298458E12, "maxY": 6352.0, "series": [{"data": [[1.62298458E12, 6352.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62298458E12, 1040.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62298458E12, 5228.4]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62298458E12, 6343.259999999996]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62298458E12, 5337.25]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298458E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 3874.5, "minX": 1.0, "maxY": 4885.5, "series": [{"data": [[1.0, 3874.5], [8.0, 4807.5], [4.0, 4885.5], [5.0, 4812.0], [6.0, 4259.0], [7.0, 4092.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 8.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 3872.0, "minX": 1.0, "maxY": 4883.5, "series": [{"data": [[1.0, 3872.0], [8.0, 4804.5], [4.0, 4883.5], [5.0, 4810.0], [6.0, 4257.0], [7.0, 4090.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 8.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298458E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298458E12, 1.6666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298458E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298458E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298458E12, 1.6666666666666667]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298458E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298458E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298458E12, 1.6666666666666667]], "isOverall": false, "label": " View Pages-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298458E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298458E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298458E12, 1.6666666666666667]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298458E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

