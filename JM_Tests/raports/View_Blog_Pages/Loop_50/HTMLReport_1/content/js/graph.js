/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 1049.0, "minX": 0.0, "maxY": 9804.0, "series": [{"data": [[0.0, 1049.0], [0.1, 1049.0], [0.2, 1049.0], [0.3, 1049.0], [0.4, 1049.0], [0.5, 1049.0], [0.6, 1049.0], [0.7, 1049.0], [0.8, 1049.0], [0.9, 1049.0], [1.0, 1049.0], [1.1, 1049.0], [1.2, 1049.0], [1.3, 1049.0], [1.4, 1049.0], [1.5, 1049.0], [1.6, 1049.0], [1.7, 1049.0], [1.8, 1049.0], [1.9, 1049.0], [2.0, 1334.0], [2.1, 1334.0], [2.2, 1334.0], [2.3, 1334.0], [2.4, 1334.0], [2.5, 1334.0], [2.6, 1334.0], [2.7, 1334.0], [2.8, 1334.0], [2.9, 1334.0], [3.0, 1334.0], [3.1, 1334.0], [3.2, 1334.0], [3.3, 1334.0], [3.4, 1334.0], [3.5, 1334.0], [3.6, 1334.0], [3.7, 1334.0], [3.8, 1334.0], [3.9, 1334.0], [4.0, 1604.0], [4.1, 1604.0], [4.2, 1604.0], [4.3, 1604.0], [4.4, 1604.0], [4.5, 1604.0], [4.6, 1604.0], [4.7, 1604.0], [4.8, 1604.0], [4.9, 1604.0], [5.0, 1604.0], [5.1, 1604.0], [5.2, 1604.0], [5.3, 1604.0], [5.4, 1604.0], [5.5, 1604.0], [5.6, 1604.0], [5.7, 1604.0], [5.8, 1604.0], [5.9, 1604.0], [6.0, 1619.0], [6.1, 1619.0], [6.2, 1619.0], [6.3, 1619.0], [6.4, 1619.0], [6.5, 1619.0], [6.6, 1619.0], [6.7, 1619.0], [6.8, 1619.0], [6.9, 1619.0], [7.0, 1619.0], [7.1, 1619.0], [7.2, 1619.0], [7.3, 1619.0], [7.4, 1619.0], [7.5, 1619.0], [7.6, 1619.0], [7.7, 1619.0], [7.8, 1619.0], [7.9, 1619.0], [8.0, 1676.0], [8.1, 1676.0], [8.2, 1676.0], [8.3, 1676.0], [8.4, 1676.0], [8.5, 1676.0], [8.6, 1676.0], [8.7, 1676.0], [8.8, 1676.0], [8.9, 1676.0], [9.0, 1676.0], [9.1, 1676.0], [9.2, 1676.0], [9.3, 1676.0], [9.4, 1676.0], [9.5, 1676.0], [9.6, 1676.0], [9.7, 1676.0], [9.8, 1676.0], [9.9, 1676.0], [10.0, 2003.0], [10.1, 2003.0], [10.2, 2003.0], [10.3, 2003.0], [10.4, 2003.0], [10.5, 2003.0], [10.6, 2003.0], [10.7, 2003.0], [10.8, 2003.0], [10.9, 2003.0], [11.0, 2003.0], [11.1, 2003.0], [11.2, 2003.0], [11.3, 2003.0], [11.4, 2003.0], [11.5, 2003.0], [11.6, 2003.0], [11.7, 2003.0], [11.8, 2003.0], [11.9, 2003.0], [12.0, 2018.0], [12.1, 2018.0], [12.2, 2018.0], [12.3, 2018.0], [12.4, 2018.0], [12.5, 2018.0], [12.6, 2018.0], [12.7, 2018.0], [12.8, 2018.0], [12.9, 2018.0], [13.0, 2018.0], [13.1, 2018.0], [13.2, 2018.0], [13.3, 2018.0], [13.4, 2018.0], [13.5, 2018.0], [13.6, 2018.0], [13.7, 2018.0], [13.8, 2018.0], [13.9, 2018.0], [14.0, 2111.0], [14.1, 2111.0], [14.2, 2111.0], [14.3, 2111.0], [14.4, 2111.0], [14.5, 2111.0], [14.6, 2111.0], [14.7, 2111.0], [14.8, 2111.0], [14.9, 2111.0], [15.0, 2111.0], [15.1, 2111.0], [15.2, 2111.0], [15.3, 2111.0], [15.4, 2111.0], [15.5, 2111.0], [15.6, 2111.0], [15.7, 2111.0], [15.8, 2111.0], [15.9, 2111.0], [16.0, 2374.0], [16.1, 2374.0], [16.2, 2374.0], [16.3, 2374.0], [16.4, 2374.0], [16.5, 2374.0], [16.6, 2374.0], [16.7, 2374.0], [16.8, 2374.0], [16.9, 2374.0], [17.0, 2374.0], [17.1, 2374.0], [17.2, 2374.0], [17.3, 2374.0], [17.4, 2374.0], [17.5, 2374.0], [17.6, 2374.0], [17.7, 2374.0], [17.8, 2374.0], [17.9, 2374.0], [18.0, 2661.0], [18.1, 2661.0], [18.2, 2661.0], [18.3, 2661.0], [18.4, 2661.0], [18.5, 2661.0], [18.6, 2661.0], [18.7, 2661.0], [18.8, 2661.0], [18.9, 2661.0], [19.0, 2661.0], [19.1, 2661.0], [19.2, 2661.0], [19.3, 2661.0], [19.4, 2661.0], [19.5, 2661.0], [19.6, 2661.0], [19.7, 2661.0], [19.8, 2661.0], [19.9, 2661.0], [20.0, 2739.0], [20.1, 2739.0], [20.2, 2739.0], [20.3, 2739.0], [20.4, 2739.0], [20.5, 2739.0], [20.6, 2739.0], [20.7, 2739.0], [20.8, 2739.0], [20.9, 2739.0], [21.0, 2739.0], [21.1, 2739.0], [21.2, 2739.0], [21.3, 2739.0], [21.4, 2739.0], [21.5, 2739.0], [21.6, 2739.0], [21.7, 2739.0], [21.8, 2739.0], [21.9, 2739.0], [22.0, 2753.0], [22.1, 2753.0], [22.2, 2753.0], [22.3, 2753.0], [22.4, 2753.0], [22.5, 2753.0], [22.6, 2753.0], [22.7, 2753.0], [22.8, 2753.0], [22.9, 2753.0], [23.0, 2753.0], [23.1, 2753.0], [23.2, 2753.0], [23.3, 2753.0], [23.4, 2753.0], [23.5, 2753.0], [23.6, 2753.0], [23.7, 2753.0], [23.8, 2753.0], [23.9, 2753.0], [24.0, 3703.0], [24.1, 3703.0], [24.2, 3703.0], [24.3, 3703.0], [24.4, 3703.0], [24.5, 3703.0], [24.6, 3703.0], [24.7, 3703.0], [24.8, 3703.0], [24.9, 3703.0], [25.0, 3703.0], [25.1, 3703.0], [25.2, 3703.0], [25.3, 3703.0], [25.4, 3703.0], [25.5, 3703.0], [25.6, 3703.0], [25.7, 3703.0], [25.8, 3703.0], [25.9, 3703.0], [26.0, 3766.0], [26.1, 3766.0], [26.2, 3766.0], [26.3, 3766.0], [26.4, 3766.0], [26.5, 3766.0], [26.6, 3766.0], [26.7, 3766.0], [26.8, 3766.0], [26.9, 3766.0], [27.0, 3766.0], [27.1, 3766.0], [27.2, 3766.0], [27.3, 3766.0], [27.4, 3766.0], [27.5, 3766.0], [27.6, 3766.0], [27.7, 3766.0], [27.8, 3766.0], [27.9, 3766.0], [28.0, 3813.0], [28.1, 3813.0], [28.2, 3813.0], [28.3, 3813.0], [28.4, 3813.0], [28.5, 3813.0], [28.6, 3813.0], [28.7, 3813.0], [28.8, 3813.0], [28.9, 3813.0], [29.0, 3813.0], [29.1, 3813.0], [29.2, 3813.0], [29.3, 3813.0], [29.4, 3813.0], [29.5, 3813.0], [29.6, 3813.0], [29.7, 3813.0], [29.8, 3813.0], [29.9, 3813.0], [30.0, 3901.0], [30.1, 3901.0], [30.2, 3901.0], [30.3, 3901.0], [30.4, 3901.0], [30.5, 3901.0], [30.6, 3901.0], [30.7, 3901.0], [30.8, 3901.0], [30.9, 3901.0], [31.0, 3901.0], [31.1, 3901.0], [31.2, 3901.0], [31.3, 3901.0], [31.4, 3901.0], [31.5, 3901.0], [31.6, 3901.0], [31.7, 3901.0], [31.8, 3901.0], [31.9, 3901.0], [32.0, 3966.0], [32.1, 3966.0], [32.2, 3966.0], [32.3, 3966.0], [32.4, 3966.0], [32.5, 3966.0], [32.6, 3966.0], [32.7, 3966.0], [32.8, 3966.0], [32.9, 3966.0], [33.0, 3966.0], [33.1, 3966.0], [33.2, 3966.0], [33.3, 3966.0], [33.4, 3966.0], [33.5, 3966.0], [33.6, 3966.0], [33.7, 3966.0], [33.8, 3966.0], [33.9, 3966.0], [34.0, 4262.0], [34.1, 4262.0], [34.2, 4262.0], [34.3, 4262.0], [34.4, 4262.0], [34.5, 4262.0], [34.6, 4262.0], [34.7, 4262.0], [34.8, 4262.0], [34.9, 4262.0], [35.0, 4262.0], [35.1, 4262.0], [35.2, 4262.0], [35.3, 4262.0], [35.4, 4262.0], [35.5, 4262.0], [35.6, 4262.0], [35.7, 4262.0], [35.8, 4262.0], [35.9, 4262.0], [36.0, 4369.0], [36.1, 4369.0], [36.2, 4369.0], [36.3, 4369.0], [36.4, 4369.0], [36.5, 4369.0], [36.6, 4369.0], [36.7, 4369.0], [36.8, 4369.0], [36.9, 4369.0], [37.0, 4369.0], [37.1, 4369.0], [37.2, 4369.0], [37.3, 4369.0], [37.4, 4369.0], [37.5, 4369.0], [37.6, 4369.0], [37.7, 4369.0], [37.8, 4369.0], [37.9, 4369.0], [38.0, 4452.0], [38.1, 4452.0], [38.2, 4452.0], [38.3, 4452.0], [38.4, 4452.0], [38.5, 4452.0], [38.6, 4452.0], [38.7, 4452.0], [38.8, 4452.0], [38.9, 4452.0], [39.0, 4452.0], [39.1, 4452.0], [39.2, 4452.0], [39.3, 4452.0], [39.4, 4452.0], [39.5, 4452.0], [39.6, 4452.0], [39.7, 4452.0], [39.8, 4452.0], [39.9, 4452.0], [40.0, 4592.0], [40.1, 4592.0], [40.2, 4592.0], [40.3, 4592.0], [40.4, 4592.0], [40.5, 4592.0], [40.6, 4592.0], [40.7, 4592.0], [40.8, 4592.0], [40.9, 4592.0], [41.0, 4592.0], [41.1, 4592.0], [41.2, 4592.0], [41.3, 4592.0], [41.4, 4592.0], [41.5, 4592.0], [41.6, 4592.0], [41.7, 4592.0], [41.8, 4592.0], [41.9, 4592.0], [42.0, 4954.0], [42.1, 4954.0], [42.2, 4954.0], [42.3, 4954.0], [42.4, 4954.0], [42.5, 4954.0], [42.6, 4954.0], [42.7, 4954.0], [42.8, 4954.0], [42.9, 4954.0], [43.0, 4954.0], [43.1, 4954.0], [43.2, 4954.0], [43.3, 4954.0], [43.4, 4954.0], [43.5, 4954.0], [43.6, 4954.0], [43.7, 4954.0], [43.8, 4954.0], [43.9, 4954.0], [44.0, 5058.0], [44.1, 5058.0], [44.2, 5058.0], [44.3, 5058.0], [44.4, 5058.0], [44.5, 5058.0], [44.6, 5058.0], [44.7, 5058.0], [44.8, 5058.0], [44.9, 5058.0], [45.0, 5058.0], [45.1, 5058.0], [45.2, 5058.0], [45.3, 5058.0], [45.4, 5058.0], [45.5, 5058.0], [45.6, 5058.0], [45.7, 5058.0], [45.8, 5058.0], [45.9, 5058.0], [46.0, 5099.0], [46.1, 5099.0], [46.2, 5099.0], [46.3, 5099.0], [46.4, 5099.0], [46.5, 5099.0], [46.6, 5099.0], [46.7, 5099.0], [46.8, 5099.0], [46.9, 5099.0], [47.0, 5099.0], [47.1, 5099.0], [47.2, 5099.0], [47.3, 5099.0], [47.4, 5099.0], [47.5, 5099.0], [47.6, 5099.0], [47.7, 5099.0], [47.8, 5099.0], [47.9, 5099.0], [48.0, 5639.0], [48.1, 5639.0], [48.2, 5639.0], [48.3, 5639.0], [48.4, 5639.0], [48.5, 5639.0], [48.6, 5639.0], [48.7, 5639.0], [48.8, 5639.0], [48.9, 5639.0], [49.0, 5639.0], [49.1, 5639.0], [49.2, 5639.0], [49.3, 5639.0], [49.4, 5639.0], [49.5, 5639.0], [49.6, 5639.0], [49.7, 5639.0], [49.8, 5639.0], [49.9, 5639.0], [50.0, 5695.0], [50.1, 5695.0], [50.2, 5695.0], [50.3, 5695.0], [50.4, 5695.0], [50.5, 5695.0], [50.6, 5695.0], [50.7, 5695.0], [50.8, 5695.0], [50.9, 5695.0], [51.0, 5695.0], [51.1, 5695.0], [51.2, 5695.0], [51.3, 5695.0], [51.4, 5695.0], [51.5, 5695.0], [51.6, 5695.0], [51.7, 5695.0], [51.8, 5695.0], [51.9, 5695.0], [52.0, 5880.0], [52.1, 5880.0], [52.2, 5880.0], [52.3, 5880.0], [52.4, 5880.0], [52.5, 5880.0], [52.6, 5880.0], [52.7, 5880.0], [52.8, 5880.0], [52.9, 5880.0], [53.0, 5880.0], [53.1, 5880.0], [53.2, 5880.0], [53.3, 5880.0], [53.4, 5880.0], [53.5, 5880.0], [53.6, 5880.0], [53.7, 5880.0], [53.8, 5880.0], [53.9, 5880.0], [54.0, 5940.0], [54.1, 5940.0], [54.2, 5940.0], [54.3, 5940.0], [54.4, 5940.0], [54.5, 5940.0], [54.6, 5940.0], [54.7, 5940.0], [54.8, 5940.0], [54.9, 5940.0], [55.0, 5940.0], [55.1, 5940.0], [55.2, 5940.0], [55.3, 5940.0], [55.4, 5940.0], [55.5, 5940.0], [55.6, 5940.0], [55.7, 5940.0], [55.8, 5940.0], [55.9, 5940.0], [56.0, 6005.0], [56.1, 6005.0], [56.2, 6005.0], [56.3, 6005.0], [56.4, 6005.0], [56.5, 6005.0], [56.6, 6005.0], [56.7, 6005.0], [56.8, 6005.0], [56.9, 6005.0], [57.0, 6005.0], [57.1, 6005.0], [57.2, 6005.0], [57.3, 6005.0], [57.4, 6005.0], [57.5, 6005.0], [57.6, 6005.0], [57.7, 6005.0], [57.8, 6005.0], [57.9, 6005.0], [58.0, 6480.0], [58.1, 6480.0], [58.2, 6480.0], [58.3, 6480.0], [58.4, 6480.0], [58.5, 6480.0], [58.6, 6480.0], [58.7, 6480.0], [58.8, 6480.0], [58.9, 6480.0], [59.0, 6480.0], [59.1, 6480.0], [59.2, 6480.0], [59.3, 6480.0], [59.4, 6480.0], [59.5, 6480.0], [59.6, 6480.0], [59.7, 6480.0], [59.8, 6480.0], [59.9, 6480.0], [60.0, 6589.0], [60.1, 6589.0], [60.2, 6589.0], [60.3, 6589.0], [60.4, 6589.0], [60.5, 6589.0], [60.6, 6589.0], [60.7, 6589.0], [60.8, 6589.0], [60.9, 6589.0], [61.0, 6589.0], [61.1, 6589.0], [61.2, 6589.0], [61.3, 6589.0], [61.4, 6589.0], [61.5, 6589.0], [61.6, 6589.0], [61.7, 6589.0], [61.8, 6589.0], [61.9, 6589.0], [62.0, 6701.0], [62.1, 6701.0], [62.2, 6701.0], [62.3, 6701.0], [62.4, 6701.0], [62.5, 6701.0], [62.6, 6701.0], [62.7, 6701.0], [62.8, 6701.0], [62.9, 6701.0], [63.0, 6701.0], [63.1, 6701.0], [63.2, 6701.0], [63.3, 6701.0], [63.4, 6701.0], [63.5, 6701.0], [63.6, 6701.0], [63.7, 6701.0], [63.8, 6701.0], [63.9, 6701.0], [64.0, 6739.0], [64.1, 6739.0], [64.2, 6739.0], [64.3, 6739.0], [64.4, 6739.0], [64.5, 6739.0], [64.6, 6739.0], [64.7, 6739.0], [64.8, 6739.0], [64.9, 6739.0], [65.0, 6739.0], [65.1, 6739.0], [65.2, 6739.0], [65.3, 6739.0], [65.4, 6739.0], [65.5, 6739.0], [65.6, 6739.0], [65.7, 6739.0], [65.8, 6739.0], [65.9, 6739.0], [66.0, 6987.0], [66.1, 6987.0], [66.2, 6987.0], [66.3, 6987.0], [66.4, 6987.0], [66.5, 6987.0], [66.6, 6987.0], [66.7, 6987.0], [66.8, 6987.0], [66.9, 6987.0], [67.0, 6987.0], [67.1, 6987.0], [67.2, 6987.0], [67.3, 6987.0], [67.4, 6987.0], [67.5, 6987.0], [67.6, 6987.0], [67.7, 6987.0], [67.8, 6987.0], [67.9, 6987.0], [68.0, 7136.0], [68.1, 7136.0], [68.2, 7136.0], [68.3, 7136.0], [68.4, 7136.0], [68.5, 7136.0], [68.6, 7136.0], [68.7, 7136.0], [68.8, 7136.0], [68.9, 7136.0], [69.0, 7136.0], [69.1, 7136.0], [69.2, 7136.0], [69.3, 7136.0], [69.4, 7136.0], [69.5, 7136.0], [69.6, 7136.0], [69.7, 7136.0], [69.8, 7136.0], [69.9, 7136.0], [70.0, 7331.0], [70.1, 7331.0], [70.2, 7331.0], [70.3, 7331.0], [70.4, 7331.0], [70.5, 7331.0], [70.6, 7331.0], [70.7, 7331.0], [70.8, 7331.0], [70.9, 7331.0], [71.0, 7331.0], [71.1, 7331.0], [71.2, 7331.0], [71.3, 7331.0], [71.4, 7331.0], [71.5, 7331.0], [71.6, 7331.0], [71.7, 7331.0], [71.8, 7331.0], [71.9, 7331.0], [72.0, 7703.0], [72.1, 7703.0], [72.2, 7703.0], [72.3, 7703.0], [72.4, 7703.0], [72.5, 7703.0], [72.6, 7703.0], [72.7, 7703.0], [72.8, 7703.0], [72.9, 7703.0], [73.0, 7703.0], [73.1, 7703.0], [73.2, 7703.0], [73.3, 7703.0], [73.4, 7703.0], [73.5, 7703.0], [73.6, 7703.0], [73.7, 7703.0], [73.8, 7703.0], [73.9, 7703.0], [74.0, 7854.0], [74.1, 7854.0], [74.2, 7854.0], [74.3, 7854.0], [74.4, 7854.0], [74.5, 7854.0], [74.6, 7854.0], [74.7, 7854.0], [74.8, 7854.0], [74.9, 7854.0], [75.0, 7854.0], [75.1, 7854.0], [75.2, 7854.0], [75.3, 7854.0], [75.4, 7854.0], [75.5, 7854.0], [75.6, 7854.0], [75.7, 7854.0], [75.8, 7854.0], [75.9, 7854.0], [76.0, 8073.0], [76.1, 8073.0], [76.2, 8073.0], [76.3, 8073.0], [76.4, 8073.0], [76.5, 8073.0], [76.6, 8073.0], [76.7, 8073.0], [76.8, 8073.0], [76.9, 8073.0], [77.0, 8073.0], [77.1, 8073.0], [77.2, 8073.0], [77.3, 8073.0], [77.4, 8073.0], [77.5, 8073.0], [77.6, 8073.0], [77.7, 8073.0], [77.8, 8073.0], [77.9, 8073.0], [78.0, 8170.0], [78.1, 8170.0], [78.2, 8170.0], [78.3, 8170.0], [78.4, 8170.0], [78.5, 8170.0], [78.6, 8170.0], [78.7, 8170.0], [78.8, 8170.0], [78.9, 8170.0], [79.0, 8170.0], [79.1, 8170.0], [79.2, 8170.0], [79.3, 8170.0], [79.4, 8170.0], [79.5, 8170.0], [79.6, 8170.0], [79.7, 8170.0], [79.8, 8170.0], [79.9, 8170.0], [80.0, 8194.0], [80.1, 8194.0], [80.2, 8194.0], [80.3, 8194.0], [80.4, 8194.0], [80.5, 8194.0], [80.6, 8194.0], [80.7, 8194.0], [80.8, 8194.0], [80.9, 8194.0], [81.0, 8194.0], [81.1, 8194.0], [81.2, 8194.0], [81.3, 8194.0], [81.4, 8194.0], [81.5, 8194.0], [81.6, 8194.0], [81.7, 8194.0], [81.8, 8194.0], [81.9, 8194.0], [82.0, 8463.0], [82.1, 8463.0], [82.2, 8463.0], [82.3, 8463.0], [82.4, 8463.0], [82.5, 8463.0], [82.6, 8463.0], [82.7, 8463.0], [82.8, 8463.0], [82.9, 8463.0], [83.0, 8463.0], [83.1, 8463.0], [83.2, 8463.0], [83.3, 8463.0], [83.4, 8463.0], [83.5, 8463.0], [83.6, 8463.0], [83.7, 8463.0], [83.8, 8463.0], [83.9, 8463.0], [84.0, 8639.0], [84.1, 8639.0], [84.2, 8639.0], [84.3, 8639.0], [84.4, 8639.0], [84.5, 8639.0], [84.6, 8639.0], [84.7, 8639.0], [84.8, 8639.0], [84.9, 8639.0], [85.0, 8639.0], [85.1, 8639.0], [85.2, 8639.0], [85.3, 8639.0], [85.4, 8639.0], [85.5, 8639.0], [85.6, 8639.0], [85.7, 8639.0], [85.8, 8639.0], [85.9, 8639.0], [86.0, 8766.0], [86.1, 8766.0], [86.2, 8766.0], [86.3, 8766.0], [86.4, 8766.0], [86.5, 8766.0], [86.6, 8766.0], [86.7, 8766.0], [86.8, 8766.0], [86.9, 8766.0], [87.0, 8766.0], [87.1, 8766.0], [87.2, 8766.0], [87.3, 8766.0], [87.4, 8766.0], [87.5, 8766.0], [87.6, 8766.0], [87.7, 8766.0], [87.8, 8766.0], [87.9, 8766.0], [88.0, 9053.0], [88.1, 9053.0], [88.2, 9053.0], [88.3, 9053.0], [88.4, 9053.0], [88.5, 9053.0], [88.6, 9053.0], [88.7, 9053.0], [88.8, 9053.0], [88.9, 9053.0], [89.0, 9053.0], [89.1, 9053.0], [89.2, 9053.0], [89.3, 9053.0], [89.4, 9053.0], [89.5, 9053.0], [89.6, 9053.0], [89.7, 9053.0], [89.8, 9053.0], [89.9, 9053.0], [90.0, 9226.0], [90.1, 9226.0], [90.2, 9226.0], [90.3, 9226.0], [90.4, 9226.0], [90.5, 9226.0], [90.6, 9226.0], [90.7, 9226.0], [90.8, 9226.0], [90.9, 9226.0], [91.0, 9226.0], [91.1, 9226.0], [91.2, 9226.0], [91.3, 9226.0], [91.4, 9226.0], [91.5, 9226.0], [91.6, 9226.0], [91.7, 9226.0], [91.8, 9226.0], [91.9, 9226.0], [92.0, 9331.0], [92.1, 9331.0], [92.2, 9331.0], [92.3, 9331.0], [92.4, 9331.0], [92.5, 9331.0], [92.6, 9331.0], [92.7, 9331.0], [92.8, 9331.0], [92.9, 9331.0], [93.0, 9331.0], [93.1, 9331.0], [93.2, 9331.0], [93.3, 9331.0], [93.4, 9331.0], [93.5, 9331.0], [93.6, 9331.0], [93.7, 9331.0], [93.8, 9331.0], [93.9, 9331.0], [94.0, 9529.0], [94.1, 9529.0], [94.2, 9529.0], [94.3, 9529.0], [94.4, 9529.0], [94.5, 9529.0], [94.6, 9529.0], [94.7, 9529.0], [94.8, 9529.0], [94.9, 9529.0], [95.0, 9529.0], [95.1, 9529.0], [95.2, 9529.0], [95.3, 9529.0], [95.4, 9529.0], [95.5, 9529.0], [95.6, 9529.0], [95.7, 9529.0], [95.8, 9529.0], [95.9, 9529.0], [96.0, 9599.0], [96.1, 9599.0], [96.2, 9599.0], [96.3, 9599.0], [96.4, 9599.0], [96.5, 9599.0], [96.6, 9599.0], [96.7, 9599.0], [96.8, 9599.0], [96.9, 9599.0], [97.0, 9599.0], [97.1, 9599.0], [97.2, 9599.0], [97.3, 9599.0], [97.4, 9599.0], [97.5, 9599.0], [97.6, 9599.0], [97.7, 9599.0], [97.8, 9599.0], [97.9, 9599.0], [98.0, 9804.0], [98.1, 9804.0], [98.2, 9804.0], [98.3, 9804.0], [98.4, 9804.0], [98.5, 9804.0], [98.6, 9804.0], [98.7, 9804.0], [98.8, 9804.0], [98.9, 9804.0], [99.0, 9804.0], [99.1, 9804.0], [99.2, 9804.0], [99.3, 9804.0], [99.4, 9804.0], [99.5, 9804.0], [99.6, 9804.0], [99.7, 9804.0], [99.8, 9804.0], [99.9, 9804.0]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 1000.0, "maxY": 3.0, "series": [{"data": [[8400.0, 1.0], [8600.0, 1.0], [8700.0, 1.0], [9000.0, 1.0], [9200.0, 1.0], [9300.0, 1.0], [9500.0, 2.0], [9800.0, 1.0], [1000.0, 1.0], [1300.0, 1.0], [1600.0, 3.0], [2000.0, 2.0], [2100.0, 1.0], [2300.0, 1.0], [2600.0, 1.0], [2700.0, 2.0], [3700.0, 2.0], [3800.0, 1.0], [3900.0, 2.0], [4200.0, 1.0], [4300.0, 1.0], [4400.0, 1.0], [4500.0, 1.0], [4900.0, 1.0], [5000.0, 2.0], [5600.0, 2.0], [5800.0, 1.0], [5900.0, 1.0], [6000.0, 1.0], [6400.0, 1.0], [6500.0, 1.0], [6700.0, 2.0], [6900.0, 1.0], [7100.0, 1.0], [7300.0, 1.0], [7700.0, 1.0], [7800.0, 1.0], [8000.0, 1.0], [8100.0, 2.0]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 9800.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 2.0, "minX": 1.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 48.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 2.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 48.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 19.5, "minX": 1.6229844E12, "maxY": 44.5, "series": [{"data": [[1.6229844E12, 44.5], [1.62298446E12, 19.5]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298446E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 1049.0, "minX": 1.0, "maxY": 9804.0, "series": [{"data": [[2.0, 9599.0], [3.0, 9529.0], [4.0, 9331.0], [5.0, 9226.0], [6.0, 9053.0], [7.0, 8766.0], [8.0, 8639.0], [9.0, 8463.0], [10.0, 8194.0], [11.0, 8170.0], [12.0, 8073.0], [13.0, 7854.0], [14.0, 7703.0], [15.0, 7331.0], [16.0, 7136.0], [17.0, 6987.0], [18.0, 6739.0], [19.0, 6701.0], [20.0, 6589.0], [21.0, 6480.0], [22.0, 6005.0], [23.0, 5940.0], [24.0, 5880.0], [25.0, 5639.0], [26.0, 5695.0], [27.0, 5099.0], [28.0, 5058.0], [29.0, 4954.0], [30.0, 4592.0], [31.0, 4452.0], [33.0, 4262.0], [32.0, 4369.0], [35.0, 3901.0], [34.0, 3966.0], [37.0, 3766.0], [36.0, 3813.0], [39.0, 2753.0], [38.0, 3703.0], [41.0, 2661.0], [40.0, 2739.0], [43.0, 2111.0], [42.0, 2374.0], [45.0, 2018.0], [44.0, 2003.0], [47.0, 1676.0], [46.0, 1619.0], [49.0, 1334.0], [48.0, 1604.0], [50.0, 1049.0], [1.0, 9804.0]], "isOverall": false, "label": " View Pages", "isController": false}, {"data": [[25.5, 5508.039999999998]], "isOverall": false, "label": " View Pages-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 50.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 46.4, "minX": 1.6229844E12, "maxY": 9174.466666666667, "series": [{"data": [[1.6229844E12, 2897.2], [1.62298446E12, 9174.466666666667]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.6229844E12, 46.4], [1.62298446E12, 146.93333333333334]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298446E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 1995.0833333333333, "minX": 1.6229844E12, "maxY": 6617.394736842104, "series": [{"data": [[1.6229844E12, 1995.0833333333333], [1.62298446E12, 6617.394736842104]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298446E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 1993.0833333333333, "minX": 1.6229844E12, "maxY": 6615.131578947368, "series": [{"data": [[1.6229844E12, 1993.0833333333333], [1.62298446E12, 6615.131578947368]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298446E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 0.8333333333333333, "minX": 1.6229844E12, "maxY": 0.9736842105263157, "series": [{"data": [[1.6229844E12, 0.8333333333333333], [1.62298446E12, 0.9736842105263157]], "isOverall": false, "label": " View Pages", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298446E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 1049.0, "minX": 1.6229844E12, "maxY": 9804.0, "series": [{"data": [[1.6229844E12, 2753.0], [1.62298446E12, 9804.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.6229844E12, 1049.0], [1.62298446E12, 3703.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.6229844E12, 2748.8], [1.62298446E12, 9350.800000000001]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.6229844E12, 2753.0], [1.62298446E12, 9804.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.6229844E12, 2753.0], [1.62298446E12, 9609.25]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298446E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 2132.5, "minX": 1.0, "maxY": 9804.0, "series": [{"data": [[4.0, 6645.0], [1.0, 9804.0], [5.0, 7331.0], [3.0, 2132.5], [6.0, 4315.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 6.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 2130.5, "minX": 1.0, "maxY": 9802.0, "series": [{"data": [[4.0, 6643.0], [1.0, 9802.0], [5.0, 7329.0], [3.0, 2130.5], [6.0, 4313.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 6.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.8333333333333334, "minX": 1.6229844E12, "maxY": 0.8333333333333334, "series": [{"data": [[1.6229844E12, 0.8333333333333334]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229844E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.2, "minX": 1.6229844E12, "maxY": 0.6333333333333333, "series": [{"data": [[1.6229844E12, 0.2], [1.62298446E12, 0.6333333333333333]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298446E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.2, "minX": 1.6229844E12, "maxY": 0.6333333333333333, "series": [{"data": [[1.6229844E12, 0.2], [1.62298446E12, 0.6333333333333333]], "isOverall": false, "label": " View Pages-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298446E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.2, "minX": 1.6229844E12, "maxY": 0.6333333333333333, "series": [{"data": [[1.6229844E12, 0.2], [1.62298446E12, 0.6333333333333333]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298446E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

