/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 1763.0, "minX": 0.0, "maxY": 21504.0, "series": [{"data": [[0.0, 1763.0], [0.1, 1763.0], [0.2, 1763.0], [0.3, 1763.0], [0.4, 1763.0], [0.5, 1763.0], [0.6, 1763.0], [0.7, 1763.0], [0.8, 1763.0], [0.9, 1763.0], [1.0, 1822.0], [1.1, 1822.0], [1.2, 1822.0], [1.3, 1822.0], [1.4, 1822.0], [1.5, 1822.0], [1.6, 1822.0], [1.7, 1822.0], [1.8, 1822.0], [1.9, 1822.0], [2.0, 2079.0], [2.1, 2079.0], [2.2, 2079.0], [2.3, 2079.0], [2.4, 2079.0], [2.5, 2079.0], [2.6, 2079.0], [2.7, 2079.0], [2.8, 2079.0], [2.9, 2079.0], [3.0, 2171.0], [3.1, 2171.0], [3.2, 2171.0], [3.3, 2171.0], [3.4, 2171.0], [3.5, 2171.0], [3.6, 2171.0], [3.7, 2171.0], [3.8, 2171.0], [3.9, 2171.0], [4.0, 2421.0], [4.1, 2421.0], [4.2, 2421.0], [4.3, 2421.0], [4.4, 2421.0], [4.5, 2421.0], [4.6, 2421.0], [4.7, 2421.0], [4.8, 2421.0], [4.9, 2421.0], [5.0, 2428.0], [5.1, 2428.0], [5.2, 2428.0], [5.3, 2428.0], [5.4, 2428.0], [5.5, 2428.0], [5.6, 2428.0], [5.7, 2428.0], [5.8, 2428.0], [5.9, 2428.0], [6.0, 2562.0], [6.1, 2562.0], [6.2, 2562.0], [6.3, 2562.0], [6.4, 2562.0], [6.5, 2562.0], [6.6, 2562.0], [6.7, 2562.0], [6.8, 2562.0], [6.9, 2562.0], [7.0, 2680.0], [7.1, 2680.0], [7.2, 2680.0], [7.3, 2680.0], [7.4, 2680.0], [7.5, 2680.0], [7.6, 2680.0], [7.7, 2680.0], [7.8, 2680.0], [7.9, 2680.0], [8.0, 2961.0], [8.1, 2961.0], [8.2, 2961.0], [8.3, 2961.0], [8.4, 2961.0], [8.5, 2961.0], [8.6, 2961.0], [8.7, 2961.0], [8.8, 2961.0], [8.9, 2961.0], [9.0, 3199.0], [9.1, 3199.0], [9.2, 3199.0], [9.3, 3199.0], [9.4, 3199.0], [9.5, 3199.0], [9.6, 3199.0], [9.7, 3199.0], [9.8, 3199.0], [9.9, 3199.0], [10.0, 3496.0], [10.1, 3496.0], [10.2, 3496.0], [10.3, 3496.0], [10.4, 3496.0], [10.5, 3496.0], [10.6, 3496.0], [10.7, 3496.0], [10.8, 3496.0], [10.9, 3496.0], [11.0, 3497.0], [11.1, 3497.0], [11.2, 3497.0], [11.3, 3497.0], [11.4, 3497.0], [11.5, 3497.0], [11.6, 3497.0], [11.7, 3497.0], [11.8, 3497.0], [11.9, 3497.0], [12.0, 3775.0], [12.1, 3775.0], [12.2, 3775.0], [12.3, 3775.0], [12.4, 3775.0], [12.5, 3775.0], [12.6, 3775.0], [12.7, 3775.0], [12.8, 3775.0], [12.9, 3775.0], [13.0, 4231.0], [13.1, 4231.0], [13.2, 4231.0], [13.3, 4231.0], [13.4, 4231.0], [13.5, 4231.0], [13.6, 4231.0], [13.7, 4231.0], [13.8, 4231.0], [13.9, 4231.0], [14.0, 4405.0], [14.1, 4405.0], [14.2, 4405.0], [14.3, 4405.0], [14.4, 4405.0], [14.5, 4405.0], [14.6, 4405.0], [14.7, 4405.0], [14.8, 4405.0], [14.9, 4405.0], [15.0, 4423.0], [15.1, 4423.0], [15.2, 4423.0], [15.3, 4423.0], [15.4, 4423.0], [15.5, 4423.0], [15.6, 4423.0], [15.7, 4423.0], [15.8, 4423.0], [15.9, 4423.0], [16.0, 4469.0], [16.1, 4469.0], [16.2, 4469.0], [16.3, 4469.0], [16.4, 4469.0], [16.5, 4469.0], [16.6, 4469.0], [16.7, 4469.0], [16.8, 4469.0], [16.9, 4469.0], [17.0, 4640.0], [17.1, 4640.0], [17.2, 4640.0], [17.3, 4640.0], [17.4, 4640.0], [17.5, 4640.0], [17.6, 4640.0], [17.7, 4640.0], [17.8, 4640.0], [17.9, 4640.0], [18.0, 5104.0], [18.1, 5104.0], [18.2, 5104.0], [18.3, 5104.0], [18.4, 5104.0], [18.5, 5104.0], [18.6, 5104.0], [18.7, 5104.0], [18.8, 5104.0], [18.9, 5104.0], [19.0, 5381.0], [19.1, 5381.0], [19.2, 5381.0], [19.3, 5381.0], [19.4, 5381.0], [19.5, 5381.0], [19.6, 5381.0], [19.7, 5381.0], [19.8, 5381.0], [19.9, 5381.0], [20.0, 5533.0], [20.1, 5533.0], [20.2, 5533.0], [20.3, 5533.0], [20.4, 5533.0], [20.5, 5533.0], [20.6, 5533.0], [20.7, 5533.0], [20.8, 5533.0], [20.9, 5533.0], [21.0, 5587.0], [21.1, 5587.0], [21.2, 5587.0], [21.3, 5587.0], [21.4, 5587.0], [21.5, 5587.0], [21.6, 5587.0], [21.7, 5587.0], [21.8, 5587.0], [21.9, 5587.0], [22.0, 5895.0], [22.1, 5895.0], [22.2, 5895.0], [22.3, 5895.0], [22.4, 5895.0], [22.5, 5895.0], [22.6, 5895.0], [22.7, 5895.0], [22.8, 5895.0], [22.9, 5895.0], [23.0, 6149.0], [23.1, 6149.0], [23.2, 6149.0], [23.3, 6149.0], [23.4, 6149.0], [23.5, 6149.0], [23.6, 6149.0], [23.7, 6149.0], [23.8, 6149.0], [23.9, 6149.0], [24.0, 6323.0], [24.1, 6323.0], [24.2, 6323.0], [24.3, 6323.0], [24.4, 6323.0], [24.5, 6323.0], [24.6, 6323.0], [24.7, 6323.0], [24.8, 6323.0], [24.9, 6323.0], [25.0, 6503.0], [25.1, 6503.0], [25.2, 6503.0], [25.3, 6503.0], [25.4, 6503.0], [25.5, 6503.0], [25.6, 6503.0], [25.7, 6503.0], [25.8, 6503.0], [25.9, 6503.0], [26.0, 7175.0], [26.1, 7175.0], [26.2, 7175.0], [26.3, 7175.0], [26.4, 7175.0], [26.5, 7175.0], [26.6, 7175.0], [26.7, 7175.0], [26.8, 7175.0], [26.9, 7175.0], [27.0, 7213.0], [27.1, 7213.0], [27.2, 7213.0], [27.3, 7213.0], [27.4, 7213.0], [27.5, 7213.0], [27.6, 7213.0], [27.7, 7213.0], [27.8, 7213.0], [27.9, 7213.0], [28.0, 7455.0], [28.1, 7455.0], [28.2, 7455.0], [28.3, 7455.0], [28.4, 7455.0], [28.5, 7455.0], [28.6, 7455.0], [28.7, 7455.0], [28.8, 7455.0], [28.9, 7455.0], [29.0, 7606.0], [29.1, 7606.0], [29.2, 7606.0], [29.3, 7606.0], [29.4, 7606.0], [29.5, 7606.0], [29.6, 7606.0], [29.7, 7606.0], [29.8, 7606.0], [29.9, 7606.0], [30.0, 7798.0], [30.1, 7798.0], [30.2, 7798.0], [30.3, 7798.0], [30.4, 7798.0], [30.5, 7798.0], [30.6, 7798.0], [30.7, 7798.0], [30.8, 7798.0], [30.9, 7798.0], [31.0, 7894.0], [31.1, 7894.0], [31.2, 7894.0], [31.3, 7894.0], [31.4, 7894.0], [31.5, 7894.0], [31.6, 7894.0], [31.7, 7894.0], [31.8, 7894.0], [31.9, 7894.0], [32.0, 8182.0], [32.1, 8182.0], [32.2, 8182.0], [32.3, 8182.0], [32.4, 8182.0], [32.5, 8182.0], [32.6, 8182.0], [32.7, 8182.0], [32.8, 8182.0], [32.9, 8182.0], [33.0, 8261.0], [33.1, 8261.0], [33.2, 8261.0], [33.3, 8261.0], [33.4, 8261.0], [33.5, 8261.0], [33.6, 8261.0], [33.7, 8261.0], [33.8, 8261.0], [33.9, 8261.0], [34.0, 8411.0], [34.1, 8411.0], [34.2, 8411.0], [34.3, 8411.0], [34.4, 8411.0], [34.5, 8411.0], [34.6, 8411.0], [34.7, 8411.0], [34.8, 8411.0], [34.9, 8411.0], [35.0, 8818.0], [35.1, 8818.0], [35.2, 8818.0], [35.3, 8818.0], [35.4, 8818.0], [35.5, 8818.0], [35.6, 8818.0], [35.7, 8818.0], [35.8, 8818.0], [35.9, 8818.0], [36.0, 9315.0], [36.1, 9315.0], [36.2, 9315.0], [36.3, 9315.0], [36.4, 9315.0], [36.5, 9315.0], [36.6, 9315.0], [36.7, 9315.0], [36.8, 9315.0], [36.9, 9315.0], [37.0, 9408.0], [37.1, 9408.0], [37.2, 9408.0], [37.3, 9408.0], [37.4, 9408.0], [37.5, 9408.0], [37.6, 9408.0], [37.7, 9408.0], [37.8, 9408.0], [37.9, 9408.0], [38.0, 9437.0], [38.1, 9437.0], [38.2, 9437.0], [38.3, 9437.0], [38.4, 9437.0], [38.5, 9437.0], [38.6, 9437.0], [38.7, 9437.0], [38.8, 9437.0], [38.9, 9437.0], [39.0, 9654.0], [39.1, 9654.0], [39.2, 9654.0], [39.3, 9654.0], [39.4, 9654.0], [39.5, 9654.0], [39.6, 9654.0], [39.7, 9654.0], [39.8, 9654.0], [39.9, 9654.0], [40.0, 9701.0], [40.1, 9701.0], [40.2, 9701.0], [40.3, 9701.0], [40.4, 9701.0], [40.5, 9701.0], [40.6, 9701.0], [40.7, 9701.0], [40.8, 9701.0], [40.9, 9701.0], [41.0, 9709.0], [41.1, 9709.0], [41.2, 9709.0], [41.3, 9709.0], [41.4, 9709.0], [41.5, 9709.0], [41.6, 9709.0], [41.7, 9709.0], [41.8, 9709.0], [41.9, 9709.0], [42.0, 10167.0], [42.1, 10167.0], [42.2, 10167.0], [42.3, 10167.0], [42.4, 10167.0], [42.5, 10167.0], [42.6, 10167.0], [42.7, 10167.0], [42.8, 10167.0], [42.9, 10167.0], [43.0, 10208.0], [43.1, 10208.0], [43.2, 10208.0], [43.3, 10208.0], [43.4, 10208.0], [43.5, 10208.0], [43.6, 10208.0], [43.7, 10208.0], [43.8, 10208.0], [43.9, 10208.0], [44.0, 10302.0], [44.1, 10302.0], [44.2, 10302.0], [44.3, 10302.0], [44.4, 10302.0], [44.5, 10302.0], [44.6, 10302.0], [44.7, 10302.0], [44.8, 10302.0], [44.9, 10302.0], [45.0, 10870.0], [45.1, 10870.0], [45.2, 10870.0], [45.3, 10870.0], [45.4, 10870.0], [45.5, 10870.0], [45.6, 10870.0], [45.7, 10870.0], [45.8, 10870.0], [45.9, 10870.0], [46.0, 10902.0], [46.1, 10902.0], [46.2, 10902.0], [46.3, 10902.0], [46.4, 10902.0], [46.5, 10902.0], [46.6, 10902.0], [46.7, 10902.0], [46.8, 10902.0], [46.9, 10902.0], [47.0, 11309.0], [47.1, 11309.0], [47.2, 11309.0], [47.3, 11309.0], [47.4, 11309.0], [47.5, 11309.0], [47.6, 11309.0], [47.7, 11309.0], [47.8, 11309.0], [47.9, 11309.0], [48.0, 11549.0], [48.1, 11549.0], [48.2, 11549.0], [48.3, 11549.0], [48.4, 11549.0], [48.5, 11549.0], [48.6, 11549.0], [48.7, 11549.0], [48.8, 11549.0], [48.9, 11549.0], [49.0, 11659.0], [49.1, 11659.0], [49.2, 11659.0], [49.3, 11659.0], [49.4, 11659.0], [49.5, 11659.0], [49.6, 11659.0], [49.7, 11659.0], [49.8, 11659.0], [49.9, 11659.0], [50.0, 11876.0], [50.1, 11876.0], [50.2, 11876.0], [50.3, 11876.0], [50.4, 11876.0], [50.5, 11876.0], [50.6, 11876.0], [50.7, 11876.0], [50.8, 11876.0], [50.9, 11876.0], [51.0, 12060.0], [51.1, 12060.0], [51.2, 12060.0], [51.3, 12060.0], [51.4, 12060.0], [51.5, 12060.0], [51.6, 12060.0], [51.7, 12060.0], [51.8, 12060.0], [51.9, 12060.0], [52.0, 12095.0], [52.1, 12095.0], [52.2, 12095.0], [52.3, 12095.0], [52.4, 12095.0], [52.5, 12095.0], [52.6, 12095.0], [52.7, 12095.0], [52.8, 12095.0], [52.9, 12095.0], [53.0, 12226.0], [53.1, 12226.0], [53.2, 12226.0], [53.3, 12226.0], [53.4, 12226.0], [53.5, 12226.0], [53.6, 12226.0], [53.7, 12226.0], [53.8, 12226.0], [53.9, 12226.0], [54.0, 12759.0], [54.1, 12759.0], [54.2, 12759.0], [54.3, 12759.0], [54.4, 12759.0], [54.5, 12759.0], [54.6, 12759.0], [54.7, 12759.0], [54.8, 12759.0], [54.9, 12759.0], [55.0, 12830.0], [55.1, 12830.0], [55.2, 12830.0], [55.3, 12830.0], [55.4, 12830.0], [55.5, 12830.0], [55.6, 12830.0], [55.7, 12830.0], [55.8, 12830.0], [55.9, 12830.0], [56.0, 12914.0], [56.1, 12914.0], [56.2, 12914.0], [56.3, 12914.0], [56.4, 12914.0], [56.5, 12914.0], [56.6, 12914.0], [56.7, 12914.0], [56.8, 12914.0], [56.9, 12914.0], [57.0, 13138.0], [57.1, 13138.0], [57.2, 13138.0], [57.3, 13138.0], [57.4, 13138.0], [57.5, 13138.0], [57.6, 13138.0], [57.7, 13138.0], [57.8, 13138.0], [57.9, 13138.0], [58.0, 13551.0], [58.1, 13551.0], [58.2, 13551.0], [58.3, 13551.0], [58.4, 13551.0], [58.5, 13551.0], [58.6, 13551.0], [58.7, 13551.0], [58.8, 13551.0], [58.9, 13551.0], [59.0, 13560.0], [59.1, 13560.0], [59.2, 13560.0], [59.3, 13560.0], [59.4, 13560.0], [59.5, 13560.0], [59.6, 13560.0], [59.7, 13560.0], [59.8, 13560.0], [59.9, 13560.0], [60.0, 13828.0], [60.1, 13828.0], [60.2, 13828.0], [60.3, 13828.0], [60.4, 13828.0], [60.5, 13828.0], [60.6, 13828.0], [60.7, 13828.0], [60.8, 13828.0], [60.9, 13828.0], [61.0, 13896.0], [61.1, 13896.0], [61.2, 13896.0], [61.3, 13896.0], [61.4, 13896.0], [61.5, 13896.0], [61.6, 13896.0], [61.7, 13896.0], [61.8, 13896.0], [61.9, 13896.0], [62.0, 14161.0], [62.1, 14161.0], [62.2, 14161.0], [62.3, 14161.0], [62.4, 14161.0], [62.5, 14161.0], [62.6, 14161.0], [62.7, 14161.0], [62.8, 14161.0], [62.9, 14161.0], [63.0, 14580.0], [63.1, 14580.0], [63.2, 14580.0], [63.3, 14580.0], [63.4, 14580.0], [63.5, 14580.0], [63.6, 14580.0], [63.7, 14580.0], [63.8, 14580.0], [63.9, 14580.0], [64.0, 14667.0], [64.1, 14667.0], [64.2, 14667.0], [64.3, 14667.0], [64.4, 14667.0], [64.5, 14667.0], [64.6, 14667.0], [64.7, 14667.0], [64.8, 14667.0], [64.9, 14667.0], [65.0, 14856.0], [65.1, 14856.0], [65.2, 14856.0], [65.3, 14856.0], [65.4, 14856.0], [65.5, 14856.0], [65.6, 14856.0], [65.7, 14856.0], [65.8, 14856.0], [65.9, 14856.0], [66.0, 15201.0], [66.1, 15201.0], [66.2, 15201.0], [66.3, 15201.0], [66.4, 15201.0], [66.5, 15201.0], [66.6, 15201.0], [66.7, 15201.0], [66.8, 15201.0], [66.9, 15201.0], [67.0, 15329.0], [67.1, 15329.0], [67.2, 15329.0], [67.3, 15329.0], [67.4, 15329.0], [67.5, 15329.0], [67.6, 15329.0], [67.7, 15329.0], [67.8, 15329.0], [67.9, 15329.0], [68.0, 15354.0], [68.1, 15354.0], [68.2, 15354.0], [68.3, 15354.0], [68.4, 15354.0], [68.5, 15354.0], [68.6, 15354.0], [68.7, 15354.0], [68.8, 15354.0], [68.9, 15354.0], [69.0, 15547.0], [69.1, 15547.0], [69.2, 15547.0], [69.3, 15547.0], [69.4, 15547.0], [69.5, 15547.0], [69.6, 15547.0], [69.7, 15547.0], [69.8, 15547.0], [69.9, 15547.0], [70.0, 15575.0], [70.1, 15575.0], [70.2, 15575.0], [70.3, 15575.0], [70.4, 15575.0], [70.5, 15575.0], [70.6, 15575.0], [70.7, 15575.0], [70.8, 15575.0], [70.9, 15575.0], [71.0, 16215.0], [71.1, 16215.0], [71.2, 16215.0], [71.3, 16215.0], [71.4, 16215.0], [71.5, 16215.0], [71.6, 16215.0], [71.7, 16215.0], [71.8, 16215.0], [71.9, 16215.0], [72.0, 16380.0], [72.1, 16380.0], [72.2, 16380.0], [72.3, 16380.0], [72.4, 16380.0], [72.5, 16380.0], [72.6, 16380.0], [72.7, 16380.0], [72.8, 16380.0], [72.9, 16380.0], [73.0, 16401.0], [73.1, 16401.0], [73.2, 16401.0], [73.3, 16401.0], [73.4, 16401.0], [73.5, 16401.0], [73.6, 16401.0], [73.7, 16401.0], [73.8, 16401.0], [73.9, 16401.0], [74.0, 16453.0], [74.1, 16453.0], [74.2, 16453.0], [74.3, 16453.0], [74.4, 16453.0], [74.5, 16453.0], [74.6, 16453.0], [74.7, 16453.0], [74.8, 16453.0], [74.9, 16453.0], [75.0, 17060.0], [75.1, 17060.0], [75.2, 17060.0], [75.3, 17060.0], [75.4, 17060.0], [75.5, 17060.0], [75.6, 17060.0], [75.7, 17060.0], [75.8, 17060.0], [75.9, 17060.0], [76.0, 17244.0], [76.1, 17244.0], [76.2, 17244.0], [76.3, 17244.0], [76.4, 17244.0], [76.5, 17244.0], [76.6, 17244.0], [76.7, 17244.0], [76.8, 17244.0], [76.9, 17244.0], [77.0, 17299.0], [77.1, 17299.0], [77.2, 17299.0], [77.3, 17299.0], [77.4, 17299.0], [77.5, 17299.0], [77.6, 17299.0], [77.7, 17299.0], [77.8, 17299.0], [77.9, 17299.0], [78.0, 17508.0], [78.1, 17508.0], [78.2, 17508.0], [78.3, 17508.0], [78.4, 17508.0], [78.5, 17508.0], [78.6, 17508.0], [78.7, 17508.0], [78.8, 17508.0], [78.9, 17508.0], [79.0, 17709.0], [79.1, 17709.0], [79.2, 17709.0], [79.3, 17709.0], [79.4, 17709.0], [79.5, 17709.0], [79.6, 17709.0], [79.7, 17709.0], [79.8, 17709.0], [79.9, 17709.0], [80.0, 17892.0], [80.1, 17892.0], [80.2, 17892.0], [80.3, 17892.0], [80.4, 17892.0], [80.5, 17892.0], [80.6, 17892.0], [80.7, 17892.0], [80.8, 17892.0], [80.9, 17892.0], [81.0, 17924.0], [81.1, 17924.0], [81.2, 17924.0], [81.3, 17924.0], [81.4, 17924.0], [81.5, 17924.0], [81.6, 17924.0], [81.7, 17924.0], [81.8, 17924.0], [81.9, 17924.0], [82.0, 18429.0], [82.1, 18429.0], [82.2, 18429.0], [82.3, 18429.0], [82.4, 18429.0], [82.5, 18429.0], [82.6, 18429.0], [82.7, 18429.0], [82.8, 18429.0], [82.9, 18429.0], [83.0, 18621.0], [83.1, 18621.0], [83.2, 18621.0], [83.3, 18621.0], [83.4, 18621.0], [83.5, 18621.0], [83.6, 18621.0], [83.7, 18621.0], [83.8, 18621.0], [83.9, 18621.0], [84.0, 19103.0], [84.1, 19103.0], [84.2, 19103.0], [84.3, 19103.0], [84.4, 19103.0], [84.5, 19103.0], [84.6, 19103.0], [84.7, 19103.0], [84.8, 19103.0], [84.9, 19103.0], [85.0, 19121.0], [85.1, 19121.0], [85.2, 19121.0], [85.3, 19121.0], [85.4, 19121.0], [85.5, 19121.0], [85.6, 19121.0], [85.7, 19121.0], [85.8, 19121.0], [85.9, 19121.0], [86.0, 19299.0], [86.1, 19299.0], [86.2, 19299.0], [86.3, 19299.0], [86.4, 19299.0], [86.5, 19299.0], [86.6, 19299.0], [86.7, 19299.0], [86.8, 19299.0], [86.9, 19299.0], [87.0, 19324.0], [87.1, 19324.0], [87.2, 19324.0], [87.3, 19324.0], [87.4, 19324.0], [87.5, 19324.0], [87.6, 19324.0], [87.7, 19324.0], [87.8, 19324.0], [87.9, 19324.0], [88.0, 19449.0], [88.1, 19449.0], [88.2, 19449.0], [88.3, 19449.0], [88.4, 19449.0], [88.5, 19449.0], [88.6, 19449.0], [88.7, 19449.0], [88.8, 19449.0], [88.9, 19449.0], [89.0, 19517.0], [89.1, 19517.0], [89.2, 19517.0], [89.3, 19517.0], [89.4, 19517.0], [89.5, 19517.0], [89.6, 19517.0], [89.7, 19517.0], [89.8, 19517.0], [89.9, 19517.0], [90.0, 20011.0], [90.1, 20011.0], [90.2, 20011.0], [90.3, 20011.0], [90.4, 20011.0], [90.5, 20011.0], [90.6, 20011.0], [90.7, 20011.0], [90.8, 20011.0], [90.9, 20011.0], [91.0, 20296.0], [91.1, 20296.0], [91.2, 20296.0], [91.3, 20296.0], [91.4, 20296.0], [91.5, 20296.0], [91.6, 20296.0], [91.7, 20296.0], [91.8, 20296.0], [91.9, 20296.0], [92.0, 20373.0], [92.1, 20373.0], [92.2, 20373.0], [92.3, 20373.0], [92.4, 20373.0], [92.5, 20373.0], [92.6, 20373.0], [92.7, 20373.0], [92.8, 20373.0], [92.9, 20373.0], [93.0, 21125.0], [93.1, 21125.0], [93.2, 21125.0], [93.3, 21125.0], [93.4, 21125.0], [93.5, 21125.0], [93.6, 21125.0], [93.7, 21125.0], [93.8, 21125.0], [93.9, 21125.0], [94.0, 21167.0], [94.1, 21167.0], [94.2, 21167.0], [94.3, 21167.0], [94.4, 21167.0], [94.5, 21167.0], [94.6, 21167.0], [94.7, 21167.0], [94.8, 21167.0], [94.9, 21167.0], [95.0, 21303.0], [95.1, 21303.0], [95.2, 21303.0], [95.3, 21303.0], [95.4, 21303.0], [95.5, 21303.0], [95.6, 21303.0], [95.7, 21303.0], [95.8, 21303.0], [95.9, 21303.0], [96.0, 21373.0], [96.1, 21373.0], [96.2, 21373.0], [96.3, 21373.0], [96.4, 21373.0], [96.5, 21373.0], [96.6, 21373.0], [96.7, 21373.0], [96.8, 21373.0], [96.9, 21373.0], [97.0, 21475.0], [97.1, 21475.0], [97.2, 21475.0], [97.3, 21475.0], [97.4, 21475.0], [97.5, 21475.0], [97.6, 21475.0], [97.7, 21475.0], [97.8, 21475.0], [97.9, 21475.0], [98.0, 21476.0], [98.1, 21476.0], [98.2, 21476.0], [98.3, 21476.0], [98.4, 21476.0], [98.5, 21476.0], [98.6, 21476.0], [98.7, 21476.0], [98.8, 21476.0], [98.9, 21476.0], [99.0, 21504.0], [99.1, 21504.0], [99.2, 21504.0], [99.3, 21504.0], [99.4, 21504.0], [99.5, 21504.0], [99.6, 21504.0], [99.7, 21504.0], [99.8, 21504.0], [99.9, 21504.0]], "isOverall": false, "label": "View Blog Post", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 1700.0, "maxY": 3.0, "series": [{"data": [[1700.0, 1.0], [1800.0, 1.0], [2000.0, 1.0], [2100.0, 1.0], [2400.0, 2.0], [2500.0, 1.0], [2600.0, 1.0], [2900.0, 1.0], [3100.0, 1.0], [3400.0, 2.0], [3700.0, 1.0], [4200.0, 1.0], [4400.0, 3.0], [4600.0, 1.0], [5100.0, 1.0], [5300.0, 1.0], [5500.0, 2.0], [5800.0, 1.0], [6100.0, 1.0], [6300.0, 1.0], [6500.0, 1.0], [7100.0, 1.0], [7200.0, 1.0], [7400.0, 1.0], [7600.0, 1.0], [7700.0, 1.0], [7800.0, 1.0], [8100.0, 1.0], [8200.0, 1.0], [8400.0, 1.0], [8800.0, 1.0], [9400.0, 2.0], [9300.0, 1.0], [9600.0, 1.0], [9700.0, 2.0], [10100.0, 1.0], [10200.0, 1.0], [10300.0, 1.0], [10800.0, 1.0], [10900.0, 1.0], [11300.0, 1.0], [11500.0, 1.0], [11600.0, 1.0], [11800.0, 1.0], [12000.0, 2.0], [12200.0, 1.0], [12700.0, 1.0], [12800.0, 1.0], [12900.0, 1.0], [13100.0, 1.0], [13500.0, 2.0], [13800.0, 2.0], [14100.0, 1.0], [14500.0, 1.0], [14600.0, 1.0], [14800.0, 1.0], [15200.0, 1.0], [15300.0, 2.0], [15500.0, 2.0], [16200.0, 1.0], [16300.0, 1.0], [16400.0, 2.0], [17200.0, 2.0], [17000.0, 1.0], [17500.0, 1.0], [17700.0, 1.0], [17800.0, 1.0], [17900.0, 1.0], [18400.0, 1.0], [18600.0, 1.0], [19100.0, 2.0], [19200.0, 1.0], [19300.0, 1.0], [19400.0, 1.0], [19500.0, 1.0], [20000.0, 1.0], [20200.0, 1.0], [20300.0, 1.0], [21100.0, 2.0], [21300.0, 2.0], [21400.0, 2.0], [21500.0, 1.0]], "isOverall": false, "label": "View Blog Post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 21500.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 100.0, "minX": 2.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 100.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 100.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 50.519999999999996, "minX": 1.62298332E12, "maxY": 50.519999999999996, "series": [{"data": [[1.62298332E12, 50.519999999999996]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298332E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 1763.0, "minX": 1.0, "maxY": 21504.0, "series": [{"data": [[2.0, 21476.0], [3.0, 21475.0], [4.0, 21373.0], [5.0, 21303.0], [6.0, 21167.0], [7.0, 21125.0], [8.0, 20373.0], [9.0, 20296.0], [10.0, 20011.0], [11.0, 19517.0], [12.0, 19449.0], [13.0, 19324.0], [14.0, 19299.0], [15.0, 19121.0], [16.0, 19103.0], [17.0, 18621.0], [18.0, 18429.0], [19.0, 17924.0], [20.0, 17892.0], [21.0, 17709.0], [22.0, 17508.0], [23.0, 17299.0], [24.0, 17060.0], [25.0, 17244.0], [26.0, 16453.0], [27.0, 16401.0], [28.0, 16380.0], [29.0, 16215.0], [30.0, 15575.0], [31.0, 15547.0], [33.0, 15354.0], [32.0, 15329.0], [35.0, 14856.0], [34.0, 15201.0], [37.0, 14580.0], [36.0, 14667.0], [39.0, 13896.0], [38.0, 14161.0], [41.0, 13560.0], [40.0, 13828.0], [43.0, 13138.0], [42.0, 13551.0], [45.0, 12830.0], [44.0, 12914.0], [47.0, 12226.0], [46.0, 12759.0], [49.0, 12095.0], [48.0, 12060.0], [51.0, 11659.0], [50.0, 11876.0], [53.0, 11309.0], [52.0, 11549.0], [55.0, 10870.0], [54.0, 10902.0], [57.0, 10208.0], [56.0, 10302.0], [58.0, 10167.0], [61.0, 9654.0], [60.0, 9705.0], [63.0, 9361.5], [67.0, 8261.0], [66.0, 8411.0], [65.0, 8818.0], [64.0, 9437.0], [71.0, 7606.0], [70.0, 7798.0], [69.0, 7894.0], [68.0, 8182.0], [75.0, 6503.0], [74.0, 7175.0], [73.0, 7213.0], [72.0, 7455.0], [79.0, 5587.0], [78.0, 5895.0], [77.0, 6149.0], [76.0, 6323.0], [83.0, 4640.0], [82.0, 5104.0], [81.0, 5381.0], [80.0, 5533.0], [87.0, 4231.0], [86.0, 4405.0], [85.0, 4423.0], [84.0, 4469.0], [91.0, 3199.0], [90.0, 3496.0], [89.0, 3497.0], [88.0, 3775.0], [95.0, 2428.0], [94.0, 2562.0], [93.0, 2680.0], [92.0, 2961.0], [99.0, 1822.0], [98.0, 2079.0], [97.0, 2171.0], [96.0, 2421.0], [100.0, 1763.0], [1.0, 21504.0]], "isOverall": false, "label": "View Blog Post", "isController": false}, {"data": [[50.519999999999996, 11675.54]], "isOverall": false, "label": "View Blog Post-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 386.6666666666667, "minX": 1.62298332E12, "maxY": 29010.0, "series": [{"data": [[1.62298332E12, 29010.0]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62298332E12, 386.6666666666667]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298332E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 11675.54, "minX": 1.62298332E12, "maxY": 11675.54, "series": [{"data": [[1.62298332E12, 11675.54]], "isOverall": false, "label": "View Blog Post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298332E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 11673.070000000003, "minX": 1.62298332E12, "maxY": 11673.070000000003, "series": [{"data": [[1.62298332E12, 11673.070000000003]], "isOverall": false, "label": "View Blog Post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298332E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 1.1500000000000001, "minX": 1.62298332E12, "maxY": 1.1500000000000001, "series": [{"data": [[1.62298332E12, 1.1500000000000001]], "isOverall": false, "label": "View Blog Post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298332E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 1763.0, "minX": 1.62298332E12, "maxY": 21504.0, "series": [{"data": [[1.62298332E12, 21504.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62298332E12, 1763.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62298332E12, 19961.600000000002]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62298332E12, 21503.72]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62298332E12, 21296.199999999997]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298332E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 7605.0, "minX": 2.0, "maxY": 19524.5, "series": [{"data": [[4.0, 11105.5], [2.0, 18525.0], [5.0, 7605.0], [3.0, 13257.0], [6.0, 13498.5], [7.0, 19524.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 7.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 7602.5, "minX": 2.0, "maxY": 19522.0, "series": [{"data": [[4.0, 11103.5], [2.0, 18523.0], [5.0, 7602.5], [3.0, 13253.5], [6.0, 13490.0], [7.0, 19522.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 7.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298332E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298332E12, 1.6666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298332E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298332E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298332E12, 1.6666666666666667]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298332E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298332E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298332E12, 1.6666666666666667]], "isOverall": false, "label": "View Blog Post-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298332E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298332E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298332E12, 1.6666666666666667]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298332E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

