/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 962.0, "minX": 0.0, "maxY": 6291.0, "series": [{"data": [[0.0, 962.0], [0.1, 962.0], [0.2, 962.0], [0.3, 962.0], [0.4, 962.0], [0.5, 962.0], [0.6, 962.0], [0.7, 962.0], [0.8, 962.0], [0.9, 962.0], [1.0, 1151.0], [1.1, 1151.0], [1.2, 1151.0], [1.3, 1151.0], [1.4, 1151.0], [1.5, 1151.0], [1.6, 1151.0], [1.7, 1151.0], [1.8, 1151.0], [1.9, 1151.0], [2.0, 1181.0], [2.1, 1181.0], [2.2, 1181.0], [2.3, 1181.0], [2.4, 1181.0], [2.5, 1181.0], [2.6, 1181.0], [2.7, 1181.0], [2.8, 1181.0], [2.9, 1181.0], [3.0, 1525.0], [3.1, 1525.0], [3.2, 1525.0], [3.3, 1525.0], [3.4, 1525.0], [3.5, 1525.0], [3.6, 1525.0], [3.7, 1525.0], [3.8, 1525.0], [3.9, 1525.0], [4.0, 1628.0], [4.1, 1628.0], [4.2, 1628.0], [4.3, 1628.0], [4.4, 1628.0], [4.5, 1628.0], [4.6, 1628.0], [4.7, 1628.0], [4.8, 1628.0], [4.9, 1628.0], [5.0, 1821.0], [5.1, 1821.0], [5.2, 1821.0], [5.3, 1821.0], [5.4, 1821.0], [5.5, 1821.0], [5.6, 1821.0], [5.7, 1821.0], [5.8, 1821.0], [5.9, 1821.0], [6.0, 1981.0], [6.1, 1981.0], [6.2, 1981.0], [6.3, 1981.0], [6.4, 1981.0], [6.5, 1981.0], [6.6, 1981.0], [6.7, 1981.0], [6.8, 1981.0], [6.9, 1981.0], [7.0, 2147.0], [7.1, 2147.0], [7.2, 2147.0], [7.3, 2147.0], [7.4, 2147.0], [7.5, 2147.0], [7.6, 2147.0], [7.7, 2147.0], [7.8, 2147.0], [7.9, 2147.0], [8.0, 2149.0], [8.1, 2149.0], [8.2, 2149.0], [8.3, 2149.0], [8.4, 2149.0], [8.5, 2149.0], [8.6, 2149.0], [8.7, 2149.0], [8.8, 2149.0], [8.9, 2149.0], [9.0, 2197.0], [9.1, 2197.0], [9.2, 2197.0], [9.3, 2197.0], [9.4, 2197.0], [9.5, 2197.0], [9.6, 2197.0], [9.7, 2197.0], [9.8, 2197.0], [9.9, 2197.0], [10.0, 2478.0], [10.1, 2478.0], [10.2, 2478.0], [10.3, 2478.0], [10.4, 2478.0], [10.5, 2478.0], [10.6, 2478.0], [10.7, 2478.0], [10.8, 2478.0], [10.9, 2478.0], [11.0, 3313.0], [11.1, 3313.0], [11.2, 3313.0], [11.3, 3313.0], [11.4, 3313.0], [11.5, 3313.0], [11.6, 3313.0], [11.7, 3313.0], [11.8, 3313.0], [11.9, 3313.0], [12.0, 3528.0], [12.1, 3528.0], [12.2, 3528.0], [12.3, 3528.0], [12.4, 3528.0], [12.5, 3528.0], [12.6, 3528.0], [12.7, 3528.0], [12.8, 3528.0], [12.9, 3528.0], [13.0, 3701.0], [13.1, 3701.0], [13.2, 3701.0], [13.3, 3701.0], [13.4, 3701.0], [13.5, 3701.0], [13.6, 3701.0], [13.7, 3701.0], [13.8, 3701.0], [13.9, 3701.0], [14.0, 3809.0], [14.1, 3809.0], [14.2, 3809.0], [14.3, 3809.0], [14.4, 3809.0], [14.5, 3809.0], [14.6, 3809.0], [14.7, 3809.0], [14.8, 3809.0], [14.9, 3809.0], [15.0, 3834.0], [15.1, 3834.0], [15.2, 3834.0], [15.3, 3834.0], [15.4, 3834.0], [15.5, 3834.0], [15.6, 3834.0], [15.7, 3834.0], [15.8, 3834.0], [15.9, 3834.0], [16.0, 3886.0], [16.1, 3886.0], [16.2, 3886.0], [16.3, 3886.0], [16.4, 3886.0], [16.5, 3886.0], [16.6, 3886.0], [16.7, 3886.0], [16.8, 3886.0], [16.9, 3886.0], [17.0, 4076.0], [17.1, 4076.0], [17.2, 4076.0], [17.3, 4076.0], [17.4, 4076.0], [17.5, 4076.0], [17.6, 4076.0], [17.7, 4076.0], [17.8, 4076.0], [17.9, 4076.0], [18.0, 4091.0], [18.1, 4091.0], [18.2, 4091.0], [18.3, 4091.0], [18.4, 4091.0], [18.5, 4091.0], [18.6, 4091.0], [18.7, 4091.0], [18.8, 4091.0], [18.9, 4091.0], [19.0, 4116.0], [19.1, 4116.0], [19.2, 4116.0], [19.3, 4116.0], [19.4, 4116.0], [19.5, 4116.0], [19.6, 4116.0], [19.7, 4116.0], [19.8, 4116.0], [19.9, 4116.0], [20.0, 4276.0], [20.1, 4276.0], [20.2, 4276.0], [20.3, 4276.0], [20.4, 4276.0], [20.5, 4276.0], [20.6, 4276.0], [20.7, 4276.0], [20.8, 4276.0], [20.9, 4276.0], [21.0, 4287.0], [21.1, 4287.0], [21.2, 4287.0], [21.3, 4287.0], [21.4, 4287.0], [21.5, 4287.0], [21.6, 4287.0], [21.7, 4287.0], [21.8, 4287.0], [21.9, 4287.0], [22.0, 4296.0], [22.1, 4296.0], [22.2, 4296.0], [22.3, 4296.0], [22.4, 4296.0], [22.5, 4296.0], [22.6, 4296.0], [22.7, 4296.0], [22.8, 4296.0], [22.9, 4296.0], [23.0, 4313.0], [23.1, 4313.0], [23.2, 4313.0], [23.3, 4313.0], [23.4, 4313.0], [23.5, 4313.0], [23.6, 4313.0], [23.7, 4313.0], [23.8, 4313.0], [23.9, 4313.0], [24.0, 4316.0], [24.1, 4316.0], [24.2, 4316.0], [24.3, 4316.0], [24.4, 4316.0], [24.5, 4316.0], [24.6, 4316.0], [24.7, 4316.0], [24.8, 4316.0], [24.9, 4316.0], [25.0, 4329.0], [25.1, 4329.0], [25.2, 4329.0], [25.3, 4329.0], [25.4, 4329.0], [25.5, 4329.0], [25.6, 4329.0], [25.7, 4329.0], [25.8, 4329.0], [25.9, 4329.0], [26.0, 4431.0], [26.1, 4431.0], [26.2, 4431.0], [26.3, 4431.0], [26.4, 4431.0], [26.5, 4431.0], [26.6, 4431.0], [26.7, 4431.0], [26.8, 4431.0], [26.9, 4431.0], [27.0, 4437.0], [27.1, 4437.0], [27.2, 4437.0], [27.3, 4437.0], [27.4, 4437.0], [27.5, 4437.0], [27.6, 4437.0], [27.7, 4437.0], [27.8, 4437.0], [27.9, 4437.0], [28.0, 4467.0], [28.1, 4467.0], [28.2, 4467.0], [28.3, 4467.0], [28.4, 4467.0], [28.5, 4467.0], [28.6, 4467.0], [28.7, 4467.0], [28.8, 4467.0], [28.9, 4467.0], [29.0, 4481.0], [29.1, 4481.0], [29.2, 4481.0], [29.3, 4481.0], [29.4, 4481.0], [29.5, 4481.0], [29.6, 4481.0], [29.7, 4481.0], [29.8, 4481.0], [29.9, 4481.0], [30.0, 4495.0], [30.1, 4495.0], [30.2, 4495.0], [30.3, 4495.0], [30.4, 4495.0], [30.5, 4495.0], [30.6, 4495.0], [30.7, 4495.0], [30.8, 4495.0], [30.9, 4495.0], [31.0, 4522.0], [31.1, 4522.0], [31.2, 4522.0], [31.3, 4522.0], [31.4, 4522.0], [31.5, 4522.0], [31.6, 4522.0], [31.7, 4522.0], [31.8, 4522.0], [31.9, 4522.0], [32.0, 4554.0], [32.1, 4554.0], [32.2, 4554.0], [32.3, 4554.0], [32.4, 4554.0], [32.5, 4554.0], [32.6, 4554.0], [32.7, 4554.0], [32.8, 4554.0], [32.9, 4554.0], [33.0, 4770.0], [33.1, 4770.0], [33.2, 4770.0], [33.3, 4770.0], [33.4, 4770.0], [33.5, 4770.0], [33.6, 4770.0], [33.7, 4770.0], [33.8, 4770.0], [33.9, 4770.0], [34.0, 4798.0], [34.1, 4798.0], [34.2, 4798.0], [34.3, 4798.0], [34.4, 4798.0], [34.5, 4798.0], [34.6, 4798.0], [34.7, 4798.0], [34.8, 4798.0], [34.9, 4798.0], [35.0, 4806.0], [35.1, 4806.0], [35.2, 4806.0], [35.3, 4806.0], [35.4, 4806.0], [35.5, 4806.0], [35.6, 4806.0], [35.7, 4806.0], [35.8, 4806.0], [35.9, 4806.0], [36.0, 4823.0], [36.1, 4823.0], [36.2, 4823.0], [36.3, 4823.0], [36.4, 4823.0], [36.5, 4823.0], [36.6, 4823.0], [36.7, 4823.0], [36.8, 4823.0], [36.9, 4823.0], [37.0, 4901.0], [37.1, 4901.0], [37.2, 4901.0], [37.3, 4901.0], [37.4, 4901.0], [37.5, 4901.0], [37.6, 4901.0], [37.7, 4901.0], [37.8, 4901.0], [37.9, 4901.0], [38.0, 4920.0], [38.1, 4920.0], [38.2, 4920.0], [38.3, 4920.0], [38.4, 4920.0], [38.5, 4920.0], [38.6, 4920.0], [38.7, 4920.0], [38.8, 4920.0], [38.9, 4920.0], [39.0, 4978.0], [39.1, 4978.0], [39.2, 4978.0], [39.3, 4978.0], [39.4, 4978.0], [39.5, 4978.0], [39.6, 4978.0], [39.7, 4978.0], [39.8, 4978.0], [39.9, 4978.0], [40.0, 4981.0], [40.1, 4981.0], [40.2, 4981.0], [40.3, 4981.0], [40.4, 4981.0], [40.5, 4981.0], [40.6, 4981.0], [40.7, 4981.0], [40.8, 4981.0], [40.9, 4981.0], [41.0, 4995.0], [41.1, 4995.0], [41.2, 4995.0], [41.3, 4995.0], [41.4, 4995.0], [41.5, 4995.0], [41.6, 4995.0], [41.7, 4995.0], [41.8, 4995.0], [41.9, 4995.0], [42.0, 5006.0], [42.1, 5006.0], [42.2, 5006.0], [42.3, 5006.0], [42.4, 5006.0], [42.5, 5006.0], [42.6, 5006.0], [42.7, 5006.0], [42.8, 5006.0], [42.9, 5006.0], [43.0, 5007.0], [43.1, 5007.0], [43.2, 5007.0], [43.3, 5007.0], [43.4, 5007.0], [43.5, 5007.0], [43.6, 5007.0], [43.7, 5007.0], [43.8, 5007.0], [43.9, 5007.0], [44.0, 5040.0], [44.1, 5040.0], [44.2, 5040.0], [44.3, 5040.0], [44.4, 5040.0], [44.5, 5040.0], [44.6, 5040.0], [44.7, 5040.0], [44.8, 5040.0], [44.9, 5040.0], [45.0, 5043.0], [45.1, 5043.0], [45.2, 5043.0], [45.3, 5043.0], [45.4, 5043.0], [45.5, 5043.0], [45.6, 5043.0], [45.7, 5043.0], [45.8, 5043.0], [45.9, 5043.0], [46.0, 5046.0], [46.1, 5046.0], [46.2, 5046.0], [46.3, 5046.0], [46.4, 5046.0], [46.5, 5046.0], [46.6, 5046.0], [46.7, 5046.0], [46.8, 5046.0], [46.9, 5046.0], [47.0, 5051.0], [47.1, 5051.0], [47.2, 5051.0], [47.3, 5051.0], [47.4, 5051.0], [47.5, 5051.0], [47.6, 5051.0], [47.7, 5051.0], [47.8, 5051.0], [47.9, 5051.0], [48.0, 5065.0], [48.1, 5065.0], [48.2, 5065.0], [48.3, 5065.0], [48.4, 5065.0], [48.5, 5065.0], [48.6, 5065.0], [48.7, 5065.0], [48.8, 5065.0], [48.9, 5065.0], [49.0, 5084.0], [49.1, 5084.0], [49.2, 5084.0], [49.3, 5084.0], [49.4, 5084.0], [49.5, 5084.0], [49.6, 5084.0], [49.7, 5084.0], [49.8, 5084.0], [49.9, 5084.0], [50.0, 5122.0], [50.1, 5122.0], [50.2, 5122.0], [50.3, 5122.0], [50.4, 5122.0], [50.5, 5122.0], [50.6, 5122.0], [50.7, 5122.0], [50.8, 5122.0], [50.9, 5122.0], [51.0, 5122.0], [51.1, 5122.0], [51.2, 5122.0], [51.3, 5122.0], [51.4, 5122.0], [51.5, 5122.0], [51.6, 5122.0], [51.7, 5122.0], [51.8, 5122.0], [51.9, 5122.0], [52.0, 5163.0], [52.1, 5163.0], [52.2, 5163.0], [52.3, 5163.0], [52.4, 5163.0], [52.5, 5163.0], [52.6, 5163.0], [52.7, 5163.0], [52.8, 5163.0], [52.9, 5163.0], [53.0, 5183.0], [53.1, 5183.0], [53.2, 5183.0], [53.3, 5183.0], [53.4, 5183.0], [53.5, 5183.0], [53.6, 5183.0], [53.7, 5183.0], [53.8, 5183.0], [53.9, 5183.0], [54.0, 5186.0], [54.1, 5186.0], [54.2, 5186.0], [54.3, 5186.0], [54.4, 5186.0], [54.5, 5186.0], [54.6, 5186.0], [54.7, 5186.0], [54.8, 5186.0], [54.9, 5186.0], [55.0, 5194.0], [55.1, 5194.0], [55.2, 5194.0], [55.3, 5194.0], [55.4, 5194.0], [55.5, 5194.0], [55.6, 5194.0], [55.7, 5194.0], [55.8, 5194.0], [55.9, 5194.0], [56.0, 5235.0], [56.1, 5235.0], [56.2, 5235.0], [56.3, 5235.0], [56.4, 5235.0], [56.5, 5235.0], [56.6, 5235.0], [56.7, 5235.0], [56.8, 5235.0], [56.9, 5235.0], [57.0, 5255.0], [57.1, 5255.0], [57.2, 5255.0], [57.3, 5255.0], [57.4, 5255.0], [57.5, 5255.0], [57.6, 5255.0], [57.7, 5255.0], [57.8, 5255.0], [57.9, 5255.0], [58.0, 5268.0], [58.1, 5268.0], [58.2, 5268.0], [58.3, 5268.0], [58.4, 5268.0], [58.5, 5268.0], [58.6, 5268.0], [58.7, 5268.0], [58.8, 5268.0], [58.9, 5268.0], [59.0, 5273.0], [59.1, 5273.0], [59.2, 5273.0], [59.3, 5273.0], [59.4, 5273.0], [59.5, 5273.0], [59.6, 5273.0], [59.7, 5273.0], [59.8, 5273.0], [59.9, 5273.0], [60.0, 5330.0], [60.1, 5330.0], [60.2, 5330.0], [60.3, 5330.0], [60.4, 5330.0], [60.5, 5330.0], [60.6, 5330.0], [60.7, 5330.0], [60.8, 5330.0], [60.9, 5330.0], [61.0, 5348.0], [61.1, 5348.0], [61.2, 5348.0], [61.3, 5348.0], [61.4, 5348.0], [61.5, 5348.0], [61.6, 5348.0], [61.7, 5348.0], [61.8, 5348.0], [61.9, 5348.0], [62.0, 5358.0], [62.1, 5358.0], [62.2, 5358.0], [62.3, 5358.0], [62.4, 5358.0], [62.5, 5358.0], [62.6, 5358.0], [62.7, 5358.0], [62.8, 5358.0], [62.9, 5358.0], [63.0, 5359.0], [63.1, 5359.0], [63.2, 5359.0], [63.3, 5359.0], [63.4, 5359.0], [63.5, 5359.0], [63.6, 5359.0], [63.7, 5359.0], [63.8, 5359.0], [63.9, 5359.0], [64.0, 5373.0], [64.1, 5373.0], [64.2, 5373.0], [64.3, 5373.0], [64.4, 5373.0], [64.5, 5373.0], [64.6, 5373.0], [64.7, 5373.0], [64.8, 5373.0], [64.9, 5373.0], [65.0, 5405.0], [65.1, 5405.0], [65.2, 5405.0], [65.3, 5405.0], [65.4, 5405.0], [65.5, 5405.0], [65.6, 5405.0], [65.7, 5405.0], [65.8, 5405.0], [65.9, 5405.0], [66.0, 5447.0], [66.1, 5447.0], [66.2, 5447.0], [66.3, 5447.0], [66.4, 5447.0], [66.5, 5447.0], [66.6, 5447.0], [66.7, 5447.0], [66.8, 5447.0], [66.9, 5447.0], [67.0, 5448.0], [67.1, 5448.0], [67.2, 5448.0], [67.3, 5448.0], [67.4, 5448.0], [67.5, 5448.0], [67.6, 5448.0], [67.7, 5448.0], [67.8, 5448.0], [67.9, 5448.0], [68.0, 5450.0], [68.1, 5450.0], [68.2, 5450.0], [68.3, 5450.0], [68.4, 5450.0], [68.5, 5450.0], [68.6, 5450.0], [68.7, 5450.0], [68.8, 5450.0], [68.9, 5450.0], [69.0, 5454.0], [69.1, 5454.0], [69.2, 5454.0], [69.3, 5454.0], [69.4, 5454.0], [69.5, 5454.0], [69.6, 5454.0], [69.7, 5454.0], [69.8, 5454.0], [69.9, 5454.0], [70.0, 5459.0], [70.1, 5459.0], [70.2, 5459.0], [70.3, 5459.0], [70.4, 5459.0], [70.5, 5459.0], [70.6, 5459.0], [70.7, 5459.0], [70.8, 5459.0], [70.9, 5459.0], [71.0, 5467.0], [71.1, 5467.0], [71.2, 5467.0], [71.3, 5467.0], [71.4, 5467.0], [71.5, 5467.0], [71.6, 5467.0], [71.7, 5467.0], [71.8, 5467.0], [71.9, 5467.0], [72.0, 5490.0], [72.1, 5490.0], [72.2, 5490.0], [72.3, 5490.0], [72.4, 5490.0], [72.5, 5490.0], [72.6, 5490.0], [72.7, 5490.0], [72.8, 5490.0], [72.9, 5490.0], [73.0, 5539.0], [73.1, 5539.0], [73.2, 5539.0], [73.3, 5539.0], [73.4, 5539.0], [73.5, 5539.0], [73.6, 5539.0], [73.7, 5539.0], [73.8, 5539.0], [73.9, 5539.0], [74.0, 5569.0], [74.1, 5569.0], [74.2, 5569.0], [74.3, 5569.0], [74.4, 5569.0], [74.5, 5569.0], [74.6, 5569.0], [74.7, 5569.0], [74.8, 5569.0], [74.9, 5569.0], [75.0, 5604.0], [75.1, 5604.0], [75.2, 5604.0], [75.3, 5604.0], [75.4, 5604.0], [75.5, 5604.0], [75.6, 5604.0], [75.7, 5604.0], [75.8, 5604.0], [75.9, 5604.0], [76.0, 5605.0], [76.1, 5605.0], [76.2, 5605.0], [76.3, 5605.0], [76.4, 5605.0], [76.5, 5605.0], [76.6, 5605.0], [76.7, 5605.0], [76.8, 5605.0], [76.9, 5605.0], [77.0, 5658.0], [77.1, 5658.0], [77.2, 5658.0], [77.3, 5658.0], [77.4, 5658.0], [77.5, 5658.0], [77.6, 5658.0], [77.7, 5658.0], [77.8, 5658.0], [77.9, 5658.0], [78.0, 5670.0], [78.1, 5670.0], [78.2, 5670.0], [78.3, 5670.0], [78.4, 5670.0], [78.5, 5670.0], [78.6, 5670.0], [78.7, 5670.0], [78.8, 5670.0], [78.9, 5670.0], [79.0, 5702.0], [79.1, 5702.0], [79.2, 5702.0], [79.3, 5702.0], [79.4, 5702.0], [79.5, 5702.0], [79.6, 5702.0], [79.7, 5702.0], [79.8, 5702.0], [79.9, 5702.0], [80.0, 5731.0], [80.1, 5731.0], [80.2, 5731.0], [80.3, 5731.0], [80.4, 5731.0], [80.5, 5731.0], [80.6, 5731.0], [80.7, 5731.0], [80.8, 5731.0], [80.9, 5731.0], [81.0, 5769.0], [81.1, 5769.0], [81.2, 5769.0], [81.3, 5769.0], [81.4, 5769.0], [81.5, 5769.0], [81.6, 5769.0], [81.7, 5769.0], [81.8, 5769.0], [81.9, 5769.0], [82.0, 5808.0], [82.1, 5808.0], [82.2, 5808.0], [82.3, 5808.0], [82.4, 5808.0], [82.5, 5808.0], [82.6, 5808.0], [82.7, 5808.0], [82.8, 5808.0], [82.9, 5808.0], [83.0, 5829.0], [83.1, 5829.0], [83.2, 5829.0], [83.3, 5829.0], [83.4, 5829.0], [83.5, 5829.0], [83.6, 5829.0], [83.7, 5829.0], [83.8, 5829.0], [83.9, 5829.0], [84.0, 5838.0], [84.1, 5838.0], [84.2, 5838.0], [84.3, 5838.0], [84.4, 5838.0], [84.5, 5838.0], [84.6, 5838.0], [84.7, 5838.0], [84.8, 5838.0], [84.9, 5838.0], [85.0, 5848.0], [85.1, 5848.0], [85.2, 5848.0], [85.3, 5848.0], [85.4, 5848.0], [85.5, 5848.0], [85.6, 5848.0], [85.7, 5848.0], [85.8, 5848.0], [85.9, 5848.0], [86.0, 5853.0], [86.1, 5853.0], [86.2, 5853.0], [86.3, 5853.0], [86.4, 5853.0], [86.5, 5853.0], [86.6, 5853.0], [86.7, 5853.0], [86.8, 5853.0], [86.9, 5853.0], [87.0, 5866.0], [87.1, 5866.0], [87.2, 5866.0], [87.3, 5866.0], [87.4, 5866.0], [87.5, 5866.0], [87.6, 5866.0], [87.7, 5866.0], [87.8, 5866.0], [87.9, 5866.0], [88.0, 5931.0], [88.1, 5931.0], [88.2, 5931.0], [88.3, 5931.0], [88.4, 5931.0], [88.5, 5931.0], [88.6, 5931.0], [88.7, 5931.0], [88.8, 5931.0], [88.9, 5931.0], [89.0, 5938.0], [89.1, 5938.0], [89.2, 5938.0], [89.3, 5938.0], [89.4, 5938.0], [89.5, 5938.0], [89.6, 5938.0], [89.7, 5938.0], [89.8, 5938.0], [89.9, 5938.0], [90.0, 5941.0], [90.1, 5941.0], [90.2, 5941.0], [90.3, 5941.0], [90.4, 5941.0], [90.5, 5941.0], [90.6, 5941.0], [90.7, 5941.0], [90.8, 5941.0], [90.9, 5941.0], [91.0, 5970.0], [91.1, 5970.0], [91.2, 5970.0], [91.3, 5970.0], [91.4, 5970.0], [91.5, 5970.0], [91.6, 5970.0], [91.7, 5970.0], [91.8, 5970.0], [91.9, 5970.0], [92.0, 6023.0], [92.1, 6023.0], [92.2, 6023.0], [92.3, 6023.0], [92.4, 6023.0], [92.5, 6023.0], [92.6, 6023.0], [92.7, 6023.0], [92.8, 6023.0], [92.9, 6023.0], [93.0, 6053.0], [93.1, 6053.0], [93.2, 6053.0], [93.3, 6053.0], [93.4, 6053.0], [93.5, 6053.0], [93.6, 6053.0], [93.7, 6053.0], [93.8, 6053.0], [93.9, 6053.0], [94.0, 6062.0], [94.1, 6062.0], [94.2, 6062.0], [94.3, 6062.0], [94.4, 6062.0], [94.5, 6062.0], [94.6, 6062.0], [94.7, 6062.0], [94.8, 6062.0], [94.9, 6062.0], [95.0, 6083.0], [95.1, 6083.0], [95.2, 6083.0], [95.3, 6083.0], [95.4, 6083.0], [95.5, 6083.0], [95.6, 6083.0], [95.7, 6083.0], [95.8, 6083.0], [95.9, 6083.0], [96.0, 6144.0], [96.1, 6144.0], [96.2, 6144.0], [96.3, 6144.0], [96.4, 6144.0], [96.5, 6144.0], [96.6, 6144.0], [96.7, 6144.0], [96.8, 6144.0], [96.9, 6144.0], [97.0, 6162.0], [97.1, 6162.0], [97.2, 6162.0], [97.3, 6162.0], [97.4, 6162.0], [97.5, 6162.0], [97.6, 6162.0], [97.7, 6162.0], [97.8, 6162.0], [97.9, 6162.0], [98.0, 6244.0], [98.1, 6244.0], [98.2, 6244.0], [98.3, 6244.0], [98.4, 6244.0], [98.5, 6244.0], [98.6, 6244.0], [98.7, 6244.0], [98.8, 6244.0], [98.9, 6244.0], [99.0, 6291.0], [99.1, 6291.0], [99.2, 6291.0], [99.3, 6291.0], [99.4, 6291.0], [99.5, 6291.0], [99.6, 6291.0], [99.7, 6291.0], [99.8, 6291.0], [99.9, 6291.0]], "isOverall": false, "label": "View Blog Post", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 900.0, "maxY": 8.0, "series": [{"data": [[900.0, 1.0], [1100.0, 2.0], [1500.0, 1.0], [1600.0, 1.0], [1800.0, 1.0], [1900.0, 1.0], [2100.0, 3.0], [2400.0, 1.0], [3300.0, 1.0], [3500.0, 1.0], [3700.0, 1.0], [3800.0, 3.0], [4000.0, 2.0], [4100.0, 1.0], [4200.0, 3.0], [4300.0, 3.0], [4400.0, 5.0], [4500.0, 2.0], [4800.0, 2.0], [4700.0, 2.0], [5000.0, 8.0], [5100.0, 6.0], [4900.0, 5.0], [5200.0, 4.0], [5300.0, 5.0], [5400.0, 8.0], [5600.0, 4.0], [5500.0, 2.0], [5700.0, 3.0], [5800.0, 6.0], [6000.0, 4.0], [5900.0, 4.0], [6100.0, 2.0], [6200.0, 2.0]], "isOverall": false, "label": "View Blog Post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 6200.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 3.0, "minX": 1.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 97.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 3.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 97.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 21.209999999999997, "minX": 1.62298338E12, "maxY": 21.209999999999997, "series": [{"data": [[1.62298338E12, 21.209999999999997]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298338E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 3701.0, "minX": 1.0, "maxY": 5633.5, "series": [{"data": [[2.0, 3886.0], [3.0, 4522.0], [4.0, 4316.0], [5.0, 4296.0], [6.0, 5051.0], [7.0, 4437.0], [8.0, 4495.0], [9.0, 4313.0], [10.0, 5046.0], [11.0, 4091.0], [12.0, 5065.0], [13.0, 4554.0], [14.0, 3809.0], [15.0, 4467.0], [16.0, 5454.0], [1.0, 3701.0], [17.0, 4276.0], [18.0, 5403.333333333333], [19.0, 4806.0], [20.0, 4770.0], [21.0, 5629.2], [22.0, 5426.0], [23.0, 5567.5], [24.0, 5633.5], [25.0, 4491.589285714285]], "isOverall": false, "label": "View Blog Post", "isController": false}, {"data": [[21.209999999999997, 4762.120000000002]], "isOverall": false, "label": "View Blog Post-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 25.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 386.6666666666667, "minX": 1.62298338E12, "maxY": 29009.933333333334, "series": [{"data": [[1.62298338E12, 29009.933333333334]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62298338E12, 386.6666666666667]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298338E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 4762.120000000002, "minX": 1.62298338E12, "maxY": 4762.120000000002, "series": [{"data": [[1.62298338E12, 4762.120000000002]], "isOverall": false, "label": "View Blog Post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298338E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 4759.810000000002, "minX": 1.62298338E12, "maxY": 4759.810000000002, "series": [{"data": [[1.62298338E12, 4759.810000000002]], "isOverall": false, "label": "View Blog Post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298338E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 0.8800000000000001, "minX": 1.62298338E12, "maxY": 0.8800000000000001, "series": [{"data": [[1.62298338E12, 0.8800000000000001]], "isOverall": false, "label": "View Blog Post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298338E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 962.0, "minX": 1.62298338E12, "maxY": 6291.0, "series": [{"data": [[1.62298338E12, 6291.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62298338E12, 962.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62298338E12, 5940.7]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62298338E12, 6290.53]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62298338E12, 6081.95]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298338E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 3800.0, "minX": 2.0, "maxY": 5702.0, "series": [{"data": [[2.0, 3800.0], [4.0, 5093.5], [8.0, 5526.5], [5.0, 5043.0], [6.0, 3981.0], [3.0, 5702.0], [7.0, 5450.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 8.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 3798.0, "minX": 2.0, "maxY": 5700.0, "series": [{"data": [[2.0, 3798.0], [4.0, 5091.0], [8.0, 5524.0], [5.0, 5040.5], [6.0, 3979.0], [3.0, 5700.0], [7.0, 5448.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 8.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298338E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298338E12, 1.6666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298338E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298338E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298338E12, 1.6666666666666667]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62298338E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298338E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298338E12, 1.6666666666666667]], "isOverall": false, "label": "View Blog Post-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298338E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62298338E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62298338E12, 1.6666666666666667]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62298338E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

