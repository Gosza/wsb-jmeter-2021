/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 1991.0, "minX": 0.0, "maxY": 19818.0, "series": [{"data": [[0.0, 1991.0], [0.1, 1991.0], [0.2, 1991.0], [0.3, 1991.0], [0.4, 1991.0], [0.5, 1991.0], [0.6, 1991.0], [0.7, 1991.0], [0.8, 1991.0], [0.9, 1991.0], [1.0, 2199.0], [1.1, 2199.0], [1.2, 2199.0], [1.3, 2199.0], [1.4, 2199.0], [1.5, 2199.0], [1.6, 2199.0], [1.7, 2199.0], [1.8, 2199.0], [1.9, 2199.0], [2.0, 2684.0], [2.1, 2684.0], [2.2, 2684.0], [2.3, 2684.0], [2.4, 2684.0], [2.5, 2684.0], [2.6, 2684.0], [2.7, 2684.0], [2.8, 2684.0], [2.9, 2684.0], [3.0, 2698.0], [3.1, 2698.0], [3.2, 2698.0], [3.3, 2698.0], [3.4, 2698.0], [3.5, 2698.0], [3.6, 2698.0], [3.7, 2698.0], [3.8, 2698.0], [3.9, 2698.0], [4.0, 2761.0], [4.1, 2761.0], [4.2, 2761.0], [4.3, 2761.0], [4.4, 2761.0], [4.5, 2761.0], [4.6, 2761.0], [4.7, 2761.0], [4.8, 2761.0], [4.9, 2761.0], [5.0, 3073.0], [5.1, 3073.0], [5.2, 3073.0], [5.3, 3073.0], [5.4, 3073.0], [5.5, 3073.0], [5.6, 3073.0], [5.7, 3073.0], [5.8, 3073.0], [5.9, 3073.0], [6.0, 3100.0], [6.1, 3100.0], [6.2, 3100.0], [6.3, 3100.0], [6.4, 3100.0], [6.5, 3100.0], [6.6, 3100.0], [6.7, 3100.0], [6.8, 3100.0], [6.9, 3100.0], [7.0, 3700.0], [7.1, 3700.0], [7.2, 3700.0], [7.3, 3700.0], [7.4, 3700.0], [7.5, 3700.0], [7.6, 3700.0], [7.7, 3700.0], [7.8, 3700.0], [7.9, 3700.0], [8.0, 3704.0], [8.1, 3704.0], [8.2, 3704.0], [8.3, 3704.0], [8.4, 3704.0], [8.5, 3704.0], [8.6, 3704.0], [8.7, 3704.0], [8.8, 3704.0], [8.9, 3704.0], [9.0, 3809.0], [9.1, 3809.0], [9.2, 3809.0], [9.3, 3809.0], [9.4, 3809.0], [9.5, 3809.0], [9.6, 3809.0], [9.7, 3809.0], [9.8, 3809.0], [9.9, 3809.0], [10.0, 4025.0], [10.1, 4025.0], [10.2, 4025.0], [10.3, 4025.0], [10.4, 4025.0], [10.5, 4025.0], [10.6, 4025.0], [10.7, 4025.0], [10.8, 4025.0], [10.9, 4025.0], [11.0, 4058.0], [11.1, 4058.0], [11.2, 4058.0], [11.3, 4058.0], [11.4, 4058.0], [11.5, 4058.0], [11.6, 4058.0], [11.7, 4058.0], [11.8, 4058.0], [11.9, 4058.0], [12.0, 4361.0], [12.1, 4361.0], [12.2, 4361.0], [12.3, 4361.0], [12.4, 4361.0], [12.5, 4361.0], [12.6, 4361.0], [12.7, 4361.0], [12.8, 4361.0], [12.9, 4361.0], [13.0, 4455.0], [13.1, 4455.0], [13.2, 4455.0], [13.3, 4455.0], [13.4, 4455.0], [13.5, 4455.0], [13.6, 4455.0], [13.7, 4455.0], [13.8, 4455.0], [13.9, 4455.0], [14.0, 4472.0], [14.1, 4472.0], [14.2, 4472.0], [14.3, 4472.0], [14.4, 4472.0], [14.5, 4472.0], [14.6, 4472.0], [14.7, 4472.0], [14.8, 4472.0], [14.9, 4472.0], [15.0, 4483.0], [15.1, 4483.0], [15.2, 4483.0], [15.3, 4483.0], [15.4, 4483.0], [15.5, 4483.0], [15.6, 4483.0], [15.7, 4483.0], [15.8, 4483.0], [15.9, 4483.0], [16.0, 4544.0], [16.1, 4544.0], [16.2, 4544.0], [16.3, 4544.0], [16.4, 4544.0], [16.5, 4544.0], [16.6, 4544.0], [16.7, 4544.0], [16.8, 4544.0], [16.9, 4544.0], [17.0, 4717.0], [17.1, 4717.0], [17.2, 4717.0], [17.3, 4717.0], [17.4, 4717.0], [17.5, 4717.0], [17.6, 4717.0], [17.7, 4717.0], [17.8, 4717.0], [17.9, 4717.0], [18.0, 4798.0], [18.1, 4798.0], [18.2, 4798.0], [18.3, 4798.0], [18.4, 4798.0], [18.5, 4798.0], [18.6, 4798.0], [18.7, 4798.0], [18.8, 4798.0], [18.9, 4798.0], [19.0, 4917.0], [19.1, 4917.0], [19.2, 4917.0], [19.3, 4917.0], [19.4, 4917.0], [19.5, 4917.0], [19.6, 4917.0], [19.7, 4917.0], [19.8, 4917.0], [19.9, 4917.0], [20.0, 4964.0], [20.1, 4964.0], [20.2, 4964.0], [20.3, 4964.0], [20.4, 4964.0], [20.5, 4964.0], [20.6, 4964.0], [20.7, 4964.0], [20.8, 4964.0], [20.9, 4964.0], [21.0, 5024.0], [21.1, 5024.0], [21.2, 5024.0], [21.3, 5024.0], [21.4, 5024.0], [21.5, 5024.0], [21.6, 5024.0], [21.7, 5024.0], [21.8, 5024.0], [21.9, 5024.0], [22.0, 5751.0], [22.1, 5751.0], [22.2, 5751.0], [22.3, 5751.0], [22.4, 5751.0], [22.5, 5751.0], [22.6, 5751.0], [22.7, 5751.0], [22.8, 5751.0], [22.9, 5751.0], [23.0, 6547.0], [23.1, 6547.0], [23.2, 6547.0], [23.3, 6547.0], [23.4, 6547.0], [23.5, 6547.0], [23.6, 6547.0], [23.7, 6547.0], [23.8, 6547.0], [23.9, 6547.0], [24.0, 6595.0], [24.1, 6595.0], [24.2, 6595.0], [24.3, 6595.0], [24.4, 6595.0], [24.5, 6595.0], [24.6, 6595.0], [24.7, 6595.0], [24.8, 6595.0], [24.9, 6595.0], [25.0, 6743.0], [25.1, 6743.0], [25.2, 6743.0], [25.3, 6743.0], [25.4, 6743.0], [25.5, 6743.0], [25.6, 6743.0], [25.7, 6743.0], [25.8, 6743.0], [25.9, 6743.0], [26.0, 6752.0], [26.1, 6752.0], [26.2, 6752.0], [26.3, 6752.0], [26.4, 6752.0], [26.5, 6752.0], [26.6, 6752.0], [26.7, 6752.0], [26.8, 6752.0], [26.9, 6752.0], [27.0, 6763.0], [27.1, 6763.0], [27.2, 6763.0], [27.3, 6763.0], [27.4, 6763.0], [27.5, 6763.0], [27.6, 6763.0], [27.7, 6763.0], [27.8, 6763.0], [27.9, 6763.0], [28.0, 6768.0], [28.1, 6768.0], [28.2, 6768.0], [28.3, 6768.0], [28.4, 6768.0], [28.5, 6768.0], [28.6, 6768.0], [28.7, 6768.0], [28.8, 6768.0], [28.9, 6768.0], [29.0, 7258.0], [29.1, 7258.0], [29.2, 7258.0], [29.3, 7258.0], [29.4, 7258.0], [29.5, 7258.0], [29.6, 7258.0], [29.7, 7258.0], [29.8, 7258.0], [29.9, 7258.0], [30.0, 7705.0], [30.1, 7705.0], [30.2, 7705.0], [30.3, 7705.0], [30.4, 7705.0], [30.5, 7705.0], [30.6, 7705.0], [30.7, 7705.0], [30.8, 7705.0], [30.9, 7705.0], [31.0, 7938.0], [31.1, 7938.0], [31.2, 7938.0], [31.3, 7938.0], [31.4, 7938.0], [31.5, 7938.0], [31.6, 7938.0], [31.7, 7938.0], [31.8, 7938.0], [31.9, 7938.0], [32.0, 7966.0], [32.1, 7966.0], [32.2, 7966.0], [32.3, 7966.0], [32.4, 7966.0], [32.5, 7966.0], [32.6, 7966.0], [32.7, 7966.0], [32.8, 7966.0], [32.9, 7966.0], [33.0, 8082.0], [33.1, 8082.0], [33.2, 8082.0], [33.3, 8082.0], [33.4, 8082.0], [33.5, 8082.0], [33.6, 8082.0], [33.7, 8082.0], [33.8, 8082.0], [33.9, 8082.0], [34.0, 8166.0], [34.1, 8166.0], [34.2, 8166.0], [34.3, 8166.0], [34.4, 8166.0], [34.5, 8166.0], [34.6, 8166.0], [34.7, 8166.0], [34.8, 8166.0], [34.9, 8166.0], [35.0, 8621.0], [35.1, 8621.0], [35.2, 8621.0], [35.3, 8621.0], [35.4, 8621.0], [35.5, 8621.0], [35.6, 8621.0], [35.7, 8621.0], [35.8, 8621.0], [35.9, 8621.0], [36.0, 8716.0], [36.1, 8716.0], [36.2, 8716.0], [36.3, 8716.0], [36.4, 8716.0], [36.5, 8716.0], [36.6, 8716.0], [36.7, 8716.0], [36.8, 8716.0], [36.9, 8716.0], [37.0, 8728.0], [37.1, 8728.0], [37.2, 8728.0], [37.3, 8728.0], [37.4, 8728.0], [37.5, 8728.0], [37.6, 8728.0], [37.7, 8728.0], [37.8, 8728.0], [37.9, 8728.0], [38.0, 8824.0], [38.1, 8824.0], [38.2, 8824.0], [38.3, 8824.0], [38.4, 8824.0], [38.5, 8824.0], [38.6, 8824.0], [38.7, 8824.0], [38.8, 8824.0], [38.9, 8824.0], [39.0, 8917.0], [39.1, 8917.0], [39.2, 8917.0], [39.3, 8917.0], [39.4, 8917.0], [39.5, 8917.0], [39.6, 8917.0], [39.7, 8917.0], [39.8, 8917.0], [39.9, 8917.0], [40.0, 9284.0], [40.1, 9284.0], [40.2, 9284.0], [40.3, 9284.0], [40.4, 9284.0], [40.5, 9284.0], [40.6, 9284.0], [40.7, 9284.0], [40.8, 9284.0], [40.9, 9284.0], [41.0, 9543.0], [41.1, 9543.0], [41.2, 9543.0], [41.3, 9543.0], [41.4, 9543.0], [41.5, 9543.0], [41.6, 9543.0], [41.7, 9543.0], [41.8, 9543.0], [41.9, 9543.0], [42.0, 9839.0], [42.1, 9839.0], [42.2, 9839.0], [42.3, 9839.0], [42.4, 9839.0], [42.5, 9839.0], [42.6, 9839.0], [42.7, 9839.0], [42.8, 9839.0], [42.9, 9839.0], [43.0, 10293.0], [43.1, 10293.0], [43.2, 10293.0], [43.3, 10293.0], [43.4, 10293.0], [43.5, 10293.0], [43.6, 10293.0], [43.7, 10293.0], [43.8, 10293.0], [43.9, 10293.0], [44.0, 10300.0], [44.1, 10300.0], [44.2, 10300.0], [44.3, 10300.0], [44.4, 10300.0], [44.5, 10300.0], [44.6, 10300.0], [44.7, 10300.0], [44.8, 10300.0], [44.9, 10300.0], [45.0, 10308.0], [45.1, 10308.0], [45.2, 10308.0], [45.3, 10308.0], [45.4, 10308.0], [45.5, 10308.0], [45.6, 10308.0], [45.7, 10308.0], [45.8, 10308.0], [45.9, 10308.0], [46.0, 10576.0], [46.1, 10576.0], [46.2, 10576.0], [46.3, 10576.0], [46.4, 10576.0], [46.5, 10576.0], [46.6, 10576.0], [46.7, 10576.0], [46.8, 10576.0], [46.9, 10576.0], [47.0, 10926.0], [47.1, 10926.0], [47.2, 10926.0], [47.3, 10926.0], [47.4, 10926.0], [47.5, 10926.0], [47.6, 10926.0], [47.7, 10926.0], [47.8, 10926.0], [47.9, 10926.0], [48.0, 10966.0], [48.1, 10966.0], [48.2, 10966.0], [48.3, 10966.0], [48.4, 10966.0], [48.5, 10966.0], [48.6, 10966.0], [48.7, 10966.0], [48.8, 10966.0], [48.9, 10966.0], [49.0, 11148.0], [49.1, 11148.0], [49.2, 11148.0], [49.3, 11148.0], [49.4, 11148.0], [49.5, 11148.0], [49.6, 11148.0], [49.7, 11148.0], [49.8, 11148.0], [49.9, 11148.0], [50.0, 11304.0], [50.1, 11304.0], [50.2, 11304.0], [50.3, 11304.0], [50.4, 11304.0], [50.5, 11304.0], [50.6, 11304.0], [50.7, 11304.0], [50.8, 11304.0], [50.9, 11304.0], [51.0, 11983.0], [51.1, 11983.0], [51.2, 11983.0], [51.3, 11983.0], [51.4, 11983.0], [51.5, 11983.0], [51.6, 11983.0], [51.7, 11983.0], [51.8, 11983.0], [51.9, 11983.0], [52.0, 11986.0], [52.1, 11986.0], [52.2, 11986.0], [52.3, 11986.0], [52.4, 11986.0], [52.5, 11986.0], [52.6, 11986.0], [52.7, 11986.0], [52.8, 11986.0], [52.9, 11986.0], [53.0, 11994.0], [53.1, 11994.0], [53.2, 11994.0], [53.3, 11994.0], [53.4, 11994.0], [53.5, 11994.0], [53.6, 11994.0], [53.7, 11994.0], [53.8, 11994.0], [53.9, 11994.0], [54.0, 12012.0], [54.1, 12012.0], [54.2, 12012.0], [54.3, 12012.0], [54.4, 12012.0], [54.5, 12012.0], [54.6, 12012.0], [54.7, 12012.0], [54.8, 12012.0], [54.9, 12012.0], [55.0, 12156.0], [55.1, 12156.0], [55.2, 12156.0], [55.3, 12156.0], [55.4, 12156.0], [55.5, 12156.0], [55.6, 12156.0], [55.7, 12156.0], [55.8, 12156.0], [55.9, 12156.0], [56.0, 12276.0], [56.1, 12276.0], [56.2, 12276.0], [56.3, 12276.0], [56.4, 12276.0], [56.5, 12276.0], [56.6, 12276.0], [56.7, 12276.0], [56.8, 12276.0], [56.9, 12276.0], [57.0, 12688.0], [57.1, 12688.0], [57.2, 12688.0], [57.3, 12688.0], [57.4, 12688.0], [57.5, 12688.0], [57.6, 12688.0], [57.7, 12688.0], [57.8, 12688.0], [57.9, 12688.0], [58.0, 12880.0], [58.1, 12880.0], [58.2, 12880.0], [58.3, 12880.0], [58.4, 12880.0], [58.5, 12880.0], [58.6, 12880.0], [58.7, 12880.0], [58.8, 12880.0], [58.9, 12880.0], [59.0, 12905.0], [59.1, 12905.0], [59.2, 12905.0], [59.3, 12905.0], [59.4, 12905.0], [59.5, 12905.0], [59.6, 12905.0], [59.7, 12905.0], [59.8, 12905.0], [59.9, 12905.0], [60.0, 12951.0], [60.1, 12951.0], [60.2, 12951.0], [60.3, 12951.0], [60.4, 12951.0], [60.5, 12951.0], [60.6, 12951.0], [60.7, 12951.0], [60.8, 12951.0], [60.9, 12951.0], [61.0, 13028.0], [61.1, 13028.0], [61.2, 13028.0], [61.3, 13028.0], [61.4, 13028.0], [61.5, 13028.0], [61.6, 13028.0], [61.7, 13028.0], [61.8, 13028.0], [61.9, 13028.0], [62.0, 13067.0], [62.1, 13067.0], [62.2, 13067.0], [62.3, 13067.0], [62.4, 13067.0], [62.5, 13067.0], [62.6, 13067.0], [62.7, 13067.0], [62.8, 13067.0], [62.9, 13067.0], [63.0, 13482.0], [63.1, 13482.0], [63.2, 13482.0], [63.3, 13482.0], [63.4, 13482.0], [63.5, 13482.0], [63.6, 13482.0], [63.7, 13482.0], [63.8, 13482.0], [63.9, 13482.0], [64.0, 13759.0], [64.1, 13759.0], [64.2, 13759.0], [64.3, 13759.0], [64.4, 13759.0], [64.5, 13759.0], [64.6, 13759.0], [64.7, 13759.0], [64.8, 13759.0], [64.9, 13759.0], [65.0, 14149.0], [65.1, 14149.0], [65.2, 14149.0], [65.3, 14149.0], [65.4, 14149.0], [65.5, 14149.0], [65.6, 14149.0], [65.7, 14149.0], [65.8, 14149.0], [65.9, 14149.0], [66.0, 14179.0], [66.1, 14179.0], [66.2, 14179.0], [66.3, 14179.0], [66.4, 14179.0], [66.5, 14179.0], [66.6, 14179.0], [66.7, 14179.0], [66.8, 14179.0], [66.9, 14179.0], [67.0, 14221.0], [67.1, 14221.0], [67.2, 14221.0], [67.3, 14221.0], [67.4, 14221.0], [67.5, 14221.0], [67.6, 14221.0], [67.7, 14221.0], [67.8, 14221.0], [67.9, 14221.0], [68.0, 14351.0], [68.1, 14351.0], [68.2, 14351.0], [68.3, 14351.0], [68.4, 14351.0], [68.5, 14351.0], [68.6, 14351.0], [68.7, 14351.0], [68.8, 14351.0], [68.9, 14351.0], [69.0, 14363.0], [69.1, 14363.0], [69.2, 14363.0], [69.3, 14363.0], [69.4, 14363.0], [69.5, 14363.0], [69.6, 14363.0], [69.7, 14363.0], [69.8, 14363.0], [69.9, 14363.0], [70.0, 14498.0], [70.1, 14498.0], [70.2, 14498.0], [70.3, 14498.0], [70.4, 14498.0], [70.5, 14498.0], [70.6, 14498.0], [70.7, 14498.0], [70.8, 14498.0], [70.9, 14498.0], [71.0, 14593.0], [71.1, 14593.0], [71.2, 14593.0], [71.3, 14593.0], [71.4, 14593.0], [71.5, 14593.0], [71.6, 14593.0], [71.7, 14593.0], [71.8, 14593.0], [71.9, 14593.0], [72.0, 14714.0], [72.1, 14714.0], [72.2, 14714.0], [72.3, 14714.0], [72.4, 14714.0], [72.5, 14714.0], [72.6, 14714.0], [72.7, 14714.0], [72.8, 14714.0], [72.9, 14714.0], [73.0, 15127.0], [73.1, 15127.0], [73.2, 15127.0], [73.3, 15127.0], [73.4, 15127.0], [73.5, 15127.0], [73.6, 15127.0], [73.7, 15127.0], [73.8, 15127.0], [73.9, 15127.0], [74.0, 15658.0], [74.1, 15658.0], [74.2, 15658.0], [74.3, 15658.0], [74.4, 15658.0], [74.5, 15658.0], [74.6, 15658.0], [74.7, 15658.0], [74.8, 15658.0], [74.9, 15658.0], [75.0, 15686.0], [75.1, 15686.0], [75.2, 15686.0], [75.3, 15686.0], [75.4, 15686.0], [75.5, 15686.0], [75.6, 15686.0], [75.7, 15686.0], [75.8, 15686.0], [75.9, 15686.0], [76.0, 15715.0], [76.1, 15715.0], [76.2, 15715.0], [76.3, 15715.0], [76.4, 15715.0], [76.5, 15715.0], [76.6, 15715.0], [76.7, 15715.0], [76.8, 15715.0], [76.9, 15715.0], [77.0, 15763.0], [77.1, 15763.0], [77.2, 15763.0], [77.3, 15763.0], [77.4, 15763.0], [77.5, 15763.0], [77.6, 15763.0], [77.7, 15763.0], [77.8, 15763.0], [77.9, 15763.0], [78.0, 15815.0], [78.1, 15815.0], [78.2, 15815.0], [78.3, 15815.0], [78.4, 15815.0], [78.5, 15815.0], [78.6, 15815.0], [78.7, 15815.0], [78.8, 15815.0], [78.9, 15815.0], [79.0, 15854.0], [79.1, 15854.0], [79.2, 15854.0], [79.3, 15854.0], [79.4, 15854.0], [79.5, 15854.0], [79.6, 15854.0], [79.7, 15854.0], [79.8, 15854.0], [79.9, 15854.0], [80.0, 16528.0], [80.1, 16528.0], [80.2, 16528.0], [80.3, 16528.0], [80.4, 16528.0], [80.5, 16528.0], [80.6, 16528.0], [80.7, 16528.0], [80.8, 16528.0], [80.9, 16528.0], [81.0, 16615.0], [81.1, 16615.0], [81.2, 16615.0], [81.3, 16615.0], [81.4, 16615.0], [81.5, 16615.0], [81.6, 16615.0], [81.7, 16615.0], [81.8, 16615.0], [81.9, 16615.0], [82.0, 17118.0], [82.1, 17118.0], [82.2, 17118.0], [82.3, 17118.0], [82.4, 17118.0], [82.5, 17118.0], [82.6, 17118.0], [82.7, 17118.0], [82.8, 17118.0], [82.9, 17118.0], [83.0, 17404.0], [83.1, 17404.0], [83.2, 17404.0], [83.3, 17404.0], [83.4, 17404.0], [83.5, 17404.0], [83.6, 17404.0], [83.7, 17404.0], [83.8, 17404.0], [83.9, 17404.0], [84.0, 17469.0], [84.1, 17469.0], [84.2, 17469.0], [84.3, 17469.0], [84.4, 17469.0], [84.5, 17469.0], [84.6, 17469.0], [84.7, 17469.0], [84.8, 17469.0], [84.9, 17469.0], [85.0, 17557.0], [85.1, 17557.0], [85.2, 17557.0], [85.3, 17557.0], [85.4, 17557.0], [85.5, 17557.0], [85.6, 17557.0], [85.7, 17557.0], [85.8, 17557.0], [85.9, 17557.0], [86.0, 17702.0], [86.1, 17702.0], [86.2, 17702.0], [86.3, 17702.0], [86.4, 17702.0], [86.5, 17702.0], [86.6, 17702.0], [86.7, 17702.0], [86.8, 17702.0], [86.9, 17702.0], [87.0, 18319.0], [87.1, 18319.0], [87.2, 18319.0], [87.3, 18319.0], [87.4, 18319.0], [87.5, 18319.0], [87.6, 18319.0], [87.7, 18319.0], [87.8, 18319.0], [87.9, 18319.0], [88.0, 18897.0], [88.1, 18897.0], [88.2, 18897.0], [88.3, 18897.0], [88.4, 18897.0], [88.5, 18897.0], [88.6, 18897.0], [88.7, 18897.0], [88.8, 18897.0], [88.9, 18897.0], [89.0, 18902.0], [89.1, 18902.0], [89.2, 18902.0], [89.3, 18902.0], [89.4, 18902.0], [89.5, 18902.0], [89.6, 18902.0], [89.7, 18902.0], [89.8, 18902.0], [89.9, 18902.0], [90.0, 18939.0], [90.1, 18939.0], [90.2, 18939.0], [90.3, 18939.0], [90.4, 18939.0], [90.5, 18939.0], [90.6, 18939.0], [90.7, 18939.0], [90.8, 18939.0], [90.9, 18939.0], [91.0, 18986.0], [91.1, 18986.0], [91.2, 18986.0], [91.3, 18986.0], [91.4, 18986.0], [91.5, 18986.0], [91.6, 18986.0], [91.7, 18986.0], [91.8, 18986.0], [91.9, 18986.0], [92.0, 19077.0], [92.1, 19077.0], [92.2, 19077.0], [92.3, 19077.0], [92.4, 19077.0], [92.5, 19077.0], [92.6, 19077.0], [92.7, 19077.0], [92.8, 19077.0], [92.9, 19077.0], [93.0, 19218.0], [93.1, 19218.0], [93.2, 19218.0], [93.3, 19218.0], [93.4, 19218.0], [93.5, 19218.0], [93.6, 19218.0], [93.7, 19218.0], [93.8, 19218.0], [93.9, 19218.0], [94.0, 19350.0], [94.1, 19350.0], [94.2, 19350.0], [94.3, 19350.0], [94.4, 19350.0], [94.5, 19350.0], [94.6, 19350.0], [94.7, 19350.0], [94.8, 19350.0], [94.9, 19350.0], [95.0, 19571.0], [95.1, 19571.0], [95.2, 19571.0], [95.3, 19571.0], [95.4, 19571.0], [95.5, 19571.0], [95.6, 19571.0], [95.7, 19571.0], [95.8, 19571.0], [95.9, 19571.0], [96.0, 19636.0], [96.1, 19636.0], [96.2, 19636.0], [96.3, 19636.0], [96.4, 19636.0], [96.5, 19636.0], [96.6, 19636.0], [96.7, 19636.0], [96.8, 19636.0], [96.9, 19636.0], [97.0, 19668.0], [97.1, 19668.0], [97.2, 19668.0], [97.3, 19668.0], [97.4, 19668.0], [97.5, 19668.0], [97.6, 19668.0], [97.7, 19668.0], [97.8, 19668.0], [97.9, 19668.0], [98.0, 19788.0], [98.1, 19788.0], [98.2, 19788.0], [98.3, 19788.0], [98.4, 19788.0], [98.5, 19788.0], [98.6, 19788.0], [98.7, 19788.0], [98.8, 19788.0], [98.9, 19788.0], [99.0, 19818.0], [99.1, 19818.0], [99.2, 19818.0], [99.3, 19818.0], [99.4, 19818.0], [99.5, 19818.0], [99.6, 19818.0], [99.7, 19818.0], [99.8, 19818.0], [99.9, 19818.0]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 1900.0, "maxY": 4.0, "series": [{"data": [[1900.0, 1.0], [2100.0, 1.0], [2600.0, 2.0], [2700.0, 1.0], [3000.0, 1.0], [3100.0, 1.0], [3700.0, 2.0], [3800.0, 1.0], [4000.0, 2.0], [4300.0, 1.0], [4400.0, 3.0], [4500.0, 1.0], [4700.0, 2.0], [4900.0, 2.0], [5000.0, 1.0], [5700.0, 1.0], [6500.0, 2.0], [6700.0, 4.0], [7200.0, 1.0], [7700.0, 1.0], [7900.0, 2.0], [8000.0, 1.0], [8100.0, 1.0], [8600.0, 1.0], [8700.0, 2.0], [8800.0, 1.0], [8900.0, 1.0], [9200.0, 1.0], [9500.0, 1.0], [9800.0, 1.0], [10200.0, 1.0], [10300.0, 2.0], [10500.0, 1.0], [10900.0, 2.0], [11100.0, 1.0], [11300.0, 1.0], [11900.0, 3.0], [12000.0, 1.0], [12100.0, 1.0], [12200.0, 1.0], [12600.0, 1.0], [12800.0, 1.0], [12900.0, 2.0], [13000.0, 2.0], [13400.0, 1.0], [13700.0, 1.0], [14100.0, 2.0], [14200.0, 1.0], [14300.0, 2.0], [14400.0, 1.0], [14500.0, 1.0], [14700.0, 1.0], [15100.0, 1.0], [15600.0, 2.0], [15700.0, 2.0], [15800.0, 2.0], [16500.0, 1.0], [16600.0, 1.0], [17100.0, 1.0], [17400.0, 2.0], [17500.0, 1.0], [17700.0, 1.0], [18300.0, 1.0], [18800.0, 1.0], [18900.0, 3.0], [19000.0, 1.0], [19200.0, 1.0], [19300.0, 1.0], [19500.0, 1.0], [19600.0, 2.0], [19800.0, 1.0], [19700.0, 1.0]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 19800.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 100.0, "minX": 2.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 100.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 100.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 50.12, "minX": 1.62292326E12, "maxY": 50.12, "series": [{"data": [[1.62292326E12, 50.12]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292326E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 2199.0, "minX": 1.0, "maxY": 19818.0, "series": [{"data": [[2.0, 19818.0], [3.0, 19668.0], [4.0, 19636.0], [5.0, 19571.0], [6.0, 19350.0], [7.0, 19218.0], [8.0, 19077.0], [9.0, 18986.0], [10.0, 18939.0], [11.0, 18902.0], [12.0, 18897.0], [13.0, 18319.0], [14.0, 17702.0], [15.0, 17557.0], [16.0, 17469.0], [17.0, 17404.0], [18.0, 17118.0], [19.0, 16615.0], [20.0, 16528.0], [21.0, 15854.0], [22.0, 15815.0], [23.0, 15763.0], [24.0, 15715.0], [25.0, 15686.0], [26.0, 15658.0], [27.0, 15127.0], [28.0, 14714.0], [29.0, 14593.0], [30.0, 14363.0], [31.0, 14351.0], [33.0, 14179.0], [32.0, 14221.0], [35.0, 13759.0], [34.0, 14149.0], [37.0, 13067.0], [36.0, 14498.0], [39.0, 12951.0], [38.0, 13028.0], [41.0, 12880.0], [40.0, 12905.0], [43.0, 13482.0], [42.0, 12276.0], [45.0, 12688.0], [44.0, 12156.0], [47.0, 11994.0], [46.0, 12012.0], [49.0, 11986.0], [48.0, 11983.0], [51.0, 11148.0], [50.0, 11304.0], [53.0, 10926.0], [52.0, 10966.0], [55.0, 10308.0], [54.0, 10576.0], [57.0, 10293.0], [56.0, 10300.0], [59.0, 9543.0], [58.0, 9839.0], [60.0, 5637.5], [61.0, 8917.0], [63.0, 8728.0], [62.0, 8824.0], [67.0, 8082.0], [66.0, 8166.0], [65.0, 8621.0], [64.0, 8716.0], [71.0, 7258.0], [70.0, 7705.0], [69.0, 7938.0], [68.0, 7966.0], [75.0, 6755.5], [73.0, 6752.0], [72.0, 6763.0], [79.0, 5024.0], [78.0, 5751.0], [77.0, 6547.0], [76.0, 6595.0], [83.0, 4717.0], [82.0, 4798.0], [81.0, 4917.0], [80.0, 4964.0], [87.0, 4463.5], [85.0, 4483.0], [84.0, 4544.0], [91.0, 3809.0], [90.0, 4025.0], [89.0, 4058.0], [88.0, 4361.0], [95.0, 3073.0], [94.0, 3100.0], [93.0, 3700.0], [92.0, 3704.0], [99.0, 2199.0], [98.0, 2684.0], [97.0, 2698.0], [96.0, 2761.0], [1.0, 19788.0]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}, {"data": [[50.12, 11082.790000000003]], "isOverall": false, "label": "HTTP Request create blog page-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 99.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 828.3333333333334, "minX": 1.62292326E12, "maxY": 4434.333333333333, "series": [{"data": [[1.62292326E12, 4434.333333333333]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62292326E12, 828.3333333333334]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292326E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 11082.790000000003, "minX": 1.62292326E12, "maxY": 11082.790000000003, "series": [{"data": [[1.62292326E12, 11082.790000000003]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292326E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 11082.739999999998, "minX": 1.62292326E12, "maxY": 11082.739999999998, "series": [{"data": [[1.62292326E12, 11082.739999999998]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292326E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 2.3300000000000005, "minX": 1.62292326E12, "maxY": 2.3300000000000005, "series": [{"data": [[1.62292326E12, 2.3300000000000005]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292326E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 1991.0, "minX": 1.62292326E12, "maxY": 19818.0, "series": [{"data": [[1.62292326E12, 19818.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62292326E12, 1991.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62292326E12, 18935.3]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62292326E12, 19817.7]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62292326E12, 19559.949999999997]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292326E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 4472.0, "minX": 1.0, "maxY": 19284.0, "series": [{"data": [[1.0, 10155.0], [4.0, 11990.0], [2.0, 14608.0], [9.0, 4472.0], [5.0, 10293.0], [3.0, 5387.5], [6.0, 8776.0], [12.0, 19284.0], [7.0, 14920.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 12.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 4472.0, "minX": 1.0, "maxY": 19284.0, "series": [{"data": [[1.0, 10155.0], [4.0, 11990.0], [2.0, 14607.5], [9.0, 4472.0], [5.0, 10293.0], [3.0, 5387.5], [6.0, 8776.0], [12.0, 19284.0], [7.0, 14920.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 12.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62292326E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62292326E12, 1.6666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292326E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62292326E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62292326E12, 1.6666666666666667]], "isOverall": false, "label": "201", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292326E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62292326E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62292326E12, 1.6666666666666667]], "isOverall": false, "label": "HTTP Request create blog page-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292326E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62292326E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62292326E12, 1.6666666666666667]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292326E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

