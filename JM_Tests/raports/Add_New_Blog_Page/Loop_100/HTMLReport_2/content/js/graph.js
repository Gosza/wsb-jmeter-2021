/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 1349.0, "minX": 0.0, "maxY": 7599.0, "series": [{"data": [[0.0, 1349.0], [0.1, 1349.0], [0.2, 1349.0], [0.3, 1349.0], [0.4, 1349.0], [0.5, 1349.0], [0.6, 1349.0], [0.7, 1349.0], [0.8, 1349.0], [0.9, 1349.0], [1.0, 1620.0], [1.1, 1620.0], [1.2, 1620.0], [1.3, 1620.0], [1.4, 1620.0], [1.5, 1620.0], [1.6, 1620.0], [1.7, 1620.0], [1.8, 1620.0], [1.9, 1620.0], [2.0, 1816.0], [2.1, 1816.0], [2.2, 1816.0], [2.3, 1816.0], [2.4, 1816.0], [2.5, 1816.0], [2.6, 1816.0], [2.7, 1816.0], [2.8, 1816.0], [2.9, 1816.0], [3.0, 1817.0], [3.1, 1817.0], [3.2, 1817.0], [3.3, 1817.0], [3.4, 1817.0], [3.5, 1817.0], [3.6, 1817.0], [3.7, 1817.0], [3.8, 1817.0], [3.9, 1817.0], [4.0, 2210.0], [4.1, 2210.0], [4.2, 2210.0], [4.3, 2210.0], [4.4, 2210.0], [4.5, 2210.0], [4.6, 2210.0], [4.7, 2210.0], [4.8, 2210.0], [4.9, 2210.0], [5.0, 2237.0], [5.1, 2237.0], [5.2, 2237.0], [5.3, 2237.0], [5.4, 2237.0], [5.5, 2237.0], [5.6, 2237.0], [5.7, 2237.0], [5.8, 2237.0], [5.9, 2237.0], [6.0, 2271.0], [6.1, 2271.0], [6.2, 2271.0], [6.3, 2271.0], [6.4, 2271.0], [6.5, 2271.0], [6.6, 2271.0], [6.7, 2271.0], [6.8, 2271.0], [6.9, 2271.0], [7.0, 2497.0], [7.1, 2497.0], [7.2, 2497.0], [7.3, 2497.0], [7.4, 2497.0], [7.5, 2497.0], [7.6, 2497.0], [7.7, 2497.0], [7.8, 2497.0], [7.9, 2497.0], [8.0, 2522.0], [8.1, 2522.0], [8.2, 2522.0], [8.3, 2522.0], [8.4, 2522.0], [8.5, 2522.0], [8.6, 2522.0], [8.7, 2522.0], [8.8, 2522.0], [8.9, 2522.0], [9.0, 2533.0], [9.1, 2533.0], [9.2, 2533.0], [9.3, 2533.0], [9.4, 2533.0], [9.5, 2533.0], [9.6, 2533.0], [9.7, 2533.0], [9.8, 2533.0], [9.9, 2533.0], [10.0, 2842.0], [10.1, 2842.0], [10.2, 2842.0], [10.3, 2842.0], [10.4, 2842.0], [10.5, 2842.0], [10.6, 2842.0], [10.7, 2842.0], [10.8, 2842.0], [10.9, 2842.0], [11.0, 2919.0], [11.1, 2919.0], [11.2, 2919.0], [11.3, 2919.0], [11.4, 2919.0], [11.5, 2919.0], [11.6, 2919.0], [11.7, 2919.0], [11.8, 2919.0], [11.9, 2919.0], [12.0, 2924.0], [12.1, 2924.0], [12.2, 2924.0], [12.3, 2924.0], [12.4, 2924.0], [12.5, 2924.0], [12.6, 2924.0], [12.7, 2924.0], [12.8, 2924.0], [12.9, 2924.0], [13.0, 2927.0], [13.1, 2927.0], [13.2, 2927.0], [13.3, 2927.0], [13.4, 2927.0], [13.5, 2927.0], [13.6, 2927.0], [13.7, 2927.0], [13.8, 2927.0], [13.9, 2927.0], [14.0, 2928.0], [14.1, 2928.0], [14.2, 2928.0], [14.3, 2928.0], [14.4, 2928.0], [14.5, 2928.0], [14.6, 2928.0], [14.7, 2928.0], [14.8, 2928.0], [14.9, 2928.0], [15.0, 2930.0], [15.1, 2930.0], [15.2, 2930.0], [15.3, 2930.0], [15.4, 2930.0], [15.5, 2930.0], [15.6, 2930.0], [15.7, 2930.0], [15.8, 2930.0], [15.9, 2930.0], [16.0, 2933.0], [16.1, 2933.0], [16.2, 2933.0], [16.3, 2933.0], [16.4, 2933.0], [16.5, 2933.0], [16.6, 2933.0], [16.7, 2933.0], [16.8, 2933.0], [16.9, 2933.0], [17.0, 2964.0], [17.1, 2964.0], [17.2, 2964.0], [17.3, 2964.0], [17.4, 2964.0], [17.5, 2964.0], [17.6, 2964.0], [17.7, 2964.0], [17.8, 2964.0], [17.9, 2964.0], [18.0, 3107.0], [18.1, 3107.0], [18.2, 3107.0], [18.3, 3107.0], [18.4, 3107.0], [18.5, 3107.0], [18.6, 3107.0], [18.7, 3107.0], [18.8, 3107.0], [18.9, 3107.0], [19.0, 3464.0], [19.1, 3464.0], [19.2, 3464.0], [19.3, 3464.0], [19.4, 3464.0], [19.5, 3464.0], [19.6, 3464.0], [19.7, 3464.0], [19.8, 3464.0], [19.9, 3464.0], [20.0, 3701.0], [20.1, 3701.0], [20.2, 3701.0], [20.3, 3701.0], [20.4, 3701.0], [20.5, 3701.0], [20.6, 3701.0], [20.7, 3701.0], [20.8, 3701.0], [20.9, 3701.0], [21.0, 3719.0], [21.1, 3719.0], [21.2, 3719.0], [21.3, 3719.0], [21.4, 3719.0], [21.5, 3719.0], [21.6, 3719.0], [21.7, 3719.0], [21.8, 3719.0], [21.9, 3719.0], [22.0, 3808.0], [22.1, 3808.0], [22.2, 3808.0], [22.3, 3808.0], [22.4, 3808.0], [22.5, 3808.0], [22.6, 3808.0], [22.7, 3808.0], [22.8, 3808.0], [22.9, 3808.0], [23.0, 3856.0], [23.1, 3856.0], [23.2, 3856.0], [23.3, 3856.0], [23.4, 3856.0], [23.5, 3856.0], [23.6, 3856.0], [23.7, 3856.0], [23.8, 3856.0], [23.9, 3856.0], [24.0, 3875.0], [24.1, 3875.0], [24.2, 3875.0], [24.3, 3875.0], [24.4, 3875.0], [24.5, 3875.0], [24.6, 3875.0], [24.7, 3875.0], [24.8, 3875.0], [24.9, 3875.0], [25.0, 3979.0], [25.1, 3979.0], [25.2, 3979.0], [25.3, 3979.0], [25.4, 3979.0], [25.5, 3979.0], [25.6, 3979.0], [25.7, 3979.0], [25.8, 3979.0], [25.9, 3979.0], [26.0, 4042.0], [26.1, 4042.0], [26.2, 4042.0], [26.3, 4042.0], [26.4, 4042.0], [26.5, 4042.0], [26.6, 4042.0], [26.7, 4042.0], [26.8, 4042.0], [26.9, 4042.0], [27.0, 4047.0], [27.1, 4047.0], [27.2, 4047.0], [27.3, 4047.0], [27.4, 4047.0], [27.5, 4047.0], [27.6, 4047.0], [27.7, 4047.0], [27.8, 4047.0], [27.9, 4047.0], [28.0, 4112.0], [28.1, 4112.0], [28.2, 4112.0], [28.3, 4112.0], [28.4, 4112.0], [28.5, 4112.0], [28.6, 4112.0], [28.7, 4112.0], [28.8, 4112.0], [28.9, 4112.0], [29.0, 4127.0], [29.1, 4127.0], [29.2, 4127.0], [29.3, 4127.0], [29.4, 4127.0], [29.5, 4127.0], [29.6, 4127.0], [29.7, 4127.0], [29.8, 4127.0], [29.9, 4127.0], [30.0, 4143.0], [30.1, 4143.0], [30.2, 4143.0], [30.3, 4143.0], [30.4, 4143.0], [30.5, 4143.0], [30.6, 4143.0], [30.7, 4143.0], [30.8, 4143.0], [30.9, 4143.0], [31.0, 4159.0], [31.1, 4159.0], [31.2, 4159.0], [31.3, 4159.0], [31.4, 4159.0], [31.5, 4159.0], [31.6, 4159.0], [31.7, 4159.0], [31.8, 4159.0], [31.9, 4159.0], [32.0, 4218.0], [32.1, 4218.0], [32.2, 4218.0], [32.3, 4218.0], [32.4, 4218.0], [32.5, 4218.0], [32.6, 4218.0], [32.7, 4218.0], [32.8, 4218.0], [32.9, 4218.0], [33.0, 4229.0], [33.1, 4229.0], [33.2, 4229.0], [33.3, 4229.0], [33.4, 4229.0], [33.5, 4229.0], [33.6, 4229.0], [33.7, 4229.0], [33.8, 4229.0], [33.9, 4229.0], [34.0, 4250.0], [34.1, 4250.0], [34.2, 4250.0], [34.3, 4250.0], [34.4, 4250.0], [34.5, 4250.0], [34.6, 4250.0], [34.7, 4250.0], [34.8, 4250.0], [34.9, 4250.0], [35.0, 4255.0], [35.1, 4255.0], [35.2, 4255.0], [35.3, 4255.0], [35.4, 4255.0], [35.5, 4255.0], [35.6, 4255.0], [35.7, 4255.0], [35.8, 4255.0], [35.9, 4255.0], [36.0, 4330.0], [36.1, 4330.0], [36.2, 4330.0], [36.3, 4330.0], [36.4, 4330.0], [36.5, 4330.0], [36.6, 4330.0], [36.7, 4330.0], [36.8, 4330.0], [36.9, 4330.0], [37.0, 4338.0], [37.1, 4338.0], [37.2, 4338.0], [37.3, 4338.0], [37.4, 4338.0], [37.5, 4338.0], [37.6, 4338.0], [37.7, 4338.0], [37.8, 4338.0], [37.9, 4338.0], [38.0, 4371.0], [38.1, 4371.0], [38.2, 4371.0], [38.3, 4371.0], [38.4, 4371.0], [38.5, 4371.0], [38.6, 4371.0], [38.7, 4371.0], [38.8, 4371.0], [38.9, 4371.0], [39.0, 4378.0], [39.1, 4378.0], [39.2, 4378.0], [39.3, 4378.0], [39.4, 4378.0], [39.5, 4378.0], [39.6, 4378.0], [39.7, 4378.0], [39.8, 4378.0], [39.9, 4378.0], [40.0, 4409.0], [40.1, 4409.0], [40.2, 4409.0], [40.3, 4409.0], [40.4, 4409.0], [40.5, 4409.0], [40.6, 4409.0], [40.7, 4409.0], [40.8, 4409.0], [40.9, 4409.0], [41.0, 4414.0], [41.1, 4414.0], [41.2, 4414.0], [41.3, 4414.0], [41.4, 4414.0], [41.5, 4414.0], [41.6, 4414.0], [41.7, 4414.0], [41.8, 4414.0], [41.9, 4414.0], [42.0, 4433.0], [42.1, 4433.0], [42.2, 4433.0], [42.3, 4433.0], [42.4, 4433.0], [42.5, 4433.0], [42.6, 4433.0], [42.7, 4433.0], [42.8, 4433.0], [42.9, 4433.0], [43.0, 4532.0], [43.1, 4532.0], [43.2, 4532.0], [43.3, 4532.0], [43.4, 4532.0], [43.5, 4532.0], [43.6, 4532.0], [43.7, 4532.0], [43.8, 4532.0], [43.9, 4532.0], [44.0, 4607.0], [44.1, 4607.0], [44.2, 4607.0], [44.3, 4607.0], [44.4, 4607.0], [44.5, 4607.0], [44.6, 4607.0], [44.7, 4607.0], [44.8, 4607.0], [44.9, 4607.0], [45.0, 4631.0], [45.1, 4631.0], [45.2, 4631.0], [45.3, 4631.0], [45.4, 4631.0], [45.5, 4631.0], [45.6, 4631.0], [45.7, 4631.0], [45.8, 4631.0], [45.9, 4631.0], [46.0, 4696.0], [46.1, 4696.0], [46.2, 4696.0], [46.3, 4696.0], [46.4, 4696.0], [46.5, 4696.0], [46.6, 4696.0], [46.7, 4696.0], [46.8, 4696.0], [46.9, 4696.0], [47.0, 4708.0], [47.1, 4708.0], [47.2, 4708.0], [47.3, 4708.0], [47.4, 4708.0], [47.5, 4708.0], [47.6, 4708.0], [47.7, 4708.0], [47.8, 4708.0], [47.9, 4708.0], [48.0, 4713.0], [48.1, 4713.0], [48.2, 4713.0], [48.3, 4713.0], [48.4, 4713.0], [48.5, 4713.0], [48.6, 4713.0], [48.7, 4713.0], [48.8, 4713.0], [48.9, 4713.0], [49.0, 4827.0], [49.1, 4827.0], [49.2, 4827.0], [49.3, 4827.0], [49.4, 4827.0], [49.5, 4827.0], [49.6, 4827.0], [49.7, 4827.0], [49.8, 4827.0], [49.9, 4827.0], [50.0, 4854.0], [50.1, 4854.0], [50.2, 4854.0], [50.3, 4854.0], [50.4, 4854.0], [50.5, 4854.0], [50.6, 4854.0], [50.7, 4854.0], [50.8, 4854.0], [50.9, 4854.0], [51.0, 4873.0], [51.1, 4873.0], [51.2, 4873.0], [51.3, 4873.0], [51.4, 4873.0], [51.5, 4873.0], [51.6, 4873.0], [51.7, 4873.0], [51.8, 4873.0], [51.9, 4873.0], [52.0, 4875.0], [52.1, 4875.0], [52.2, 4875.0], [52.3, 4875.0], [52.4, 4875.0], [52.5, 4875.0], [52.6, 4875.0], [52.7, 4875.0], [52.8, 4875.0], [52.9, 4875.0], [53.0, 4978.0], [53.1, 4978.0], [53.2, 4978.0], [53.3, 4978.0], [53.4, 4978.0], [53.5, 4978.0], [53.6, 4978.0], [53.7, 4978.0], [53.8, 4978.0], [53.9, 4978.0], [54.0, 5036.0], [54.1, 5036.0], [54.2, 5036.0], [54.3, 5036.0], [54.4, 5036.0], [54.5, 5036.0], [54.6, 5036.0], [54.7, 5036.0], [54.8, 5036.0], [54.9, 5036.0], [55.0, 5089.0], [55.1, 5089.0], [55.2, 5089.0], [55.3, 5089.0], [55.4, 5089.0], [55.5, 5089.0], [55.6, 5089.0], [55.7, 5089.0], [55.8, 5089.0], [55.9, 5089.0], [56.0, 5110.0], [56.1, 5110.0], [56.2, 5110.0], [56.3, 5110.0], [56.4, 5110.0], [56.5, 5110.0], [56.6, 5110.0], [56.7, 5110.0], [56.8, 5110.0], [56.9, 5110.0], [57.0, 5147.0], [57.1, 5147.0], [57.2, 5147.0], [57.3, 5147.0], [57.4, 5147.0], [57.5, 5147.0], [57.6, 5147.0], [57.7, 5147.0], [57.8, 5147.0], [57.9, 5147.0], [58.0, 5209.0], [58.1, 5209.0], [58.2, 5209.0], [58.3, 5209.0], [58.4, 5209.0], [58.5, 5209.0], [58.6, 5209.0], [58.7, 5209.0], [58.8, 5209.0], [58.9, 5209.0], [59.0, 5213.0], [59.1, 5213.0], [59.2, 5213.0], [59.3, 5213.0], [59.4, 5213.0], [59.5, 5213.0], [59.6, 5213.0], [59.7, 5213.0], [59.8, 5213.0], [59.9, 5213.0], [60.0, 5252.0], [60.1, 5252.0], [60.2, 5252.0], [60.3, 5252.0], [60.4, 5252.0], [60.5, 5252.0], [60.6, 5252.0], [60.7, 5252.0], [60.8, 5252.0], [60.9, 5252.0], [61.0, 5296.0], [61.1, 5296.0], [61.2, 5296.0], [61.3, 5296.0], [61.4, 5296.0], [61.5, 5296.0], [61.6, 5296.0], [61.7, 5296.0], [61.8, 5296.0], [61.9, 5296.0], [62.0, 5304.0], [62.1, 5304.0], [62.2, 5304.0], [62.3, 5304.0], [62.4, 5304.0], [62.5, 5304.0], [62.6, 5304.0], [62.7, 5304.0], [62.8, 5304.0], [62.9, 5304.0], [63.0, 5308.0], [63.1, 5308.0], [63.2, 5308.0], [63.3, 5308.0], [63.4, 5308.0], [63.5, 5308.0], [63.6, 5308.0], [63.7, 5308.0], [63.8, 5308.0], [63.9, 5308.0], [64.0, 5372.0], [64.1, 5372.0], [64.2, 5372.0], [64.3, 5372.0], [64.4, 5372.0], [64.5, 5372.0], [64.6, 5372.0], [64.7, 5372.0], [64.8, 5372.0], [64.9, 5372.0], [65.0, 5383.0], [65.1, 5383.0], [65.2, 5383.0], [65.3, 5383.0], [65.4, 5383.0], [65.5, 5383.0], [65.6, 5383.0], [65.7, 5383.0], [65.8, 5383.0], [65.9, 5383.0], [66.0, 5388.0], [66.1, 5388.0], [66.2, 5388.0], [66.3, 5388.0], [66.4, 5388.0], [66.5, 5388.0], [66.6, 5388.0], [66.7, 5388.0], [66.8, 5388.0], [66.9, 5388.0], [67.0, 5413.0], [67.1, 5413.0], [67.2, 5413.0], [67.3, 5413.0], [67.4, 5413.0], [67.5, 5413.0], [67.6, 5413.0], [67.7, 5413.0], [67.8, 5413.0], [67.9, 5413.0], [68.0, 5420.0], [68.1, 5420.0], [68.2, 5420.0], [68.3, 5420.0], [68.4, 5420.0], [68.5, 5420.0], [68.6, 5420.0], [68.7, 5420.0], [68.8, 5420.0], [68.9, 5420.0], [69.0, 5438.0], [69.1, 5438.0], [69.2, 5438.0], [69.3, 5438.0], [69.4, 5438.0], [69.5, 5438.0], [69.6, 5438.0], [69.7, 5438.0], [69.8, 5438.0], [69.9, 5438.0], [70.0, 5449.0], [70.1, 5449.0], [70.2, 5449.0], [70.3, 5449.0], [70.4, 5449.0], [70.5, 5449.0], [70.6, 5449.0], [70.7, 5449.0], [70.8, 5449.0], [70.9, 5449.0], [71.0, 5465.0], [71.1, 5465.0], [71.2, 5465.0], [71.3, 5465.0], [71.4, 5465.0], [71.5, 5465.0], [71.6, 5465.0], [71.7, 5465.0], [71.8, 5465.0], [71.9, 5465.0], [72.0, 5488.0], [72.1, 5488.0], [72.2, 5488.0], [72.3, 5488.0], [72.4, 5488.0], [72.5, 5488.0], [72.6, 5488.0], [72.7, 5488.0], [72.8, 5488.0], [72.9, 5488.0], [73.0, 5503.0], [73.1, 5503.0], [73.2, 5503.0], [73.3, 5503.0], [73.4, 5503.0], [73.5, 5503.0], [73.6, 5503.0], [73.7, 5503.0], [73.8, 5503.0], [73.9, 5503.0], [74.0, 5548.0], [74.1, 5548.0], [74.2, 5548.0], [74.3, 5548.0], [74.4, 5548.0], [74.5, 5548.0], [74.6, 5548.0], [74.7, 5548.0], [74.8, 5548.0], [74.9, 5548.0], [75.0, 5591.0], [75.1, 5591.0], [75.2, 5591.0], [75.3, 5591.0], [75.4, 5591.0], [75.5, 5591.0], [75.6, 5591.0], [75.7, 5591.0], [75.8, 5591.0], [75.9, 5591.0], [76.0, 5691.0], [76.1, 5691.0], [76.2, 5691.0], [76.3, 5691.0], [76.4, 5691.0], [76.5, 5691.0], [76.6, 5691.0], [76.7, 5691.0], [76.8, 5691.0], [76.9, 5691.0], [77.0, 5760.0], [77.1, 5760.0], [77.2, 5760.0], [77.3, 5760.0], [77.4, 5760.0], [77.5, 5760.0], [77.6, 5760.0], [77.7, 5760.0], [77.8, 5760.0], [77.9, 5760.0], [78.0, 5771.0], [78.1, 5771.0], [78.2, 5771.0], [78.3, 5771.0], [78.4, 5771.0], [78.5, 5771.0], [78.6, 5771.0], [78.7, 5771.0], [78.8, 5771.0], [78.9, 5771.0], [79.0, 5783.0], [79.1, 5783.0], [79.2, 5783.0], [79.3, 5783.0], [79.4, 5783.0], [79.5, 5783.0], [79.6, 5783.0], [79.7, 5783.0], [79.8, 5783.0], [79.9, 5783.0], [80.0, 5798.0], [80.1, 5798.0], [80.2, 5798.0], [80.3, 5798.0], [80.4, 5798.0], [80.5, 5798.0], [80.6, 5798.0], [80.7, 5798.0], [80.8, 5798.0], [80.9, 5798.0], [81.0, 5828.0], [81.1, 5828.0], [81.2, 5828.0], [81.3, 5828.0], [81.4, 5828.0], [81.5, 5828.0], [81.6, 5828.0], [81.7, 5828.0], [81.8, 5828.0], [81.9, 5828.0], [82.0, 5923.0], [82.1, 5923.0], [82.2, 5923.0], [82.3, 5923.0], [82.4, 5923.0], [82.5, 5923.0], [82.6, 5923.0], [82.7, 5923.0], [82.8, 5923.0], [82.9, 5923.0], [83.0, 5928.0], [83.1, 5928.0], [83.2, 5928.0], [83.3, 5928.0], [83.4, 5928.0], [83.5, 5928.0], [83.6, 5928.0], [83.7, 5928.0], [83.8, 5928.0], [83.9, 5928.0], [84.0, 5949.0], [84.1, 5949.0], [84.2, 5949.0], [84.3, 5949.0], [84.4, 5949.0], [84.5, 5949.0], [84.6, 5949.0], [84.7, 5949.0], [84.8, 5949.0], [84.9, 5949.0], [85.0, 5999.0], [85.1, 5999.0], [85.2, 5999.0], [85.3, 5999.0], [85.4, 5999.0], [85.5, 5999.0], [85.6, 5999.0], [85.7, 5999.0], [85.8, 5999.0], [85.9, 5999.0], [86.0, 6009.0], [86.1, 6009.0], [86.2, 6009.0], [86.3, 6009.0], [86.4, 6009.0], [86.5, 6009.0], [86.6, 6009.0], [86.7, 6009.0], [86.8, 6009.0], [86.9, 6009.0], [87.0, 6044.0], [87.1, 6044.0], [87.2, 6044.0], [87.3, 6044.0], [87.4, 6044.0], [87.5, 6044.0], [87.6, 6044.0], [87.7, 6044.0], [87.8, 6044.0], [87.9, 6044.0], [88.0, 6068.0], [88.1, 6068.0], [88.2, 6068.0], [88.3, 6068.0], [88.4, 6068.0], [88.5, 6068.0], [88.6, 6068.0], [88.7, 6068.0], [88.8, 6068.0], [88.9, 6068.0], [89.0, 6108.0], [89.1, 6108.0], [89.2, 6108.0], [89.3, 6108.0], [89.4, 6108.0], [89.5, 6108.0], [89.6, 6108.0], [89.7, 6108.0], [89.8, 6108.0], [89.9, 6108.0], [90.0, 6235.0], [90.1, 6235.0], [90.2, 6235.0], [90.3, 6235.0], [90.4, 6235.0], [90.5, 6235.0], [90.6, 6235.0], [90.7, 6235.0], [90.8, 6235.0], [90.9, 6235.0], [91.0, 6300.0], [91.1, 6300.0], [91.2, 6300.0], [91.3, 6300.0], [91.4, 6300.0], [91.5, 6300.0], [91.6, 6300.0], [91.7, 6300.0], [91.8, 6300.0], [91.9, 6300.0], [92.0, 6326.0], [92.1, 6326.0], [92.2, 6326.0], [92.3, 6326.0], [92.4, 6326.0], [92.5, 6326.0], [92.6, 6326.0], [92.7, 6326.0], [92.8, 6326.0], [92.9, 6326.0], [93.0, 6338.0], [93.1, 6338.0], [93.2, 6338.0], [93.3, 6338.0], [93.4, 6338.0], [93.5, 6338.0], [93.6, 6338.0], [93.7, 6338.0], [93.8, 6338.0], [93.9, 6338.0], [94.0, 6461.0], [94.1, 6461.0], [94.2, 6461.0], [94.3, 6461.0], [94.4, 6461.0], [94.5, 6461.0], [94.6, 6461.0], [94.7, 6461.0], [94.8, 6461.0], [94.9, 6461.0], [95.0, 6600.0], [95.1, 6600.0], [95.2, 6600.0], [95.3, 6600.0], [95.4, 6600.0], [95.5, 6600.0], [95.6, 6600.0], [95.7, 6600.0], [95.8, 6600.0], [95.9, 6600.0], [96.0, 6792.0], [96.1, 6792.0], [96.2, 6792.0], [96.3, 6792.0], [96.4, 6792.0], [96.5, 6792.0], [96.6, 6792.0], [96.7, 6792.0], [96.8, 6792.0], [96.9, 6792.0], [97.0, 6930.0], [97.1, 6930.0], [97.2, 6930.0], [97.3, 6930.0], [97.4, 6930.0], [97.5, 6930.0], [97.6, 6930.0], [97.7, 6930.0], [97.8, 6930.0], [97.9, 6930.0], [98.0, 7228.0], [98.1, 7228.0], [98.2, 7228.0], [98.3, 7228.0], [98.4, 7228.0], [98.5, 7228.0], [98.6, 7228.0], [98.7, 7228.0], [98.8, 7228.0], [98.9, 7228.0], [99.0, 7599.0], [99.1, 7599.0], [99.2, 7599.0], [99.3, 7599.0], [99.4, 7599.0], [99.5, 7599.0], [99.6, 7599.0], [99.7, 7599.0], [99.8, 7599.0], [99.9, 7599.0]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 1300.0, "maxY": 7.0, "series": [{"data": [[1300.0, 1.0], [1600.0, 1.0], [1800.0, 2.0], [2200.0, 3.0], [2400.0, 1.0], [2500.0, 2.0], [2800.0, 1.0], [2900.0, 7.0], [3100.0, 1.0], [3400.0, 1.0], [3700.0, 2.0], [3800.0, 3.0], [3900.0, 1.0], [4000.0, 2.0], [4200.0, 4.0], [4100.0, 4.0], [4300.0, 4.0], [4400.0, 3.0], [4600.0, 3.0], [4500.0, 1.0], [4700.0, 2.0], [4800.0, 4.0], [5000.0, 2.0], [5100.0, 2.0], [4900.0, 1.0], [5200.0, 4.0], [5300.0, 5.0], [5400.0, 6.0], [5500.0, 3.0], [5600.0, 1.0], [5700.0, 4.0], [5800.0, 1.0], [6000.0, 3.0], [5900.0, 4.0], [6100.0, 1.0], [6300.0, 3.0], [6200.0, 1.0], [6600.0, 1.0], [6400.0, 1.0], [6900.0, 1.0], [6700.0, 1.0], [7200.0, 1.0], [7500.0, 1.0]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 7500.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 1.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 99.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 1.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 99.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 21.289999999999996, "minX": 1.62292332E12, "maxY": 21.289999999999996, "series": [{"data": [[1.62292332E12, 21.289999999999996]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292332E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 2497.0, "minX": 1.0, "maxY": 6050.0, "series": [{"data": [[2.0, 2928.0], [3.0, 2933.0], [4.0, 3107.0], [5.0, 3464.0], [6.0, 4532.0], [7.0, 2930.0], [8.0, 4143.0], [9.0, 2919.0], [10.0, 3701.0], [11.0, 4338.0], [12.0, 4708.0], [13.0, 2964.0], [14.0, 4978.0], [15.0, 5964.5], [16.0, 4873.0], [1.0, 2497.0], [17.0, 6050.0], [18.0, 4409.0], [19.0, 5923.0], [20.0, 5948.0], [21.0, 6036.0], [22.0, 4756.363636363636], [23.0, 5438.0], [24.0, 4200.0], [25.0, 4620.649122807017]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}, {"data": [[21.289999999999996, 4651.869999999998]], "isOverall": false, "label": "HTTP Request create blog page-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 25.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 828.3333333333334, "minX": 1.62292332E12, "maxY": 4435.616666666667, "series": [{"data": [[1.62292332E12, 4435.616666666667]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62292332E12, 828.3333333333334]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292332E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 4651.869999999998, "minX": 1.62292332E12, "maxY": 4651.869999999998, "series": [{"data": [[1.62292332E12, 4651.869999999998]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292332E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 4651.840000000001, "minX": 1.62292332E12, "maxY": 4651.840000000001, "series": [{"data": [[1.62292332E12, 4651.840000000001]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292332E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 1.1500000000000004, "minX": 1.62292332E12, "maxY": 1.1500000000000004, "series": [{"data": [[1.62292332E12, 1.1500000000000004]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292332E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 1349.0, "minX": 1.62292332E12, "maxY": 7599.0, "series": [{"data": [[1.62292332E12, 7599.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62292332E12, 1349.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62292332E12, 6222.300000000001]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62292332E12, 7595.289999999998]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62292332E12, 6593.049999999998]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292332E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 1817.0, "minX": 1.0, "maxY": 5798.0, "series": [{"data": [[1.0, 2210.0], [4.0, 5099.5], [2.0, 4493.5], [9.0, 5798.0], [10.0, 4194.0], [5.0, 5346.0], [11.0, 5308.0], [3.0, 1817.0], [6.0, 2687.5], [7.0, 4234.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 11.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 1817.0, "minX": 1.0, "maxY": 5797.0, "series": [{"data": [[1.0, 2210.0], [4.0, 5099.5], [2.0, 4493.5], [9.0, 5797.0], [10.0, 4194.0], [5.0, 5346.0], [11.0, 5308.0], [3.0, 1817.0], [6.0, 2687.5], [7.0, 4234.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 11.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62292332E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62292332E12, 1.6666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292332E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62292332E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62292332E12, 1.6666666666666667]], "isOverall": false, "label": "201", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292332E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62292332E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62292332E12, 1.6666666666666667]], "isOverall": false, "label": "HTTP Request create blog page-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292332E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.62292332E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.62292332E12, 1.6666666666666667]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292332E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

