/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 2018.0, "minX": 0.0, "maxY": 14346.0, "series": [{"data": [[0.0, 2018.0], [0.1, 2018.0], [0.2, 2018.0], [0.3, 2018.0], [0.4, 2018.0], [0.5, 2018.0], [0.6, 2018.0], [0.7, 2018.0], [0.8, 2018.0], [0.9, 2018.0], [1.0, 2018.0], [1.1, 2018.0], [1.2, 2018.0], [1.3, 2018.0], [1.4, 2018.0], [1.5, 2018.0], [1.6, 2018.0], [1.7, 2018.0], [1.8, 2018.0], [1.9, 2018.0], [2.0, 2581.0], [2.1, 2581.0], [2.2, 2581.0], [2.3, 2581.0], [2.4, 2581.0], [2.5, 2581.0], [2.6, 2581.0], [2.7, 2581.0], [2.8, 2581.0], [2.9, 2581.0], [3.0, 2581.0], [3.1, 2581.0], [3.2, 2581.0], [3.3, 2581.0], [3.4, 2581.0], [3.5, 2581.0], [3.6, 2581.0], [3.7, 2581.0], [3.8, 2581.0], [3.9, 2581.0], [4.0, 2631.0], [4.1, 2631.0], [4.2, 2631.0], [4.3, 2631.0], [4.4, 2631.0], [4.5, 2631.0], [4.6, 2631.0], [4.7, 2631.0], [4.8, 2631.0], [4.9, 2631.0], [5.0, 2631.0], [5.1, 2631.0], [5.2, 2631.0], [5.3, 2631.0], [5.4, 2631.0], [5.5, 2631.0], [5.6, 2631.0], [5.7, 2631.0], [5.8, 2631.0], [5.9, 2631.0], [6.0, 2884.0], [6.1, 2884.0], [6.2, 2884.0], [6.3, 2884.0], [6.4, 2884.0], [6.5, 2884.0], [6.6, 2884.0], [6.7, 2884.0], [6.8, 2884.0], [6.9, 2884.0], [7.0, 2884.0], [7.1, 2884.0], [7.2, 2884.0], [7.3, 2884.0], [7.4, 2884.0], [7.5, 2884.0], [7.6, 2884.0], [7.7, 2884.0], [7.8, 2884.0], [7.9, 2884.0], [8.0, 3702.0], [8.1, 3702.0], [8.2, 3702.0], [8.3, 3702.0], [8.4, 3702.0], [8.5, 3702.0], [8.6, 3702.0], [8.7, 3702.0], [8.8, 3702.0], [8.9, 3702.0], [9.0, 3702.0], [9.1, 3702.0], [9.2, 3702.0], [9.3, 3702.0], [9.4, 3702.0], [9.5, 3702.0], [9.6, 3702.0], [9.7, 3702.0], [9.8, 3702.0], [9.9, 3702.0], [10.0, 4436.0], [10.1, 4436.0], [10.2, 4436.0], [10.3, 4436.0], [10.4, 4436.0], [10.5, 4436.0], [10.6, 4436.0], [10.7, 4436.0], [10.8, 4436.0], [10.9, 4436.0], [11.0, 4436.0], [11.1, 4436.0], [11.2, 4436.0], [11.3, 4436.0], [11.4, 4436.0], [11.5, 4436.0], [11.6, 4436.0], [11.7, 4436.0], [11.8, 4436.0], [11.9, 4436.0], [12.0, 4973.0], [12.1, 4973.0], [12.2, 4973.0], [12.3, 4973.0], [12.4, 4973.0], [12.5, 4973.0], [12.6, 4973.0], [12.7, 4973.0], [12.8, 4973.0], [12.9, 4973.0], [13.0, 4973.0], [13.1, 4973.0], [13.2, 4973.0], [13.3, 4973.0], [13.4, 4973.0], [13.5, 4973.0], [13.6, 4973.0], [13.7, 4973.0], [13.8, 4973.0], [13.9, 4973.0], [14.0, 5347.0], [14.1, 5347.0], [14.2, 5347.0], [14.3, 5347.0], [14.4, 5347.0], [14.5, 5347.0], [14.6, 5347.0], [14.7, 5347.0], [14.8, 5347.0], [14.9, 5347.0], [15.0, 5347.0], [15.1, 5347.0], [15.2, 5347.0], [15.3, 5347.0], [15.4, 5347.0], [15.5, 5347.0], [15.6, 5347.0], [15.7, 5347.0], [15.8, 5347.0], [15.9, 5347.0], [16.0, 5621.0], [16.1, 5621.0], [16.2, 5621.0], [16.3, 5621.0], [16.4, 5621.0], [16.5, 5621.0], [16.6, 5621.0], [16.7, 5621.0], [16.8, 5621.0], [16.9, 5621.0], [17.0, 5621.0], [17.1, 5621.0], [17.2, 5621.0], [17.3, 5621.0], [17.4, 5621.0], [17.5, 5621.0], [17.6, 5621.0], [17.7, 5621.0], [17.8, 5621.0], [17.9, 5621.0], [18.0, 5701.0], [18.1, 5701.0], [18.2, 5701.0], [18.3, 5701.0], [18.4, 5701.0], [18.5, 5701.0], [18.6, 5701.0], [18.7, 5701.0], [18.8, 5701.0], [18.9, 5701.0], [19.0, 5701.0], [19.1, 5701.0], [19.2, 5701.0], [19.3, 5701.0], [19.4, 5701.0], [19.5, 5701.0], [19.6, 5701.0], [19.7, 5701.0], [19.8, 5701.0], [19.9, 5701.0], [20.0, 5750.0], [20.1, 5750.0], [20.2, 5750.0], [20.3, 5750.0], [20.4, 5750.0], [20.5, 5750.0], [20.6, 5750.0], [20.7, 5750.0], [20.8, 5750.0], [20.9, 5750.0], [21.0, 5750.0], [21.1, 5750.0], [21.2, 5750.0], [21.3, 5750.0], [21.4, 5750.0], [21.5, 5750.0], [21.6, 5750.0], [21.7, 5750.0], [21.8, 5750.0], [21.9, 5750.0], [22.0, 6377.0], [22.1, 6377.0], [22.2, 6377.0], [22.3, 6377.0], [22.4, 6377.0], [22.5, 6377.0], [22.6, 6377.0], [22.7, 6377.0], [22.8, 6377.0], [22.9, 6377.0], [23.0, 6377.0], [23.1, 6377.0], [23.2, 6377.0], [23.3, 6377.0], [23.4, 6377.0], [23.5, 6377.0], [23.6, 6377.0], [23.7, 6377.0], [23.8, 6377.0], [23.9, 6377.0], [24.0, 6387.0], [24.1, 6387.0], [24.2, 6387.0], [24.3, 6387.0], [24.4, 6387.0], [24.5, 6387.0], [24.6, 6387.0], [24.7, 6387.0], [24.8, 6387.0], [24.9, 6387.0], [25.0, 6387.0], [25.1, 6387.0], [25.2, 6387.0], [25.3, 6387.0], [25.4, 6387.0], [25.5, 6387.0], [25.6, 6387.0], [25.7, 6387.0], [25.8, 6387.0], [25.9, 6387.0], [26.0, 6405.0], [26.1, 6405.0], [26.2, 6405.0], [26.3, 6405.0], [26.4, 6405.0], [26.5, 6405.0], [26.6, 6405.0], [26.7, 6405.0], [26.8, 6405.0], [26.9, 6405.0], [27.0, 6405.0], [27.1, 6405.0], [27.2, 6405.0], [27.3, 6405.0], [27.4, 6405.0], [27.5, 6405.0], [27.6, 6405.0], [27.7, 6405.0], [27.8, 6405.0], [27.9, 6405.0], [28.0, 6448.0], [28.1, 6448.0], [28.2, 6448.0], [28.3, 6448.0], [28.4, 6448.0], [28.5, 6448.0], [28.6, 6448.0], [28.7, 6448.0], [28.8, 6448.0], [28.9, 6448.0], [29.0, 6448.0], [29.1, 6448.0], [29.2, 6448.0], [29.3, 6448.0], [29.4, 6448.0], [29.5, 6448.0], [29.6, 6448.0], [29.7, 6448.0], [29.8, 6448.0], [29.9, 6448.0], [30.0, 6656.0], [30.1, 6656.0], [30.2, 6656.0], [30.3, 6656.0], [30.4, 6656.0], [30.5, 6656.0], [30.6, 6656.0], [30.7, 6656.0], [30.8, 6656.0], [30.9, 6656.0], [31.0, 6656.0], [31.1, 6656.0], [31.2, 6656.0], [31.3, 6656.0], [31.4, 6656.0], [31.5, 6656.0], [31.6, 6656.0], [31.7, 6656.0], [31.8, 6656.0], [31.9, 6656.0], [32.0, 6721.0], [32.1, 6721.0], [32.2, 6721.0], [32.3, 6721.0], [32.4, 6721.0], [32.5, 6721.0], [32.6, 6721.0], [32.7, 6721.0], [32.8, 6721.0], [32.9, 6721.0], [33.0, 6721.0], [33.1, 6721.0], [33.2, 6721.0], [33.3, 6721.0], [33.4, 6721.0], [33.5, 6721.0], [33.6, 6721.0], [33.7, 6721.0], [33.8, 6721.0], [33.9, 6721.0], [34.0, 6871.0], [34.1, 6871.0], [34.2, 6871.0], [34.3, 6871.0], [34.4, 6871.0], [34.5, 6871.0], [34.6, 6871.0], [34.7, 6871.0], [34.8, 6871.0], [34.9, 6871.0], [35.0, 6871.0], [35.1, 6871.0], [35.2, 6871.0], [35.3, 6871.0], [35.4, 6871.0], [35.5, 6871.0], [35.6, 6871.0], [35.7, 6871.0], [35.8, 6871.0], [35.9, 6871.0], [36.0, 7031.0], [36.1, 7031.0], [36.2, 7031.0], [36.3, 7031.0], [36.4, 7031.0], [36.5, 7031.0], [36.6, 7031.0], [36.7, 7031.0], [36.8, 7031.0], [36.9, 7031.0], [37.0, 7031.0], [37.1, 7031.0], [37.2, 7031.0], [37.3, 7031.0], [37.4, 7031.0], [37.5, 7031.0], [37.6, 7031.0], [37.7, 7031.0], [37.8, 7031.0], [37.9, 7031.0], [38.0, 7374.0], [38.1, 7374.0], [38.2, 7374.0], [38.3, 7374.0], [38.4, 7374.0], [38.5, 7374.0], [38.6, 7374.0], [38.7, 7374.0], [38.8, 7374.0], [38.9, 7374.0], [39.0, 7374.0], [39.1, 7374.0], [39.2, 7374.0], [39.3, 7374.0], [39.4, 7374.0], [39.5, 7374.0], [39.6, 7374.0], [39.7, 7374.0], [39.8, 7374.0], [39.9, 7374.0], [40.0, 7862.0], [40.1, 7862.0], [40.2, 7862.0], [40.3, 7862.0], [40.4, 7862.0], [40.5, 7862.0], [40.6, 7862.0], [40.7, 7862.0], [40.8, 7862.0], [40.9, 7862.0], [41.0, 7862.0], [41.1, 7862.0], [41.2, 7862.0], [41.3, 7862.0], [41.4, 7862.0], [41.5, 7862.0], [41.6, 7862.0], [41.7, 7862.0], [41.8, 7862.0], [41.9, 7862.0], [42.0, 8590.0], [42.1, 8590.0], [42.2, 8590.0], [42.3, 8590.0], [42.4, 8590.0], [42.5, 8590.0], [42.6, 8590.0], [42.7, 8590.0], [42.8, 8590.0], [42.9, 8590.0], [43.0, 8590.0], [43.1, 8590.0], [43.2, 8590.0], [43.3, 8590.0], [43.4, 8590.0], [43.5, 8590.0], [43.6, 8590.0], [43.7, 8590.0], [43.8, 8590.0], [43.9, 8590.0], [44.0, 8592.0], [44.1, 8592.0], [44.2, 8592.0], [44.3, 8592.0], [44.4, 8592.0], [44.5, 8592.0], [44.6, 8592.0], [44.7, 8592.0], [44.8, 8592.0], [44.9, 8592.0], [45.0, 8592.0], [45.1, 8592.0], [45.2, 8592.0], [45.3, 8592.0], [45.4, 8592.0], [45.5, 8592.0], [45.6, 8592.0], [45.7, 8592.0], [45.8, 8592.0], [45.9, 8592.0], [46.0, 9216.0], [46.1, 9216.0], [46.2, 9216.0], [46.3, 9216.0], [46.4, 9216.0], [46.5, 9216.0], [46.6, 9216.0], [46.7, 9216.0], [46.8, 9216.0], [46.9, 9216.0], [47.0, 9216.0], [47.1, 9216.0], [47.2, 9216.0], [47.3, 9216.0], [47.4, 9216.0], [47.5, 9216.0], [47.6, 9216.0], [47.7, 9216.0], [47.8, 9216.0], [47.9, 9216.0], [48.0, 9819.0], [48.1, 9819.0], [48.2, 9819.0], [48.3, 9819.0], [48.4, 9819.0], [48.5, 9819.0], [48.6, 9819.0], [48.7, 9819.0], [48.8, 9819.0], [48.9, 9819.0], [49.0, 9819.0], [49.1, 9819.0], [49.2, 9819.0], [49.3, 9819.0], [49.4, 9819.0], [49.5, 9819.0], [49.6, 9819.0], [49.7, 9819.0], [49.8, 9819.0], [49.9, 9819.0], [50.0, 9966.0], [50.1, 9966.0], [50.2, 9966.0], [50.3, 9966.0], [50.4, 9966.0], [50.5, 9966.0], [50.6, 9966.0], [50.7, 9966.0], [50.8, 9966.0], [50.9, 9966.0], [51.0, 9966.0], [51.1, 9966.0], [51.2, 9966.0], [51.3, 9966.0], [51.4, 9966.0], [51.5, 9966.0], [51.6, 9966.0], [51.7, 9966.0], [51.8, 9966.0], [51.9, 9966.0], [52.0, 10145.0], [52.1, 10145.0], [52.2, 10145.0], [52.3, 10145.0], [52.4, 10145.0], [52.5, 10145.0], [52.6, 10145.0], [52.7, 10145.0], [52.8, 10145.0], [52.9, 10145.0], [53.0, 10145.0], [53.1, 10145.0], [53.2, 10145.0], [53.3, 10145.0], [53.4, 10145.0], [53.5, 10145.0], [53.6, 10145.0], [53.7, 10145.0], [53.8, 10145.0], [53.9, 10145.0], [54.0, 10174.0], [54.1, 10174.0], [54.2, 10174.0], [54.3, 10174.0], [54.4, 10174.0], [54.5, 10174.0], [54.6, 10174.0], [54.7, 10174.0], [54.8, 10174.0], [54.9, 10174.0], [55.0, 10174.0], [55.1, 10174.0], [55.2, 10174.0], [55.3, 10174.0], [55.4, 10174.0], [55.5, 10174.0], [55.6, 10174.0], [55.7, 10174.0], [55.8, 10174.0], [55.9, 10174.0], [56.0, 10185.0], [56.1, 10185.0], [56.2, 10185.0], [56.3, 10185.0], [56.4, 10185.0], [56.5, 10185.0], [56.6, 10185.0], [56.7, 10185.0], [56.8, 10185.0], [56.9, 10185.0], [57.0, 10185.0], [57.1, 10185.0], [57.2, 10185.0], [57.3, 10185.0], [57.4, 10185.0], [57.5, 10185.0], [57.6, 10185.0], [57.7, 10185.0], [57.8, 10185.0], [57.9, 10185.0], [58.0, 10486.0], [58.1, 10486.0], [58.2, 10486.0], [58.3, 10486.0], [58.4, 10486.0], [58.5, 10486.0], [58.6, 10486.0], [58.7, 10486.0], [58.8, 10486.0], [58.9, 10486.0], [59.0, 10486.0], [59.1, 10486.0], [59.2, 10486.0], [59.3, 10486.0], [59.4, 10486.0], [59.5, 10486.0], [59.6, 10486.0], [59.7, 10486.0], [59.8, 10486.0], [59.9, 10486.0], [60.0, 10608.0], [60.1, 10608.0], [60.2, 10608.0], [60.3, 10608.0], [60.4, 10608.0], [60.5, 10608.0], [60.6, 10608.0], [60.7, 10608.0], [60.8, 10608.0], [60.9, 10608.0], [61.0, 10608.0], [61.1, 10608.0], [61.2, 10608.0], [61.3, 10608.0], [61.4, 10608.0], [61.5, 10608.0], [61.6, 10608.0], [61.7, 10608.0], [61.8, 10608.0], [61.9, 10608.0], [62.0, 10717.0], [62.1, 10717.0], [62.2, 10717.0], [62.3, 10717.0], [62.4, 10717.0], [62.5, 10717.0], [62.6, 10717.0], [62.7, 10717.0], [62.8, 10717.0], [62.9, 10717.0], [63.0, 10717.0], [63.1, 10717.0], [63.2, 10717.0], [63.3, 10717.0], [63.4, 10717.0], [63.5, 10717.0], [63.6, 10717.0], [63.7, 10717.0], [63.8, 10717.0], [63.9, 10717.0], [64.0, 10855.0], [64.1, 10855.0], [64.2, 10855.0], [64.3, 10855.0], [64.4, 10855.0], [64.5, 10855.0], [64.6, 10855.0], [64.7, 10855.0], [64.8, 10855.0], [64.9, 10855.0], [65.0, 10855.0], [65.1, 10855.0], [65.2, 10855.0], [65.3, 10855.0], [65.4, 10855.0], [65.5, 10855.0], [65.6, 10855.0], [65.7, 10855.0], [65.8, 10855.0], [65.9, 10855.0], [66.0, 11242.0], [66.1, 11242.0], [66.2, 11242.0], [66.3, 11242.0], [66.4, 11242.0], [66.5, 11242.0], [66.6, 11242.0], [66.7, 11242.0], [66.8, 11242.0], [66.9, 11242.0], [67.0, 11242.0], [67.1, 11242.0], [67.2, 11242.0], [67.3, 11242.0], [67.4, 11242.0], [67.5, 11242.0], [67.6, 11242.0], [67.7, 11242.0], [67.8, 11242.0], [67.9, 11242.0], [68.0, 11383.0], [68.1, 11383.0], [68.2, 11383.0], [68.3, 11383.0], [68.4, 11383.0], [68.5, 11383.0], [68.6, 11383.0], [68.7, 11383.0], [68.8, 11383.0], [68.9, 11383.0], [69.0, 11383.0], [69.1, 11383.0], [69.2, 11383.0], [69.3, 11383.0], [69.4, 11383.0], [69.5, 11383.0], [69.6, 11383.0], [69.7, 11383.0], [69.8, 11383.0], [69.9, 11383.0], [70.0, 11536.0], [70.1, 11536.0], [70.2, 11536.0], [70.3, 11536.0], [70.4, 11536.0], [70.5, 11536.0], [70.6, 11536.0], [70.7, 11536.0], [70.8, 11536.0], [70.9, 11536.0], [71.0, 11536.0], [71.1, 11536.0], [71.2, 11536.0], [71.3, 11536.0], [71.4, 11536.0], [71.5, 11536.0], [71.6, 11536.0], [71.7, 11536.0], [71.8, 11536.0], [71.9, 11536.0], [72.0, 11569.0], [72.1, 11569.0], [72.2, 11569.0], [72.3, 11569.0], [72.4, 11569.0], [72.5, 11569.0], [72.6, 11569.0], [72.7, 11569.0], [72.8, 11569.0], [72.9, 11569.0], [73.0, 11569.0], [73.1, 11569.0], [73.2, 11569.0], [73.3, 11569.0], [73.4, 11569.0], [73.5, 11569.0], [73.6, 11569.0], [73.7, 11569.0], [73.8, 11569.0], [73.9, 11569.0], [74.0, 11698.0], [74.1, 11698.0], [74.2, 11698.0], [74.3, 11698.0], [74.4, 11698.0], [74.5, 11698.0], [74.6, 11698.0], [74.7, 11698.0], [74.8, 11698.0], [74.9, 11698.0], [75.0, 11698.0], [75.1, 11698.0], [75.2, 11698.0], [75.3, 11698.0], [75.4, 11698.0], [75.5, 11698.0], [75.6, 11698.0], [75.7, 11698.0], [75.8, 11698.0], [75.9, 11698.0], [76.0, 11787.0], [76.1, 11787.0], [76.2, 11787.0], [76.3, 11787.0], [76.4, 11787.0], [76.5, 11787.0], [76.6, 11787.0], [76.7, 11787.0], [76.8, 11787.0], [76.9, 11787.0], [77.0, 11787.0], [77.1, 11787.0], [77.2, 11787.0], [77.3, 11787.0], [77.4, 11787.0], [77.5, 11787.0], [77.6, 11787.0], [77.7, 11787.0], [77.8, 11787.0], [77.9, 11787.0], [78.0, 11851.0], [78.1, 11851.0], [78.2, 11851.0], [78.3, 11851.0], [78.4, 11851.0], [78.5, 11851.0], [78.6, 11851.0], [78.7, 11851.0], [78.8, 11851.0], [78.9, 11851.0], [79.0, 11851.0], [79.1, 11851.0], [79.2, 11851.0], [79.3, 11851.0], [79.4, 11851.0], [79.5, 11851.0], [79.6, 11851.0], [79.7, 11851.0], [79.8, 11851.0], [79.9, 11851.0], [80.0, 12238.0], [80.1, 12238.0], [80.2, 12238.0], [80.3, 12238.0], [80.4, 12238.0], [80.5, 12238.0], [80.6, 12238.0], [80.7, 12238.0], [80.8, 12238.0], [80.9, 12238.0], [81.0, 12238.0], [81.1, 12238.0], [81.2, 12238.0], [81.3, 12238.0], [81.4, 12238.0], [81.5, 12238.0], [81.6, 12238.0], [81.7, 12238.0], [81.8, 12238.0], [81.9, 12238.0], [82.0, 12714.0], [82.1, 12714.0], [82.2, 12714.0], [82.3, 12714.0], [82.4, 12714.0], [82.5, 12714.0], [82.6, 12714.0], [82.7, 12714.0], [82.8, 12714.0], [82.9, 12714.0], [83.0, 12714.0], [83.1, 12714.0], [83.2, 12714.0], [83.3, 12714.0], [83.4, 12714.0], [83.5, 12714.0], [83.6, 12714.0], [83.7, 12714.0], [83.8, 12714.0], [83.9, 12714.0], [84.0, 12744.0], [84.1, 12744.0], [84.2, 12744.0], [84.3, 12744.0], [84.4, 12744.0], [84.5, 12744.0], [84.6, 12744.0], [84.7, 12744.0], [84.8, 12744.0], [84.9, 12744.0], [85.0, 12744.0], [85.1, 12744.0], [85.2, 12744.0], [85.3, 12744.0], [85.4, 12744.0], [85.5, 12744.0], [85.6, 12744.0], [85.7, 12744.0], [85.8, 12744.0], [85.9, 12744.0], [86.0, 12835.0], [86.1, 12835.0], [86.2, 12835.0], [86.3, 12835.0], [86.4, 12835.0], [86.5, 12835.0], [86.6, 12835.0], [86.7, 12835.0], [86.8, 12835.0], [86.9, 12835.0], [87.0, 12835.0], [87.1, 12835.0], [87.2, 12835.0], [87.3, 12835.0], [87.4, 12835.0], [87.5, 12835.0], [87.6, 12835.0], [87.7, 12835.0], [87.8, 12835.0], [87.9, 12835.0], [88.0, 13097.0], [88.1, 13097.0], [88.2, 13097.0], [88.3, 13097.0], [88.4, 13097.0], [88.5, 13097.0], [88.6, 13097.0], [88.7, 13097.0], [88.8, 13097.0], [88.9, 13097.0], [89.0, 13097.0], [89.1, 13097.0], [89.2, 13097.0], [89.3, 13097.0], [89.4, 13097.0], [89.5, 13097.0], [89.6, 13097.0], [89.7, 13097.0], [89.8, 13097.0], [89.9, 13097.0], [90.0, 13584.0], [90.1, 13584.0], [90.2, 13584.0], [90.3, 13584.0], [90.4, 13584.0], [90.5, 13584.0], [90.6, 13584.0], [90.7, 13584.0], [90.8, 13584.0], [90.9, 13584.0], [91.0, 13584.0], [91.1, 13584.0], [91.2, 13584.0], [91.3, 13584.0], [91.4, 13584.0], [91.5, 13584.0], [91.6, 13584.0], [91.7, 13584.0], [91.8, 13584.0], [91.9, 13584.0], [92.0, 14184.0], [92.1, 14184.0], [92.2, 14184.0], [92.3, 14184.0], [92.4, 14184.0], [92.5, 14184.0], [92.6, 14184.0], [92.7, 14184.0], [92.8, 14184.0], [92.9, 14184.0], [93.0, 14184.0], [93.1, 14184.0], [93.2, 14184.0], [93.3, 14184.0], [93.4, 14184.0], [93.5, 14184.0], [93.6, 14184.0], [93.7, 14184.0], [93.8, 14184.0], [93.9, 14184.0], [94.0, 14216.0], [94.1, 14216.0], [94.2, 14216.0], [94.3, 14216.0], [94.4, 14216.0], [94.5, 14216.0], [94.6, 14216.0], [94.7, 14216.0], [94.8, 14216.0], [94.9, 14216.0], [95.0, 14216.0], [95.1, 14216.0], [95.2, 14216.0], [95.3, 14216.0], [95.4, 14216.0], [95.5, 14216.0], [95.6, 14216.0], [95.7, 14216.0], [95.8, 14216.0], [95.9, 14216.0], [96.0, 14227.0], [96.1, 14227.0], [96.2, 14227.0], [96.3, 14227.0], [96.4, 14227.0], [96.5, 14227.0], [96.6, 14227.0], [96.7, 14227.0], [96.8, 14227.0], [96.9, 14227.0], [97.0, 14227.0], [97.1, 14227.0], [97.2, 14227.0], [97.3, 14227.0], [97.4, 14227.0], [97.5, 14227.0], [97.6, 14227.0], [97.7, 14227.0], [97.8, 14227.0], [97.9, 14227.0], [98.0, 14346.0], [98.1, 14346.0], [98.2, 14346.0], [98.3, 14346.0], [98.4, 14346.0], [98.5, 14346.0], [98.6, 14346.0], [98.7, 14346.0], [98.8, 14346.0], [98.9, 14346.0], [99.0, 14346.0], [99.1, 14346.0], [99.2, 14346.0], [99.3, 14346.0], [99.4, 14346.0], [99.5, 14346.0], [99.6, 14346.0], [99.7, 14346.0], [99.8, 14346.0], [99.9, 14346.0]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 2000.0, "maxY": 3.0, "series": [{"data": [[8500.0, 2.0], [9200.0, 1.0], [9900.0, 1.0], [9800.0, 1.0], [10100.0, 3.0], [10600.0, 1.0], [10400.0, 1.0], [10700.0, 1.0], [10800.0, 1.0], [11200.0, 1.0], [11300.0, 1.0], [11500.0, 2.0], [11600.0, 1.0], [11700.0, 1.0], [11800.0, 1.0], [12200.0, 1.0], [12700.0, 2.0], [12800.0, 1.0], [13000.0, 1.0], [13500.0, 1.0], [14100.0, 1.0], [14200.0, 2.0], [14300.0, 1.0], [2000.0, 1.0], [2500.0, 1.0], [2600.0, 1.0], [2800.0, 1.0], [3700.0, 1.0], [4400.0, 1.0], [4900.0, 1.0], [5300.0, 1.0], [5600.0, 1.0], [5700.0, 2.0], [6300.0, 2.0], [6400.0, 2.0], [6600.0, 1.0], [6700.0, 1.0], [6800.0, 1.0], [7000.0, 1.0], [7300.0, 1.0], [7800.0, 1.0]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 14300.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 50.0, "minX": 2.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 50.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 50.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 17.0, "minX": 1.62292314E12, "maxY": 41.70588235294118, "series": [{"data": [[1.62292314E12, 41.70588235294118], [1.6229232E12, 17.0]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229232E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 2581.0, "minX": 1.0, "maxY": 14346.0, "series": [{"data": [[2.0, 14216.0], [3.0, 14227.0], [4.0, 14184.0], [5.0, 13584.0], [6.0, 13097.0], [7.0, 12835.0], [8.0, 12744.0], [9.0, 12714.0], [10.0, 12238.0], [11.0, 11787.0], [12.0, 11698.0], [13.0, 11569.0], [14.0, 11536.0], [15.0, 11383.0], [16.0, 11851.0], [17.0, 11242.0], [18.0, 10717.0], [19.0, 10486.0], [20.0, 10185.0], [21.0, 10855.0], [22.0, 10174.0], [23.0, 10145.0], [24.0, 10608.0], [25.0, 9819.0], [26.0, 9966.0], [27.0, 9216.0], [28.0, 8592.0], [29.0, 8590.0], [30.0, 7862.0], [31.0, 7374.0], [33.0, 6871.0], [32.0, 7031.0], [35.0, 6656.0], [34.0, 6721.0], [37.0, 6405.0], [36.0, 6448.0], [39.0, 6377.0], [38.0, 6387.0], [41.0, 5621.0], [40.0, 5750.0], [43.0, 5347.0], [42.0, 5701.0], [45.0, 3227.0], [44.0, 4973.0], [47.0, 2884.0], [46.0, 3702.0], [49.0, 2581.0], [48.0, 2631.0], [1.0, 14346.0]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}, {"data": [[25.40000000000001, 8967.6]], "isOverall": false, "label": "HTTP Request create blog page-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 49.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 140.81666666666666, "minX": 1.62292314E12, "maxY": 1463.1166666666666, "series": [{"data": [[1.62292314E12, 753.7166666666667], [1.6229232E12, 1463.1166666666666]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62292314E12, 140.81666666666666], [1.6229232E12, 273.35]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229232E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 4978.705882352941, "minX": 1.62292314E12, "maxY": 11022.484848484846, "series": [{"data": [[1.62292314E12, 4978.705882352941], [1.6229232E12, 11022.484848484846]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6229232E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 4978.588235294118, "minX": 1.62292314E12, "maxY": 11022.454545454542, "series": [{"data": [[1.62292314E12, 4978.588235294118], [1.6229232E12, 11022.454545454542]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6229232E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 1.606060606060606, "minX": 1.62292314E12, "maxY": 4.294117647058823, "series": [{"data": [[1.62292314E12, 4.294117647058823], [1.6229232E12, 1.606060606060606]], "isOverall": false, "label": "HTTP Request create blog page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6229232E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 2018.0, "minX": 1.62292314E12, "maxY": 14346.0, "series": [{"data": [[1.62292314E12, 6721.0], [1.6229232E12, 14346.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62292314E12, 2018.0], [1.6229232E12, 6871.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62292314E12, 6669.0], [1.6229232E12, 14203.2]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62292314E12, 6721.0], [1.6229232E12, 14346.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62292314E12, 6721.0], [1.6229232E12, 14262.699999999999]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229232E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 4436.0, "minX": 1.0, "maxY": 13640.5, "series": [{"data": [[1.0, 4436.0], [2.0, 7618.0], [8.0, 10179.5], [4.0, 13640.5], [5.0, 5621.0], [3.0, 6800.5], [6.0, 9052.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 8.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 4436.0, "minX": 1.0, "maxY": 13640.5, "series": [{"data": [[1.0, 4436.0], [2.0, 7618.0], [8.0, 10179.5], [4.0, 13640.5], [5.0, 5621.0], [3.0, 6800.5], [6.0, 9051.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 8.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.8333333333333334, "minX": 1.62292314E12, "maxY": 0.8333333333333334, "series": [{"data": [[1.62292314E12, 0.8333333333333334]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292314E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.2833333333333333, "minX": 1.62292314E12, "maxY": 0.55, "series": [{"data": [[1.62292314E12, 0.2833333333333333], [1.6229232E12, 0.55]], "isOverall": false, "label": "201", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229232E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.2833333333333333, "minX": 1.62292314E12, "maxY": 0.55, "series": [{"data": [[1.62292314E12, 0.2833333333333333], [1.6229232E12, 0.55]], "isOverall": false, "label": "HTTP Request create blog page-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6229232E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.2833333333333333, "minX": 1.62292314E12, "maxY": 0.55, "series": [{"data": [[1.62292314E12, 0.2833333333333333], [1.6229232E12, 0.55]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6229232E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

