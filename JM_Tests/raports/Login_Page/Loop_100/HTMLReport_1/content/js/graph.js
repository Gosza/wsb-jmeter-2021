/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 1590.0, "minX": 0.0, "maxY": 19046.0, "series": [{"data": [[0.0, 1590.0], [0.1, 1590.0], [0.2, 1590.0], [0.3, 1590.0], [0.4, 1590.0], [0.5, 1590.0], [0.6, 1590.0], [0.7, 1590.0], [0.8, 1590.0], [0.9, 1590.0], [1.0, 1704.0], [1.1, 1704.0], [1.2, 1704.0], [1.3, 1704.0], [1.4, 1704.0], [1.5, 1704.0], [1.6, 1704.0], [1.7, 1704.0], [1.8, 1704.0], [1.9, 1704.0], [2.0, 2176.0], [2.1, 2176.0], [2.2, 2176.0], [2.3, 2176.0], [2.4, 2176.0], [2.5, 2176.0], [2.6, 2176.0], [2.7, 2176.0], [2.8, 2176.0], [2.9, 2176.0], [3.0, 2216.0], [3.1, 2216.0], [3.2, 2216.0], [3.3, 2216.0], [3.4, 2216.0], [3.5, 2216.0], [3.6, 2216.0], [3.7, 2216.0], [3.8, 2216.0], [3.9, 2216.0], [4.0, 2218.0], [4.1, 2218.0], [4.2, 2218.0], [4.3, 2218.0], [4.4, 2218.0], [4.5, 2218.0], [4.6, 2218.0], [4.7, 2218.0], [4.8, 2218.0], [4.9, 2218.0], [5.0, 2275.0], [5.1, 2275.0], [5.2, 2275.0], [5.3, 2275.0], [5.4, 2275.0], [5.5, 2275.0], [5.6, 2275.0], [5.7, 2275.0], [5.8, 2275.0], [5.9, 2275.0], [6.0, 2277.0], [6.1, 2277.0], [6.2, 2277.0], [6.3, 2277.0], [6.4, 2277.0], [6.5, 2277.0], [6.6, 2277.0], [6.7, 2277.0], [6.8, 2277.0], [6.9, 2277.0], [7.0, 2426.0], [7.1, 2426.0], [7.2, 2426.0], [7.3, 2426.0], [7.4, 2426.0], [7.5, 2426.0], [7.6, 2426.0], [7.7, 2426.0], [7.8, 2426.0], [7.9, 2426.0], [8.0, 2427.0], [8.1, 2427.0], [8.2, 2427.0], [8.3, 2427.0], [8.4, 2427.0], [8.5, 2427.0], [8.6, 2427.0], [8.7, 2427.0], [8.8, 2427.0], [8.9, 2427.0], [9.0, 2655.0], [9.1, 2655.0], [9.2, 2655.0], [9.3, 2655.0], [9.4, 2655.0], [9.5, 2655.0], [9.6, 2655.0], [9.7, 2655.0], [9.8, 2655.0], [9.9, 2655.0], [10.0, 2998.0], [10.1, 2998.0], [10.2, 2998.0], [10.3, 2998.0], [10.4, 2998.0], [10.5, 2998.0], [10.6, 2998.0], [10.7, 2998.0], [10.8, 2998.0], [10.9, 2998.0], [11.0, 3023.0], [11.1, 3023.0], [11.2, 3023.0], [11.3, 3023.0], [11.4, 3023.0], [11.5, 3023.0], [11.6, 3023.0], [11.7, 3023.0], [11.8, 3023.0], [11.9, 3023.0], [12.0, 3164.0], [12.1, 3164.0], [12.2, 3164.0], [12.3, 3164.0], [12.4, 3164.0], [12.5, 3164.0], [12.6, 3164.0], [12.7, 3164.0], [12.8, 3164.0], [12.9, 3164.0], [13.0, 3557.0], [13.1, 3557.0], [13.2, 3557.0], [13.3, 3557.0], [13.4, 3557.0], [13.5, 3557.0], [13.6, 3557.0], [13.7, 3557.0], [13.8, 3557.0], [13.9, 3557.0], [14.0, 3563.0], [14.1, 3563.0], [14.2, 3563.0], [14.3, 3563.0], [14.4, 3563.0], [14.5, 3563.0], [14.6, 3563.0], [14.7, 3563.0], [14.8, 3563.0], [14.9, 3563.0], [15.0, 3653.0], [15.1, 3653.0], [15.2, 3653.0], [15.3, 3653.0], [15.4, 3653.0], [15.5, 3653.0], [15.6, 3653.0], [15.7, 3653.0], [15.8, 3653.0], [15.9, 3653.0], [16.0, 4019.0], [16.1, 4019.0], [16.2, 4019.0], [16.3, 4019.0], [16.4, 4019.0], [16.5, 4019.0], [16.6, 4019.0], [16.7, 4019.0], [16.8, 4019.0], [16.9, 4019.0], [17.0, 4247.0], [17.1, 4247.0], [17.2, 4247.0], [17.3, 4247.0], [17.4, 4247.0], [17.5, 4247.0], [17.6, 4247.0], [17.7, 4247.0], [17.8, 4247.0], [17.9, 4247.0], [18.0, 4394.0], [18.1, 4394.0], [18.2, 4394.0], [18.3, 4394.0], [18.4, 4394.0], [18.5, 4394.0], [18.6, 4394.0], [18.7, 4394.0], [18.8, 4394.0], [18.9, 4394.0], [19.0, 4487.0], [19.1, 4487.0], [19.2, 4487.0], [19.3, 4487.0], [19.4, 4487.0], [19.5, 4487.0], [19.6, 4487.0], [19.7, 4487.0], [19.8, 4487.0], [19.9, 4487.0], [20.0, 4572.0], [20.1, 4572.0], [20.2, 4572.0], [20.3, 4572.0], [20.4, 4572.0], [20.5, 4572.0], [20.6, 4572.0], [20.7, 4572.0], [20.8, 4572.0], [20.9, 4572.0], [21.0, 4963.0], [21.1, 4963.0], [21.2, 4963.0], [21.3, 4963.0], [21.4, 4963.0], [21.5, 4963.0], [21.6, 4963.0], [21.7, 4963.0], [21.8, 4963.0], [21.9, 4963.0], [22.0, 5044.0], [22.1, 5044.0], [22.2, 5044.0], [22.3, 5044.0], [22.4, 5044.0], [22.5, 5044.0], [22.6, 5044.0], [22.7, 5044.0], [22.8, 5044.0], [22.9, 5044.0], [23.0, 5135.0], [23.1, 5135.0], [23.2, 5135.0], [23.3, 5135.0], [23.4, 5135.0], [23.5, 5135.0], [23.6, 5135.0], [23.7, 5135.0], [23.8, 5135.0], [23.9, 5135.0], [24.0, 5538.0], [24.1, 5538.0], [24.2, 5538.0], [24.3, 5538.0], [24.4, 5538.0], [24.5, 5538.0], [24.6, 5538.0], [24.7, 5538.0], [24.8, 5538.0], [24.9, 5538.0], [25.0, 5661.0], [25.1, 5661.0], [25.2, 5661.0], [25.3, 5661.0], [25.4, 5661.0], [25.5, 5661.0], [25.6, 5661.0], [25.7, 5661.0], [25.8, 5661.0], [25.9, 5661.0], [26.0, 5695.0], [26.1, 5695.0], [26.2, 5695.0], [26.3, 5695.0], [26.4, 5695.0], [26.5, 5695.0], [26.6, 5695.0], [26.7, 5695.0], [26.8, 5695.0], [26.9, 5695.0], [27.0, 5833.0], [27.1, 5833.0], [27.2, 5833.0], [27.3, 5833.0], [27.4, 5833.0], [27.5, 5833.0], [27.6, 5833.0], [27.7, 5833.0], [27.8, 5833.0], [27.9, 5833.0], [28.0, 6028.0], [28.1, 6028.0], [28.2, 6028.0], [28.3, 6028.0], [28.4, 6028.0], [28.5, 6028.0], [28.6, 6028.0], [28.7, 6028.0], [28.8, 6028.0], [28.9, 6028.0], [29.0, 6548.0], [29.1, 6548.0], [29.2, 6548.0], [29.3, 6548.0], [29.4, 6548.0], [29.5, 6548.0], [29.6, 6548.0], [29.7, 6548.0], [29.8, 6548.0], [29.9, 6548.0], [30.0, 6574.0], [30.1, 6574.0], [30.2, 6574.0], [30.3, 6574.0], [30.4, 6574.0], [30.5, 6574.0], [30.6, 6574.0], [30.7, 6574.0], [30.8, 6574.0], [30.9, 6574.0], [31.0, 6576.0], [31.1, 6576.0], [31.2, 6576.0], [31.3, 6576.0], [31.4, 6576.0], [31.5, 6576.0], [31.6, 6576.0], [31.7, 6576.0], [31.8, 6576.0], [31.9, 6576.0], [32.0, 6821.0], [32.1, 6821.0], [32.2, 6821.0], [32.3, 6821.0], [32.4, 6821.0], [32.5, 6821.0], [32.6, 6821.0], [32.7, 6821.0], [32.8, 6821.0], [32.9, 6821.0], [33.0, 7368.0], [33.1, 7368.0], [33.2, 7368.0], [33.3, 7368.0], [33.4, 7368.0], [33.5, 7368.0], [33.6, 7368.0], [33.7, 7368.0], [33.8, 7368.0], [33.9, 7368.0], [34.0, 7531.0], [34.1, 7531.0], [34.2, 7531.0], [34.3, 7531.0], [34.4, 7531.0], [34.5, 7531.0], [34.6, 7531.0], [34.7, 7531.0], [34.8, 7531.0], [34.9, 7531.0], [35.0, 7563.0], [35.1, 7563.0], [35.2, 7563.0], [35.3, 7563.0], [35.4, 7563.0], [35.5, 7563.0], [35.6, 7563.0], [35.7, 7563.0], [35.8, 7563.0], [35.9, 7563.0], [36.0, 7578.0], [36.1, 7578.0], [36.2, 7578.0], [36.3, 7578.0], [36.4, 7578.0], [36.5, 7578.0], [36.6, 7578.0], [36.7, 7578.0], [36.8, 7578.0], [36.9, 7578.0], [37.0, 7772.0], [37.1, 7772.0], [37.2, 7772.0], [37.3, 7772.0], [37.4, 7772.0], [37.5, 7772.0], [37.6, 7772.0], [37.7, 7772.0], [37.8, 7772.0], [37.9, 7772.0], [38.0, 8247.0], [38.1, 8247.0], [38.2, 8247.0], [38.3, 8247.0], [38.4, 8247.0], [38.5, 8247.0], [38.6, 8247.0], [38.7, 8247.0], [38.8, 8247.0], [38.9, 8247.0], [39.0, 8355.0], [39.1, 8355.0], [39.2, 8355.0], [39.3, 8355.0], [39.4, 8355.0], [39.5, 8355.0], [39.6, 8355.0], [39.7, 8355.0], [39.8, 8355.0], [39.9, 8355.0], [40.0, 8446.0], [40.1, 8446.0], [40.2, 8446.0], [40.3, 8446.0], [40.4, 8446.0], [40.5, 8446.0], [40.6, 8446.0], [40.7, 8446.0], [40.8, 8446.0], [40.9, 8446.0], [41.0, 8504.0], [41.1, 8504.0], [41.2, 8504.0], [41.3, 8504.0], [41.4, 8504.0], [41.5, 8504.0], [41.6, 8504.0], [41.7, 8504.0], [41.8, 8504.0], [41.9, 8504.0], [42.0, 8602.0], [42.1, 8602.0], [42.2, 8602.0], [42.3, 8602.0], [42.4, 8602.0], [42.5, 8602.0], [42.6, 8602.0], [42.7, 8602.0], [42.8, 8602.0], [42.9, 8602.0], [43.0, 8650.0], [43.1, 8650.0], [43.2, 8650.0], [43.3, 8650.0], [43.4, 8650.0], [43.5, 8650.0], [43.6, 8650.0], [43.7, 8650.0], [43.8, 8650.0], [43.9, 8650.0], [44.0, 9186.0], [44.1, 9186.0], [44.2, 9186.0], [44.3, 9186.0], [44.4, 9186.0], [44.5, 9186.0], [44.6, 9186.0], [44.7, 9186.0], [44.8, 9186.0], [44.9, 9186.0], [45.0, 9262.0], [45.1, 9262.0], [45.2, 9262.0], [45.3, 9262.0], [45.4, 9262.0], [45.5, 9262.0], [45.6, 9262.0], [45.7, 9262.0], [45.8, 9262.0], [45.9, 9262.0], [46.0, 9354.0], [46.1, 9354.0], [46.2, 9354.0], [46.3, 9354.0], [46.4, 9354.0], [46.5, 9354.0], [46.6, 9354.0], [46.7, 9354.0], [46.8, 9354.0], [46.9, 9354.0], [47.0, 9471.0], [47.1, 9471.0], [47.2, 9471.0], [47.3, 9471.0], [47.4, 9471.0], [47.5, 9471.0], [47.6, 9471.0], [47.7, 9471.0], [47.8, 9471.0], [47.9, 9471.0], [48.0, 9966.0], [48.1, 9966.0], [48.2, 9966.0], [48.3, 9966.0], [48.4, 9966.0], [48.5, 9966.0], [48.6, 9966.0], [48.7, 9966.0], [48.8, 9966.0], [48.9, 9966.0], [49.0, 10107.0], [49.1, 10107.0], [49.2, 10107.0], [49.3, 10107.0], [49.4, 10107.0], [49.5, 10107.0], [49.6, 10107.0], [49.7, 10107.0], [49.8, 10107.0], [49.9, 10107.0], [50.0, 10153.0], [50.1, 10153.0], [50.2, 10153.0], [50.3, 10153.0], [50.4, 10153.0], [50.5, 10153.0], [50.6, 10153.0], [50.7, 10153.0], [50.8, 10153.0], [50.9, 10153.0], [51.0, 10248.0], [51.1, 10248.0], [51.2, 10248.0], [51.3, 10248.0], [51.4, 10248.0], [51.5, 10248.0], [51.6, 10248.0], [51.7, 10248.0], [51.8, 10248.0], [51.9, 10248.0], [52.0, 10317.0], [52.1, 10317.0], [52.2, 10317.0], [52.3, 10317.0], [52.4, 10317.0], [52.5, 10317.0], [52.6, 10317.0], [52.7, 10317.0], [52.8, 10317.0], [52.9, 10317.0], [53.0, 10551.0], [53.1, 10551.0], [53.2, 10551.0], [53.3, 10551.0], [53.4, 10551.0], [53.5, 10551.0], [53.6, 10551.0], [53.7, 10551.0], [53.8, 10551.0], [53.9, 10551.0], [54.0, 10736.0], [54.1, 10736.0], [54.2, 10736.0], [54.3, 10736.0], [54.4, 10736.0], [54.5, 10736.0], [54.6, 10736.0], [54.7, 10736.0], [54.8, 10736.0], [54.9, 10736.0], [55.0, 10836.0], [55.1, 10836.0], [55.2, 10836.0], [55.3, 10836.0], [55.4, 10836.0], [55.5, 10836.0], [55.6, 10836.0], [55.7, 10836.0], [55.8, 10836.0], [55.9, 10836.0], [56.0, 11125.0], [56.1, 11125.0], [56.2, 11125.0], [56.3, 11125.0], [56.4, 11125.0], [56.5, 11125.0], [56.6, 11125.0], [56.7, 11125.0], [56.8, 11125.0], [56.9, 11125.0], [57.0, 11499.0], [57.1, 11499.0], [57.2, 11499.0], [57.3, 11499.0], [57.4, 11499.0], [57.5, 11499.0], [57.6, 11499.0], [57.7, 11499.0], [57.8, 11499.0], [57.9, 11499.0], [58.0, 11594.0], [58.1, 11594.0], [58.2, 11594.0], [58.3, 11594.0], [58.4, 11594.0], [58.5, 11594.0], [58.6, 11594.0], [58.7, 11594.0], [58.8, 11594.0], [58.9, 11594.0], [59.0, 11835.0], [59.1, 11835.0], [59.2, 11835.0], [59.3, 11835.0], [59.4, 11835.0], [59.5, 11835.0], [59.6, 11835.0], [59.7, 11835.0], [59.8, 11835.0], [59.9, 11835.0], [60.0, 11977.0], [60.1, 11977.0], [60.2, 11977.0], [60.3, 11977.0], [60.4, 11977.0], [60.5, 11977.0], [60.6, 11977.0], [60.7, 11977.0], [60.8, 11977.0], [60.9, 11977.0], [61.0, 12165.0], [61.1, 12165.0], [61.2, 12165.0], [61.3, 12165.0], [61.4, 12165.0], [61.5, 12165.0], [61.6, 12165.0], [61.7, 12165.0], [61.8, 12165.0], [61.9, 12165.0], [62.0, 12398.0], [62.1, 12398.0], [62.2, 12398.0], [62.3, 12398.0], [62.4, 12398.0], [62.5, 12398.0], [62.6, 12398.0], [62.7, 12398.0], [62.8, 12398.0], [62.9, 12398.0], [63.0, 12624.0], [63.1, 12624.0], [63.2, 12624.0], [63.3, 12624.0], [63.4, 12624.0], [63.5, 12624.0], [63.6, 12624.0], [63.7, 12624.0], [63.8, 12624.0], [63.9, 12624.0], [64.0, 12711.0], [64.1, 12711.0], [64.2, 12711.0], [64.3, 12711.0], [64.4, 12711.0], [64.5, 12711.0], [64.6, 12711.0], [64.7, 12711.0], [64.8, 12711.0], [64.9, 12711.0], [65.0, 12851.0], [65.1, 12851.0], [65.2, 12851.0], [65.3, 12851.0], [65.4, 12851.0], [65.5, 12851.0], [65.6, 12851.0], [65.7, 12851.0], [65.8, 12851.0], [65.9, 12851.0], [66.0, 13560.0], [66.1, 13560.0], [66.2, 13560.0], [66.3, 13560.0], [66.4, 13560.0], [66.5, 13560.0], [66.6, 13560.0], [66.7, 13560.0], [66.8, 13560.0], [66.9, 13560.0], [67.0, 13649.0], [67.1, 13649.0], [67.2, 13649.0], [67.3, 13649.0], [67.4, 13649.0], [67.5, 13649.0], [67.6, 13649.0], [67.7, 13649.0], [67.8, 13649.0], [67.9, 13649.0], [68.0, 13658.0], [68.1, 13658.0], [68.2, 13658.0], [68.3, 13658.0], [68.4, 13658.0], [68.5, 13658.0], [68.6, 13658.0], [68.7, 13658.0], [68.8, 13658.0], [68.9, 13658.0], [69.0, 13730.0], [69.1, 13730.0], [69.2, 13730.0], [69.3, 13730.0], [69.4, 13730.0], [69.5, 13730.0], [69.6, 13730.0], [69.7, 13730.0], [69.8, 13730.0], [69.9, 13730.0], [70.0, 14382.0], [70.1, 14382.0], [70.2, 14382.0], [70.3, 14382.0], [70.4, 14382.0], [70.5, 14382.0], [70.6, 14382.0], [70.7, 14382.0], [70.8, 14382.0], [70.9, 14382.0], [71.0, 14516.0], [71.1, 14516.0], [71.2, 14516.0], [71.3, 14516.0], [71.4, 14516.0], [71.5, 14516.0], [71.6, 14516.0], [71.7, 14516.0], [71.8, 14516.0], [71.9, 14516.0], [72.0, 14638.0], [72.1, 14638.0], [72.2, 14638.0], [72.3, 14638.0], [72.4, 14638.0], [72.5, 14638.0], [72.6, 14638.0], [72.7, 14638.0], [72.8, 14638.0], [72.9, 14638.0], [73.0, 14658.0], [73.1, 14658.0], [73.2, 14658.0], [73.3, 14658.0], [73.4, 14658.0], [73.5, 14658.0], [73.6, 14658.0], [73.7, 14658.0], [73.8, 14658.0], [73.9, 14658.0], [74.0, 14734.0], [74.1, 14734.0], [74.2, 14734.0], [74.3, 14734.0], [74.4, 14734.0], [74.5, 14734.0], [74.6, 14734.0], [74.7, 14734.0], [74.8, 14734.0], [74.9, 14734.0], [75.0, 15241.0], [75.1, 15241.0], [75.2, 15241.0], [75.3, 15241.0], [75.4, 15241.0], [75.5, 15241.0], [75.6, 15241.0], [75.7, 15241.0], [75.8, 15241.0], [75.9, 15241.0], [76.0, 15334.0], [76.1, 15334.0], [76.2, 15334.0], [76.3, 15334.0], [76.4, 15334.0], [76.5, 15334.0], [76.6, 15334.0], [76.7, 15334.0], [76.8, 15334.0], [76.9, 15334.0], [77.0, 15433.0], [77.1, 15433.0], [77.2, 15433.0], [77.3, 15433.0], [77.4, 15433.0], [77.5, 15433.0], [77.6, 15433.0], [77.7, 15433.0], [77.8, 15433.0], [77.9, 15433.0], [78.0, 15678.0], [78.1, 15678.0], [78.2, 15678.0], [78.3, 15678.0], [78.4, 15678.0], [78.5, 15678.0], [78.6, 15678.0], [78.7, 15678.0], [78.8, 15678.0], [78.9, 15678.0], [79.0, 15704.0], [79.1, 15704.0], [79.2, 15704.0], [79.3, 15704.0], [79.4, 15704.0], [79.5, 15704.0], [79.6, 15704.0], [79.7, 15704.0], [79.8, 15704.0], [79.9, 15704.0], [80.0, 15848.0], [80.1, 15848.0], [80.2, 15848.0], [80.3, 15848.0], [80.4, 15848.0], [80.5, 15848.0], [80.6, 15848.0], [80.7, 15848.0], [80.8, 15848.0], [80.9, 15848.0], [81.0, 16201.0], [81.1, 16201.0], [81.2, 16201.0], [81.3, 16201.0], [81.4, 16201.0], [81.5, 16201.0], [81.6, 16201.0], [81.7, 16201.0], [81.8, 16201.0], [81.9, 16201.0], [82.0, 16283.0], [82.1, 16283.0], [82.2, 16283.0], [82.3, 16283.0], [82.4, 16283.0], [82.5, 16283.0], [82.6, 16283.0], [82.7, 16283.0], [82.8, 16283.0], [82.9, 16283.0], [83.0, 16356.0], [83.1, 16356.0], [83.2, 16356.0], [83.3, 16356.0], [83.4, 16356.0], [83.5, 16356.0], [83.6, 16356.0], [83.7, 16356.0], [83.8, 16356.0], [83.9, 16356.0], [84.0, 16660.0], [84.1, 16660.0], [84.2, 16660.0], [84.3, 16660.0], [84.4, 16660.0], [84.5, 16660.0], [84.6, 16660.0], [84.7, 16660.0], [84.8, 16660.0], [84.9, 16660.0], [85.0, 16663.0], [85.1, 16663.0], [85.2, 16663.0], [85.3, 16663.0], [85.4, 16663.0], [85.5, 16663.0], [85.6, 16663.0], [85.7, 16663.0], [85.8, 16663.0], [85.9, 16663.0], [86.0, 17445.0], [86.1, 17445.0], [86.2, 17445.0], [86.3, 17445.0], [86.4, 17445.0], [86.5, 17445.0], [86.6, 17445.0], [86.7, 17445.0], [86.8, 17445.0], [86.9, 17445.0], [87.0, 17502.0], [87.1, 17502.0], [87.2, 17502.0], [87.3, 17502.0], [87.4, 17502.0], [87.5, 17502.0], [87.6, 17502.0], [87.7, 17502.0], [87.8, 17502.0], [87.9, 17502.0], [88.0, 17534.0], [88.1, 17534.0], [88.2, 17534.0], [88.3, 17534.0], [88.4, 17534.0], [88.5, 17534.0], [88.6, 17534.0], [88.7, 17534.0], [88.8, 17534.0], [88.9, 17534.0], [89.0, 17602.0], [89.1, 17602.0], [89.2, 17602.0], [89.3, 17602.0], [89.4, 17602.0], [89.5, 17602.0], [89.6, 17602.0], [89.7, 17602.0], [89.8, 17602.0], [89.9, 17602.0], [90.0, 17612.0], [90.1, 17612.0], [90.2, 17612.0], [90.3, 17612.0], [90.4, 17612.0], [90.5, 17612.0], [90.6, 17612.0], [90.7, 17612.0], [90.8, 17612.0], [90.9, 17612.0], [91.0, 17764.0], [91.1, 17764.0], [91.2, 17764.0], [91.3, 17764.0], [91.4, 17764.0], [91.5, 17764.0], [91.6, 17764.0], [91.7, 17764.0], [91.8, 17764.0], [91.9, 17764.0], [92.0, 18100.0], [92.1, 18100.0], [92.2, 18100.0], [92.3, 18100.0], [92.4, 18100.0], [92.5, 18100.0], [92.6, 18100.0], [92.7, 18100.0], [92.8, 18100.0], [92.9, 18100.0], [93.0, 18135.0], [93.1, 18135.0], [93.2, 18135.0], [93.3, 18135.0], [93.4, 18135.0], [93.5, 18135.0], [93.6, 18135.0], [93.7, 18135.0], [93.8, 18135.0], [93.9, 18135.0], [94.0, 18250.0], [94.1, 18250.0], [94.2, 18250.0], [94.3, 18250.0], [94.4, 18250.0], [94.5, 18250.0], [94.6, 18250.0], [94.7, 18250.0], [94.8, 18250.0], [94.9, 18250.0], [95.0, 18644.0], [95.1, 18644.0], [95.2, 18644.0], [95.3, 18644.0], [95.4, 18644.0], [95.5, 18644.0], [95.6, 18644.0], [95.7, 18644.0], [95.8, 18644.0], [95.9, 18644.0], [96.0, 18747.0], [96.1, 18747.0], [96.2, 18747.0], [96.3, 18747.0], [96.4, 18747.0], [96.5, 18747.0], [96.6, 18747.0], [96.7, 18747.0], [96.8, 18747.0], [96.9, 18747.0], [97.0, 18854.0], [97.1, 18854.0], [97.2, 18854.0], [97.3, 18854.0], [97.4, 18854.0], [97.5, 18854.0], [97.6, 18854.0], [97.7, 18854.0], [97.8, 18854.0], [97.9, 18854.0], [98.0, 19039.0], [98.1, 19039.0], [98.2, 19039.0], [98.3, 19039.0], [98.4, 19039.0], [98.5, 19039.0], [98.6, 19039.0], [98.7, 19039.0], [98.8, 19039.0], [98.9, 19039.0], [99.0, 19046.0], [99.1, 19046.0], [99.2, 19046.0], [99.3, 19046.0], [99.4, 19046.0], [99.5, 19046.0], [99.6, 19046.0], [99.7, 19046.0], [99.8, 19046.0], [99.9, 19046.0]], "isOverall": false, "label": "HTTP Request_visit_login_page", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 1500.0, "maxY": 4.0, "series": [{"data": [[1500.0, 1.0], [1700.0, 1.0], [2100.0, 1.0], [2200.0, 4.0], [2400.0, 2.0], [2600.0, 1.0], [2900.0, 1.0], [3000.0, 1.0], [3100.0, 1.0], [3500.0, 2.0], [3600.0, 1.0], [4000.0, 1.0], [4200.0, 1.0], [4300.0, 1.0], [4400.0, 1.0], [4500.0, 1.0], [4900.0, 1.0], [5000.0, 1.0], [5100.0, 1.0], [5500.0, 1.0], [5600.0, 2.0], [5800.0, 1.0], [6000.0, 1.0], [6500.0, 3.0], [6800.0, 1.0], [7300.0, 1.0], [7500.0, 3.0], [7700.0, 1.0], [8200.0, 1.0], [8300.0, 1.0], [8400.0, 1.0], [8500.0, 1.0], [8600.0, 2.0], [9100.0, 1.0], [9200.0, 1.0], [9300.0, 1.0], [9400.0, 1.0], [9900.0, 1.0], [10100.0, 2.0], [10200.0, 1.0], [10300.0, 1.0], [10500.0, 1.0], [10700.0, 1.0], [10800.0, 1.0], [11100.0, 1.0], [11400.0, 1.0], [11500.0, 1.0], [11800.0, 1.0], [11900.0, 1.0], [12100.0, 1.0], [12300.0, 1.0], [12600.0, 1.0], [12700.0, 1.0], [12800.0, 1.0], [13500.0, 1.0], [13600.0, 2.0], [13700.0, 1.0], [14300.0, 1.0], [14500.0, 1.0], [14600.0, 2.0], [14700.0, 1.0], [15200.0, 1.0], [15300.0, 1.0], [15400.0, 1.0], [15700.0, 1.0], [15600.0, 1.0], [15800.0, 1.0], [16200.0, 2.0], [16300.0, 1.0], [16600.0, 2.0], [17400.0, 1.0], [17500.0, 2.0], [17600.0, 2.0], [17700.0, 1.0], [18100.0, 2.0], [18200.0, 1.0], [18600.0, 1.0], [18700.0, 1.0], [18800.0, 1.0], [19000.0, 2.0]], "isOverall": false, "label": "HTTP Request_visit_login_page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 19000.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 100.0, "minX": 2.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 100.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 100.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 17.558823529411768, "minX": 1.6229166E12, "maxY": 67.5, "series": [{"data": [[1.62291666E12, 17.558823529411768], [1.6229166E12, 67.5]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291666E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 1590.0, "minX": 2.0, "maxY": 19042.5, "series": [{"data": [[2.0, 19042.5], [3.0, 18854.0], [4.0, 18747.0], [5.0, 18644.0], [6.0, 18250.0], [7.0, 18135.0], [8.0, 18100.0], [9.0, 17764.0], [11.0, 17607.0], [12.0, 17534.0], [13.0, 17502.0], [14.0, 17445.0], [15.0, 16663.0], [16.0, 16660.0], [17.0, 16356.0], [18.0, 16283.0], [19.0, 16201.0], [20.0, 15848.0], [21.0, 15678.0], [22.0, 15704.0], [23.0, 15433.0], [24.0, 15334.0], [25.0, 15241.0], [26.0, 14734.0], [27.0, 14658.0], [28.0, 14638.0], [29.0, 14516.0], [30.0, 14382.0], [31.0, 13730.0], [33.0, 13649.0], [32.0, 13658.0], [35.0, 12851.0], [34.0, 13560.0], [37.0, 12624.0], [36.0, 12711.0], [39.0, 12165.0], [38.0, 12398.0], [41.0, 11835.0], [40.0, 11977.0], [43.0, 11499.0], [42.0, 11594.0], [45.0, 10836.0], [44.0, 11125.0], [47.0, 10551.0], [46.0, 10736.0], [49.0, 10248.0], [48.0, 10317.0], [51.0, 10153.0], [50.0, 10107.0], [53.0, 9471.0], [52.0, 9966.0], [55.0, 9262.0], [54.0, 9354.0], [57.0, 8650.0], [56.0, 9186.0], [59.0, 8504.0], [58.0, 8602.0], [61.0, 8355.0], [60.0, 8446.0], [63.0, 7772.0], [62.0, 8247.0], [67.0, 7368.0], [66.0, 7531.0], [65.0, 7563.0], [64.0, 7578.0], [71.0, 6548.0], [70.0, 6574.0], [69.0, 6576.0], [68.0, 6821.0], [75.0, 5661.0], [74.0, 5695.0], [73.0, 5833.0], [72.0, 6028.0], [79.0, 4963.0], [78.0, 5044.0], [77.0, 5135.0], [76.0, 5538.0], [83.0, 4247.0], [82.0, 4394.0], [81.0, 4487.0], [80.0, 4572.0], [87.0, 3557.0], [86.0, 3563.0], [85.0, 3653.0], [84.0, 4019.0], [91.0, 2655.0], [90.0, 3023.0], [89.0, 2998.0], [88.0, 3164.0], [95.0, 2275.0], [94.0, 2277.0], [93.0, 2427.0], [92.0, 2426.0], [99.0, 2176.0], [98.0, 1704.0], [97.0, 1590.0], [96.0, 2216.0], [100.0, 2218.0]], "isOverall": false, "label": "HTTP Request_visit_login_page", "isController": false}, {"data": [[50.52000000000002, 10208.389999999996]], "isOverall": false, "label": "HTTP Request_visit_login_page-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 127.5, "minX": 1.6229166E12, "maxY": 10026.5, "series": [{"data": [[1.62291666E12, 5165.166666666667], [1.6229166E12, 10026.5]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62291666E12, 127.5], [1.6229166E12, 247.5]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291666E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 7024.833333333331, "minX": 1.6229166E12, "maxY": 16388.235294117647, "series": [{"data": [[1.62291666E12, 16388.235294117647], [1.6229166E12, 7024.833333333331]], "isOverall": false, "label": "HTTP Request_visit_login_page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291666E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 7023.181818181817, "minX": 1.6229166E12, "maxY": 16386.441176470587, "series": [{"data": [[1.62291666E12, 16386.441176470587], [1.6229166E12, 7023.181818181817]], "isOverall": false, "label": "HTTP Request_visit_login_page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291666E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 2.7121212121212124, "minX": 1.6229166E12, "maxY": 11.38235294117647, "series": [{"data": [[1.62291666E12, 11.38235294117647], [1.6229166E12, 2.7121212121212124]], "isOverall": false, "label": "HTTP Request_visit_login_page", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291666E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 1590.0, "minX": 1.6229166E12, "maxY": 19046.0, "series": [{"data": [[1.62291666E12, 19046.0], [1.6229166E12, 12851.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62291666E12, 13560.0], [1.6229166E12, 1590.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62291666E12, 18800.5], [1.6229166E12, 11877.6]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62291666E12, 19046.0], [1.6229166E12, 12851.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62291666E12, 19040.75], [1.6229166E12, 12544.9]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291666E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 1940.0, "minX": 3.0, "maxY": 17607.0, "series": [{"data": [[4.0, 1940.0], [8.0, 17607.0], [5.0, 8114.0], [6.0, 8400.5], [3.0, 12711.0], [7.0, 14734.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 8.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 1938.5, "minX": 3.0, "maxY": 17605.0, "series": [{"data": [[4.0, 1938.5], [8.0, 17605.0], [5.0, 8113.0], [6.0, 8399.0], [3.0, 12710.0], [7.0, 14733.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 8.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.6229166E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.6229166E12, 1.6666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229166E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.5666666666666667, "minX": 1.6229166E12, "maxY": 1.1, "series": [{"data": [[1.62291666E12, 0.5666666666666667], [1.6229166E12, 1.1]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291666E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.5666666666666667, "minX": 1.6229166E12, "maxY": 1.1, "series": [{"data": [[1.62291666E12, 0.5666666666666667], [1.6229166E12, 1.1]], "isOverall": false, "label": "HTTP Request_visit_login_page-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291666E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.5666666666666667, "minX": 1.6229166E12, "maxY": 1.1, "series": [{"data": [[1.62291666E12, 0.5666666666666667], [1.6229166E12, 1.1]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291666E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

