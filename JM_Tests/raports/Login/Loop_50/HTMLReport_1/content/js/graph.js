/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 353.0, "minX": 0.0, "maxY": 1906.0, "series": [{"data": [[0.0, 353.0], [0.1, 353.0], [0.2, 353.0], [0.3, 353.0], [0.4, 353.0], [0.5, 353.0], [0.6, 353.0], [0.7, 353.0], [0.8, 353.0], [0.9, 353.0], [1.0, 353.0], [1.1, 353.0], [1.2, 353.0], [1.3, 353.0], [1.4, 353.0], [1.5, 353.0], [1.6, 353.0], [1.7, 353.0], [1.8, 353.0], [1.9, 353.0], [2.0, 368.0], [2.1, 368.0], [2.2, 368.0], [2.3, 368.0], [2.4, 368.0], [2.5, 368.0], [2.6, 368.0], [2.7, 368.0], [2.8, 368.0], [2.9, 368.0], [3.0, 368.0], [3.1, 368.0], [3.2, 368.0], [3.3, 368.0], [3.4, 368.0], [3.5, 368.0], [3.6, 368.0], [3.7, 368.0], [3.8, 368.0], [3.9, 368.0], [4.0, 386.0], [4.1, 386.0], [4.2, 386.0], [4.3, 386.0], [4.4, 386.0], [4.5, 386.0], [4.6, 386.0], [4.7, 386.0], [4.8, 386.0], [4.9, 386.0], [5.0, 386.0], [5.1, 386.0], [5.2, 386.0], [5.3, 386.0], [5.4, 386.0], [5.5, 386.0], [5.6, 386.0], [5.7, 386.0], [5.8, 386.0], [5.9, 386.0], [6.0, 408.0], [6.1, 408.0], [6.2, 408.0], [6.3, 408.0], [6.4, 408.0], [6.5, 408.0], [6.6, 408.0], [6.7, 408.0], [6.8, 408.0], [6.9, 408.0], [7.0, 408.0], [7.1, 408.0], [7.2, 408.0], [7.3, 408.0], [7.4, 408.0], [7.5, 408.0], [7.6, 408.0], [7.7, 408.0], [7.8, 408.0], [7.9, 408.0], [8.0, 410.0], [8.1, 410.0], [8.2, 410.0], [8.3, 410.0], [8.4, 410.0], [8.5, 410.0], [8.6, 410.0], [8.7, 410.0], [8.8, 410.0], [8.9, 410.0], [9.0, 410.0], [9.1, 410.0], [9.2, 410.0], [9.3, 410.0], [9.4, 410.0], [9.5, 410.0], [9.6, 410.0], [9.7, 410.0], [9.8, 410.0], [9.9, 410.0], [10.0, 414.0], [10.1, 414.0], [10.2, 414.0], [10.3, 414.0], [10.4, 414.0], [10.5, 414.0], [10.6, 414.0], [10.7, 414.0], [10.8, 414.0], [10.9, 414.0], [11.0, 414.0], [11.1, 414.0], [11.2, 414.0], [11.3, 414.0], [11.4, 414.0], [11.5, 414.0], [11.6, 414.0], [11.7, 414.0], [11.8, 414.0], [11.9, 414.0], [12.0, 427.0], [12.1, 427.0], [12.2, 427.0], [12.3, 427.0], [12.4, 427.0], [12.5, 427.0], [12.6, 427.0], [12.7, 427.0], [12.8, 427.0], [12.9, 427.0], [13.0, 427.0], [13.1, 427.0], [13.2, 427.0], [13.3, 427.0], [13.4, 427.0], [13.5, 427.0], [13.6, 427.0], [13.7, 427.0], [13.8, 427.0], [13.9, 427.0], [14.0, 443.0], [14.1, 443.0], [14.2, 443.0], [14.3, 443.0], [14.4, 443.0], [14.5, 443.0], [14.6, 443.0], [14.7, 443.0], [14.8, 443.0], [14.9, 443.0], [15.0, 443.0], [15.1, 443.0], [15.2, 443.0], [15.3, 443.0], [15.4, 443.0], [15.5, 443.0], [15.6, 443.0], [15.7, 443.0], [15.8, 443.0], [15.9, 443.0], [16.0, 460.0], [16.1, 460.0], [16.2, 460.0], [16.3, 460.0], [16.4, 460.0], [16.5, 460.0], [16.6, 460.0], [16.7, 460.0], [16.8, 460.0], [16.9, 460.0], [17.0, 460.0], [17.1, 460.0], [17.2, 460.0], [17.3, 460.0], [17.4, 460.0], [17.5, 460.0], [17.6, 460.0], [17.7, 460.0], [17.8, 460.0], [17.9, 460.0], [18.0, 477.0], [18.1, 477.0], [18.2, 477.0], [18.3, 477.0], [18.4, 477.0], [18.5, 477.0], [18.6, 477.0], [18.7, 477.0], [18.8, 477.0], [18.9, 477.0], [19.0, 477.0], [19.1, 477.0], [19.2, 477.0], [19.3, 477.0], [19.4, 477.0], [19.5, 477.0], [19.6, 477.0], [19.7, 477.0], [19.8, 477.0], [19.9, 477.0], [20.0, 488.0], [20.1, 488.0], [20.2, 488.0], [20.3, 488.0], [20.4, 488.0], [20.5, 488.0], [20.6, 488.0], [20.7, 488.0], [20.8, 488.0], [20.9, 488.0], [21.0, 488.0], [21.1, 488.0], [21.2, 488.0], [21.3, 488.0], [21.4, 488.0], [21.5, 488.0], [21.6, 488.0], [21.7, 488.0], [21.8, 488.0], [21.9, 488.0], [22.0, 490.0], [22.1, 490.0], [22.2, 490.0], [22.3, 490.0], [22.4, 490.0], [22.5, 490.0], [22.6, 490.0], [22.7, 490.0], [22.8, 490.0], [22.9, 490.0], [23.0, 490.0], [23.1, 490.0], [23.2, 490.0], [23.3, 490.0], [23.4, 490.0], [23.5, 490.0], [23.6, 490.0], [23.7, 490.0], [23.8, 490.0], [23.9, 490.0], [24.0, 493.0], [24.1, 493.0], [24.2, 493.0], [24.3, 493.0], [24.4, 493.0], [24.5, 493.0], [24.6, 493.0], [24.7, 493.0], [24.8, 493.0], [24.9, 493.0], [25.0, 493.0], [25.1, 493.0], [25.2, 493.0], [25.3, 493.0], [25.4, 493.0], [25.5, 493.0], [25.6, 493.0], [25.7, 493.0], [25.8, 493.0], [25.9, 493.0], [26.0, 495.0], [26.1, 495.0], [26.2, 495.0], [26.3, 495.0], [26.4, 495.0], [26.5, 495.0], [26.6, 495.0], [26.7, 495.0], [26.8, 495.0], [26.9, 495.0], [27.0, 495.0], [27.1, 495.0], [27.2, 495.0], [27.3, 495.0], [27.4, 495.0], [27.5, 495.0], [27.6, 495.0], [27.7, 495.0], [27.8, 495.0], [27.9, 495.0], [28.0, 515.0], [28.1, 515.0], [28.2, 515.0], [28.3, 515.0], [28.4, 515.0], [28.5, 515.0], [28.6, 515.0], [28.7, 515.0], [28.8, 515.0], [28.9, 515.0], [29.0, 515.0], [29.1, 515.0], [29.2, 515.0], [29.3, 515.0], [29.4, 515.0], [29.5, 515.0], [29.6, 515.0], [29.7, 515.0], [29.8, 515.0], [29.9, 515.0], [30.0, 516.0], [30.1, 516.0], [30.2, 516.0], [30.3, 516.0], [30.4, 516.0], [30.5, 516.0], [30.6, 516.0], [30.7, 516.0], [30.8, 516.0], [30.9, 516.0], [31.0, 516.0], [31.1, 516.0], [31.2, 516.0], [31.3, 516.0], [31.4, 516.0], [31.5, 516.0], [31.6, 516.0], [31.7, 516.0], [31.8, 516.0], [31.9, 516.0], [32.0, 539.0], [32.1, 539.0], [32.2, 539.0], [32.3, 539.0], [32.4, 539.0], [32.5, 539.0], [32.6, 539.0], [32.7, 539.0], [32.8, 539.0], [32.9, 539.0], [33.0, 539.0], [33.1, 539.0], [33.2, 539.0], [33.3, 539.0], [33.4, 539.0], [33.5, 539.0], [33.6, 539.0], [33.7, 539.0], [33.8, 539.0], [33.9, 539.0], [34.0, 553.0], [34.1, 553.0], [34.2, 553.0], [34.3, 553.0], [34.4, 553.0], [34.5, 553.0], [34.6, 553.0], [34.7, 553.0], [34.8, 553.0], [34.9, 553.0], [35.0, 553.0], [35.1, 553.0], [35.2, 553.0], [35.3, 553.0], [35.4, 553.0], [35.5, 553.0], [35.6, 553.0], [35.7, 553.0], [35.8, 553.0], [35.9, 553.0], [36.0, 560.0], [36.1, 560.0], [36.2, 560.0], [36.3, 560.0], [36.4, 560.0], [36.5, 560.0], [36.6, 560.0], [36.7, 560.0], [36.8, 560.0], [36.9, 560.0], [37.0, 560.0], [37.1, 560.0], [37.2, 560.0], [37.3, 560.0], [37.4, 560.0], [37.5, 560.0], [37.6, 560.0], [37.7, 560.0], [37.8, 560.0], [37.9, 560.0], [38.0, 565.0], [38.1, 565.0], [38.2, 565.0], [38.3, 565.0], [38.4, 565.0], [38.5, 565.0], [38.6, 565.0], [38.7, 565.0], [38.8, 565.0], [38.9, 565.0], [39.0, 565.0], [39.1, 565.0], [39.2, 565.0], [39.3, 565.0], [39.4, 565.0], [39.5, 565.0], [39.6, 565.0], [39.7, 565.0], [39.8, 565.0], [39.9, 565.0], [40.0, 565.0], [40.1, 565.0], [40.2, 565.0], [40.3, 565.0], [40.4, 565.0], [40.5, 565.0], [40.6, 565.0], [40.7, 565.0], [40.8, 565.0], [40.9, 565.0], [41.0, 565.0], [41.1, 565.0], [41.2, 565.0], [41.3, 565.0], [41.4, 565.0], [41.5, 565.0], [41.6, 565.0], [41.7, 565.0], [41.8, 565.0], [41.9, 565.0], [42.0, 581.0], [42.1, 581.0], [42.2, 581.0], [42.3, 581.0], [42.4, 581.0], [42.5, 581.0], [42.6, 581.0], [42.7, 581.0], [42.8, 581.0], [42.9, 581.0], [43.0, 581.0], [43.1, 581.0], [43.2, 581.0], [43.3, 581.0], [43.4, 581.0], [43.5, 581.0], [43.6, 581.0], [43.7, 581.0], [43.8, 581.0], [43.9, 581.0], [44.0, 581.0], [44.1, 581.0], [44.2, 581.0], [44.3, 581.0], [44.4, 581.0], [44.5, 581.0], [44.6, 581.0], [44.7, 581.0], [44.8, 581.0], [44.9, 581.0], [45.0, 581.0], [45.1, 581.0], [45.2, 581.0], [45.3, 581.0], [45.4, 581.0], [45.5, 581.0], [45.6, 581.0], [45.7, 581.0], [45.8, 581.0], [45.9, 581.0], [46.0, 589.0], [46.1, 589.0], [46.2, 589.0], [46.3, 589.0], [46.4, 589.0], [46.5, 589.0], [46.6, 589.0], [46.7, 589.0], [46.8, 589.0], [46.9, 589.0], [47.0, 589.0], [47.1, 589.0], [47.2, 589.0], [47.3, 589.0], [47.4, 589.0], [47.5, 589.0], [47.6, 589.0], [47.7, 589.0], [47.8, 589.0], [47.9, 589.0], [48.0, 610.0], [48.1, 610.0], [48.2, 610.0], [48.3, 610.0], [48.4, 610.0], [48.5, 610.0], [48.6, 610.0], [48.7, 610.0], [48.8, 610.0], [48.9, 610.0], [49.0, 610.0], [49.1, 610.0], [49.2, 610.0], [49.3, 610.0], [49.4, 610.0], [49.5, 610.0], [49.6, 610.0], [49.7, 610.0], [49.8, 610.0], [49.9, 610.0], [50.0, 623.0], [50.1, 623.0], [50.2, 623.0], [50.3, 623.0], [50.4, 623.0], [50.5, 623.0], [50.6, 623.0], [50.7, 623.0], [50.8, 623.0], [50.9, 623.0], [51.0, 623.0], [51.1, 623.0], [51.2, 623.0], [51.3, 623.0], [51.4, 623.0], [51.5, 623.0], [51.6, 623.0], [51.7, 623.0], [51.8, 623.0], [51.9, 623.0], [52.0, 629.0], [52.1, 629.0], [52.2, 629.0], [52.3, 629.0], [52.4, 629.0], [52.5, 629.0], [52.6, 629.0], [52.7, 629.0], [52.8, 629.0], [52.9, 629.0], [53.0, 629.0], [53.1, 629.0], [53.2, 629.0], [53.3, 629.0], [53.4, 629.0], [53.5, 629.0], [53.6, 629.0], [53.7, 629.0], [53.8, 629.0], [53.9, 629.0], [54.0, 639.0], [54.1, 639.0], [54.2, 639.0], [54.3, 639.0], [54.4, 639.0], [54.5, 639.0], [54.6, 639.0], [54.7, 639.0], [54.8, 639.0], [54.9, 639.0], [55.0, 639.0], [55.1, 639.0], [55.2, 639.0], [55.3, 639.0], [55.4, 639.0], [55.5, 639.0], [55.6, 639.0], [55.7, 639.0], [55.8, 639.0], [55.9, 639.0], [56.0, 646.0], [56.1, 646.0], [56.2, 646.0], [56.3, 646.0], [56.4, 646.0], [56.5, 646.0], [56.6, 646.0], [56.7, 646.0], [56.8, 646.0], [56.9, 646.0], [57.0, 646.0], [57.1, 646.0], [57.2, 646.0], [57.3, 646.0], [57.4, 646.0], [57.5, 646.0], [57.6, 646.0], [57.7, 646.0], [57.8, 646.0], [57.9, 646.0], [58.0, 670.0], [58.1, 670.0], [58.2, 670.0], [58.3, 670.0], [58.4, 670.0], [58.5, 670.0], [58.6, 670.0], [58.7, 670.0], [58.8, 670.0], [58.9, 670.0], [59.0, 670.0], [59.1, 670.0], [59.2, 670.0], [59.3, 670.0], [59.4, 670.0], [59.5, 670.0], [59.6, 670.0], [59.7, 670.0], [59.8, 670.0], [59.9, 670.0], [60.0, 671.0], [60.1, 671.0], [60.2, 671.0], [60.3, 671.0], [60.4, 671.0], [60.5, 671.0], [60.6, 671.0], [60.7, 671.0], [60.8, 671.0], [60.9, 671.0], [61.0, 671.0], [61.1, 671.0], [61.2, 671.0], [61.3, 671.0], [61.4, 671.0], [61.5, 671.0], [61.6, 671.0], [61.7, 671.0], [61.8, 671.0], [61.9, 671.0], [62.0, 672.0], [62.1, 672.0], [62.2, 672.0], [62.3, 672.0], [62.4, 672.0], [62.5, 672.0], [62.6, 672.0], [62.7, 672.0], [62.8, 672.0], [62.9, 672.0], [63.0, 672.0], [63.1, 672.0], [63.2, 672.0], [63.3, 672.0], [63.4, 672.0], [63.5, 672.0], [63.6, 672.0], [63.7, 672.0], [63.8, 672.0], [63.9, 672.0], [64.0, 678.0], [64.1, 678.0], [64.2, 678.0], [64.3, 678.0], [64.4, 678.0], [64.5, 678.0], [64.6, 678.0], [64.7, 678.0], [64.8, 678.0], [64.9, 678.0], [65.0, 678.0], [65.1, 678.0], [65.2, 678.0], [65.3, 678.0], [65.4, 678.0], [65.5, 678.0], [65.6, 678.0], [65.7, 678.0], [65.8, 678.0], [65.9, 678.0], [66.0, 692.0], [66.1, 692.0], [66.2, 692.0], [66.3, 692.0], [66.4, 692.0], [66.5, 692.0], [66.6, 692.0], [66.7, 692.0], [66.8, 692.0], [66.9, 692.0], [67.0, 692.0], [67.1, 692.0], [67.2, 692.0], [67.3, 692.0], [67.4, 692.0], [67.5, 692.0], [67.6, 692.0], [67.7, 692.0], [67.8, 692.0], [67.9, 692.0], [68.0, 693.0], [68.1, 693.0], [68.2, 693.0], [68.3, 693.0], [68.4, 693.0], [68.5, 693.0], [68.6, 693.0], [68.7, 693.0], [68.8, 693.0], [68.9, 693.0], [69.0, 693.0], [69.1, 693.0], [69.2, 693.0], [69.3, 693.0], [69.4, 693.0], [69.5, 693.0], [69.6, 693.0], [69.7, 693.0], [69.8, 693.0], [69.9, 693.0], [70.0, 725.0], [70.1, 725.0], [70.2, 725.0], [70.3, 725.0], [70.4, 725.0], [70.5, 725.0], [70.6, 725.0], [70.7, 725.0], [70.8, 725.0], [70.9, 725.0], [71.0, 725.0], [71.1, 725.0], [71.2, 725.0], [71.3, 725.0], [71.4, 725.0], [71.5, 725.0], [71.6, 725.0], [71.7, 725.0], [71.8, 725.0], [71.9, 725.0], [72.0, 798.0], [72.1, 798.0], [72.2, 798.0], [72.3, 798.0], [72.4, 798.0], [72.5, 798.0], [72.6, 798.0], [72.7, 798.0], [72.8, 798.0], [72.9, 798.0], [73.0, 798.0], [73.1, 798.0], [73.2, 798.0], [73.3, 798.0], [73.4, 798.0], [73.5, 798.0], [73.6, 798.0], [73.7, 798.0], [73.8, 798.0], [73.9, 798.0], [74.0, 800.0], [74.1, 800.0], [74.2, 800.0], [74.3, 800.0], [74.4, 800.0], [74.5, 800.0], [74.6, 800.0], [74.7, 800.0], [74.8, 800.0], [74.9, 800.0], [75.0, 800.0], [75.1, 800.0], [75.2, 800.0], [75.3, 800.0], [75.4, 800.0], [75.5, 800.0], [75.6, 800.0], [75.7, 800.0], [75.8, 800.0], [75.9, 800.0], [76.0, 865.0], [76.1, 865.0], [76.2, 865.0], [76.3, 865.0], [76.4, 865.0], [76.5, 865.0], [76.6, 865.0], [76.7, 865.0], [76.8, 865.0], [76.9, 865.0], [77.0, 865.0], [77.1, 865.0], [77.2, 865.0], [77.3, 865.0], [77.4, 865.0], [77.5, 865.0], [77.6, 865.0], [77.7, 865.0], [77.8, 865.0], [77.9, 865.0], [78.0, 964.0], [78.1, 964.0], [78.2, 964.0], [78.3, 964.0], [78.4, 964.0], [78.5, 964.0], [78.6, 964.0], [78.7, 964.0], [78.8, 964.0], [78.9, 964.0], [79.0, 964.0], [79.1, 964.0], [79.2, 964.0], [79.3, 964.0], [79.4, 964.0], [79.5, 964.0], [79.6, 964.0], [79.7, 964.0], [79.8, 964.0], [79.9, 964.0], [80.0, 1107.0], [80.1, 1107.0], [80.2, 1107.0], [80.3, 1107.0], [80.4, 1107.0], [80.5, 1107.0], [80.6, 1107.0], [80.7, 1107.0], [80.8, 1107.0], [80.9, 1107.0], [81.0, 1107.0], [81.1, 1107.0], [81.2, 1107.0], [81.3, 1107.0], [81.4, 1107.0], [81.5, 1107.0], [81.6, 1107.0], [81.7, 1107.0], [81.8, 1107.0], [81.9, 1107.0], [82.0, 1181.0], [82.1, 1181.0], [82.2, 1181.0], [82.3, 1181.0], [82.4, 1181.0], [82.5, 1181.0], [82.6, 1181.0], [82.7, 1181.0], [82.8, 1181.0], [82.9, 1181.0], [83.0, 1181.0], [83.1, 1181.0], [83.2, 1181.0], [83.3, 1181.0], [83.4, 1181.0], [83.5, 1181.0], [83.6, 1181.0], [83.7, 1181.0], [83.8, 1181.0], [83.9, 1181.0], [84.0, 1181.0], [84.1, 1181.0], [84.2, 1181.0], [84.3, 1181.0], [84.4, 1181.0], [84.5, 1181.0], [84.6, 1181.0], [84.7, 1181.0], [84.8, 1181.0], [84.9, 1181.0], [85.0, 1181.0], [85.1, 1181.0], [85.2, 1181.0], [85.3, 1181.0], [85.4, 1181.0], [85.5, 1181.0], [85.6, 1181.0], [85.7, 1181.0], [85.8, 1181.0], [85.9, 1181.0], [86.0, 1228.0], [86.1, 1228.0], [86.2, 1228.0], [86.3, 1228.0], [86.4, 1228.0], [86.5, 1228.0], [86.6, 1228.0], [86.7, 1228.0], [86.8, 1228.0], [86.9, 1228.0], [87.0, 1228.0], [87.1, 1228.0], [87.2, 1228.0], [87.3, 1228.0], [87.4, 1228.0], [87.5, 1228.0], [87.6, 1228.0], [87.7, 1228.0], [87.8, 1228.0], [87.9, 1228.0], [88.0, 1273.0], [88.1, 1273.0], [88.2, 1273.0], [88.3, 1273.0], [88.4, 1273.0], [88.5, 1273.0], [88.6, 1273.0], [88.7, 1273.0], [88.8, 1273.0], [88.9, 1273.0], [89.0, 1273.0], [89.1, 1273.0], [89.2, 1273.0], [89.3, 1273.0], [89.4, 1273.0], [89.5, 1273.0], [89.6, 1273.0], [89.7, 1273.0], [89.8, 1273.0], [89.9, 1273.0], [90.0, 1445.0], [90.1, 1445.0], [90.2, 1445.0], [90.3, 1445.0], [90.4, 1445.0], [90.5, 1445.0], [90.6, 1445.0], [90.7, 1445.0], [90.8, 1445.0], [90.9, 1445.0], [91.0, 1445.0], [91.1, 1445.0], [91.2, 1445.0], [91.3, 1445.0], [91.4, 1445.0], [91.5, 1445.0], [91.6, 1445.0], [91.7, 1445.0], [91.8, 1445.0], [91.9, 1445.0], [92.0, 1477.0], [92.1, 1477.0], [92.2, 1477.0], [92.3, 1477.0], [92.4, 1477.0], [92.5, 1477.0], [92.6, 1477.0], [92.7, 1477.0], [92.8, 1477.0], [92.9, 1477.0], [93.0, 1477.0], [93.1, 1477.0], [93.2, 1477.0], [93.3, 1477.0], [93.4, 1477.0], [93.5, 1477.0], [93.6, 1477.0], [93.7, 1477.0], [93.8, 1477.0], [93.9, 1477.0], [94.0, 1489.0], [94.1, 1489.0], [94.2, 1489.0], [94.3, 1489.0], [94.4, 1489.0], [94.5, 1489.0], [94.6, 1489.0], [94.7, 1489.0], [94.8, 1489.0], [94.9, 1489.0], [95.0, 1489.0], [95.1, 1489.0], [95.2, 1489.0], [95.3, 1489.0], [95.4, 1489.0], [95.5, 1489.0], [95.6, 1489.0], [95.7, 1489.0], [95.8, 1489.0], [95.9, 1489.0], [96.0, 1550.0], [96.1, 1550.0], [96.2, 1550.0], [96.3, 1550.0], [96.4, 1550.0], [96.5, 1550.0], [96.6, 1550.0], [96.7, 1550.0], [96.8, 1550.0], [96.9, 1550.0], [97.0, 1550.0], [97.1, 1550.0], [97.2, 1550.0], [97.3, 1550.0], [97.4, 1550.0], [97.5, 1550.0], [97.6, 1550.0], [97.7, 1550.0], [97.8, 1550.0], [97.9, 1550.0], [98.0, 1906.0], [98.1, 1906.0], [98.2, 1906.0], [98.3, 1906.0], [98.4, 1906.0], [98.5, 1906.0], [98.6, 1906.0], [98.7, 1906.0], [98.8, 1906.0], [98.9, 1906.0], [99.0, 1906.0], [99.1, 1906.0], [99.2, 1906.0], [99.3, 1906.0], [99.4, 1906.0], [99.5, 1906.0], [99.6, 1906.0], [99.7, 1906.0], [99.8, 1906.0], [99.9, 1906.0]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 300.0, "maxY": 11.0, "series": [{"data": [[1100.0, 3.0], [300.0, 3.0], [600.0, 11.0], [1200.0, 2.0], [700.0, 2.0], [1400.0, 3.0], [1500.0, 1.0], [400.0, 11.0], [800.0, 2.0], [900.0, 1.0], [1900.0, 1.0], [500.0, 10.0]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 1900.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 2.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 34.0, "series": [{"data": [[0.0, 14.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 34.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 2.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 25.5, "minX": 1.62291714E12, "maxY": 25.5, "series": [{"data": [[1.62291714E12, 25.5]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291714E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 353.0, "minX": 1.0, "maxY": 1906.0, "series": [{"data": [[2.0, 1181.0], [3.0, 1273.0], [4.0, 1445.0], [5.0, 1906.0], [6.0, 964.0], [7.0, 1477.0], [8.0, 1489.0], [9.0, 1550.0], [10.0, 1181.0], [11.0, 1228.0], [12.0, 800.0], [13.0, 610.0], [14.0, 581.0], [15.0, 490.0], [16.0, 477.0], [17.0, 671.0], [18.0, 639.0], [19.0, 565.0], [20.0, 646.0], [21.0, 672.0], [22.0, 539.0], [23.0, 565.0], [24.0, 670.0], [25.0, 581.0], [26.0, 692.0], [27.0, 553.0], [28.0, 623.0], [29.0, 865.0], [30.0, 629.0], [31.0, 798.0], [33.0, 560.0], [32.0, 493.0], [35.0, 589.0], [34.0, 368.0], [37.0, 678.0], [36.0, 725.0], [39.0, 495.0], [38.0, 693.0], [41.0, 443.0], [40.0, 516.0], [43.0, 460.0], [42.0, 386.0], [45.0, 515.0], [44.0, 488.0], [47.0, 410.0], [46.0, 414.0], [49.0, 353.0], [48.0, 408.0], [50.0, 427.0], [1.0, 1107.0]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}, {"data": [[25.5, 737.7600000000001]], "isOverall": false, "label": "HTTP Request  logging into my account-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 50.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 473.3333333333333, "minX": 1.62291714E12, "maxY": 8439.166666666666, "series": [{"data": [[1.62291714E12, 8439.166666666666]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62291714E12, 473.3333333333333]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291714E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 737.7600000000001, "minX": 1.62291714E12, "maxY": 737.7600000000001, "series": [{"data": [[1.62291714E12, 737.7600000000001]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291714E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 736.1200000000001, "minX": 1.62291714E12, "maxY": 736.1200000000001, "series": [{"data": [[1.62291714E12, 736.1200000000001]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291714E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 1.86, "minX": 1.62291714E12, "maxY": 1.86, "series": [{"data": [[1.62291714E12, 1.86]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291714E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 353.0, "minX": 1.62291714E12, "maxY": 1906.0, "series": [{"data": [[1.62291714E12, 1906.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62291714E12, 353.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62291714E12, 1427.7999999999997]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62291714E12, 1906.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62291714E12, 1516.4499999999998]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291714E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 406.5, "minX": 1.0, "maxY": 1359.0, "series": [{"data": [[2.0, 406.5], [4.0, 642.5], [1.0, 670.0], [5.0, 1359.0], [3.0, 539.0], [6.0, 626.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 6.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 405.0, "minX": 1.0, "maxY": 1357.5, "series": [{"data": [[2.0, 405.0], [4.0, 641.0], [1.0, 669.0], [5.0, 1357.5], [3.0, 537.0], [6.0, 625.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 6.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.8333333333333334, "minX": 1.62291714E12, "maxY": 0.8333333333333334, "series": [{"data": [[1.62291714E12, 0.8333333333333334]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291714E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.8333333333333334, "minX": 1.62291714E12, "maxY": 0.8333333333333334, "series": [{"data": [[1.62291714E12, 0.8333333333333334]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291714E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.8333333333333334, "minX": 1.62291714E12, "maxY": 0.8333333333333334, "series": [{"data": [[1.62291714E12, 0.8333333333333334]], "isOverall": false, "label": "HTTP Request  logging into my account-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291714E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.8333333333333334, "minX": 1.62291714E12, "maxY": 0.8333333333333334, "series": [{"data": [[1.62291714E12, 0.8333333333333334]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291714E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

