/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 387.0, "minX": 0.0, "maxY": 8578.0, "series": [{"data": [[0.0, 387.0], [0.1, 387.0], [0.2, 387.0], [0.3, 387.0], [0.4, 387.0], [0.5, 387.0], [0.6, 387.0], [0.7, 387.0], [0.8, 387.0], [0.9, 387.0], [1.0, 630.0], [1.1, 630.0], [1.2, 630.0], [1.3, 630.0], [1.4, 630.0], [1.5, 630.0], [1.6, 630.0], [1.7, 630.0], [1.8, 630.0], [1.9, 630.0], [2.0, 639.0], [2.1, 639.0], [2.2, 639.0], [2.3, 639.0], [2.4, 639.0], [2.5, 639.0], [2.6, 639.0], [2.7, 639.0], [2.8, 639.0], [2.9, 639.0], [3.0, 725.0], [3.1, 725.0], [3.2, 725.0], [3.3, 725.0], [3.4, 725.0], [3.5, 725.0], [3.6, 725.0], [3.7, 725.0], [3.8, 725.0], [3.9, 725.0], [4.0, 794.0], [4.1, 794.0], [4.2, 794.0], [4.3, 794.0], [4.4, 794.0], [4.5, 794.0], [4.6, 794.0], [4.7, 794.0], [4.8, 794.0], [4.9, 794.0], [5.0, 964.0], [5.1, 964.0], [5.2, 964.0], [5.3, 964.0], [5.4, 964.0], [5.5, 964.0], [5.6, 964.0], [5.7, 964.0], [5.8, 964.0], [5.9, 964.0], [6.0, 1006.0], [6.1, 1006.0], [6.2, 1006.0], [6.3, 1006.0], [6.4, 1006.0], [6.5, 1006.0], [6.6, 1006.0], [6.7, 1006.0], [6.8, 1006.0], [6.9, 1006.0], [7.0, 1132.0], [7.1, 1132.0], [7.2, 1132.0], [7.3, 1132.0], [7.4, 1132.0], [7.5, 1132.0], [7.6, 1132.0], [7.7, 1132.0], [7.8, 1132.0], [7.9, 1132.0], [8.0, 1174.0], [8.1, 1174.0], [8.2, 1174.0], [8.3, 1174.0], [8.4, 1174.0], [8.5, 1174.0], [8.6, 1174.0], [8.7, 1174.0], [8.8, 1174.0], [8.9, 1174.0], [9.0, 1189.0], [9.1, 1189.0], [9.2, 1189.0], [9.3, 1189.0], [9.4, 1189.0], [9.5, 1189.0], [9.6, 1189.0], [9.7, 1189.0], [9.8, 1189.0], [9.9, 1189.0], [10.0, 1197.0], [10.1, 1197.0], [10.2, 1197.0], [10.3, 1197.0], [10.4, 1197.0], [10.5, 1197.0], [10.6, 1197.0], [10.7, 1197.0], [10.8, 1197.0], [10.9, 1197.0], [11.0, 1342.0], [11.1, 1342.0], [11.2, 1342.0], [11.3, 1342.0], [11.4, 1342.0], [11.5, 1342.0], [11.6, 1342.0], [11.7, 1342.0], [11.8, 1342.0], [11.9, 1342.0], [12.0, 1373.0], [12.1, 1373.0], [12.2, 1373.0], [12.3, 1373.0], [12.4, 1373.0], [12.5, 1373.0], [12.6, 1373.0], [12.7, 1373.0], [12.8, 1373.0], [12.9, 1373.0], [13.0, 1505.0], [13.1, 1505.0], [13.2, 1505.0], [13.3, 1505.0], [13.4, 1505.0], [13.5, 1505.0], [13.6, 1505.0], [13.7, 1505.0], [13.8, 1505.0], [13.9, 1505.0], [14.0, 1540.0], [14.1, 1540.0], [14.2, 1540.0], [14.3, 1540.0], [14.4, 1540.0], [14.5, 1540.0], [14.6, 1540.0], [14.7, 1540.0], [14.8, 1540.0], [14.9, 1540.0], [15.0, 1549.0], [15.1, 1549.0], [15.2, 1549.0], [15.3, 1549.0], [15.4, 1549.0], [15.5, 1549.0], [15.6, 1549.0], [15.7, 1549.0], [15.8, 1549.0], [15.9, 1549.0], [16.0, 1641.0], [16.1, 1641.0], [16.2, 1641.0], [16.3, 1641.0], [16.4, 1641.0], [16.5, 1641.0], [16.6, 1641.0], [16.7, 1641.0], [16.8, 1641.0], [16.9, 1641.0], [17.0, 1705.0], [17.1, 1705.0], [17.2, 1705.0], [17.3, 1705.0], [17.4, 1705.0], [17.5, 1705.0], [17.6, 1705.0], [17.7, 1705.0], [17.8, 1705.0], [17.9, 1705.0], [18.0, 1711.0], [18.1, 1711.0], [18.2, 1711.0], [18.3, 1711.0], [18.4, 1711.0], [18.5, 1711.0], [18.6, 1711.0], [18.7, 1711.0], [18.8, 1711.0], [18.9, 1711.0], [19.0, 1719.0], [19.1, 1719.0], [19.2, 1719.0], [19.3, 1719.0], [19.4, 1719.0], [19.5, 1719.0], [19.6, 1719.0], [19.7, 1719.0], [19.8, 1719.0], [19.9, 1719.0], [20.0, 1766.0], [20.1, 1766.0], [20.2, 1766.0], [20.3, 1766.0], [20.4, 1766.0], [20.5, 1766.0], [20.6, 1766.0], [20.7, 1766.0], [20.8, 1766.0], [20.9, 1766.0], [21.0, 1770.0], [21.1, 1770.0], [21.2, 1770.0], [21.3, 1770.0], [21.4, 1770.0], [21.5, 1770.0], [21.6, 1770.0], [21.7, 1770.0], [21.8, 1770.0], [21.9, 1770.0], [22.0, 1770.0], [22.1, 1770.0], [22.2, 1770.0], [22.3, 1770.0], [22.4, 1770.0], [22.5, 1770.0], [22.6, 1770.0], [22.7, 1770.0], [22.8, 1770.0], [22.9, 1770.0], [23.0, 2084.0], [23.1, 2084.0], [23.2, 2084.0], [23.3, 2084.0], [23.4, 2084.0], [23.5, 2084.0], [23.6, 2084.0], [23.7, 2084.0], [23.8, 2084.0], [23.9, 2084.0], [24.0, 2084.0], [24.1, 2084.0], [24.2, 2084.0], [24.3, 2084.0], [24.4, 2084.0], [24.5, 2084.0], [24.6, 2084.0], [24.7, 2084.0], [24.8, 2084.0], [24.9, 2084.0], [25.0, 2268.0], [25.1, 2268.0], [25.2, 2268.0], [25.3, 2268.0], [25.4, 2268.0], [25.5, 2268.0], [25.6, 2268.0], [25.7, 2268.0], [25.8, 2268.0], [25.9, 2268.0], [26.0, 2301.0], [26.1, 2301.0], [26.2, 2301.0], [26.3, 2301.0], [26.4, 2301.0], [26.5, 2301.0], [26.6, 2301.0], [26.7, 2301.0], [26.8, 2301.0], [26.9, 2301.0], [27.0, 2316.0], [27.1, 2316.0], [27.2, 2316.0], [27.3, 2316.0], [27.4, 2316.0], [27.5, 2316.0], [27.6, 2316.0], [27.7, 2316.0], [27.8, 2316.0], [27.9, 2316.0], [28.0, 2690.0], [28.1, 2690.0], [28.2, 2690.0], [28.3, 2690.0], [28.4, 2690.0], [28.5, 2690.0], [28.6, 2690.0], [28.7, 2690.0], [28.8, 2690.0], [28.9, 2690.0], [29.0, 2771.0], [29.1, 2771.0], [29.2, 2771.0], [29.3, 2771.0], [29.4, 2771.0], [29.5, 2771.0], [29.6, 2771.0], [29.7, 2771.0], [29.8, 2771.0], [29.9, 2771.0], [30.0, 2816.0], [30.1, 2816.0], [30.2, 2816.0], [30.3, 2816.0], [30.4, 2816.0], [30.5, 2816.0], [30.6, 2816.0], [30.7, 2816.0], [30.8, 2816.0], [30.9, 2816.0], [31.0, 2842.0], [31.1, 2842.0], [31.2, 2842.0], [31.3, 2842.0], [31.4, 2842.0], [31.5, 2842.0], [31.6, 2842.0], [31.7, 2842.0], [31.8, 2842.0], [31.9, 2842.0], [32.0, 2850.0], [32.1, 2850.0], [32.2, 2850.0], [32.3, 2850.0], [32.4, 2850.0], [32.5, 2850.0], [32.6, 2850.0], [32.7, 2850.0], [32.8, 2850.0], [32.9, 2850.0], [33.0, 3070.0], [33.1, 3070.0], [33.2, 3070.0], [33.3, 3070.0], [33.4, 3070.0], [33.5, 3070.0], [33.6, 3070.0], [33.7, 3070.0], [33.8, 3070.0], [33.9, 3070.0], [34.0, 3208.0], [34.1, 3208.0], [34.2, 3208.0], [34.3, 3208.0], [34.4, 3208.0], [34.5, 3208.0], [34.6, 3208.0], [34.7, 3208.0], [34.8, 3208.0], [34.9, 3208.0], [35.0, 3538.0], [35.1, 3538.0], [35.2, 3538.0], [35.3, 3538.0], [35.4, 3538.0], [35.5, 3538.0], [35.6, 3538.0], [35.7, 3538.0], [35.8, 3538.0], [35.9, 3538.0], [36.0, 3854.0], [36.1, 3854.0], [36.2, 3854.0], [36.3, 3854.0], [36.4, 3854.0], [36.5, 3854.0], [36.6, 3854.0], [36.7, 3854.0], [36.8, 3854.0], [36.9, 3854.0], [37.0, 4051.0], [37.1, 4051.0], [37.2, 4051.0], [37.3, 4051.0], [37.4, 4051.0], [37.5, 4051.0], [37.6, 4051.0], [37.7, 4051.0], [37.8, 4051.0], [37.9, 4051.0], [38.0, 4121.0], [38.1, 4121.0], [38.2, 4121.0], [38.3, 4121.0], [38.4, 4121.0], [38.5, 4121.0], [38.6, 4121.0], [38.7, 4121.0], [38.8, 4121.0], [38.9, 4121.0], [39.0, 4249.0], [39.1, 4249.0], [39.2, 4249.0], [39.3, 4249.0], [39.4, 4249.0], [39.5, 4249.0], [39.6, 4249.0], [39.7, 4249.0], [39.8, 4249.0], [39.9, 4249.0], [40.0, 4260.0], [40.1, 4260.0], [40.2, 4260.0], [40.3, 4260.0], [40.4, 4260.0], [40.5, 4260.0], [40.6, 4260.0], [40.7, 4260.0], [40.8, 4260.0], [40.9, 4260.0], [41.0, 4348.0], [41.1, 4348.0], [41.2, 4348.0], [41.3, 4348.0], [41.4, 4348.0], [41.5, 4348.0], [41.6, 4348.0], [41.7, 4348.0], [41.8, 4348.0], [41.9, 4348.0], [42.0, 4392.0], [42.1, 4392.0], [42.2, 4392.0], [42.3, 4392.0], [42.4, 4392.0], [42.5, 4392.0], [42.6, 4392.0], [42.7, 4392.0], [42.8, 4392.0], [42.9, 4392.0], [43.0, 4401.0], [43.1, 4401.0], [43.2, 4401.0], [43.3, 4401.0], [43.4, 4401.0], [43.5, 4401.0], [43.6, 4401.0], [43.7, 4401.0], [43.8, 4401.0], [43.9, 4401.0], [44.0, 4512.0], [44.1, 4512.0], [44.2, 4512.0], [44.3, 4512.0], [44.4, 4512.0], [44.5, 4512.0], [44.6, 4512.0], [44.7, 4512.0], [44.8, 4512.0], [44.9, 4512.0], [45.0, 4519.0], [45.1, 4519.0], [45.2, 4519.0], [45.3, 4519.0], [45.4, 4519.0], [45.5, 4519.0], [45.6, 4519.0], [45.7, 4519.0], [45.8, 4519.0], [45.9, 4519.0], [46.0, 4533.0], [46.1, 4533.0], [46.2, 4533.0], [46.3, 4533.0], [46.4, 4533.0], [46.5, 4533.0], [46.6, 4533.0], [46.7, 4533.0], [46.8, 4533.0], [46.9, 4533.0], [47.0, 4604.0], [47.1, 4604.0], [47.2, 4604.0], [47.3, 4604.0], [47.4, 4604.0], [47.5, 4604.0], [47.6, 4604.0], [47.7, 4604.0], [47.8, 4604.0], [47.9, 4604.0], [48.0, 4654.0], [48.1, 4654.0], [48.2, 4654.0], [48.3, 4654.0], [48.4, 4654.0], [48.5, 4654.0], [48.6, 4654.0], [48.7, 4654.0], [48.8, 4654.0], [48.9, 4654.0], [49.0, 4680.0], [49.1, 4680.0], [49.2, 4680.0], [49.3, 4680.0], [49.4, 4680.0], [49.5, 4680.0], [49.6, 4680.0], [49.7, 4680.0], [49.8, 4680.0], [49.9, 4680.0], [50.0, 4718.0], [50.1, 4718.0], [50.2, 4718.0], [50.3, 4718.0], [50.4, 4718.0], [50.5, 4718.0], [50.6, 4718.0], [50.7, 4718.0], [50.8, 4718.0], [50.9, 4718.0], [51.0, 4739.0], [51.1, 4739.0], [51.2, 4739.0], [51.3, 4739.0], [51.4, 4739.0], [51.5, 4739.0], [51.6, 4739.0], [51.7, 4739.0], [51.8, 4739.0], [51.9, 4739.0], [52.0, 4839.0], [52.1, 4839.0], [52.2, 4839.0], [52.3, 4839.0], [52.4, 4839.0], [52.5, 4839.0], [52.6, 4839.0], [52.7, 4839.0], [52.8, 4839.0], [52.9, 4839.0], [53.0, 4844.0], [53.1, 4844.0], [53.2, 4844.0], [53.3, 4844.0], [53.4, 4844.0], [53.5, 4844.0], [53.6, 4844.0], [53.7, 4844.0], [53.8, 4844.0], [53.9, 4844.0], [54.0, 4894.0], [54.1, 4894.0], [54.2, 4894.0], [54.3, 4894.0], [54.4, 4894.0], [54.5, 4894.0], [54.6, 4894.0], [54.7, 4894.0], [54.8, 4894.0], [54.9, 4894.0], [55.0, 5102.0], [55.1, 5102.0], [55.2, 5102.0], [55.3, 5102.0], [55.4, 5102.0], [55.5, 5102.0], [55.6, 5102.0], [55.7, 5102.0], [55.8, 5102.0], [55.9, 5102.0], [56.0, 5113.0], [56.1, 5113.0], [56.2, 5113.0], [56.3, 5113.0], [56.4, 5113.0], [56.5, 5113.0], [56.6, 5113.0], [56.7, 5113.0], [56.8, 5113.0], [56.9, 5113.0], [57.0, 5412.0], [57.1, 5412.0], [57.2, 5412.0], [57.3, 5412.0], [57.4, 5412.0], [57.5, 5412.0], [57.6, 5412.0], [57.7, 5412.0], [57.8, 5412.0], [57.9, 5412.0], [58.0, 5465.0], [58.1, 5465.0], [58.2, 5465.0], [58.3, 5465.0], [58.4, 5465.0], [58.5, 5465.0], [58.6, 5465.0], [58.7, 5465.0], [58.8, 5465.0], [58.9, 5465.0], [59.0, 5613.0], [59.1, 5613.0], [59.2, 5613.0], [59.3, 5613.0], [59.4, 5613.0], [59.5, 5613.0], [59.6, 5613.0], [59.7, 5613.0], [59.8, 5613.0], [59.9, 5613.0], [60.0, 5676.0], [60.1, 5676.0], [60.2, 5676.0], [60.3, 5676.0], [60.4, 5676.0], [60.5, 5676.0], [60.6, 5676.0], [60.7, 5676.0], [60.8, 5676.0], [60.9, 5676.0], [61.0, 5709.0], [61.1, 5709.0], [61.2, 5709.0], [61.3, 5709.0], [61.4, 5709.0], [61.5, 5709.0], [61.6, 5709.0], [61.7, 5709.0], [61.8, 5709.0], [61.9, 5709.0], [62.0, 5712.0], [62.1, 5712.0], [62.2, 5712.0], [62.3, 5712.0], [62.4, 5712.0], [62.5, 5712.0], [62.6, 5712.0], [62.7, 5712.0], [62.8, 5712.0], [62.9, 5712.0], [63.0, 5936.0], [63.1, 5936.0], [63.2, 5936.0], [63.3, 5936.0], [63.4, 5936.0], [63.5, 5936.0], [63.6, 5936.0], [63.7, 5936.0], [63.8, 5936.0], [63.9, 5936.0], [64.0, 5986.0], [64.1, 5986.0], [64.2, 5986.0], [64.3, 5986.0], [64.4, 5986.0], [64.5, 5986.0], [64.6, 5986.0], [64.7, 5986.0], [64.8, 5986.0], [64.9, 5986.0], [65.0, 6407.0], [65.1, 6407.0], [65.2, 6407.0], [65.3, 6407.0], [65.4, 6407.0], [65.5, 6407.0], [65.6, 6407.0], [65.7, 6407.0], [65.8, 6407.0], [65.9, 6407.0], [66.0, 6498.0], [66.1, 6498.0], [66.2, 6498.0], [66.3, 6498.0], [66.4, 6498.0], [66.5, 6498.0], [66.6, 6498.0], [66.7, 6498.0], [66.8, 6498.0], [66.9, 6498.0], [67.0, 6692.0], [67.1, 6692.0], [67.2, 6692.0], [67.3, 6692.0], [67.4, 6692.0], [67.5, 6692.0], [67.6, 6692.0], [67.7, 6692.0], [67.8, 6692.0], [67.9, 6692.0], [68.0, 6867.0], [68.1, 6867.0], [68.2, 6867.0], [68.3, 6867.0], [68.4, 6867.0], [68.5, 6867.0], [68.6, 6867.0], [68.7, 6867.0], [68.8, 6867.0], [68.9, 6867.0], [69.0, 6910.0], [69.1, 6910.0], [69.2, 6910.0], [69.3, 6910.0], [69.4, 6910.0], [69.5, 6910.0], [69.6, 6910.0], [69.7, 6910.0], [69.8, 6910.0], [69.9, 6910.0], [70.0, 7042.0], [70.1, 7042.0], [70.2, 7042.0], [70.3, 7042.0], [70.4, 7042.0], [70.5, 7042.0], [70.6, 7042.0], [70.7, 7042.0], [70.8, 7042.0], [70.9, 7042.0], [71.0, 7082.0], [71.1, 7082.0], [71.2, 7082.0], [71.3, 7082.0], [71.4, 7082.0], [71.5, 7082.0], [71.6, 7082.0], [71.7, 7082.0], [71.8, 7082.0], [71.9, 7082.0], [72.0, 7104.0], [72.1, 7104.0], [72.2, 7104.0], [72.3, 7104.0], [72.4, 7104.0], [72.5, 7104.0], [72.6, 7104.0], [72.7, 7104.0], [72.8, 7104.0], [72.9, 7104.0], [73.0, 7228.0], [73.1, 7228.0], [73.2, 7228.0], [73.3, 7228.0], [73.4, 7228.0], [73.5, 7228.0], [73.6, 7228.0], [73.7, 7228.0], [73.8, 7228.0], [73.9, 7228.0], [74.0, 7272.0], [74.1, 7272.0], [74.2, 7272.0], [74.3, 7272.0], [74.4, 7272.0], [74.5, 7272.0], [74.6, 7272.0], [74.7, 7272.0], [74.8, 7272.0], [74.9, 7272.0], [75.0, 7350.0], [75.1, 7350.0], [75.2, 7350.0], [75.3, 7350.0], [75.4, 7350.0], [75.5, 7350.0], [75.6, 7350.0], [75.7, 7350.0], [75.8, 7350.0], [75.9, 7350.0], [76.0, 7426.0], [76.1, 7426.0], [76.2, 7426.0], [76.3, 7426.0], [76.4, 7426.0], [76.5, 7426.0], [76.6, 7426.0], [76.7, 7426.0], [76.8, 7426.0], [76.9, 7426.0], [77.0, 7431.0], [77.1, 7431.0], [77.2, 7431.0], [77.3, 7431.0], [77.4, 7431.0], [77.5, 7431.0], [77.6, 7431.0], [77.7, 7431.0], [77.8, 7431.0], [77.9, 7431.0], [78.0, 7452.0], [78.1, 7452.0], [78.2, 7452.0], [78.3, 7452.0], [78.4, 7452.0], [78.5, 7452.0], [78.6, 7452.0], [78.7, 7452.0], [78.8, 7452.0], [78.9, 7452.0], [79.0, 7461.0], [79.1, 7461.0], [79.2, 7461.0], [79.3, 7461.0], [79.4, 7461.0], [79.5, 7461.0], [79.6, 7461.0], [79.7, 7461.0], [79.8, 7461.0], [79.9, 7461.0], [80.0, 7499.0], [80.1, 7499.0], [80.2, 7499.0], [80.3, 7499.0], [80.4, 7499.0], [80.5, 7499.0], [80.6, 7499.0], [80.7, 7499.0], [80.8, 7499.0], [80.9, 7499.0], [81.0, 7504.0], [81.1, 7504.0], [81.2, 7504.0], [81.3, 7504.0], [81.4, 7504.0], [81.5, 7504.0], [81.6, 7504.0], [81.7, 7504.0], [81.8, 7504.0], [81.9, 7504.0], [82.0, 7543.0], [82.1, 7543.0], [82.2, 7543.0], [82.3, 7543.0], [82.4, 7543.0], [82.5, 7543.0], [82.6, 7543.0], [82.7, 7543.0], [82.8, 7543.0], [82.9, 7543.0], [83.0, 7554.0], [83.1, 7554.0], [83.2, 7554.0], [83.3, 7554.0], [83.4, 7554.0], [83.5, 7554.0], [83.6, 7554.0], [83.7, 7554.0], [83.8, 7554.0], [83.9, 7554.0], [84.0, 7758.0], [84.1, 7758.0], [84.2, 7758.0], [84.3, 7758.0], [84.4, 7758.0], [84.5, 7758.0], [84.6, 7758.0], [84.7, 7758.0], [84.8, 7758.0], [84.9, 7758.0], [85.0, 7788.0], [85.1, 7788.0], [85.2, 7788.0], [85.3, 7788.0], [85.4, 7788.0], [85.5, 7788.0], [85.6, 7788.0], [85.7, 7788.0], [85.8, 7788.0], [85.9, 7788.0], [86.0, 7792.0], [86.1, 7792.0], [86.2, 7792.0], [86.3, 7792.0], [86.4, 7792.0], [86.5, 7792.0], [86.6, 7792.0], [86.7, 7792.0], [86.8, 7792.0], [86.9, 7792.0], [87.0, 7830.0], [87.1, 7830.0], [87.2, 7830.0], [87.3, 7830.0], [87.4, 7830.0], [87.5, 7830.0], [87.6, 7830.0], [87.7, 7830.0], [87.8, 7830.0], [87.9, 7830.0], [88.0, 7831.0], [88.1, 7831.0], [88.2, 7831.0], [88.3, 7831.0], [88.4, 7831.0], [88.5, 7831.0], [88.6, 7831.0], [88.7, 7831.0], [88.8, 7831.0], [88.9, 7831.0], [89.0, 7839.0], [89.1, 7839.0], [89.2, 7839.0], [89.3, 7839.0], [89.4, 7839.0], [89.5, 7839.0], [89.6, 7839.0], [89.7, 7839.0], [89.8, 7839.0], [89.9, 7839.0], [90.0, 7972.0], [90.1, 7972.0], [90.2, 7972.0], [90.3, 7972.0], [90.4, 7972.0], [90.5, 7972.0], [90.6, 7972.0], [90.7, 7972.0], [90.8, 7972.0], [90.9, 7972.0], [91.0, 8006.0], [91.1, 8006.0], [91.2, 8006.0], [91.3, 8006.0], [91.4, 8006.0], [91.5, 8006.0], [91.6, 8006.0], [91.7, 8006.0], [91.8, 8006.0], [91.9, 8006.0], [92.0, 8009.0], [92.1, 8009.0], [92.2, 8009.0], [92.3, 8009.0], [92.4, 8009.0], [92.5, 8009.0], [92.6, 8009.0], [92.7, 8009.0], [92.8, 8009.0], [92.9, 8009.0], [93.0, 8022.0], [93.1, 8022.0], [93.2, 8022.0], [93.3, 8022.0], [93.4, 8022.0], [93.5, 8022.0], [93.6, 8022.0], [93.7, 8022.0], [93.8, 8022.0], [93.9, 8022.0], [94.0, 8032.0], [94.1, 8032.0], [94.2, 8032.0], [94.3, 8032.0], [94.4, 8032.0], [94.5, 8032.0], [94.6, 8032.0], [94.7, 8032.0], [94.8, 8032.0], [94.9, 8032.0], [95.0, 8032.0], [95.1, 8032.0], [95.2, 8032.0], [95.3, 8032.0], [95.4, 8032.0], [95.5, 8032.0], [95.6, 8032.0], [95.7, 8032.0], [95.8, 8032.0], [95.9, 8032.0], [96.0, 8053.0], [96.1, 8053.0], [96.2, 8053.0], [96.3, 8053.0], [96.4, 8053.0], [96.5, 8053.0], [96.6, 8053.0], [96.7, 8053.0], [96.8, 8053.0], [96.9, 8053.0], [97.0, 8442.0], [97.1, 8442.0], [97.2, 8442.0], [97.3, 8442.0], [97.4, 8442.0], [97.5, 8442.0], [97.6, 8442.0], [97.7, 8442.0], [97.8, 8442.0], [97.9, 8442.0], [98.0, 8463.0], [98.1, 8463.0], [98.2, 8463.0], [98.3, 8463.0], [98.4, 8463.0], [98.5, 8463.0], [98.6, 8463.0], [98.7, 8463.0], [98.8, 8463.0], [98.9, 8463.0], [99.0, 8578.0], [99.1, 8578.0], [99.2, 8578.0], [99.3, 8578.0], [99.4, 8578.0], [99.5, 8578.0], [99.6, 8578.0], [99.7, 8578.0], [99.8, 8578.0], [99.9, 8578.0]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 300.0, "maxY": 6.0, "series": [{"data": [[600.0, 2.0], [700.0, 2.0], [900.0, 1.0], [1000.0, 1.0], [1100.0, 4.0], [1300.0, 2.0], [1500.0, 3.0], [1600.0, 1.0], [1700.0, 6.0], [2000.0, 2.0], [2300.0, 2.0], [2200.0, 1.0], [2600.0, 1.0], [2800.0, 3.0], [2700.0, 1.0], [3000.0, 1.0], [3200.0, 1.0], [3500.0, 1.0], [3800.0, 1.0], [4000.0, 1.0], [4200.0, 2.0], [4300.0, 2.0], [4100.0, 1.0], [4500.0, 3.0], [4600.0, 3.0], [4400.0, 1.0], [4800.0, 3.0], [4700.0, 2.0], [5100.0, 2.0], [5400.0, 2.0], [5600.0, 2.0], [5700.0, 2.0], [5900.0, 2.0], [6400.0, 2.0], [6600.0, 1.0], [6800.0, 1.0], [6900.0, 1.0], [7000.0, 2.0], [7100.0, 1.0], [7200.0, 2.0], [7400.0, 5.0], [7300.0, 1.0], [7500.0, 3.0], [7800.0, 3.0], [7700.0, 3.0], [7900.0, 1.0], [8000.0, 6.0], [8400.0, 2.0], [8500.0, 1.0], [300.0, 1.0]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 8500.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 87.0, "series": [{"data": [[0.0, 1.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 12.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 87.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 4.142857142857143, "minX": 1.6229172E12, "maxY": 54.0, "series": [{"data": [[1.6229172E12, 54.0], [1.62291726E12, 4.142857142857143]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291726E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 387.0, "minX": 1.0, "maxY": 8578.0, "series": [{"data": [[2.0, 8009.0], [3.0, 8442.0], [4.0, 8032.0], [5.0, 8578.0], [7.0, 8247.5], [8.0, 7792.0], [9.0, 7830.0], [10.0, 7839.0], [11.0, 7972.0], [12.0, 7499.0], [13.0, 8053.0], [14.0, 7461.0], [15.0, 7504.0], [16.0, 7350.0], [17.0, 7104.0], [18.0, 8022.0], [19.0, 7426.0], [20.0, 7758.0], [21.0, 7788.0], [22.0, 7554.0], [23.0, 7543.0], [24.0, 7452.0], [25.0, 7831.0], [26.0, 7431.0], [27.0, 7082.0], [28.0, 7272.0], [29.0, 7228.0], [30.0, 7042.0], [31.0, 6910.0], [33.0, 6867.0], [32.0, 6692.0], [35.0, 6498.0], [34.0, 6407.0], [37.0, 5986.0], [36.0, 5936.0], [39.0, 5709.0], [38.0, 5712.0], [41.0, 5676.0], [40.0, 5613.0], [43.0, 5465.0], [42.0, 5412.0], [45.0, 5102.0], [44.0, 5113.0], [47.0, 4718.0], [46.0, 4894.0], [49.0, 4844.0], [48.0, 4654.0], [51.0, 4348.0], [50.0, 4519.0], [53.0, 4249.0], [52.0, 4401.0], [55.0, 4739.0], [54.0, 4121.0], [57.0, 4680.0], [56.0, 4604.0], [59.0, 4839.0], [58.0, 4512.0], [61.0, 4392.0], [60.0, 4533.0], [63.0, 4051.0], [62.0, 4260.0], [67.0, 3070.0], [66.0, 3208.0], [65.0, 3538.0], [64.0, 3854.0], [71.0, 2690.0], [70.0, 2771.0], [69.0, 2850.0], [68.0, 2816.0], [75.0, 2316.0], [74.0, 2268.0], [73.0, 2301.0], [72.0, 2842.0], [79.0, 1641.0], [78.0, 2084.0], [77.0, 1719.0], [76.0, 2084.0], [83.0, 1770.0], [82.0, 1342.0], [81.0, 1711.0], [80.0, 1705.0], [87.0, 964.0], [86.0, 1540.0], [85.0, 1770.0], [84.0, 1766.0], [91.0, 1549.0], [90.0, 1174.0], [89.0, 1505.0], [88.0, 1006.0], [95.0, 1132.0], [94.0, 1373.0], [93.0, 1189.0], [92.0, 1197.0], [99.0, 630.0], [98.0, 639.0], [97.0, 794.0], [96.0, 725.0], [100.0, 387.0], [1.0, 8006.0]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}, {"data": [[50.510000000000005, 4697.710000000003]], "isOverall": false, "label": "HTTP Request  logging into my account-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 66.26666666666667, "minX": 1.6229172E12, "maxY": 15696.85, "series": [{"data": [[1.6229172E12, 15696.85], [1.62291726E12, 1181.4833333333333]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.6229172E12, 880.4], [1.62291726E12, 66.26666666666667]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291726E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 4432.35483870968, "minX": 1.6229172E12, "maxY": 8223.142857142857, "series": [{"data": [[1.6229172E12, 4432.35483870968], [1.62291726E12, 8223.142857142857]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291726E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 4430.655913978496, "minX": 1.6229172E12, "maxY": 8221.142857142857, "series": [{"data": [[1.6229172E12, 4430.655913978496], [1.62291726E12, 8221.142857142857]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291726E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 1.5806451612903223, "minX": 1.6229172E12, "maxY": 1.7142857142857144, "series": [{"data": [[1.6229172E12, 1.5806451612903223], [1.62291726E12, 1.7142857142857144]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291726E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 387.0, "minX": 1.6229172E12, "maxY": 8578.0, "series": [{"data": [[1.6229172E12, 8053.0], [1.62291726E12, 8578.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.6229172E12, 387.0], [1.62291726E12, 8006.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.6229172E12, 7676.4000000000015], [1.62291726E12, 8578.0]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.6229172E12, 8053.0], [1.62291726E12, 8578.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.6229172E12, 7833.4], [1.62291726E12, 8578.0]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291726E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 508.5, "minX": 2.0, "maxY": 5963.0, "series": [{"data": [[2.0, 508.5], [4.0, 3964.5], [5.0, 5412.0], [6.0, 5963.0], [3.0, 4593.5], [7.0, 2842.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 7.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 507.0, "minX": 2.0, "maxY": 5961.0, "series": [{"data": [[2.0, 507.0], [4.0, 3963.5], [5.0, 5410.0], [6.0, 5961.0], [3.0, 4592.5], [7.0, 2841.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 7.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.6229172E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.6229172E12, 1.6666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229172E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.11666666666666667, "minX": 1.6229172E12, "maxY": 1.55, "series": [{"data": [[1.6229172E12, 1.55], [1.62291726E12, 0.11666666666666667]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291726E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.11666666666666667, "minX": 1.6229172E12, "maxY": 1.55, "series": [{"data": [[1.6229172E12, 1.55], [1.62291726E12, 0.11666666666666667]], "isOverall": false, "label": "HTTP Request  logging into my account-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291726E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.11666666666666667, "minX": 1.6229172E12, "maxY": 1.55, "series": [{"data": [[1.6229172E12, 1.55], [1.62291726E12, 0.11666666666666667]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291726E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

