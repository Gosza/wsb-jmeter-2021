/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 360.0, "minX": 0.0, "maxY": 1411.0, "series": [{"data": [[0.0, 360.0], [0.1, 360.0], [0.2, 360.0], [0.3, 360.0], [0.4, 360.0], [0.5, 360.0], [0.6, 360.0], [0.7, 360.0], [0.8, 360.0], [0.9, 360.0], [1.0, 362.0], [1.1, 362.0], [1.2, 362.0], [1.3, 362.0], [1.4, 362.0], [1.5, 362.0], [1.6, 362.0], [1.7, 362.0], [1.8, 362.0], [1.9, 362.0], [2.0, 364.0], [2.1, 364.0], [2.2, 364.0], [2.3, 364.0], [2.4, 364.0], [2.5, 364.0], [2.6, 364.0], [2.7, 364.0], [2.8, 364.0], [2.9, 364.0], [3.0, 367.0], [3.1, 367.0], [3.2, 367.0], [3.3, 367.0], [3.4, 367.0], [3.5, 367.0], [3.6, 367.0], [3.7, 367.0], [3.8, 367.0], [3.9, 367.0], [4.0, 369.0], [4.1, 369.0], [4.2, 369.0], [4.3, 369.0], [4.4, 369.0], [4.5, 369.0], [4.6, 369.0], [4.7, 369.0], [4.8, 369.0], [4.9, 369.0], [5.0, 379.0], [5.1, 379.0], [5.2, 379.0], [5.3, 379.0], [5.4, 379.0], [5.5, 379.0], [5.6, 379.0], [5.7, 379.0], [5.8, 379.0], [5.9, 379.0], [6.0, 379.0], [6.1, 379.0], [6.2, 379.0], [6.3, 379.0], [6.4, 379.0], [6.5, 379.0], [6.6, 379.0], [6.7, 379.0], [6.8, 379.0], [6.9, 379.0], [7.0, 385.0], [7.1, 385.0], [7.2, 385.0], [7.3, 385.0], [7.4, 385.0], [7.5, 385.0], [7.6, 385.0], [7.7, 385.0], [7.8, 385.0], [7.9, 385.0], [8.0, 387.0], [8.1, 387.0], [8.2, 387.0], [8.3, 387.0], [8.4, 387.0], [8.5, 387.0], [8.6, 387.0], [8.7, 387.0], [8.8, 387.0], [8.9, 387.0], [9.0, 387.0], [9.1, 387.0], [9.2, 387.0], [9.3, 387.0], [9.4, 387.0], [9.5, 387.0], [9.6, 387.0], [9.7, 387.0], [9.8, 387.0], [9.9, 387.0], [10.0, 387.0], [10.1, 387.0], [10.2, 387.0], [10.3, 387.0], [10.4, 387.0], [10.5, 387.0], [10.6, 387.0], [10.7, 387.0], [10.8, 387.0], [10.9, 387.0], [11.0, 389.0], [11.1, 389.0], [11.2, 389.0], [11.3, 389.0], [11.4, 389.0], [11.5, 389.0], [11.6, 389.0], [11.7, 389.0], [11.8, 389.0], [11.9, 389.0], [12.0, 389.0], [12.1, 389.0], [12.2, 389.0], [12.3, 389.0], [12.4, 389.0], [12.5, 389.0], [12.6, 389.0], [12.7, 389.0], [12.8, 389.0], [12.9, 389.0], [13.0, 389.0], [13.1, 389.0], [13.2, 389.0], [13.3, 389.0], [13.4, 389.0], [13.5, 389.0], [13.6, 389.0], [13.7, 389.0], [13.8, 389.0], [13.9, 389.0], [14.0, 389.0], [14.1, 389.0], [14.2, 389.0], [14.3, 389.0], [14.4, 389.0], [14.5, 389.0], [14.6, 389.0], [14.7, 389.0], [14.8, 389.0], [14.9, 389.0], [15.0, 390.0], [15.1, 390.0], [15.2, 390.0], [15.3, 390.0], [15.4, 390.0], [15.5, 390.0], [15.6, 390.0], [15.7, 390.0], [15.8, 390.0], [15.9, 390.0], [16.0, 391.0], [16.1, 391.0], [16.2, 391.0], [16.3, 391.0], [16.4, 391.0], [16.5, 391.0], [16.6, 391.0], [16.7, 391.0], [16.8, 391.0], [16.9, 391.0], [17.0, 391.0], [17.1, 391.0], [17.2, 391.0], [17.3, 391.0], [17.4, 391.0], [17.5, 391.0], [17.6, 391.0], [17.7, 391.0], [17.8, 391.0], [17.9, 391.0], [18.0, 391.0], [18.1, 391.0], [18.2, 391.0], [18.3, 391.0], [18.4, 391.0], [18.5, 391.0], [18.6, 391.0], [18.7, 391.0], [18.8, 391.0], [18.9, 391.0], [19.0, 393.0], [19.1, 393.0], [19.2, 393.0], [19.3, 393.0], [19.4, 393.0], [19.5, 393.0], [19.6, 393.0], [19.7, 393.0], [19.8, 393.0], [19.9, 393.0], [20.0, 393.0], [20.1, 393.0], [20.2, 393.0], [20.3, 393.0], [20.4, 393.0], [20.5, 393.0], [20.6, 393.0], [20.7, 393.0], [20.8, 393.0], [20.9, 393.0], [21.0, 394.0], [21.1, 394.0], [21.2, 394.0], [21.3, 394.0], [21.4, 394.0], [21.5, 394.0], [21.6, 394.0], [21.7, 394.0], [21.8, 394.0], [21.9, 394.0], [22.0, 394.0], [22.1, 394.0], [22.2, 394.0], [22.3, 394.0], [22.4, 394.0], [22.5, 394.0], [22.6, 394.0], [22.7, 394.0], [22.8, 394.0], [22.9, 394.0], [23.0, 395.0], [23.1, 395.0], [23.2, 395.0], [23.3, 395.0], [23.4, 395.0], [23.5, 395.0], [23.6, 395.0], [23.7, 395.0], [23.8, 395.0], [23.9, 395.0], [24.0, 395.0], [24.1, 395.0], [24.2, 395.0], [24.3, 395.0], [24.4, 395.0], [24.5, 395.0], [24.6, 395.0], [24.7, 395.0], [24.8, 395.0], [24.9, 395.0], [25.0, 395.0], [25.1, 395.0], [25.2, 395.0], [25.3, 395.0], [25.4, 395.0], [25.5, 395.0], [25.6, 395.0], [25.7, 395.0], [25.8, 395.0], [25.9, 395.0], [26.0, 397.0], [26.1, 397.0], [26.2, 397.0], [26.3, 397.0], [26.4, 397.0], [26.5, 397.0], [26.6, 397.0], [26.7, 397.0], [26.8, 397.0], [26.9, 397.0], [27.0, 398.0], [27.1, 398.0], [27.2, 398.0], [27.3, 398.0], [27.4, 398.0], [27.5, 398.0], [27.6, 398.0], [27.7, 398.0], [27.8, 398.0], [27.9, 398.0], [28.0, 398.0], [28.1, 398.0], [28.2, 398.0], [28.3, 398.0], [28.4, 398.0], [28.5, 398.0], [28.6, 398.0], [28.7, 398.0], [28.8, 398.0], [28.9, 398.0], [29.0, 399.0], [29.1, 399.0], [29.2, 399.0], [29.3, 399.0], [29.4, 399.0], [29.5, 399.0], [29.6, 399.0], [29.7, 399.0], [29.8, 399.0], [29.9, 399.0], [30.0, 401.0], [30.1, 401.0], [30.2, 401.0], [30.3, 401.0], [30.4, 401.0], [30.5, 401.0], [30.6, 401.0], [30.7, 401.0], [30.8, 401.0], [30.9, 401.0], [31.0, 402.0], [31.1, 402.0], [31.2, 402.0], [31.3, 402.0], [31.4, 402.0], [31.5, 402.0], [31.6, 402.0], [31.7, 402.0], [31.8, 402.0], [31.9, 402.0], [32.0, 404.0], [32.1, 404.0], [32.2, 404.0], [32.3, 404.0], [32.4, 404.0], [32.5, 404.0], [32.6, 404.0], [32.7, 404.0], [32.8, 404.0], [32.9, 404.0], [33.0, 405.0], [33.1, 405.0], [33.2, 405.0], [33.3, 405.0], [33.4, 405.0], [33.5, 405.0], [33.6, 405.0], [33.7, 405.0], [33.8, 405.0], [33.9, 405.0], [34.0, 406.0], [34.1, 406.0], [34.2, 406.0], [34.3, 406.0], [34.4, 406.0], [34.5, 406.0], [34.6, 406.0], [34.7, 406.0], [34.8, 406.0], [34.9, 406.0], [35.0, 407.0], [35.1, 407.0], [35.2, 407.0], [35.3, 407.0], [35.4, 407.0], [35.5, 407.0], [35.6, 407.0], [35.7, 407.0], [35.8, 407.0], [35.9, 407.0], [36.0, 408.0], [36.1, 408.0], [36.2, 408.0], [36.3, 408.0], [36.4, 408.0], [36.5, 408.0], [36.6, 408.0], [36.7, 408.0], [36.8, 408.0], [36.9, 408.0], [37.0, 408.0], [37.1, 408.0], [37.2, 408.0], [37.3, 408.0], [37.4, 408.0], [37.5, 408.0], [37.6, 408.0], [37.7, 408.0], [37.8, 408.0], [37.9, 408.0], [38.0, 409.0], [38.1, 409.0], [38.2, 409.0], [38.3, 409.0], [38.4, 409.0], [38.5, 409.0], [38.6, 409.0], [38.7, 409.0], [38.8, 409.0], [38.9, 409.0], [39.0, 411.0], [39.1, 411.0], [39.2, 411.0], [39.3, 411.0], [39.4, 411.0], [39.5, 411.0], [39.6, 411.0], [39.7, 411.0], [39.8, 411.0], [39.9, 411.0], [40.0, 413.0], [40.1, 413.0], [40.2, 413.0], [40.3, 413.0], [40.4, 413.0], [40.5, 413.0], [40.6, 413.0], [40.7, 413.0], [40.8, 413.0], [40.9, 413.0], [41.0, 414.0], [41.1, 414.0], [41.2, 414.0], [41.3, 414.0], [41.4, 414.0], [41.5, 414.0], [41.6, 414.0], [41.7, 414.0], [41.8, 414.0], [41.9, 414.0], [42.0, 416.0], [42.1, 416.0], [42.2, 416.0], [42.3, 416.0], [42.4, 416.0], [42.5, 416.0], [42.6, 416.0], [42.7, 416.0], [42.8, 416.0], [42.9, 416.0], [43.0, 419.0], [43.1, 419.0], [43.2, 419.0], [43.3, 419.0], [43.4, 419.0], [43.5, 419.0], [43.6, 419.0], [43.7, 419.0], [43.8, 419.0], [43.9, 419.0], [44.0, 420.0], [44.1, 420.0], [44.2, 420.0], [44.3, 420.0], [44.4, 420.0], [44.5, 420.0], [44.6, 420.0], [44.7, 420.0], [44.8, 420.0], [44.9, 420.0], [45.0, 421.0], [45.1, 421.0], [45.2, 421.0], [45.3, 421.0], [45.4, 421.0], [45.5, 421.0], [45.6, 421.0], [45.7, 421.0], [45.8, 421.0], [45.9, 421.0], [46.0, 422.0], [46.1, 422.0], [46.2, 422.0], [46.3, 422.0], [46.4, 422.0], [46.5, 422.0], [46.6, 422.0], [46.7, 422.0], [46.8, 422.0], [46.9, 422.0], [47.0, 426.0], [47.1, 426.0], [47.2, 426.0], [47.3, 426.0], [47.4, 426.0], [47.5, 426.0], [47.6, 426.0], [47.7, 426.0], [47.8, 426.0], [47.9, 426.0], [48.0, 430.0], [48.1, 430.0], [48.2, 430.0], [48.3, 430.0], [48.4, 430.0], [48.5, 430.0], [48.6, 430.0], [48.7, 430.0], [48.8, 430.0], [48.9, 430.0], [49.0, 433.0], [49.1, 433.0], [49.2, 433.0], [49.3, 433.0], [49.4, 433.0], [49.5, 433.0], [49.6, 433.0], [49.7, 433.0], [49.8, 433.0], [49.9, 433.0], [50.0, 437.0], [50.1, 437.0], [50.2, 437.0], [50.3, 437.0], [50.4, 437.0], [50.5, 437.0], [50.6, 437.0], [50.7, 437.0], [50.8, 437.0], [50.9, 437.0], [51.0, 440.0], [51.1, 440.0], [51.2, 440.0], [51.3, 440.0], [51.4, 440.0], [51.5, 440.0], [51.6, 440.0], [51.7, 440.0], [51.8, 440.0], [51.9, 440.0], [52.0, 440.0], [52.1, 440.0], [52.2, 440.0], [52.3, 440.0], [52.4, 440.0], [52.5, 440.0], [52.6, 440.0], [52.7, 440.0], [52.8, 440.0], [52.9, 440.0], [53.0, 444.0], [53.1, 444.0], [53.2, 444.0], [53.3, 444.0], [53.4, 444.0], [53.5, 444.0], [53.6, 444.0], [53.7, 444.0], [53.8, 444.0], [53.9, 444.0], [54.0, 446.0], [54.1, 446.0], [54.2, 446.0], [54.3, 446.0], [54.4, 446.0], [54.5, 446.0], [54.6, 446.0], [54.7, 446.0], [54.8, 446.0], [54.9, 446.0], [55.0, 448.0], [55.1, 448.0], [55.2, 448.0], [55.3, 448.0], [55.4, 448.0], [55.5, 448.0], [55.6, 448.0], [55.7, 448.0], [55.8, 448.0], [55.9, 448.0], [56.0, 459.0], [56.1, 459.0], [56.2, 459.0], [56.3, 459.0], [56.4, 459.0], [56.5, 459.0], [56.6, 459.0], [56.7, 459.0], [56.8, 459.0], [56.9, 459.0], [57.0, 477.0], [57.1, 477.0], [57.2, 477.0], [57.3, 477.0], [57.4, 477.0], [57.5, 477.0], [57.6, 477.0], [57.7, 477.0], [57.8, 477.0], [57.9, 477.0], [58.0, 489.0], [58.1, 489.0], [58.2, 489.0], [58.3, 489.0], [58.4, 489.0], [58.5, 489.0], [58.6, 489.0], [58.7, 489.0], [58.8, 489.0], [58.9, 489.0], [59.0, 521.0], [59.1, 521.0], [59.2, 521.0], [59.3, 521.0], [59.4, 521.0], [59.5, 521.0], [59.6, 521.0], [59.7, 521.0], [59.8, 521.0], [59.9, 521.0], [60.0, 526.0], [60.1, 526.0], [60.2, 526.0], [60.3, 526.0], [60.4, 526.0], [60.5, 526.0], [60.6, 526.0], [60.7, 526.0], [60.8, 526.0], [60.9, 526.0], [61.0, 527.0], [61.1, 527.0], [61.2, 527.0], [61.3, 527.0], [61.4, 527.0], [61.5, 527.0], [61.6, 527.0], [61.7, 527.0], [61.8, 527.0], [61.9, 527.0], [62.0, 541.0], [62.1, 541.0], [62.2, 541.0], [62.3, 541.0], [62.4, 541.0], [62.5, 541.0], [62.6, 541.0], [62.7, 541.0], [62.8, 541.0], [62.9, 541.0], [63.0, 563.0], [63.1, 563.0], [63.2, 563.0], [63.3, 563.0], [63.4, 563.0], [63.5, 563.0], [63.6, 563.0], [63.7, 563.0], [63.8, 563.0], [63.9, 563.0], [64.0, 568.0], [64.1, 568.0], [64.2, 568.0], [64.3, 568.0], [64.4, 568.0], [64.5, 568.0], [64.6, 568.0], [64.7, 568.0], [64.8, 568.0], [64.9, 568.0], [65.0, 581.0], [65.1, 581.0], [65.2, 581.0], [65.3, 581.0], [65.4, 581.0], [65.5, 581.0], [65.6, 581.0], [65.7, 581.0], [65.8, 581.0], [65.9, 581.0], [66.0, 582.0], [66.1, 582.0], [66.2, 582.0], [66.3, 582.0], [66.4, 582.0], [66.5, 582.0], [66.6, 582.0], [66.7, 582.0], [66.8, 582.0], [66.9, 582.0], [67.0, 599.0], [67.1, 599.0], [67.2, 599.0], [67.3, 599.0], [67.4, 599.0], [67.5, 599.0], [67.6, 599.0], [67.7, 599.0], [67.8, 599.0], [67.9, 599.0], [68.0, 606.0], [68.1, 606.0], [68.2, 606.0], [68.3, 606.0], [68.4, 606.0], [68.5, 606.0], [68.6, 606.0], [68.7, 606.0], [68.8, 606.0], [68.9, 606.0], [69.0, 606.0], [69.1, 606.0], [69.2, 606.0], [69.3, 606.0], [69.4, 606.0], [69.5, 606.0], [69.6, 606.0], [69.7, 606.0], [69.8, 606.0], [69.9, 606.0], [70.0, 613.0], [70.1, 613.0], [70.2, 613.0], [70.3, 613.0], [70.4, 613.0], [70.5, 613.0], [70.6, 613.0], [70.7, 613.0], [70.8, 613.0], [70.9, 613.0], [71.0, 614.0], [71.1, 614.0], [71.2, 614.0], [71.3, 614.0], [71.4, 614.0], [71.5, 614.0], [71.6, 614.0], [71.7, 614.0], [71.8, 614.0], [71.9, 614.0], [72.0, 614.0], [72.1, 614.0], [72.2, 614.0], [72.3, 614.0], [72.4, 614.0], [72.5, 614.0], [72.6, 614.0], [72.7, 614.0], [72.8, 614.0], [72.9, 614.0], [73.0, 617.0], [73.1, 617.0], [73.2, 617.0], [73.3, 617.0], [73.4, 617.0], [73.5, 617.0], [73.6, 617.0], [73.7, 617.0], [73.8, 617.0], [73.9, 617.0], [74.0, 652.0], [74.1, 652.0], [74.2, 652.0], [74.3, 652.0], [74.4, 652.0], [74.5, 652.0], [74.6, 652.0], [74.7, 652.0], [74.8, 652.0], [74.9, 652.0], [75.0, 664.0], [75.1, 664.0], [75.2, 664.0], [75.3, 664.0], [75.4, 664.0], [75.5, 664.0], [75.6, 664.0], [75.7, 664.0], [75.8, 664.0], [75.9, 664.0], [76.0, 681.0], [76.1, 681.0], [76.2, 681.0], [76.3, 681.0], [76.4, 681.0], [76.5, 681.0], [76.6, 681.0], [76.7, 681.0], [76.8, 681.0], [76.9, 681.0], [77.0, 692.0], [77.1, 692.0], [77.2, 692.0], [77.3, 692.0], [77.4, 692.0], [77.5, 692.0], [77.6, 692.0], [77.7, 692.0], [77.8, 692.0], [77.9, 692.0], [78.0, 709.0], [78.1, 709.0], [78.2, 709.0], [78.3, 709.0], [78.4, 709.0], [78.5, 709.0], [78.6, 709.0], [78.7, 709.0], [78.8, 709.0], [78.9, 709.0], [79.0, 724.0], [79.1, 724.0], [79.2, 724.0], [79.3, 724.0], [79.4, 724.0], [79.5, 724.0], [79.6, 724.0], [79.7, 724.0], [79.8, 724.0], [79.9, 724.0], [80.0, 766.0], [80.1, 766.0], [80.2, 766.0], [80.3, 766.0], [80.4, 766.0], [80.5, 766.0], [80.6, 766.0], [80.7, 766.0], [80.8, 766.0], [80.9, 766.0], [81.0, 780.0], [81.1, 780.0], [81.2, 780.0], [81.3, 780.0], [81.4, 780.0], [81.5, 780.0], [81.6, 780.0], [81.7, 780.0], [81.8, 780.0], [81.9, 780.0], [82.0, 781.0], [82.1, 781.0], [82.2, 781.0], [82.3, 781.0], [82.4, 781.0], [82.5, 781.0], [82.6, 781.0], [82.7, 781.0], [82.8, 781.0], [82.9, 781.0], [83.0, 825.0], [83.1, 825.0], [83.2, 825.0], [83.3, 825.0], [83.4, 825.0], [83.5, 825.0], [83.6, 825.0], [83.7, 825.0], [83.8, 825.0], [83.9, 825.0], [84.0, 826.0], [84.1, 826.0], [84.2, 826.0], [84.3, 826.0], [84.4, 826.0], [84.5, 826.0], [84.6, 826.0], [84.7, 826.0], [84.8, 826.0], [84.9, 826.0], [85.0, 841.0], [85.1, 841.0], [85.2, 841.0], [85.3, 841.0], [85.4, 841.0], [85.5, 841.0], [85.6, 841.0], [85.7, 841.0], [85.8, 841.0], [85.9, 841.0], [86.0, 879.0], [86.1, 879.0], [86.2, 879.0], [86.3, 879.0], [86.4, 879.0], [86.5, 879.0], [86.6, 879.0], [86.7, 879.0], [86.8, 879.0], [86.9, 879.0], [87.0, 917.0], [87.1, 917.0], [87.2, 917.0], [87.3, 917.0], [87.4, 917.0], [87.5, 917.0], [87.6, 917.0], [87.7, 917.0], [87.8, 917.0], [87.9, 917.0], [88.0, 945.0], [88.1, 945.0], [88.2, 945.0], [88.3, 945.0], [88.4, 945.0], [88.5, 945.0], [88.6, 945.0], [88.7, 945.0], [88.8, 945.0], [88.9, 945.0], [89.0, 1030.0], [89.1, 1030.0], [89.2, 1030.0], [89.3, 1030.0], [89.4, 1030.0], [89.5, 1030.0], [89.6, 1030.0], [89.7, 1030.0], [89.8, 1030.0], [89.9, 1030.0], [90.0, 1067.0], [90.1, 1067.0], [90.2, 1067.0], [90.3, 1067.0], [90.4, 1067.0], [90.5, 1067.0], [90.6, 1067.0], [90.7, 1067.0], [90.8, 1067.0], [90.9, 1067.0], [91.0, 1077.0], [91.1, 1077.0], [91.2, 1077.0], [91.3, 1077.0], [91.4, 1077.0], [91.5, 1077.0], [91.6, 1077.0], [91.7, 1077.0], [91.8, 1077.0], [91.9, 1077.0], [92.0, 1201.0], [92.1, 1201.0], [92.2, 1201.0], [92.3, 1201.0], [92.4, 1201.0], [92.5, 1201.0], [92.6, 1201.0], [92.7, 1201.0], [92.8, 1201.0], [92.9, 1201.0], [93.0, 1234.0], [93.1, 1234.0], [93.2, 1234.0], [93.3, 1234.0], [93.4, 1234.0], [93.5, 1234.0], [93.6, 1234.0], [93.7, 1234.0], [93.8, 1234.0], [93.9, 1234.0], [94.0, 1266.0], [94.1, 1266.0], [94.2, 1266.0], [94.3, 1266.0], [94.4, 1266.0], [94.5, 1266.0], [94.6, 1266.0], [94.7, 1266.0], [94.8, 1266.0], [94.9, 1266.0], [95.0, 1266.0], [95.1, 1266.0], [95.2, 1266.0], [95.3, 1266.0], [95.4, 1266.0], [95.5, 1266.0], [95.6, 1266.0], [95.7, 1266.0], [95.8, 1266.0], [95.9, 1266.0], [96.0, 1277.0], [96.1, 1277.0], [96.2, 1277.0], [96.3, 1277.0], [96.4, 1277.0], [96.5, 1277.0], [96.6, 1277.0], [96.7, 1277.0], [96.8, 1277.0], [96.9, 1277.0], [97.0, 1326.0], [97.1, 1326.0], [97.2, 1326.0], [97.3, 1326.0], [97.4, 1326.0], [97.5, 1326.0], [97.6, 1326.0], [97.7, 1326.0], [97.8, 1326.0], [97.9, 1326.0], [98.0, 1363.0], [98.1, 1363.0], [98.2, 1363.0], [98.3, 1363.0], [98.4, 1363.0], [98.5, 1363.0], [98.6, 1363.0], [98.7, 1363.0], [98.8, 1363.0], [98.9, 1363.0], [99.0, 1411.0], [99.1, 1411.0], [99.2, 1411.0], [99.3, 1411.0], [99.4, 1411.0], [99.5, 1411.0], [99.6, 1411.0], [99.7, 1411.0], [99.8, 1411.0], [99.9, 1411.0]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 300.0, "maxY": 30.0, "series": [{"data": [[600.0, 10.0], [300.0, 30.0], [1200.0, 5.0], [1300.0, 2.0], [700.0, 5.0], [1400.0, 1.0], [400.0, 29.0], [800.0, 4.0], [900.0, 2.0], [500.0, 9.0], [1000.0, 3.0]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 1400.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 41.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 59.0, "series": [{"data": [[0.0, 59.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [[1.0, 41.0]], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 1.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 18.59615384615385, "minX": 1.62291732E12, "maxY": 25.0, "series": [{"data": [[1.62291732E12, 25.0], [1.62291738E12, 18.59615384615385]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291738E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 364.0, "minX": 1.0, "maxY": 1009.3333333333333, "series": [{"data": [[2.0, 394.0], [9.0, 415.6666666666667], [10.0, 389.0], [11.0, 563.0], [12.0, 489.0], [3.0, 414.0], [13.0, 521.0], [14.0, 526.0], [15.0, 367.0], [16.0, 404.0], [4.0, 419.0], [1.0, 416.0], [17.0, 364.0], [18.0, 405.0], [19.0, 391.0], [20.0, 433.0], [5.0, 395.0], [21.0, 408.3333333333333], [22.0, 401.0], [23.0, 395.5], [24.0, 1009.3333333333333], [6.0, 391.0], [25.0, 606.5757575757576], [7.0, 393.0]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}, {"data": [[21.67, 578.29]], "isOverall": false, "label": "HTTP Request  logging into my account-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 25.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 454.4, "minX": 1.62291732E12, "maxY": 8776.733333333334, "series": [{"data": [[1.62291732E12, 8101.6], [1.62291738E12, 8776.733333333334]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62291732E12, 454.4], [1.62291738E12, 492.26666666666665]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291738E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 547.0961538461542, "minX": 1.62291732E12, "maxY": 612.0833333333334, "series": [{"data": [[1.62291732E12, 612.0833333333334], [1.62291738E12, 547.0961538461542]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291738E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 545.3269230769231, "minX": 1.62291732E12, "maxY": 610.3541666666666, "series": [{"data": [[1.62291732E12, 610.3541666666666], [1.62291738E12, 545.3269230769231]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291738E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 1.3750000000000002, "minX": 1.62291732E12, "maxY": 1.6153846153846152, "series": [{"data": [[1.62291732E12, 1.3750000000000002], [1.62291738E12, 1.6153846153846152]], "isOverall": false, "label": "HTTP Request  logging into my account", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291738E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 360.0, "minX": 1.62291732E12, "maxY": 1411.0, "series": [{"data": [[1.62291732E12, 1363.0], [1.62291738E12, 1411.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62291732E12, 360.0], [1.62291738E12, 364.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62291732E12, 1080.4], [1.62291738E12, 1062.9]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62291732E12, 1363.0], [1.62291738E12, 1411.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62291732E12, 1257.6499999999999], [1.62291738E12, 1286.9999999999995]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291738E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 404.5, "minX": 1.0, "maxY": 945.0, "series": [{"data": [[2.0, 404.5], [1.0, 406.0], [4.0, 602.5], [5.0, 945.0], [3.0, 415.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 5.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 402.5, "minX": 1.0, "maxY": 943.0, "series": [{"data": [[2.0, 402.5], [1.0, 404.0], [4.0, 600.5], [5.0, 943.0], [3.0, 414.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 5.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.8, "minX": 1.62291732E12, "maxY": 0.8666666666666667, "series": [{"data": [[1.62291732E12, 0.8], [1.62291738E12, 0.8666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291738E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.8, "minX": 1.62291732E12, "maxY": 0.8666666666666667, "series": [{"data": [[1.62291732E12, 0.8], [1.62291738E12, 0.8666666666666667]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62291738E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.8, "minX": 1.62291732E12, "maxY": 0.8666666666666667, "series": [{"data": [[1.62291732E12, 0.8], [1.62291738E12, 0.8666666666666667]], "isOverall": false, "label": "HTTP Request  logging into my account-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291738E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.8, "minX": 1.62291732E12, "maxY": 0.8666666666666667, "series": [{"data": [[1.62291732E12, 0.8], [1.62291738E12, 0.8666666666666667]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62291738E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

