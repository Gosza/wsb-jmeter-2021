/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 497.0, "minX": 0.0, "maxY": 43511.0, "series": [{"data": [[0.0, 497.0], [0.1, 497.0], [0.2, 497.0], [0.3, 497.0], [0.4, 497.0], [0.5, 497.0], [0.6, 497.0], [0.7, 497.0], [0.8, 497.0], [0.9, 497.0], [1.0, 2862.0], [1.1, 2862.0], [1.2, 2862.0], [1.3, 2862.0], [1.4, 2862.0], [1.5, 2862.0], [1.6, 2862.0], [1.7, 2862.0], [1.8, 2862.0], [1.9, 2862.0], [2.0, 2906.0], [2.1, 2906.0], [2.2, 2906.0], [2.3, 2906.0], [2.4, 2906.0], [2.5, 2906.0], [2.6, 2906.0], [2.7, 2906.0], [2.8, 2906.0], [2.9, 2906.0], [3.0, 3315.0], [3.1, 3315.0], [3.2, 3315.0], [3.3, 3315.0], [3.4, 3315.0], [3.5, 3315.0], [3.6, 3315.0], [3.7, 3315.0], [3.8, 3315.0], [3.9, 3315.0], [4.0, 3332.0], [4.1, 3332.0], [4.2, 3332.0], [4.3, 3332.0], [4.4, 3332.0], [4.5, 3332.0], [4.6, 3332.0], [4.7, 3332.0], [4.8, 3332.0], [4.9, 3332.0], [5.0, 3482.0], [5.1, 3482.0], [5.2, 3482.0], [5.3, 3482.0], [5.4, 3482.0], [5.5, 3482.0], [5.6, 3482.0], [5.7, 3482.0], [5.8, 3482.0], [5.9, 3482.0], [6.0, 3522.0], [6.1, 3522.0], [6.2, 3522.0], [6.3, 3522.0], [6.4, 3522.0], [6.5, 3522.0], [6.6, 3522.0], [6.7, 3522.0], [6.8, 3522.0], [6.9, 3522.0], [7.0, 3536.0], [7.1, 3536.0], [7.2, 3536.0], [7.3, 3536.0], [7.4, 3536.0], [7.5, 3536.0], [7.6, 3536.0], [7.7, 3536.0], [7.8, 3536.0], [7.9, 3536.0], [8.0, 3856.0], [8.1, 3856.0], [8.2, 3856.0], [8.3, 3856.0], [8.4, 3856.0], [8.5, 3856.0], [8.6, 3856.0], [8.7, 3856.0], [8.8, 3856.0], [8.9, 3856.0], [9.0, 3902.0], [9.1, 3902.0], [9.2, 3902.0], [9.3, 3902.0], [9.4, 3902.0], [9.5, 3902.0], [9.6, 3902.0], [9.7, 3902.0], [9.8, 3902.0], [9.9, 3902.0], [10.0, 3991.0], [10.1, 3991.0], [10.2, 3991.0], [10.3, 3991.0], [10.4, 3991.0], [10.5, 3991.0], [10.6, 3991.0], [10.7, 3991.0], [10.8, 3991.0], [10.9, 3991.0], [11.0, 3992.0], [11.1, 3992.0], [11.2, 3992.0], [11.3, 3992.0], [11.4, 3992.0], [11.5, 3992.0], [11.6, 3992.0], [11.7, 3992.0], [11.8, 3992.0], [11.9, 3992.0], [12.0, 3995.0], [12.1, 3995.0], [12.2, 3995.0], [12.3, 3995.0], [12.4, 3995.0], [12.5, 3995.0], [12.6, 3995.0], [12.7, 3995.0], [12.8, 3995.0], [12.9, 3995.0], [13.0, 4043.0], [13.1, 4043.0], [13.2, 4043.0], [13.3, 4043.0], [13.4, 4043.0], [13.5, 4043.0], [13.6, 4043.0], [13.7, 4043.0], [13.8, 4043.0], [13.9, 4043.0], [14.0, 4065.0], [14.1, 4065.0], [14.2, 4065.0], [14.3, 4065.0], [14.4, 4065.0], [14.5, 4065.0], [14.6, 4065.0], [14.7, 4065.0], [14.8, 4065.0], [14.9, 4065.0], [15.0, 4085.0], [15.1, 4085.0], [15.2, 4085.0], [15.3, 4085.0], [15.4, 4085.0], [15.5, 4085.0], [15.6, 4085.0], [15.7, 4085.0], [15.8, 4085.0], [15.9, 4085.0], [16.0, 4102.0], [16.1, 4102.0], [16.2, 4102.0], [16.3, 4102.0], [16.4, 4102.0], [16.5, 4102.0], [16.6, 4102.0], [16.7, 4102.0], [16.8, 4102.0], [16.9, 4102.0], [17.0, 4165.0], [17.1, 4165.0], [17.2, 4165.0], [17.3, 4165.0], [17.4, 4165.0], [17.5, 4165.0], [17.6, 4165.0], [17.7, 4165.0], [17.8, 4165.0], [17.9, 4165.0], [18.0, 4224.0], [18.1, 4224.0], [18.2, 4224.0], [18.3, 4224.0], [18.4, 4224.0], [18.5, 4224.0], [18.6, 4224.0], [18.7, 4224.0], [18.8, 4224.0], [18.9, 4224.0], [19.0, 4240.0], [19.1, 4240.0], [19.2, 4240.0], [19.3, 4240.0], [19.4, 4240.0], [19.5, 4240.0], [19.6, 4240.0], [19.7, 4240.0], [19.8, 4240.0], [19.9, 4240.0], [20.0, 4368.0], [20.1, 4368.0], [20.2, 4368.0], [20.3, 4368.0], [20.4, 4368.0], [20.5, 4368.0], [20.6, 4368.0], [20.7, 4368.0], [20.8, 4368.0], [20.9, 4368.0], [21.0, 4374.0], [21.1, 4374.0], [21.2, 4374.0], [21.3, 4374.0], [21.4, 4374.0], [21.5, 4374.0], [21.6, 4374.0], [21.7, 4374.0], [21.8, 4374.0], [21.9, 4374.0], [22.0, 4451.0], [22.1, 4451.0], [22.2, 4451.0], [22.3, 4451.0], [22.4, 4451.0], [22.5, 4451.0], [22.6, 4451.0], [22.7, 4451.0], [22.8, 4451.0], [22.9, 4451.0], [23.0, 4506.0], [23.1, 4506.0], [23.2, 4506.0], [23.3, 4506.0], [23.4, 4506.0], [23.5, 4506.0], [23.6, 4506.0], [23.7, 4506.0], [23.8, 4506.0], [23.9, 4506.0], [24.0, 4571.0], [24.1, 4571.0], [24.2, 4571.0], [24.3, 4571.0], [24.4, 4571.0], [24.5, 4571.0], [24.6, 4571.0], [24.7, 4571.0], [24.8, 4571.0], [24.9, 4571.0], [25.0, 4646.0], [25.1, 4646.0], [25.2, 4646.0], [25.3, 4646.0], [25.4, 4646.0], [25.5, 4646.0], [25.6, 4646.0], [25.7, 4646.0], [25.8, 4646.0], [25.9, 4646.0], [26.0, 4685.0], [26.1, 4685.0], [26.2, 4685.0], [26.3, 4685.0], [26.4, 4685.0], [26.5, 4685.0], [26.6, 4685.0], [26.7, 4685.0], [26.8, 4685.0], [26.9, 4685.0], [27.0, 4695.0], [27.1, 4695.0], [27.2, 4695.0], [27.3, 4695.0], [27.4, 4695.0], [27.5, 4695.0], [27.6, 4695.0], [27.7, 4695.0], [27.8, 4695.0], [27.9, 4695.0], [28.0, 4719.0], [28.1, 4719.0], [28.2, 4719.0], [28.3, 4719.0], [28.4, 4719.0], [28.5, 4719.0], [28.6, 4719.0], [28.7, 4719.0], [28.8, 4719.0], [28.9, 4719.0], [29.0, 4722.0], [29.1, 4722.0], [29.2, 4722.0], [29.3, 4722.0], [29.4, 4722.0], [29.5, 4722.0], [29.6, 4722.0], [29.7, 4722.0], [29.8, 4722.0], [29.9, 4722.0], [30.0, 4887.0], [30.1, 4887.0], [30.2, 4887.0], [30.3, 4887.0], [30.4, 4887.0], [30.5, 4887.0], [30.6, 4887.0], [30.7, 4887.0], [30.8, 4887.0], [30.9, 4887.0], [31.0, 5039.0], [31.1, 5039.0], [31.2, 5039.0], [31.3, 5039.0], [31.4, 5039.0], [31.5, 5039.0], [31.6, 5039.0], [31.7, 5039.0], [31.8, 5039.0], [31.9, 5039.0], [32.0, 5135.0], [32.1, 5135.0], [32.2, 5135.0], [32.3, 5135.0], [32.4, 5135.0], [32.5, 5135.0], [32.6, 5135.0], [32.7, 5135.0], [32.8, 5135.0], [32.9, 5135.0], [33.0, 5167.0], [33.1, 5167.0], [33.2, 5167.0], [33.3, 5167.0], [33.4, 5167.0], [33.5, 5167.0], [33.6, 5167.0], [33.7, 5167.0], [33.8, 5167.0], [33.9, 5167.0], [34.0, 5202.0], [34.1, 5202.0], [34.2, 5202.0], [34.3, 5202.0], [34.4, 5202.0], [34.5, 5202.0], [34.6, 5202.0], [34.7, 5202.0], [34.8, 5202.0], [34.9, 5202.0], [35.0, 5213.0], [35.1, 5213.0], [35.2, 5213.0], [35.3, 5213.0], [35.4, 5213.0], [35.5, 5213.0], [35.6, 5213.0], [35.7, 5213.0], [35.8, 5213.0], [35.9, 5213.0], [36.0, 5272.0], [36.1, 5272.0], [36.2, 5272.0], [36.3, 5272.0], [36.4, 5272.0], [36.5, 5272.0], [36.6, 5272.0], [36.7, 5272.0], [36.8, 5272.0], [36.9, 5272.0], [37.0, 5420.0], [37.1, 5420.0], [37.2, 5420.0], [37.3, 5420.0], [37.4, 5420.0], [37.5, 5420.0], [37.6, 5420.0], [37.7, 5420.0], [37.8, 5420.0], [37.9, 5420.0], [38.0, 5571.0], [38.1, 5571.0], [38.2, 5571.0], [38.3, 5571.0], [38.4, 5571.0], [38.5, 5571.0], [38.6, 5571.0], [38.7, 5571.0], [38.8, 5571.0], [38.9, 5571.0], [39.0, 5628.0], [39.1, 5628.0], [39.2, 5628.0], [39.3, 5628.0], [39.4, 5628.0], [39.5, 5628.0], [39.6, 5628.0], [39.7, 5628.0], [39.8, 5628.0], [39.9, 5628.0], [40.0, 5756.0], [40.1, 5756.0], [40.2, 5756.0], [40.3, 5756.0], [40.4, 5756.0], [40.5, 5756.0], [40.6, 5756.0], [40.7, 5756.0], [40.8, 5756.0], [40.9, 5756.0], [41.0, 5856.0], [41.1, 5856.0], [41.2, 5856.0], [41.3, 5856.0], [41.4, 5856.0], [41.5, 5856.0], [41.6, 5856.0], [41.7, 5856.0], [41.8, 5856.0], [41.9, 5856.0], [42.0, 5883.0], [42.1, 5883.0], [42.2, 5883.0], [42.3, 5883.0], [42.4, 5883.0], [42.5, 5883.0], [42.6, 5883.0], [42.7, 5883.0], [42.8, 5883.0], [42.9, 5883.0], [43.0, 5884.0], [43.1, 5884.0], [43.2, 5884.0], [43.3, 5884.0], [43.4, 5884.0], [43.5, 5884.0], [43.6, 5884.0], [43.7, 5884.0], [43.8, 5884.0], [43.9, 5884.0], [44.0, 5895.0], [44.1, 5895.0], [44.2, 5895.0], [44.3, 5895.0], [44.4, 5895.0], [44.5, 5895.0], [44.6, 5895.0], [44.7, 5895.0], [44.8, 5895.0], [44.9, 5895.0], [45.0, 5973.0], [45.1, 5973.0], [45.2, 5973.0], [45.3, 5973.0], [45.4, 5973.0], [45.5, 5973.0], [45.6, 5973.0], [45.7, 5973.0], [45.8, 5973.0], [45.9, 5973.0], [46.0, 5977.0], [46.1, 5977.0], [46.2, 5977.0], [46.3, 5977.0], [46.4, 5977.0], [46.5, 5977.0], [46.6, 5977.0], [46.7, 5977.0], [46.8, 5977.0], [46.9, 5977.0], [47.0, 6077.0], [47.1, 6077.0], [47.2, 6077.0], [47.3, 6077.0], [47.4, 6077.0], [47.5, 6077.0], [47.6, 6077.0], [47.7, 6077.0], [47.8, 6077.0], [47.9, 6077.0], [48.0, 6124.0], [48.1, 6124.0], [48.2, 6124.0], [48.3, 6124.0], [48.4, 6124.0], [48.5, 6124.0], [48.6, 6124.0], [48.7, 6124.0], [48.8, 6124.0], [48.9, 6124.0], [49.0, 6194.0], [49.1, 6194.0], [49.2, 6194.0], [49.3, 6194.0], [49.4, 6194.0], [49.5, 6194.0], [49.6, 6194.0], [49.7, 6194.0], [49.8, 6194.0], [49.9, 6194.0], [50.0, 6210.0], [50.1, 6210.0], [50.2, 6210.0], [50.3, 6210.0], [50.4, 6210.0], [50.5, 6210.0], [50.6, 6210.0], [50.7, 6210.0], [50.8, 6210.0], [50.9, 6210.0], [51.0, 6333.0], [51.1, 6333.0], [51.2, 6333.0], [51.3, 6333.0], [51.4, 6333.0], [51.5, 6333.0], [51.6, 6333.0], [51.7, 6333.0], [51.8, 6333.0], [51.9, 6333.0], [52.0, 6377.0], [52.1, 6377.0], [52.2, 6377.0], [52.3, 6377.0], [52.4, 6377.0], [52.5, 6377.0], [52.6, 6377.0], [52.7, 6377.0], [52.8, 6377.0], [52.9, 6377.0], [53.0, 6502.0], [53.1, 6502.0], [53.2, 6502.0], [53.3, 6502.0], [53.4, 6502.0], [53.5, 6502.0], [53.6, 6502.0], [53.7, 6502.0], [53.8, 6502.0], [53.9, 6502.0], [54.0, 6568.0], [54.1, 6568.0], [54.2, 6568.0], [54.3, 6568.0], [54.4, 6568.0], [54.5, 6568.0], [54.6, 6568.0], [54.7, 6568.0], [54.8, 6568.0], [54.9, 6568.0], [55.0, 6644.0], [55.1, 6644.0], [55.2, 6644.0], [55.3, 6644.0], [55.4, 6644.0], [55.5, 6644.0], [55.6, 6644.0], [55.7, 6644.0], [55.8, 6644.0], [55.9, 6644.0], [56.0, 6829.0], [56.1, 6829.0], [56.2, 6829.0], [56.3, 6829.0], [56.4, 6829.0], [56.5, 6829.0], [56.6, 6829.0], [56.7, 6829.0], [56.8, 6829.0], [56.9, 6829.0], [57.0, 6864.0], [57.1, 6864.0], [57.2, 6864.0], [57.3, 6864.0], [57.4, 6864.0], [57.5, 6864.0], [57.6, 6864.0], [57.7, 6864.0], [57.8, 6864.0], [57.9, 6864.0], [58.0, 6940.0], [58.1, 6940.0], [58.2, 6940.0], [58.3, 6940.0], [58.4, 6940.0], [58.5, 6940.0], [58.6, 6940.0], [58.7, 6940.0], [58.8, 6940.0], [58.9, 6940.0], [59.0, 7124.0], [59.1, 7124.0], [59.2, 7124.0], [59.3, 7124.0], [59.4, 7124.0], [59.5, 7124.0], [59.6, 7124.0], [59.7, 7124.0], [59.8, 7124.0], [59.9, 7124.0], [60.0, 7344.0], [60.1, 7344.0], [60.2, 7344.0], [60.3, 7344.0], [60.4, 7344.0], [60.5, 7344.0], [60.6, 7344.0], [60.7, 7344.0], [60.8, 7344.0], [60.9, 7344.0], [61.0, 7534.0], [61.1, 7534.0], [61.2, 7534.0], [61.3, 7534.0], [61.4, 7534.0], [61.5, 7534.0], [61.6, 7534.0], [61.7, 7534.0], [61.8, 7534.0], [61.9, 7534.0], [62.0, 7599.0], [62.1, 7599.0], [62.2, 7599.0], [62.3, 7599.0], [62.4, 7599.0], [62.5, 7599.0], [62.6, 7599.0], [62.7, 7599.0], [62.8, 7599.0], [62.9, 7599.0], [63.0, 7696.0], [63.1, 7696.0], [63.2, 7696.0], [63.3, 7696.0], [63.4, 7696.0], [63.5, 7696.0], [63.6, 7696.0], [63.7, 7696.0], [63.8, 7696.0], [63.9, 7696.0], [64.0, 7954.0], [64.1, 7954.0], [64.2, 7954.0], [64.3, 7954.0], [64.4, 7954.0], [64.5, 7954.0], [64.6, 7954.0], [64.7, 7954.0], [64.8, 7954.0], [64.9, 7954.0], [65.0, 8302.0], [65.1, 8302.0], [65.2, 8302.0], [65.3, 8302.0], [65.4, 8302.0], [65.5, 8302.0], [65.6, 8302.0], [65.7, 8302.0], [65.8, 8302.0], [65.9, 8302.0], [66.0, 8367.0], [66.1, 8367.0], [66.2, 8367.0], [66.3, 8367.0], [66.4, 8367.0], [66.5, 8367.0], [66.6, 8367.0], [66.7, 8367.0], [66.8, 8367.0], [66.9, 8367.0], [67.0, 8687.0], [67.1, 8687.0], [67.2, 8687.0], [67.3, 8687.0], [67.4, 8687.0], [67.5, 8687.0], [67.6, 8687.0], [67.7, 8687.0], [67.8, 8687.0], [67.9, 8687.0], [68.0, 9356.0], [68.1, 9356.0], [68.2, 9356.0], [68.3, 9356.0], [68.4, 9356.0], [68.5, 9356.0], [68.6, 9356.0], [68.7, 9356.0], [68.8, 9356.0], [68.9, 9356.0], [69.0, 9591.0], [69.1, 9591.0], [69.2, 9591.0], [69.3, 9591.0], [69.4, 9591.0], [69.5, 9591.0], [69.6, 9591.0], [69.7, 9591.0], [69.8, 9591.0], [69.9, 9591.0], [70.0, 10405.0], [70.1, 10405.0], [70.2, 10405.0], [70.3, 10405.0], [70.4, 10405.0], [70.5, 10405.0], [70.6, 10405.0], [70.7, 10405.0], [70.8, 10405.0], [70.9, 10405.0], [71.0, 10907.0], [71.1, 10907.0], [71.2, 10907.0], [71.3, 10907.0], [71.4, 10907.0], [71.5, 10907.0], [71.6, 10907.0], [71.7, 10907.0], [71.8, 10907.0], [71.9, 10907.0], [72.0, 11056.0], [72.1, 11056.0], [72.2, 11056.0], [72.3, 11056.0], [72.4, 11056.0], [72.5, 11056.0], [72.6, 11056.0], [72.7, 11056.0], [72.8, 11056.0], [72.9, 11056.0], [73.0, 11675.0], [73.1, 11675.0], [73.2, 11675.0], [73.3, 11675.0], [73.4, 11675.0], [73.5, 11675.0], [73.6, 11675.0], [73.7, 11675.0], [73.8, 11675.0], [73.9, 11675.0], [74.0, 12289.0], [74.1, 12289.0], [74.2, 12289.0], [74.3, 12289.0], [74.4, 12289.0], [74.5, 12289.0], [74.6, 12289.0], [74.7, 12289.0], [74.8, 12289.0], [74.9, 12289.0], [75.0, 29735.0], [75.1, 29735.0], [75.2, 29735.0], [75.3, 29735.0], [75.4, 29735.0], [75.5, 29735.0], [75.6, 29735.0], [75.7, 29735.0], [75.8, 29735.0], [75.9, 29735.0], [76.0, 31088.0], [76.1, 31088.0], [76.2, 31088.0], [76.3, 31088.0], [76.4, 31088.0], [76.5, 31088.0], [76.6, 31088.0], [76.7, 31088.0], [76.8, 31088.0], [76.9, 31088.0], [77.0, 31738.0], [77.1, 31738.0], [77.2, 31738.0], [77.3, 31738.0], [77.4, 31738.0], [77.5, 31738.0], [77.6, 31738.0], [77.7, 31738.0], [77.8, 31738.0], [77.9, 31738.0], [78.0, 32412.0], [78.1, 32412.0], [78.2, 32412.0], [78.3, 32412.0], [78.4, 32412.0], [78.5, 32412.0], [78.6, 32412.0], [78.7, 32412.0], [78.8, 32412.0], [78.9, 32412.0], [79.0, 33787.0], [79.1, 33787.0], [79.2, 33787.0], [79.3, 33787.0], [79.4, 33787.0], [79.5, 33787.0], [79.6, 33787.0], [79.7, 33787.0], [79.8, 33787.0], [79.9, 33787.0], [80.0, 34552.0], [80.1, 34552.0], [80.2, 34552.0], [80.3, 34552.0], [80.4, 34552.0], [80.5, 34552.0], [80.6, 34552.0], [80.7, 34552.0], [80.8, 34552.0], [80.9, 34552.0], [81.0, 34757.0], [81.1, 34757.0], [81.2, 34757.0], [81.3, 34757.0], [81.4, 34757.0], [81.5, 34757.0], [81.6, 34757.0], [81.7, 34757.0], [81.8, 34757.0], [81.9, 34757.0], [82.0, 35012.0], [82.1, 35012.0], [82.2, 35012.0], [82.3, 35012.0], [82.4, 35012.0], [82.5, 35012.0], [82.6, 35012.0], [82.7, 35012.0], [82.8, 35012.0], [82.9, 35012.0], [83.0, 35087.0], [83.1, 35087.0], [83.2, 35087.0], [83.3, 35087.0], [83.4, 35087.0], [83.5, 35087.0], [83.6, 35087.0], [83.7, 35087.0], [83.8, 35087.0], [83.9, 35087.0], [84.0, 35115.0], [84.1, 35115.0], [84.2, 35115.0], [84.3, 35115.0], [84.4, 35115.0], [84.5, 35115.0], [84.6, 35115.0], [84.7, 35115.0], [84.8, 35115.0], [84.9, 35115.0], [85.0, 35616.0], [85.1, 35616.0], [85.2, 35616.0], [85.3, 35616.0], [85.4, 35616.0], [85.5, 35616.0], [85.6, 35616.0], [85.7, 35616.0], [85.8, 35616.0], [85.9, 35616.0], [86.0, 35644.0], [86.1, 35644.0], [86.2, 35644.0], [86.3, 35644.0], [86.4, 35644.0], [86.5, 35644.0], [86.6, 35644.0], [86.7, 35644.0], [86.8, 35644.0], [86.9, 35644.0], [87.0, 35661.0], [87.1, 35661.0], [87.2, 35661.0], [87.3, 35661.0], [87.4, 35661.0], [87.5, 35661.0], [87.6, 35661.0], [87.7, 35661.0], [87.8, 35661.0], [87.9, 35661.0], [88.0, 35694.0], [88.1, 35694.0], [88.2, 35694.0], [88.3, 35694.0], [88.4, 35694.0], [88.5, 35694.0], [88.6, 35694.0], [88.7, 35694.0], [88.8, 35694.0], [88.9, 35694.0], [89.0, 35717.0], [89.1, 35717.0], [89.2, 35717.0], [89.3, 35717.0], [89.4, 35717.0], [89.5, 35717.0], [89.6, 35717.0], [89.7, 35717.0], [89.8, 35717.0], [89.9, 35717.0], [90.0, 35720.0], [90.1, 35720.0], [90.2, 35720.0], [90.3, 35720.0], [90.4, 35720.0], [90.5, 35720.0], [90.6, 35720.0], [90.7, 35720.0], [90.8, 35720.0], [90.9, 35720.0], [91.0, 35779.0], [91.1, 35779.0], [91.2, 35779.0], [91.3, 35779.0], [91.4, 35779.0], [91.5, 35779.0], [91.6, 35779.0], [91.7, 35779.0], [91.8, 35779.0], [91.9, 35779.0], [92.0, 35967.0], [92.1, 35967.0], [92.2, 35967.0], [92.3, 35967.0], [92.4, 35967.0], [92.5, 35967.0], [92.6, 35967.0], [92.7, 35967.0], [92.8, 35967.0], [92.9, 35967.0], [93.0, 35971.0], [93.1, 35971.0], [93.2, 35971.0], [93.3, 35971.0], [93.4, 35971.0], [93.5, 35971.0], [93.6, 35971.0], [93.7, 35971.0], [93.8, 35971.0], [93.9, 35971.0], [94.0, 36039.0], [94.1, 36039.0], [94.2, 36039.0], [94.3, 36039.0], [94.4, 36039.0], [94.5, 36039.0], [94.6, 36039.0], [94.7, 36039.0], [94.8, 36039.0], [94.9, 36039.0], [95.0, 36244.0], [95.1, 36244.0], [95.2, 36244.0], [95.3, 36244.0], [95.4, 36244.0], [95.5, 36244.0], [95.6, 36244.0], [95.7, 36244.0], [95.8, 36244.0], [95.9, 36244.0], [96.0, 36255.0], [96.1, 36255.0], [96.2, 36255.0], [96.3, 36255.0], [96.4, 36255.0], [96.5, 36255.0], [96.6, 36255.0], [96.7, 36255.0], [96.8, 36255.0], [96.9, 36255.0], [97.0, 36902.0], [97.1, 36902.0], [97.2, 36902.0], [97.3, 36902.0], [97.4, 36902.0], [97.5, 36902.0], [97.6, 36902.0], [97.7, 36902.0], [97.8, 36902.0], [97.9, 36902.0], [98.0, 37341.0], [98.1, 37341.0], [98.2, 37341.0], [98.3, 37341.0], [98.4, 37341.0], [98.5, 37341.0], [98.6, 37341.0], [98.7, 37341.0], [98.8, 37341.0], [98.9, 37341.0], [99.0, 43511.0], [99.1, 43511.0], [99.2, 43511.0], [99.3, 43511.0], [99.4, 43511.0], [99.5, 43511.0], [99.6, 43511.0], [99.7, 43511.0], [99.8, 43511.0], [99.9, 43511.0]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 400.0, "maxY": 4.0, "series": [{"data": [[2800.0, 1.0], [2900.0, 1.0], [3300.0, 2.0], [3400.0, 1.0], [3500.0, 2.0], [3800.0, 1.0], [3900.0, 4.0], [4000.0, 3.0], [4200.0, 2.0], [4100.0, 2.0], [4300.0, 2.0], [4500.0, 2.0], [4600.0, 3.0], [4400.0, 1.0], [4700.0, 2.0], [4800.0, 1.0], [5000.0, 1.0], [5100.0, 2.0], [5200.0, 3.0], [5600.0, 1.0], [5400.0, 1.0], [5500.0, 1.0], [5800.0, 4.0], [5700.0, 1.0], [5900.0, 2.0], [6100.0, 2.0], [6000.0, 1.0], [6200.0, 1.0], [6300.0, 2.0], [6500.0, 2.0], [6600.0, 1.0], [6800.0, 2.0], [6900.0, 1.0], [7100.0, 1.0], [7300.0, 1.0], [7500.0, 2.0], [7600.0, 1.0], [7900.0, 1.0], [8300.0, 2.0], [8600.0, 1.0], [9300.0, 1.0], [9500.0, 1.0], [10400.0, 1.0], [10900.0, 1.0], [11000.0, 1.0], [11600.0, 1.0], [12200.0, 1.0], [29700.0, 1.0], [31000.0, 1.0], [31700.0, 1.0], [32400.0, 1.0], [33700.0, 1.0], [34500.0, 1.0], [34700.0, 1.0], [35000.0, 2.0], [35600.0, 4.0], [35100.0, 1.0], [35700.0, 3.0], [35900.0, 2.0], [36200.0, 2.0], [36000.0, 1.0], [36900.0, 1.0], [37300.0, 1.0], [43500.0, 1.0], [400.0, 1.0]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 43500.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 0.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 99.0, "series": [{"data": [[0.0, 1.0]], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 99.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 12.440000000000001, "minX": 1.62292116E12, "maxY": 24.653333333333336, "series": [{"data": [[1.62292122E12, 12.440000000000001], [1.62292116E12, 24.653333333333336]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292122E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 497.0, "minX": 1.0, "maxY": 18718.966101694914, "series": [{"data": [[8.0, 4913.666666666667], [11.0, 4876.666666666667], [12.0, 3315.0], [3.0, 4285.333333333333], [15.0, 4892.0], [16.0, 7696.0], [1.0, 497.0], [17.0, 4085.0], [18.0, 4368.0], [19.0, 3856.0], [20.0, 4345.0], [5.0, 5167.0], [22.0, 5271.0], [23.0, 6367.0], [24.0, 6119.375], [6.0, 3991.0], [25.0, 18718.966101694914]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}, {"data": [[21.600000000000005, 13164.99]], "isOverall": false, "label": "HTTP Request_param_of_user-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 25.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 171.66666666666666, "minX": 1.62292116E12, "maxY": 73333.75, "series": [{"data": [[1.62292122E12, 24444.583333333332], [1.62292116E12, 73333.75]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62292122E12, 171.66666666666666], [1.62292116E12, 515.0]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292122E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 4595.0, "minX": 1.62292116E12, "maxY": 16021.653333333334, "series": [{"data": [[1.62292122E12, 4595.0], [1.62292116E12, 16021.653333333334]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292122E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 4559.8, "minX": 1.62292116E12, "maxY": 15938.093333333329, "series": [{"data": [[1.62292122E12, 4559.8], [1.62292116E12, 15938.093333333329]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292122E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 25.360000000000003, "minX": 1.62292116E12, "maxY": 79.87999999999997, "series": [{"data": [[1.62292122E12, 25.360000000000003], [1.62292116E12, 79.87999999999997]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292122E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 497.0, "minX": 1.62292116E12, "maxY": 43511.0, "series": [{"data": [[1.62292122E12, 7954.0], [1.62292116E12, 43511.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62292122E12, 497.0], [1.62292116E12, 2862.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62292122E12, 6421.000000000005], [1.62292116E12, 35968.6]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62292122E12, 7954.0], [1.62292116E12, 43511.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62292122E12, 7876.599999999999], [1.62292116E12, 36384.4]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292122E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 5272.0, "minX": 1.0, "maxY": 34884.5, "series": [{"data": [[1.0, 19545.5], [2.0, 6237.5], [4.0, 34884.5], [9.0, 6217.5], [5.0, 8302.0], [6.0, 6027.0], [3.0, 8772.5], [7.0, 5272.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 9.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 5233.0, "minX": 1.0, "maxY": 34877.5, "series": [{"data": [[1.0, 19476.5], [2.0, 6188.0], [4.0, 34877.5], [9.0, 6192.5], [5.0, 7634.0], [6.0, 5982.5], [3.0, 8750.0], [7.0, 5233.0]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 9.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.08333333333333333, "minX": 1.62292116E12, "maxY": 1.5833333333333333, "series": [{"data": [[1.62292122E12, 0.08333333333333333], [1.62292116E12, 1.5833333333333333]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292122E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.4166666666666667, "minX": 1.62292116E12, "maxY": 1.25, "series": [{"data": [[1.62292122E12, 0.4166666666666667], [1.62292116E12, 1.25]], "isOverall": false, "label": "200", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292122E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.4166666666666667, "minX": 1.62292116E12, "maxY": 1.25, "series": [{"data": [[1.62292122E12, 0.4166666666666667], [1.62292116E12, 1.25]], "isOverall": false, "label": "HTTP Request_param_of_user-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292122E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.4166666666666667, "minX": 1.62292116E12, "maxY": 1.25, "series": [{"data": [[1.62292122E12, 0.4166666666666667], [1.62292116E12, 1.25]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292122E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

