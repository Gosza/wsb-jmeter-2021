/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 44795.0, "minX": 0.0, "maxY": 81094.0, "series": [{"data": [[0.0, 44795.0], [0.1, 44795.0], [0.2, 44795.0], [0.3, 44795.0], [0.4, 44795.0], [0.5, 44795.0], [0.6, 44795.0], [0.7, 44795.0], [0.8, 44795.0], [0.9, 44795.0], [1.0, 44796.0], [1.1, 44796.0], [1.2, 44796.0], [1.3, 44796.0], [1.4, 44796.0], [1.5, 44796.0], [1.6, 44796.0], [1.7, 44796.0], [1.8, 44796.0], [1.9, 44796.0], [2.0, 44872.0], [2.1, 44872.0], [2.2, 44872.0], [2.3, 44872.0], [2.4, 44872.0], [2.5, 44872.0], [2.6, 44872.0], [2.7, 44872.0], [2.8, 44872.0], [2.9, 44872.0], [3.0, 44892.0], [3.1, 44892.0], [3.2, 44892.0], [3.3, 44892.0], [3.4, 44892.0], [3.5, 44892.0], [3.6, 44892.0], [3.7, 44892.0], [3.8, 44892.0], [3.9, 44892.0], [4.0, 44945.0], [4.1, 44945.0], [4.2, 44945.0], [4.3, 44945.0], [4.4, 44945.0], [4.5, 44945.0], [4.6, 44945.0], [4.7, 44945.0], [4.8, 44945.0], [4.9, 44945.0], [5.0, 45392.0], [5.1, 45392.0], [5.2, 45392.0], [5.3, 45392.0], [5.4, 45392.0], [5.5, 45392.0], [5.6, 45392.0], [5.7, 45392.0], [5.8, 45392.0], [5.9, 45392.0], [6.0, 45394.0], [6.1, 45394.0], [6.2, 45394.0], [6.3, 45394.0], [6.4, 45394.0], [6.5, 45394.0], [6.6, 45394.0], [6.7, 45394.0], [6.8, 45394.0], [6.9, 45394.0], [7.0, 45394.0], [7.1, 45394.0], [7.2, 45394.0], [7.3, 45394.0], [7.4, 45394.0], [7.5, 45394.0], [7.6, 45394.0], [7.7, 45394.0], [7.8, 45394.0], [7.9, 45394.0], [8.0, 45697.0], [8.1, 45697.0], [8.2, 45697.0], [8.3, 45697.0], [8.4, 45697.0], [8.5, 45697.0], [8.6, 45697.0], [8.7, 45697.0], [8.8, 45697.0], [8.9, 45697.0], [9.0, 46121.0], [9.1, 46121.0], [9.2, 46121.0], [9.3, 46121.0], [9.4, 46121.0], [9.5, 46121.0], [9.6, 46121.0], [9.7, 46121.0], [9.8, 46121.0], [9.9, 46121.0], [10.0, 46346.0], [10.1, 46346.0], [10.2, 46346.0], [10.3, 46346.0], [10.4, 46346.0], [10.5, 46346.0], [10.6, 46346.0], [10.7, 46346.0], [10.8, 46346.0], [10.9, 46346.0], [11.0, 46468.0], [11.1, 46468.0], [11.2, 46468.0], [11.3, 46468.0], [11.4, 46468.0], [11.5, 46468.0], [11.6, 46468.0], [11.7, 46468.0], [11.8, 46468.0], [11.9, 46468.0], [12.0, 46482.0], [12.1, 46482.0], [12.2, 46482.0], [12.3, 46482.0], [12.4, 46482.0], [12.5, 46482.0], [12.6, 46482.0], [12.7, 46482.0], [12.8, 46482.0], [12.9, 46482.0], [13.0, 46637.0], [13.1, 46637.0], [13.2, 46637.0], [13.3, 46637.0], [13.4, 46637.0], [13.5, 46637.0], [13.6, 46637.0], [13.7, 46637.0], [13.8, 46637.0], [13.9, 46637.0], [14.0, 47036.0], [14.1, 47036.0], [14.2, 47036.0], [14.3, 47036.0], [14.4, 47036.0], [14.5, 47036.0], [14.6, 47036.0], [14.7, 47036.0], [14.8, 47036.0], [14.9, 47036.0], [15.0, 47250.0], [15.1, 47250.0], [15.2, 47250.0], [15.3, 47250.0], [15.4, 47250.0], [15.5, 47250.0], [15.6, 47250.0], [15.7, 47250.0], [15.8, 47250.0], [15.9, 47250.0], [16.0, 48000.0], [16.1, 48000.0], [16.2, 48000.0], [16.3, 48000.0], [16.4, 48000.0], [16.5, 48000.0], [16.6, 48000.0], [16.7, 48000.0], [16.8, 48000.0], [16.9, 48000.0], [17.0, 48401.0], [17.1, 48401.0], [17.2, 48401.0], [17.3, 48401.0], [17.4, 48401.0], [17.5, 48401.0], [17.6, 48401.0], [17.7, 48401.0], [17.8, 48401.0], [17.9, 48401.0], [18.0, 48554.0], [18.1, 48554.0], [18.2, 48554.0], [18.3, 48554.0], [18.4, 48554.0], [18.5, 48554.0], [18.6, 48554.0], [18.7, 48554.0], [18.8, 48554.0], [18.9, 48554.0], [19.0, 48576.0], [19.1, 48576.0], [19.2, 48576.0], [19.3, 48576.0], [19.4, 48576.0], [19.5, 48576.0], [19.6, 48576.0], [19.7, 48576.0], [19.8, 48576.0], [19.9, 48576.0], [20.0, 48599.0], [20.1, 48599.0], [20.2, 48599.0], [20.3, 48599.0], [20.4, 48599.0], [20.5, 48599.0], [20.6, 48599.0], [20.7, 48599.0], [20.8, 48599.0], [20.9, 48599.0], [21.0, 48684.0], [21.1, 48684.0], [21.2, 48684.0], [21.3, 48684.0], [21.4, 48684.0], [21.5, 48684.0], [21.6, 48684.0], [21.7, 48684.0], [21.8, 48684.0], [21.9, 48684.0], [22.0, 48689.0], [22.1, 48689.0], [22.2, 48689.0], [22.3, 48689.0], [22.4, 48689.0], [22.5, 48689.0], [22.6, 48689.0], [22.7, 48689.0], [22.8, 48689.0], [22.9, 48689.0], [23.0, 48717.0], [23.1, 48717.0], [23.2, 48717.0], [23.3, 48717.0], [23.4, 48717.0], [23.5, 48717.0], [23.6, 48717.0], [23.7, 48717.0], [23.8, 48717.0], [23.9, 48717.0], [24.0, 48760.0], [24.1, 48760.0], [24.2, 48760.0], [24.3, 48760.0], [24.4, 48760.0], [24.5, 48760.0], [24.6, 48760.0], [24.7, 48760.0], [24.8, 48760.0], [24.9, 48760.0], [25.0, 48781.0], [25.1, 48781.0], [25.2, 48781.0], [25.3, 48781.0], [25.4, 48781.0], [25.5, 48781.0], [25.6, 48781.0], [25.7, 48781.0], [25.8, 48781.0], [25.9, 48781.0], [26.0, 48838.0], [26.1, 48838.0], [26.2, 48838.0], [26.3, 48838.0], [26.4, 48838.0], [26.5, 48838.0], [26.6, 48838.0], [26.7, 48838.0], [26.8, 48838.0], [26.9, 48838.0], [27.0, 48841.0], [27.1, 48841.0], [27.2, 48841.0], [27.3, 48841.0], [27.4, 48841.0], [27.5, 48841.0], [27.6, 48841.0], [27.7, 48841.0], [27.8, 48841.0], [27.9, 48841.0], [28.0, 48884.0], [28.1, 48884.0], [28.2, 48884.0], [28.3, 48884.0], [28.4, 48884.0], [28.5, 48884.0], [28.6, 48884.0], [28.7, 48884.0], [28.8, 48884.0], [28.9, 48884.0], [29.0, 48948.0], [29.1, 48948.0], [29.2, 48948.0], [29.3, 48948.0], [29.4, 48948.0], [29.5, 48948.0], [29.6, 48948.0], [29.7, 48948.0], [29.8, 48948.0], [29.9, 48948.0], [30.0, 48969.0], [30.1, 48969.0], [30.2, 48969.0], [30.3, 48969.0], [30.4, 48969.0], [30.5, 48969.0], [30.6, 48969.0], [30.7, 48969.0], [30.8, 48969.0], [30.9, 48969.0], [31.0, 49011.0], [31.1, 49011.0], [31.2, 49011.0], [31.3, 49011.0], [31.4, 49011.0], [31.5, 49011.0], [31.6, 49011.0], [31.7, 49011.0], [31.8, 49011.0], [31.9, 49011.0], [32.0, 49015.0], [32.1, 49015.0], [32.2, 49015.0], [32.3, 49015.0], [32.4, 49015.0], [32.5, 49015.0], [32.6, 49015.0], [32.7, 49015.0], [32.8, 49015.0], [32.9, 49015.0], [33.0, 49041.0], [33.1, 49041.0], [33.2, 49041.0], [33.3, 49041.0], [33.4, 49041.0], [33.5, 49041.0], [33.6, 49041.0], [33.7, 49041.0], [33.8, 49041.0], [33.9, 49041.0], [34.0, 49050.0], [34.1, 49050.0], [34.2, 49050.0], [34.3, 49050.0], [34.4, 49050.0], [34.5, 49050.0], [34.6, 49050.0], [34.7, 49050.0], [34.8, 49050.0], [34.9, 49050.0], [35.0, 49231.0], [35.1, 49231.0], [35.2, 49231.0], [35.3, 49231.0], [35.4, 49231.0], [35.5, 49231.0], [35.6, 49231.0], [35.7, 49231.0], [35.8, 49231.0], [35.9, 49231.0], [36.0, 49253.0], [36.1, 49253.0], [36.2, 49253.0], [36.3, 49253.0], [36.4, 49253.0], [36.5, 49253.0], [36.6, 49253.0], [36.7, 49253.0], [36.8, 49253.0], [36.9, 49253.0], [37.0, 49300.0], [37.1, 49300.0], [37.2, 49300.0], [37.3, 49300.0], [37.4, 49300.0], [37.5, 49300.0], [37.6, 49300.0], [37.7, 49300.0], [37.8, 49300.0], [37.9, 49300.0], [38.0, 49348.0], [38.1, 49348.0], [38.2, 49348.0], [38.3, 49348.0], [38.4, 49348.0], [38.5, 49348.0], [38.6, 49348.0], [38.7, 49348.0], [38.8, 49348.0], [38.9, 49348.0], [39.0, 49474.0], [39.1, 49474.0], [39.2, 49474.0], [39.3, 49474.0], [39.4, 49474.0], [39.5, 49474.0], [39.6, 49474.0], [39.7, 49474.0], [39.8, 49474.0], [39.9, 49474.0], [40.0, 49730.0], [40.1, 49730.0], [40.2, 49730.0], [40.3, 49730.0], [40.4, 49730.0], [40.5, 49730.0], [40.6, 49730.0], [40.7, 49730.0], [40.8, 49730.0], [40.9, 49730.0], [41.0, 49986.0], [41.1, 49986.0], [41.2, 49986.0], [41.3, 49986.0], [41.4, 49986.0], [41.5, 49986.0], [41.6, 49986.0], [41.7, 49986.0], [41.8, 49986.0], [41.9, 49986.0], [42.0, 50861.0], [42.1, 50861.0], [42.2, 50861.0], [42.3, 50861.0], [42.4, 50861.0], [42.5, 50861.0], [42.6, 50861.0], [42.7, 50861.0], [42.8, 50861.0], [42.9, 50861.0], [43.0, 51989.0], [43.1, 51989.0], [43.2, 51989.0], [43.3, 51989.0], [43.4, 51989.0], [43.5, 51989.0], [43.6, 51989.0], [43.7, 51989.0], [43.8, 51989.0], [43.9, 51989.0], [44.0, 52034.0], [44.1, 52034.0], [44.2, 52034.0], [44.3, 52034.0], [44.4, 52034.0], [44.5, 52034.0], [44.6, 52034.0], [44.7, 52034.0], [44.8, 52034.0], [44.9, 52034.0], [45.0, 52305.0], [45.1, 52305.0], [45.2, 52305.0], [45.3, 52305.0], [45.4, 52305.0], [45.5, 52305.0], [45.6, 52305.0], [45.7, 52305.0], [45.8, 52305.0], [45.9, 52305.0], [46.0, 52472.0], [46.1, 52472.0], [46.2, 52472.0], [46.3, 52472.0], [46.4, 52472.0], [46.5, 52472.0], [46.6, 52472.0], [46.7, 52472.0], [46.8, 52472.0], [46.9, 52472.0], [47.0, 52498.0], [47.1, 52498.0], [47.2, 52498.0], [47.3, 52498.0], [47.4, 52498.0], [47.5, 52498.0], [47.6, 52498.0], [47.7, 52498.0], [47.8, 52498.0], [47.9, 52498.0], [48.0, 52571.0], [48.1, 52571.0], [48.2, 52571.0], [48.3, 52571.0], [48.4, 52571.0], [48.5, 52571.0], [48.6, 52571.0], [48.7, 52571.0], [48.8, 52571.0], [48.9, 52571.0], [49.0, 52633.0], [49.1, 52633.0], [49.2, 52633.0], [49.3, 52633.0], [49.4, 52633.0], [49.5, 52633.0], [49.6, 52633.0], [49.7, 52633.0], [49.8, 52633.0], [49.9, 52633.0], [50.0, 52647.0], [50.1, 52647.0], [50.2, 52647.0], [50.3, 52647.0], [50.4, 52647.0], [50.5, 52647.0], [50.6, 52647.0], [50.7, 52647.0], [50.8, 52647.0], [50.9, 52647.0], [51.0, 52818.0], [51.1, 52818.0], [51.2, 52818.0], [51.3, 52818.0], [51.4, 52818.0], [51.5, 52818.0], [51.6, 52818.0], [51.7, 52818.0], [51.8, 52818.0], [51.9, 52818.0], [52.0, 52936.0], [52.1, 52936.0], [52.2, 52936.0], [52.3, 52936.0], [52.4, 52936.0], [52.5, 52936.0], [52.6, 52936.0], [52.7, 52936.0], [52.8, 52936.0], [52.9, 52936.0], [53.0, 52979.0], [53.1, 52979.0], [53.2, 52979.0], [53.3, 52979.0], [53.4, 52979.0], [53.5, 52979.0], [53.6, 52979.0], [53.7, 52979.0], [53.8, 52979.0], [53.9, 52979.0], [54.0, 52988.0], [54.1, 52988.0], [54.2, 52988.0], [54.3, 52988.0], [54.4, 52988.0], [54.5, 52988.0], [54.6, 52988.0], [54.7, 52988.0], [54.8, 52988.0], [54.9, 52988.0], [55.0, 53052.0], [55.1, 53052.0], [55.2, 53052.0], [55.3, 53052.0], [55.4, 53052.0], [55.5, 53052.0], [55.6, 53052.0], [55.7, 53052.0], [55.8, 53052.0], [55.9, 53052.0], [56.0, 53068.0], [56.1, 53068.0], [56.2, 53068.0], [56.3, 53068.0], [56.4, 53068.0], [56.5, 53068.0], [56.6, 53068.0], [56.7, 53068.0], [56.8, 53068.0], [56.9, 53068.0], [57.0, 53186.0], [57.1, 53186.0], [57.2, 53186.0], [57.3, 53186.0], [57.4, 53186.0], [57.5, 53186.0], [57.6, 53186.0], [57.7, 53186.0], [57.8, 53186.0], [57.9, 53186.0], [58.0, 53194.0], [58.1, 53194.0], [58.2, 53194.0], [58.3, 53194.0], [58.4, 53194.0], [58.5, 53194.0], [58.6, 53194.0], [58.7, 53194.0], [58.8, 53194.0], [58.9, 53194.0], [59.0, 53476.0], [59.1, 53476.0], [59.2, 53476.0], [59.3, 53476.0], [59.4, 53476.0], [59.5, 53476.0], [59.6, 53476.0], [59.7, 53476.0], [59.8, 53476.0], [59.9, 53476.0], [60.0, 53756.0], [60.1, 53756.0], [60.2, 53756.0], [60.3, 53756.0], [60.4, 53756.0], [60.5, 53756.0], [60.6, 53756.0], [60.7, 53756.0], [60.8, 53756.0], [60.9, 53756.0], [61.0, 54725.0], [61.1, 54725.0], [61.2, 54725.0], [61.3, 54725.0], [61.4, 54725.0], [61.5, 54725.0], [61.6, 54725.0], [61.7, 54725.0], [61.8, 54725.0], [61.9, 54725.0], [62.0, 55347.0], [62.1, 55347.0], [62.2, 55347.0], [62.3, 55347.0], [62.4, 55347.0], [62.5, 55347.0], [62.6, 55347.0], [62.7, 55347.0], [62.8, 55347.0], [62.9, 55347.0], [63.0, 55368.0], [63.1, 55368.0], [63.2, 55368.0], [63.3, 55368.0], [63.4, 55368.0], [63.5, 55368.0], [63.6, 55368.0], [63.7, 55368.0], [63.8, 55368.0], [63.9, 55368.0], [64.0, 55377.0], [64.1, 55377.0], [64.2, 55377.0], [64.3, 55377.0], [64.4, 55377.0], [64.5, 55377.0], [64.6, 55377.0], [64.7, 55377.0], [64.8, 55377.0], [64.9, 55377.0], [65.0, 55746.0], [65.1, 55746.0], [65.2, 55746.0], [65.3, 55746.0], [65.4, 55746.0], [65.5, 55746.0], [65.6, 55746.0], [65.7, 55746.0], [65.8, 55746.0], [65.9, 55746.0], [66.0, 55800.0], [66.1, 55800.0], [66.2, 55800.0], [66.3, 55800.0], [66.4, 55800.0], [66.5, 55800.0], [66.6, 55800.0], [66.7, 55800.0], [66.8, 55800.0], [66.9, 55800.0], [67.0, 56356.0], [67.1, 56356.0], [67.2, 56356.0], [67.3, 56356.0], [67.4, 56356.0], [67.5, 56356.0], [67.6, 56356.0], [67.7, 56356.0], [67.8, 56356.0], [67.9, 56356.0], [68.0, 56464.0], [68.1, 56464.0], [68.2, 56464.0], [68.3, 56464.0], [68.4, 56464.0], [68.5, 56464.0], [68.6, 56464.0], [68.7, 56464.0], [68.8, 56464.0], [68.9, 56464.0], [69.0, 56529.0], [69.1, 56529.0], [69.2, 56529.0], [69.3, 56529.0], [69.4, 56529.0], [69.5, 56529.0], [69.6, 56529.0], [69.7, 56529.0], [69.8, 56529.0], [69.9, 56529.0], [70.0, 57792.0], [70.1, 57792.0], [70.2, 57792.0], [70.3, 57792.0], [70.4, 57792.0], [70.5, 57792.0], [70.6, 57792.0], [70.7, 57792.0], [70.8, 57792.0], [70.9, 57792.0], [71.0, 58366.0], [71.1, 58366.0], [71.2, 58366.0], [71.3, 58366.0], [71.4, 58366.0], [71.5, 58366.0], [71.6, 58366.0], [71.7, 58366.0], [71.8, 58366.0], [71.9, 58366.0], [72.0, 58379.0], [72.1, 58379.0], [72.2, 58379.0], [72.3, 58379.0], [72.4, 58379.0], [72.5, 58379.0], [72.6, 58379.0], [72.7, 58379.0], [72.8, 58379.0], [72.9, 58379.0], [73.0, 58710.0], [73.1, 58710.0], [73.2, 58710.0], [73.3, 58710.0], [73.4, 58710.0], [73.5, 58710.0], [73.6, 58710.0], [73.7, 58710.0], [73.8, 58710.0], [73.9, 58710.0], [74.0, 58938.0], [74.1, 58938.0], [74.2, 58938.0], [74.3, 58938.0], [74.4, 58938.0], [74.5, 58938.0], [74.6, 58938.0], [74.7, 58938.0], [74.8, 58938.0], [74.9, 58938.0], [75.0, 59014.0], [75.1, 59014.0], [75.2, 59014.0], [75.3, 59014.0], [75.4, 59014.0], [75.5, 59014.0], [75.6, 59014.0], [75.7, 59014.0], [75.8, 59014.0], [75.9, 59014.0], [76.0, 59117.0], [76.1, 59117.0], [76.2, 59117.0], [76.3, 59117.0], [76.4, 59117.0], [76.5, 59117.0], [76.6, 59117.0], [76.7, 59117.0], [76.8, 59117.0], [76.9, 59117.0], [77.0, 60074.0], [77.1, 60074.0], [77.2, 60074.0], [77.3, 60074.0], [77.4, 60074.0], [77.5, 60074.0], [77.6, 60074.0], [77.7, 60074.0], [77.8, 60074.0], [77.9, 60074.0], [78.0, 61230.0], [78.1, 61230.0], [78.2, 61230.0], [78.3, 61230.0], [78.4, 61230.0], [78.5, 61230.0], [78.6, 61230.0], [78.7, 61230.0], [78.8, 61230.0], [78.9, 61230.0], [79.0, 65236.0], [79.1, 65236.0], [79.2, 65236.0], [79.3, 65236.0], [79.4, 65236.0], [79.5, 65236.0], [79.6, 65236.0], [79.7, 65236.0], [79.8, 65236.0], [79.9, 65236.0], [80.0, 66825.0], [80.1, 66825.0], [80.2, 66825.0], [80.3, 66825.0], [80.4, 66825.0], [80.5, 66825.0], [80.6, 66825.0], [80.7, 66825.0], [80.8, 66825.0], [80.9, 66825.0], [81.0, 67914.0], [81.1, 67914.0], [81.2, 67914.0], [81.3, 67914.0], [81.4, 67914.0], [81.5, 67914.0], [81.6, 67914.0], [81.7, 67914.0], [81.8, 67914.0], [81.9, 67914.0], [82.0, 69195.0], [82.1, 69195.0], [82.2, 69195.0], [82.3, 69195.0], [82.4, 69195.0], [82.5, 69195.0], [82.6, 69195.0], [82.7, 69195.0], [82.8, 69195.0], [82.9, 69195.0], [83.0, 69208.0], [83.1, 69208.0], [83.2, 69208.0], [83.3, 69208.0], [83.4, 69208.0], [83.5, 69208.0], [83.6, 69208.0], [83.7, 69208.0], [83.8, 69208.0], [83.9, 69208.0], [84.0, 69318.0], [84.1, 69318.0], [84.2, 69318.0], [84.3, 69318.0], [84.4, 69318.0], [84.5, 69318.0], [84.6, 69318.0], [84.7, 69318.0], [84.8, 69318.0], [84.9, 69318.0], [85.0, 69359.0], [85.1, 69359.0], [85.2, 69359.0], [85.3, 69359.0], [85.4, 69359.0], [85.5, 69359.0], [85.6, 69359.0], [85.7, 69359.0], [85.8, 69359.0], [85.9, 69359.0], [86.0, 69366.0], [86.1, 69366.0], [86.2, 69366.0], [86.3, 69366.0], [86.4, 69366.0], [86.5, 69366.0], [86.6, 69366.0], [86.7, 69366.0], [86.8, 69366.0], [86.9, 69366.0], [87.0, 69426.0], [87.1, 69426.0], [87.2, 69426.0], [87.3, 69426.0], [87.4, 69426.0], [87.5, 69426.0], [87.6, 69426.0], [87.7, 69426.0], [87.8, 69426.0], [87.9, 69426.0], [88.0, 69657.0], [88.1, 69657.0], [88.2, 69657.0], [88.3, 69657.0], [88.4, 69657.0], [88.5, 69657.0], [88.6, 69657.0], [88.7, 69657.0], [88.8, 69657.0], [88.9, 69657.0], [89.0, 69863.0], [89.1, 69863.0], [89.2, 69863.0], [89.3, 69863.0], [89.4, 69863.0], [89.5, 69863.0], [89.6, 69863.0], [89.7, 69863.0], [89.8, 69863.0], [89.9, 69863.0], [90.0, 70060.0], [90.1, 70060.0], [90.2, 70060.0], [90.3, 70060.0], [90.4, 70060.0], [90.5, 70060.0], [90.6, 70060.0], [90.7, 70060.0], [90.8, 70060.0], [90.9, 70060.0], [91.0, 70284.0], [91.1, 70284.0], [91.2, 70284.0], [91.3, 70284.0], [91.4, 70284.0], [91.5, 70284.0], [91.6, 70284.0], [91.7, 70284.0], [91.8, 70284.0], [91.9, 70284.0], [92.0, 70449.0], [92.1, 70449.0], [92.2, 70449.0], [92.3, 70449.0], [92.4, 70449.0], [92.5, 70449.0], [92.6, 70449.0], [92.7, 70449.0], [92.8, 70449.0], [92.9, 70449.0], [93.0, 70455.0], [93.1, 70455.0], [93.2, 70455.0], [93.3, 70455.0], [93.4, 70455.0], [93.5, 70455.0], [93.6, 70455.0], [93.7, 70455.0], [93.8, 70455.0], [93.9, 70455.0], [94.0, 71462.0], [94.1, 71462.0], [94.2, 71462.0], [94.3, 71462.0], [94.4, 71462.0], [94.5, 71462.0], [94.6, 71462.0], [94.7, 71462.0], [94.8, 71462.0], [94.9, 71462.0], [95.0, 76451.0], [95.1, 76451.0], [95.2, 76451.0], [95.3, 76451.0], [95.4, 76451.0], [95.5, 76451.0], [95.6, 76451.0], [95.7, 76451.0], [95.8, 76451.0], [95.9, 76451.0], [96.0, 76852.0], [96.1, 76852.0], [96.2, 76852.0], [96.3, 76852.0], [96.4, 76852.0], [96.5, 76852.0], [96.6, 76852.0], [96.7, 76852.0], [96.8, 76852.0], [96.9, 76852.0], [97.0, 77021.0], [97.1, 77021.0], [97.2, 77021.0], [97.3, 77021.0], [97.4, 77021.0], [97.5, 77021.0], [97.6, 77021.0], [97.7, 77021.0], [97.8, 77021.0], [97.9, 77021.0], [98.0, 77214.0], [98.1, 77214.0], [98.2, 77214.0], [98.3, 77214.0], [98.4, 77214.0], [98.5, 77214.0], [98.6, 77214.0], [98.7, 77214.0], [98.8, 77214.0], [98.9, 77214.0], [99.0, 81094.0], [99.1, 81094.0], [99.2, 81094.0], [99.3, 81094.0], [99.4, 81094.0], [99.5, 81094.0], [99.6, 81094.0], [99.7, 81094.0], [99.8, 81094.0], [99.9, 81094.0]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 44700.0, "maxY": 4.0, "series": [{"data": [[69100.0, 1.0], [67900.0, 1.0], [69300.0, 3.0], [44700.0, 2.0], [44800.0, 2.0], [44900.0, 1.0], [45300.0, 3.0], [45600.0, 1.0], [46100.0, 1.0], [46400.0, 2.0], [46300.0, 1.0], [46600.0, 1.0], [47000.0, 1.0], [48500.0, 3.0], [48700.0, 3.0], [48600.0, 2.0], [48800.0, 3.0], [48000.0, 1.0], [48900.0, 2.0], [49000.0, 4.0], [48400.0, 1.0], [47200.0, 1.0], [49700.0, 1.0], [49200.0, 2.0], [49300.0, 2.0], [49400.0, 1.0], [50800.0, 1.0], [49900.0, 1.0], [52900.0, 3.0], [51900.0, 1.0], [52000.0, 1.0], [52300.0, 1.0], [52400.0, 2.0], [52500.0, 1.0], [52600.0, 2.0], [52800.0, 1.0], [53000.0, 2.0], [53100.0, 2.0], [53400.0, 1.0], [53700.0, 1.0], [54700.0, 1.0], [55700.0, 1.0], [55300.0, 3.0], [55800.0, 1.0], [56300.0, 1.0], [56400.0, 1.0], [56500.0, 1.0], [59000.0, 1.0], [59100.0, 1.0], [57700.0, 1.0], [58300.0, 2.0], [58700.0, 1.0], [58900.0, 1.0], [61200.0, 1.0], [60000.0, 1.0], [65200.0, 1.0], [69200.0, 1.0], [66800.0, 1.0], [69400.0, 1.0], [69600.0, 1.0], [69800.0, 1.0], [70000.0, 1.0], [70200.0, 1.0], [70400.0, 2.0], [71400.0, 1.0], [76400.0, 1.0], [76800.0, 1.0], [77000.0, 1.0], [77200.0, 1.0], [81000.0, 1.0]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 81000.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 2.0, "minX": 2.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 98.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 98.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [[3.0, 2.0]], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 3.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 12.18181818181818, "minX": 1.62292086E12, "maxY": 61.82051282051282, "series": [{"data": [[1.62292092E12, 12.18181818181818], [1.62292086E12, 61.82051282051282]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292092E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 44795.5, "minX": 1.0, "maxY": 81094.0, "series": [{"data": [[2.0, 77214.0], [3.0, 77021.0], [4.0, 76852.0], [5.0, 76451.0], [6.0, 71462.0], [8.0, 70452.0], [9.0, 70284.0], [10.0, 70060.0], [11.0, 69863.0], [12.0, 69657.0], [14.0, 69426.0], [15.0, 69362.5], [20.0, 69240.33333333333], [21.0, 66658.33333333333], [22.0, 60074.0], [23.0, 58938.0], [24.0, 61230.0], [25.0, 58710.0], [26.0, 58379.0], [27.0, 58366.0], [28.0, 57792.0], [29.0, 59117.0], [30.0, 59014.0], [31.0, 56529.0], [33.0, 56356.0], [32.0, 56464.0], [34.0, 55800.0], [37.0, 55368.0], [36.0, 55362.0], [39.0, 54725.0], [38.0, 55746.0], [40.0, 53756.0], [43.0, 53068.0], [42.0, 53190.0], [44.0, 53052.0], [47.0, 52818.0], [46.0, 52983.5], [49.0, 52647.0], [48.0, 52936.0], [51.0, 52571.0], [50.0, 52633.0], [52.0, 52498.0], [55.0, 49986.0], [54.0, 52974.0], [57.0, 52169.5], [59.0, 50861.0], [58.0, 51989.0], [61.0, 48362.0], [63.0, 48192.0], [67.0, 48948.0], [66.0, 49253.0], [65.0, 49300.0], [64.0, 49231.0], [70.0, 46637.0], [69.0, 48884.0], [68.0, 49050.0], [75.0, 48928.0], [73.0, 48817.666666666664], [79.0, 48684.0], [78.0, 48760.0], [77.0, 48000.0], [76.0, 48969.0], [83.0, 48781.0], [82.0, 48003.0], [87.0, 48348.0], [91.0, 46121.0], [90.0, 48565.0], [88.0, 46468.0], [95.0, 45393.333333333336], [92.0, 45697.0], [98.0, 44872.0], [97.0, 44892.0], [96.0, 44945.0], [100.0, 44795.5], [1.0, 81094.0]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}, {"data": [[50.89999999999999, 55367.01]], "isOverall": false, "label": "HTTP Request_param_of_user-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 151.06666666666666, "minX": 1.62292086E12, "maxY": 74333.46666666666, "series": [{"data": [[1.62292092E12, 21511.233333333334], [1.62292086E12, 74333.46666666666]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.62292092E12, 151.06666666666666], [1.62292086E12, 535.6]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292092E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 51024.58974358974, "minX": 1.62292086E12, "maxY": 70762.86363636362, "series": [{"data": [[1.62292092E12, 70762.86363636362], [1.62292086E12, 51024.58974358974]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292092E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 49046.83333333335, "minX": 1.62292086E12, "maxY": 68555.18181818181, "series": [{"data": [[1.62292092E12, 68555.18181818181], [1.62292086E12, 49046.83333333335]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292092E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 1.6363636363636362, "minX": 1.62292086E12, "maxY": 4.192307692307692, "series": [{"data": [[1.62292092E12, 1.6363636363636362], [1.62292086E12, 4.192307692307692]], "isOverall": false, "label": "HTTP Request_param_of_user", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292092E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 44795.0, "minX": 1.62292086E12, "maxY": 81094.0, "series": [{"data": [[1.62292092E12, 81094.0], [1.62292086E12, 61230.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.62292092E12, 60074.0], [1.62292086E12, 44795.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.62292092E12, 77156.1], [1.62292086E12, 57964.2]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.62292092E12, 81094.0], [1.62292086E12, 61230.0]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.62292092E12, 80511.99999999999], [1.62292086E12, 58949.4]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292092E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 45909.0, "minX": 1.0, "maxY": 76936.5, "series": [{"data": [[8.0, 45909.0], [1.0, 62655.0], [2.0, 69201.5], [4.0, 76936.5], [9.0, 52732.5], [5.0, 56051.0], [3.0, 70449.0], [28.0, 48862.5], [7.0, 69426.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[28.0, 46491.5]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 28.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 43586.0, "minX": 1.0, "maxY": 76906.5, "series": [{"data": [[8.0, 43586.0], [1.0, 60887.0], [2.0, 63577.5], [4.0, 76906.5], [9.0, 51078.5], [5.0, 55670.5], [3.0, 69165.0], [28.0, 46024.5], [7.0, 65255.0]], "isOverall": false, "label": "Successes", "isController": false}, {"data": [[28.0, 46490.5]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 28.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.8, "minX": 1.6229208E12, "maxY": 0.8666666666666667, "series": [{"data": [[1.62292086E12, 0.8], [1.6229208E12, 0.8666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292086E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.03333333333333333, "minX": 1.62292086E12, "maxY": 1.2666666666666666, "series": [{"data": [[1.62292092E12, 0.36666666666666664], [1.62292086E12, 1.2666666666666666]], "isOverall": false, "label": "200", "isController": false}, {"data": [[1.62292086E12, 0.03333333333333333]], "isOverall": false, "label": "500", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.62292092E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.03333333333333333, "minX": 1.62292086E12, "maxY": 1.2666666666666666, "series": [{"data": [[1.62292092E12, 0.36666666666666664], [1.62292086E12, 1.2666666666666666]], "isOverall": false, "label": "HTTP Request_param_of_user-success", "isController": false}, {"data": [[1.62292086E12, 0.03333333333333333]], "isOverall": false, "label": "HTTP Request_param_of_user-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292092E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.03333333333333333, "minX": 1.62292086E12, "maxY": 1.2666666666666666, "series": [{"data": [[1.62292092E12, 0.36666666666666664], [1.62292086E12, 1.2666666666666666]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [[1.62292086E12, 0.03333333333333333]], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.62292092E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

