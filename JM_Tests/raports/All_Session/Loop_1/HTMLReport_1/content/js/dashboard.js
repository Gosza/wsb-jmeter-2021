/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9130434782608695, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "Delete User"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request  logging into my account"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Page"], "isController": false}, {"data": [0.75, 500, 1500, " View Pages"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request_visit_login_page"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr100"], "isController": false}, {"data": [1.0, 500, 1500, "logoff"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request create blog page"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request home_page-0"], "isController": false}, {"data": [1.0, 500, 1500, "View Blog Post"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request home_page-1"], "isController": false}, {"data": [1.0, 500, 1500, "Delete Post"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request home_page"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request create post"], "isController": false}, {"data": [0.75, 500, 1500, "HTTP Request_param_of_user"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 23, 0, 0.0, 407.6521739130435, 0, 1172, 1154.0, 1170.8, 1172.0, 0.5124320470546296, 5.560274993594599, 0.14444787535647446], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["Delete User", 1, 0, 0.0, 459.0, 459, 459, 459.0, 459.0, 459.0, 2.1786492374727673, 2.919049564270152, 0.8403969226579521], "isController": false}, {"data": ["HTTP Request  logging into my account", 1, 0, 0.0, 432.0, 432, 432, 432.0, 432.0, 432.0, 2.314814814814815, 22.892704716435187, 1.2839988425925926], "isController": false}, {"data": ["Delete Page", 1, 0, 0.0, 433.0, 433, 433, 433.0, 433.0, 433.0, 2.3094688221709005, 3.545395496535797, 0.8908595554272517], "isController": false}, {"data": [" View Pages", 2, 0, 0.0, 763.5, 391, 1136, 1136.0, 1136.0, 1136.0, 0.04605323754259925, 0.6508167973427282, 0.010433936630745141], "isController": false}, {"data": ["HTTP Request_visit_login_page", 1, 0, 0.0, 345.0, 345, 345, 345.0, 345.0, 345.0, 2.898550724637681, 25.798233695652176, 0.6368885869565218], "isController": false}, {"data": ["HTTP Request add new userr100", 1, 0, 0.0, 484.0, 484, 484, 484.0, 484.0, 484.0, 2.066115702479339, 3.228305785123967, 1.077447055785124], "isController": false}, {"data": ["logoff", 1, 0, 0.0, 320.0, 320, 320, 320.0, 320.0, 320.0, 3.125, 27.813720703125, 0.9124755859375], "isController": false}, {"data": ["HTTP Request create blog page", 1, 0, 0.0, 414.0, 414, 414, 414.0, 414.0, 414.0, 2.4154589371980677, 6.295761624396135, 1.1723467693236715], "isController": false}, {"data": ["Debug Sampler", 5, 0, 0.0, 0.2, 0, 1, 1.0, 1.0, 1.0, 0.2220643098241251, 0.30759376665482324, 0.0], "isController": false}, {"data": ["HTTP Request home_page-0", 1, 0, 0.0, 6.0, 6, 6, 6.0, 6.0, 6.0, 166.66666666666666, 101.23697916666667, 34.505208333333336], "isController": false}, {"data": ["View Blog Post", 2, 0, 0.0, 400.0, 397, 403, 403.0, 403.0, 403.0, 0.048755515467687284, 0.8299626639404207, 0.016759708442017504], "isController": false}, {"data": ["HTTP Request home_page-1", 1, 0, 0.0, 1166.0, 1166, 1166, 1166.0, 1166.0, 1166.0, 0.8576329331046312, 14.77071719554031, 0.17839435034305318], "isController": false}, {"data": ["Delete Post", 1, 0, 0.0, 419.0, 419, 419, 419.0, 419.0, 419.0, 2.3866348448687353, 3.8223448687350836, 0.9136336515513127], "isController": false}, {"data": ["HTTP Request home_page", 1, 0, 0.0, 1172.0, 1172, 1172, 1172.0, 1172.0, 1172.0, 0.8532423208191127, 15.213377239761092, 0.35412889291808874], "isController": false}, {"data": ["HTTP Request create post", 1, 0, 0.0, 440.0, 440, 440, 440.0, 440.0, 440.0, 2.2727272727272725, 7.670454545454546, 1.0919744318181819], "isController": false}, {"data": ["HTTP Request_param_of_user", 2, 0, 0.0, 479.0, 398, 560, 560.0, 560.0, 560.0, 0.08244023083264633, 4.326501957955482, 0.033169311624072544], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 23, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
