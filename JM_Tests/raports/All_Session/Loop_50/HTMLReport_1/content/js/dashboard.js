/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 92.37288135593221, "KoPercent": 7.627118644067797};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.1271186440677966, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "HTTP Request add new userr140"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr141"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr142"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr143"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr100"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr144"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr101"], "isController": false}, {"data": [0.041666666666666664, 500, 1500, "HTTP Request create blog page"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr145"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr102"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr146"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr103"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr147"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr104"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr148"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr105"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr149"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr106"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr107"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr108"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr109"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request_param_of_user"], "isController": false}, {"data": [0.0, 500, 1500, "Delete User"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr110"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr111"], "isController": false}, {"data": [0.0, 500, 1500, "logoff"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr112"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr113"], "isController": false}, {"data": [0.0, 500, 1500, "View Blog Post"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr114"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr115"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr116"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr117"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr118"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr119"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request create post"], "isController": false}, {"data": [0.0, 500, 1500, " View Pages"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr120"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr121"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr122"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr123"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr124"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr125"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr126"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr127"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr128"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr129"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request  logging into my account"], "isController": false}, {"data": [0.0, 500, 1500, "Delete Page"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request_visit_login_page"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr130"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr131"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr132"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr133"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr134"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr135"], "isController": false}, {"data": [0.15, 500, 1500, "HTTP Request home_page-0"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr136"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request home_page-1"], "isController": false}, {"data": [0.0, 500, 1500, "Delete Post"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr137"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr138"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr139"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request home_page"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 826, 63, 7.627118644067797, 31217.769975786956, 0, 88777, 67960.6, 74633.6, 84939.97, 2.6007147242644164, 33.48575483403126, 0.8383454143118023], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["HTTP Request add new userr140", 1, 0, 0.0, 17645.0, 17645, 17645, 17645.0, 17645.0, 17645.0, 0.05667327854916407, 0.08855199773306886, 0.029554229243411733], "isController": false}, {"data": ["HTTP Request add new userr141", 1, 0, 0.0, 15835.0, 15835, 15835, 15835.0, 15835.0, 15835.0, 0.06315124723713295, 0.0986738238080202, 0.03293238869592674], "isController": false}, {"data": ["HTTP Request add new userr142", 1, 0, 0.0, 27897.0, 27897, 27897, 27897.0, 27897.0, 27897.0, 0.03584614833136179, 0.05600960676775281, 0.0186932062587375], "isController": false}, {"data": ["HTTP Request add new userr143", 1, 0, 0.0, 28479.0, 28479, 28479, 28479.0, 28479.0, 28479.0, 0.03511359247164578, 0.05486498823694652, 0.018311189824080903], "isController": false}, {"data": ["HTTP Request add new userr100", 1, 1, 100.0, 30379.0, 30379, 30379, 30379.0, 30379.0, 30379.0, 0.03291747588794891, 0.024623814970868033, 0.017165949340004608], "isController": false}, {"data": ["HTTP Request add new userr144", 1, 0, 0.0, 10614.0, 10614, 10614, 10614.0, 10614.0, 10614.0, 0.09421518748822309, 0.1472112304503486, 0.04913174816280384], "isController": false}, {"data": ["HTTP Request add new userr101", 1, 0, 0.0, 54072.0, 54072, 54072, 54072.0, 54072.0, 54072.0, 0.01849386003846723, 0.028896656310105042, 0.009644259043497558], "isController": false}, {"data": ["HTTP Request create blog page", 36, 0, 0.0, 7092.666666666667, 417, 22894, 19700.800000000003, 21157.449999999997, 22894.0, 0.795211062269444, 2.0655555570895276, 0.38595693158975947], "isController": false}, {"data": ["HTTP Request add new userr145", 1, 0, 0.0, 31006.0, 31006, 31006, 31006.0, 31006.0, 31006.0, 0.03225182222795588, 0.05039347223118106, 0.01681882135715668], "isController": false}, {"data": ["HTTP Request add new userr102", 1, 0, 0.0, 60374.0, 60374, 60374, 60374.0, 60374.0, 60374.0, 0.016563421340312055, 0.025880345844237585, 0.008637565425514294], "isController": false}, {"data": ["HTTP Request add new userr146", 1, 1, 100.0, 6098.0, 6098, 6098, 6098.0, 6098.0, 6098.0, 0.1639881928501148, 0.12267085519842572, 0.08551728025582159], "isController": false}, {"data": ["HTTP Request add new userr103", 1, 1, 100.0, 8712.0, 8712, 8712, 8712.0, 8712.0, 8712.0, 0.1147842056932966, 0.08586396636822774, 0.05985816976584022], "isController": false}, {"data": ["HTTP Request add new userr147", 1, 0, 0.0, 18252.0, 18252, 18252, 18252.0, 18252.0, 18252.0, 0.05478851632697787, 0.08560705676090292, 0.02857135519395135], "isController": false}, {"data": ["HTTP Request add new userr104", 1, 0, 0.0, 6441.0, 6441, 6441, 6441.0, 6441.0, 6441.0, 0.1552553951249806, 0.2425865548827822, 0.08096326269212856], "isController": false}, {"data": ["HTTP Request add new userr148", 1, 1, 100.0, 6162.0, 6162, 6162, 6162.0, 6162.0, 6162.0, 0.16228497241155468, 0.1213967664719247, 0.08462907740993184], "isController": false}, {"data": ["HTTP Request add new userr105", 1, 0, 0.0, 41623.0, 41623, 41623, 41623.0, 41623.0, 41623.0, 0.024025178386949526, 0.037539341229608635, 0.01252875513538188], "isController": false}, {"data": ["HTTP Request add new userr149", 1, 0, 0.0, 26433.0, 26433, 26433, 26433.0, 26433.0, 26433.0, 0.03783149850565581, 0.0591117164150872, 0.019728535353535352], "isController": false}, {"data": ["HTTP Request add new userr106", 1, 0, 0.0, 19272.0, 19272, 19272, 19272.0, 19272.0, 19272.0, 0.05188875051888751, 0.08107617268576173, 0.027059172633872978], "isController": false}, {"data": ["HTTP Request add new userr107", 1, 0, 0.0, 10862.0, 10862, 10862, 10862.0, 10862.0, 10862.0, 0.09206407659731172, 0.14385011968329958, 0.04800997744430123], "isController": false}, {"data": ["HTTP Request add new userr108", 1, 0, 0.0, 11080.0, 11080, 11080, 11080.0, 11080.0, 11080.0, 0.09025270758122743, 0.14101985559566788, 0.047065376805054154], "isController": false}, {"data": ["HTTP Request add new userr109", 1, 0, 0.0, 65631.0, 65631, 65631, 65631.0, 65631.0, 65631.0, 0.015236702168182717, 0.023807347137785498, 0.00794570210723591], "isController": false}, {"data": ["HTTP Request_param_of_user", 100, 0, 0.0, 63262.119999999966, 9958, 88777, 84738.2, 85800.84999999999, 88773.31, 0.4069904682832328, 21.49502272914581, 0.16375007122333196], "isController": false}, {"data": ["Delete User", 50, 9, 18.0, 62376.200000000004, 50768, 74237, 69914.2, 72418.34999999999, 74237.0, 0.6180928128167725, 0.7439688628019383, 0.23842447369396988], "isController": false}, {"data": ["HTTP Request add new userr110", 1, 0, 0.0, 12245.0, 12245, 12245, 12245.0, 12245.0, 12245.0, 0.08166598611678236, 0.12760310330747246, 0.042587535728868926], "isController": false}, {"data": ["HTTP Request add new userr111", 1, 1, 100.0, 6581.0, 6581, 6581, 6581.0, 6581.0, 6581.0, 0.151952590791673, 0.11366766068986475, 0.07924090183862634], "isController": false}, {"data": ["logoff", 50, 0, 0.0, 40034.18000000001, 18424, 61300, 54249.8, 59125.45, 61300.0, 0.3029531876734407, 2.6966975641048947, 0.08845996397886599], "isController": false}, {"data": ["HTTP Request add new userr112", 1, 0, 0.0, 27769.0, 27769, 27769, 27769.0, 27769.0, 27769.0, 0.03601137959595232, 0.0562677806186755, 0.01877937178148295], "isController": false}, {"data": ["HTTP Request add new userr113", 1, 1, 100.0, 7924.0, 7924, 7924, 7924.0, 7924.0, 7924.0, 0.12619888944977284, 0.09440268488137304, 0.06581074899040888], "isController": false}, {"data": ["View Blog Post", 38, 0, 0.0, 11768.000000000002, 4707, 31576, 21160.7, 22830.299999999974, 31576.0, 0.4131377814501136, 7.0116102418758635, 0.185334424162037], "isController": false}, {"data": ["HTTP Request add new userr114", 1, 1, 100.0, 4951.0, 4951, 4951, 4951.0, 4951.0, 4951.0, 0.20197939810139365, 0.15109005756412847, 0.10532910018178146], "isController": false}, {"data": ["HTTP Request add new userr115", 1, 0, 0.0, 18058.0, 18058, 18058, 18058.0, 18058.0, 18058.0, 0.05537711817477018, 0.08652674714807841, 0.028878301860671172], "isController": false}, {"data": ["HTTP Request add new userr116", 1, 1, 100.0, 6470.0, 6470, 6470, 6470.0, 6470.0, 6470.0, 0.1545595054095827, 0.11561775502318393, 0.08060036707882536], "isController": false}, {"data": ["HTTP Request add new userr117", 1, 0, 0.0, 11810.0, 11810, 11810, 11810.0, 11810.0, 11810.0, 0.0846740050804403, 0.13230313293818796, 0.04415617061812024], "isController": false}, {"data": ["HTTP Request add new userr118", 1, 0, 0.0, 6794.0, 6794, 6794, 6794.0, 6794.0, 6794.0, 0.14718869590815425, 0.22998233735649104, 0.07675660509272889], "isController": false}, {"data": ["HTTP Request add new userr119", 1, 0, 0.0, 7756.0, 7756, 7756, 7756.0, 7756.0, 7756.0, 0.12893243940175347, 0.2014569365652398, 0.06723625257864879], "isController": false}, {"data": ["HTTP Request create post", 36, 0, 0.0, 9791.638888888889, 4864, 25796, 15913.800000000007, 25331.05, 25796.0, 0.5960560955014321, 2.0124330989121977, 0.28638632713545376], "isController": false}, {"data": [" View Pages", 77, 0, 0.0, 28863.2207792208, 2945, 72262, 63844.6, 67325.69999999998, 72262.0, 0.4549536774437512, 6.428788347942073, 0.10307544254584988], "isController": false}, {"data": ["HTTP Request add new userr120", 1, 0, 0.0, 17150.0, 17150, 17150, 17150.0, 17150.0, 17150.0, 0.05830903790087464, 0.09110787172011663, 0.030407252186588924], "isController": false}, {"data": ["HTTP Request add new userr121", 1, 0, 0.0, 8689.0, 8689, 8689, 8689.0, 8689.0, 8689.0, 0.11508804235239958, 0.17982506617562435, 0.060016615836114624], "isController": false}, {"data": ["HTTP Request add new userr122", 1, 0, 0.0, 25802.0, 25802, 25802, 25802.0, 25802.0, 25802.0, 0.03875668552825362, 0.06055732113789629, 0.020211005929772888], "isController": false}, {"data": ["HTTP Request add new userr123", 1, 0, 0.0, 6735.0, 6735, 6735, 6735.0, 6735.0, 6735.0, 0.14847809948032667, 0.23199703043801037, 0.07742900890868597], "isController": false}, {"data": ["HTTP Request add new userr124", 1, 1, 100.0, 8215.0, 8215, 8215, 8215.0, 8215.0, 8215.0, 0.12172854534388314, 0.09105865794278759, 0.06347953438831407], "isController": false}, {"data": ["HTTP Request add new userr125", 1, 1, 100.0, 7212.0, 7212, 7212, 7212.0, 7212.0, 7212.0, 0.13865779256794233, 0.10372252842484748, 0.07230787229617305], "isController": false}, {"data": ["HTTP Request add new userr126", 1, 0, 0.0, 16102.0, 16102, 16102, 16102.0, 16102.0, 16102.0, 0.06210408644888833, 0.09703763507638802, 0.032386310706744505], "isController": false}, {"data": ["HTTP Request add new userr127", 1, 1, 100.0, 7138.0, 7138, 7138, 7138.0, 7138.0, 7138.0, 0.14009526478005044, 0.10479782502101428, 0.07305749159428411], "isController": false}, {"data": ["HTTP Request add new userr128", 1, 1, 100.0, 7658.0, 7658, 7658, 7658.0, 7658.0, 7658.0, 0.13058239749281797, 0.09768175437451031, 0.06809667994254374], "isController": false}, {"data": ["HTTP Request add new userr129", 1, 0, 0.0, 27692.0, 27692, 27692, 27692.0, 27692.0, 27692.0, 0.03611151235013722, 0.05642423804708941, 0.018831589448216092], "isController": false}, {"data": ["HTTP Request  logging into my account", 50, 0, 0.0, 54811.899999999994, 15649, 70530, 67242.8, 68661.25, 70530.0, 0.25714476736112896, 2.54307134674429, 0.14263498814562622], "isController": false}, {"data": ["Delete Page", 41, 39, 95.1219512195122, 34298.24390243902, 20532, 49530, 48352.20000000001, 49259.4, 49530.0, 0.6314492530417373, 0.4902064001617126, 0.24357661616356074], "isController": false}, {"data": ["HTTP Request_visit_login_page", 50, 0, 0.0, 34982.579999999994, 6686, 58438, 54739.6, 56571.549999999996, 58438.0, 0.38028308272678185, 3.3850319271415645, 0.0835582945444589], "isController": false}, {"data": ["HTTP Request add new userr130", 1, 0, 0.0, 26695.0, 26695, 26695, 26695.0, 26695.0, 26695.0, 0.037460198539052254, 0.05853156021726915, 0.01953490822251358], "isController": false}, {"data": ["HTTP Request add new userr131", 1, 0, 0.0, 15642.0, 15642, 15642, 15642.0, 15642.0, 15642.0, 0.06393044367727913, 0.09989131824574862, 0.0333387274645186], "isController": false}, {"data": ["HTTP Request add new userr132", 1, 0, 0.0, 15923.0, 15923, 15923, 15923.0, 15923.0, 15923.0, 0.06280223575959304, 0.09812849337436412, 0.03275038466369403], "isController": false}, {"data": ["HTTP Request add new userr133", 1, 0, 0.0, 16974.0, 16974, 16974, 16974.0, 16974.0, 16974.0, 0.058913632614587014, 0.0920525509602922, 0.030722538882997526], "isController": false}, {"data": ["HTTP Request add new userr134", 1, 1, 100.0, 4255.0, 4255, 4255, 4255.0, 4255.0, 4255.0, 0.23501762632197415, 0.17580420094007052, 0.12255801997649823], "isController": false}, {"data": ["Debug Sampler", 96, 0, 0.0, 1.5937500000000009, 0, 15, 3.299999999999997, 6.0, 15.0, 0.5415620680901475, 0.43438341783488, 0.0], "isController": false}, {"data": ["HTTP Request add new userr135", 1, 1, 100.0, 6043.0, 6043, 6043, 6043.0, 6043.0, 6043.0, 0.16548072149594573, 0.12378733658778752, 0.08629561062386232], "isController": false}, {"data": ["HTTP Request home_page-0", 50, 0, 0.0, 3105.74, 3, 8035, 6772.8, 7242.399999999999, 8035.0, 3.80778310867413, 2.3129307554641687, 0.7883300967176909], "isController": false}, {"data": ["HTTP Request add new userr136", 1, 0, 0.0, 26339.0, 26339, 26339, 26339.0, 26339.0, 26339.0, 0.03796651353506208, 0.059322677398534494, 0.01979894358176089], "isController": false}, {"data": ["HTTP Request home_page-1", 50, 0, 0.0, 39317.200000000004, 1873, 67573, 65675.3, 66847.34999999999, 67573.0, 0.6447536396342957, 11.104722901004525, 0.13411379418174316], "isController": false}, {"data": ["Delete Post", 2, 1, 50.0, 5970.5, 5379, 6562, 6562.0, 6562.0, 6562.0, 0.18065215427693976, 0.21099607081564448, 0.069155902809141], "isController": false}, {"data": ["HTTP Request add new userr137", 1, 0, 0.0, 27907.0, 27907, 27907, 27907.0, 27907.0, 27907.0, 0.035833303472247106, 0.0559895366753861, 0.018686507865410113], "isController": false}, {"data": ["HTTP Request add new userr138", 1, 0, 0.0, 14621.0, 14621, 14621, 14621.0, 14621.0, 14621.0, 0.06839477463921757, 0.10686683537377745, 0.03566680630599822], "isController": false}, {"data": ["HTTP Request add new userr139", 1, 0, 0.0, 17314.0, 17314, 17314, 17314.0, 17314.0, 17314.0, 0.05775672865888876, 0.09024488852951369, 0.030119231546725193], "isController": false}, {"data": ["HTTP Request home_page", 50, 0, 0.0, 42422.72, 1888, 72935, 71593.1, 71924.3, 72935.0, 0.6446289515754731, 11.494137099685423, 0.2675461957222423], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["500\/Internal Server Error", 17, 26.984126984126984, 2.0581113801452786], "isController": false}, {"data": ["404\/Not Found", 46, 73.01587301587301, 5.5690072639225185], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 826, 63, "404\/Not Found", 46, "500\/Internal Server Error", 17, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr100", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr146", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr103", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr148", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Delete User", 50, 9, "404\/Not Found", 9, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr111", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr113", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr114", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr116", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr124", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr125", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr127", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr128", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Delete Page", 41, 39, "404\/Not Found", 36, "500\/Internal Server Error", 3, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr134", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr135", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["Delete Post", 2, 1, "404\/Not Found", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
