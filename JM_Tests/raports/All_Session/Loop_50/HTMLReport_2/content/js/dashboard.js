/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 99.26131117266851, "KoPercent": 0.7386888273314867};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9210526315789473, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "HTTP Request add new userr180"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr181"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr182"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr183"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr184"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr185"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr186"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr187"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr100"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr188"], "isController": false}, {"data": [0.8846153846153846, 500, 1500, "HTTP Request create blog page"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr145"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr189"], "isController": false}, {"data": [0.9123711340206185, 500, 1500, "HTTP Request_param_of_user"], "isController": false}, {"data": [0.96, 500, 1500, "Delete User"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr190"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr191"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr192"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr193"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr194"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr195"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr196"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr154"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr155"], "isController": false}, {"data": [0.9574468085106383, 500, 1500, "logoff"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr156"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr157"], "isController": false}, {"data": [0.9382022471910112, 500, 1500, "View Blog Post"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr158"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr159"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr118"], "isController": false}, {"data": [0.8717948717948718, 500, 1500, "HTTP Request create post"], "isController": false}, {"data": [0.7528089887640449, 500, 1500, " View Pages"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr160"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr161"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr162"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr163"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr164"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr165"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr166"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr167"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr168"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr169"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr128"], "isController": false}, {"data": [0.8723404255319149, 500, 1500, "HTTP Request  logging into my account"], "isController": false}, {"data": [0.93, 500, 1500, "Delete Page"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr170"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr171"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr172"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr173"], "isController": false}, {"data": [0.9895833333333334, 500, 1500, "HTTP Request_visit_login_page"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr174"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr175"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr176"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr177"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr178"], "isController": false}, {"data": [1.0, 500, 1500, "Debug Sampler"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr179"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request home_page-0"], "isController": false}, {"data": [0.9375, 500, 1500, "HTTP Request home_page-1"], "isController": false}, {"data": [0.9, 500, 1500, "Delete Post"], "isController": false}, {"data": [0.9375, 500, 1500, "HTTP Request home_page"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1083, 8, 0.7386888273314867, 363.8439519852263, 0, 7792, 550.0, 1005.9999999999982, 1438.760000000001, 0.6454727506794451, 7.280232578605529, 0.17795038002729702], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["HTTP Request add new userr180", 1, 1, 100.0, 360.0, 360, 360, 360.0, 360.0, 360.0, 2.7777777777777777, 2.077907986111111, 1.4485677083333335], "isController": false}, {"data": ["HTTP Request add new userr181", 1, 0, 0.0, 488.0, 488, 488, 488.0, 488.0, 488.0, 2.0491803278688527, 3.201844262295082, 1.0686155225409837], "isController": false}, {"data": ["HTTP Request add new userr182", 1, 0, 0.0, 477.0, 477, 477, 477.0, 477.0, 477.0, 2.0964360587002098, 3.2756813417190775, 1.0932586477987423], "isController": false}, {"data": ["HTTP Request add new userr183", 1, 0, 0.0, 485.0, 485, 485, 485.0, 485.0, 485.0, 2.061855670103093, 3.2216494845360826, 1.0752255154639176], "isController": false}, {"data": ["HTTP Request add new userr184", 1, 0, 0.0, 468.0, 468, 468, 468.0, 468.0, 468.0, 2.136752136752137, 3.3386752136752134, 1.1142828525641024], "isController": false}, {"data": ["HTTP Request add new userr185", 1, 0, 0.0, 480.0, 480, 480, 480.0, 480.0, 480.0, 2.0833333333333335, 3.2552083333333335, 1.08642578125], "isController": false}, {"data": ["HTTP Request add new userr186", 1, 0, 0.0, 471.0, 471, 471, 471.0, 471.0, 471.0, 2.1231422505307855, 3.3174097664543525, 1.10718550955414], "isController": false}, {"data": ["HTTP Request add new userr187", 1, 1, 100.0, 369.0, 369, 369, 369.0, 369.0, 369.0, 2.710027100271003, 2.027227303523035, 1.4132367886178863], "isController": false}, {"data": ["HTTP Request add new userr100", 1, 0, 0.0, 550.0, 550, 550, 550.0, 550.0, 550.0, 1.8181818181818181, 2.840909090909091, 0.9481534090909091], "isController": false}, {"data": ["HTTP Request add new userr188", 1, 1, 100.0, 362.0, 362, 362, 362.0, 362.0, 362.0, 2.7624309392265194, 2.066427831491713, 1.4405645718232045], "isController": false}, {"data": ["HTTP Request create blog page", 39, 0, 0.0, 452.71794871794873, 384, 1122, 559.0, 783.0, 1122.0, 0.023854217700930498, 0.06207556026064708, 0.011577681833361775], "isController": false}, {"data": ["HTTP Request add new userr145", 1, 0, 0.0, 487.0, 487, 487, 487.0, 487.0, 487.0, 2.053388090349076, 3.208418891170431, 1.0708098049281314], "isController": false}, {"data": ["HTTP Request add new userr189", 1, 0, 0.0, 461.0, 461, 461, 461.0, 461.0, 461.0, 2.1691973969631237, 3.3893709327548804, 1.1312025488069415], "isController": false}, {"data": ["HTTP Request_param_of_user", 97, 0, 0.0, 461.64948453608235, 389, 1320, 571.0, 628.1, 1320.0, 0.05871770223708392, 3.206077106255433, 0.023624700509451733], "isController": false}, {"data": ["Delete User", 50, 0, 0.0, 428.0199999999999, 363, 1575, 471.9, 644.2999999999988, 1575.0, 0.35780735651925, 0.4794828230105911, 0.1380213924073279], "isController": false}, {"data": ["HTTP Request add new userr190", 1, 0, 0.0, 462.0, 462, 462, 462.0, 462.0, 462.0, 2.1645021645021645, 3.3820346320346317, 1.1287540584415583], "isController": false}, {"data": ["HTTP Request add new userr191", 1, 0, 0.0, 461.0, 461, 461, 461.0, 461.0, 461.0, 2.1691973969631237, 3.3893709327548804, 1.1312025488069415], "isController": false}, {"data": ["HTTP Request add new userr192", 1, 0, 0.0, 507.0, 507, 507, 507.0, 507.0, 507.0, 1.9723865877712032, 3.081854043392505, 1.0285687869822484], "isController": false}, {"data": ["HTTP Request add new userr193", 1, 0, 0.0, 484.0, 484, 484, 484.0, 484.0, 484.0, 2.066115702479339, 3.228305785123967, 1.077447055785124], "isController": false}, {"data": ["HTTP Request add new userr194", 1, 0, 0.0, 503.0, 503, 503, 503.0, 503.0, 503.0, 1.9880715705765406, 3.106361829025845, 1.0367482604373757], "isController": false}, {"data": ["HTTP Request add new userr195", 1, 1, 100.0, 342.0, 342, 342, 342.0, 342.0, 342.0, 2.923976608187134, 2.187271564327485, 1.5248081140350875], "isController": false}, {"data": ["HTTP Request add new userr196", 1, 0, 0.0, 516.0, 516, 516, 516.0, 516.0, 516.0, 1.937984496124031, 3.0281007751937983, 1.0106286337209303], "isController": false}, {"data": ["HTTP Request add new userr154", 1, 1, 100.0, 337.0, 337, 337, 337.0, 337.0, 337.0, 2.967359050445104, 2.219723664688427, 1.5474313798219583], "isController": false}, {"data": ["HTTP Request add new userr155", 1, 0, 0.0, 562.0, 562, 562, 562.0, 562.0, 562.0, 1.779359430604982, 2.7802491103202844, 0.927908140569395], "isController": false}, {"data": ["logoff", 47, 0, 0.0, 355.29787234042556, 308, 832, 486.6, 547.5999999999997, 832.0, 0.028785226686722416, 0.25620035983830053, 0.008405061307939455], "isController": false}, {"data": ["HTTP Request add new userr156", 1, 0, 0.0, 587.0, 587, 587, 587.0, 587.0, 587.0, 1.7035775127768313, 2.661839863713799, 0.8883890545144805], "isController": false}, {"data": ["HTTP Request add new userr157", 1, 0, 0.0, 480.0, 480, 480, 480.0, 480.0, 480.0, 2.0833333333333335, 3.2552083333333335, 1.08642578125], "isController": false}, {"data": ["View Blog Post", 89, 0, 0.0, 435.69662921348316, 387, 915, 517.0, 561.5, 915.0, 0.0531915529423296, 0.9058264193030352, 0.017514177939490724], "isController": false}, {"data": ["HTTP Request add new userr158", 1, 1, 100.0, 776.0, 776, 776, 776.0, 776.0, 776.0, 1.288659793814433, 0.9639779317010309, 0.6720159471649484], "isController": false}, {"data": ["HTTP Request add new userr159", 1, 0, 0.0, 974.0, 974, 974, 974.0, 974.0, 974.0, 1.026694045174538, 1.6042094455852156, 0.5354049024640657], "isController": false}, {"data": ["HTTP Request add new userr118", 1, 0, 0.0, 526.0, 526, 526, 526.0, 526.0, 526.0, 1.9011406844106464, 2.970532319391635, 0.9914151615969581], "isController": false}, {"data": ["HTTP Request create post", 39, 0, 0.0, 545.102564102564, 438, 2023, 837.0, 861.0, 2023.0, 0.02384814965875744, 0.08068456763304668, 0.011458290656356113], "isController": false}, {"data": [" View Pages", 89, 0, 0.0, 782.4606741573031, 346, 1563, 1380.0, 1442.5, 1563.0, 0.05311368536765711, 0.7510292361744778, 0.012033569341109813], "isController": false}, {"data": ["HTTP Request add new userr160", 1, 0, 0.0, 808.0, 808, 808, 808.0, 808.0, 808.0, 1.2376237623762376, 1.9337871287128712, 0.6454014542079207], "isController": false}, {"data": ["HTTP Request add new userr161", 1, 0, 0.0, 737.0, 737, 737, 737.0, 737.0, 737.0, 1.3568521031207597, 2.120081411126187, 0.707577170963365], "isController": false}, {"data": ["HTTP Request add new userr162", 1, 0, 0.0, 762.0, 762, 762, 762.0, 762.0, 762.0, 1.3123359580052494, 2.050524934383202, 0.6843626968503936], "isController": false}, {"data": ["HTTP Request add new userr163", 1, 0, 0.0, 1018.0, 1018, 1018, 1018.0, 1018.0, 1018.0, 0.9823182711198427, 1.5348722986247545, 0.5122636296660118], "isController": false}, {"data": ["HTTP Request add new userr164", 1, 0, 0.0, 1035.0, 1035, 1035, 1035.0, 1035.0, 1035.0, 0.966183574879227, 1.5096618357487923, 0.5038496376811594], "isController": false}, {"data": ["HTTP Request add new userr165", 1, 0, 0.0, 476.0, 476, 476, 476.0, 476.0, 476.0, 2.100840336134454, 3.2825630252100844, 1.0955554096638656], "isController": false}, {"data": ["HTTP Request add new userr166", 1, 0, 0.0, 475.0, 475, 475, 475.0, 475.0, 475.0, 2.1052631578947367, 3.2894736842105265, 1.0978618421052633], "isController": false}, {"data": ["HTTP Request add new userr167", 1, 0, 0.0, 454.0, 454, 454, 454.0, 454.0, 454.0, 2.2026431718061676, 3.4416299559471364, 1.1486439977973568], "isController": false}, {"data": ["HTTP Request add new userr168", 1, 0, 0.0, 478.0, 478, 478, 478.0, 478.0, 478.0, 2.092050209205021, 3.2688284518828454, 1.0909714958158996], "isController": false}, {"data": ["HTTP Request add new userr169", 1, 1, 100.0, 365.0, 365, 365, 365.0, 365.0, 365.0, 2.73972602739726, 2.049443493150685, 1.4287243150684932], "isController": false}, {"data": ["HTTP Request add new userr128", 1, 0, 0.0, 577.0, 577, 577, 577.0, 577.0, 577.0, 1.7331022530329288, 2.7079722703639515, 0.9037857452339688], "isController": false}, {"data": ["HTTP Request  logging into my account", 47, 0, 0.0, 555.2553191489363, 363, 2879, 1020.4000000000001, 1613.1999999999969, 2879.0, 0.028782088967274154, 0.28464474118318883, 0.01596506497403488], "isController": false}, {"data": ["Delete Page", 50, 0, 0.0, 586.0599999999998, 406, 7792, 512.5, 667.8499999999988, 7792.0, 0.3578406464033437, 0.5496068896848855, 0.13803423372003978], "isController": false}, {"data": ["HTTP Request add new userr170", 1, 0, 0.0, 489.0, 489, 489, 489.0, 489.0, 489.0, 2.044989775051125, 3.1952965235173827, 1.0664302147239264], "isController": false}, {"data": ["HTTP Request add new userr171", 1, 1, 100.0, 484.0, 484, 484, 484.0, 484.0, 484.0, 2.066115702479339, 1.5455513946280992, 1.077447055785124], "isController": false}, {"data": ["HTTP Request add new userr172", 1, 0, 0.0, 470.0, 470, 470, 470.0, 470.0, 470.0, 2.127659574468085, 3.3244680851063833, 1.1095412234042554], "isController": false}, {"data": ["HTTP Request add new userr173", 1, 0, 0.0, 469.0, 469, 469, 469.0, 469.0, 469.0, 2.1321961620469083, 3.3315565031982945, 1.1119069829424308], "isController": false}, {"data": ["HTTP Request_visit_login_page", 48, 0, 0.0, 344.3541666666667, 309, 664, 414.70000000000005, 457.19999999999993, 664.0, 0.028619997388425238, 0.25473090326947695, 0.006288573644917655], "isController": false}, {"data": ["HTTP Request add new userr174", 1, 0, 0.0, 474.0, 474, 474, 474.0, 474.0, 474.0, 2.109704641350211, 3.2964135021097047, 1.100178006329114], "isController": false}, {"data": ["HTTP Request add new userr175", 1, 0, 0.0, 457.0, 457, 457, 457.0, 457.0, 457.0, 2.1881838074398248, 3.4190371991247264, 1.1411036652078774], "isController": false}, {"data": ["HTTP Request add new userr176", 1, 0, 0.0, 476.0, 476, 476, 476.0, 476.0, 476.0, 2.100840336134454, 3.2825630252100844, 1.0955554096638656], "isController": false}, {"data": ["HTTP Request add new userr177", 1, 0, 0.0, 493.0, 493, 493, 493.0, 493.0, 493.0, 2.028397565922921, 3.169371196754564, 1.0577776369168357], "isController": false}, {"data": ["HTTP Request add new userr178", 1, 0, 0.0, 493.0, 493, 493, 493.0, 493.0, 493.0, 2.028397565922921, 3.169371196754564, 1.0577776369168357], "isController": false}, {"data": ["Debug Sampler", 247, 0, 0.0, 0.18218623481781382, 0, 3, 1.0, 1.0, 2.0, 0.14967737556780444, 0.36264428792958014, 0.0], "isController": false}, {"data": ["HTTP Request add new userr179", 1, 0, 0.0, 469.0, 469, 469, 469.0, 469.0, 469.0, 2.1321961620469083, 3.3315565031982945, 1.1119069829424308], "isController": false}, {"data": ["HTTP Request home_page-0", 48, 0, 0.0, 1.7916666666666667, 1, 7, 4.0, 6.549999999999997, 7.0, 0.028620389881261157, 0.017363106401963837, 0.005925315092604849], "isController": false}, {"data": ["HTTP Request home_page-1", 48, 0, 0.0, 432.1249999999999, 385, 927, 517.3000000000001, 629.7999999999997, 927.0, 0.0286137871956879, 0.4928083316269382, 0.00595189128191555], "isController": false}, {"data": ["Delete Post", 50, 0, 0.0, 505.2599999999999, 380, 2789, 568.3, 861.399999999998, 2789.0, 0.35788931199358665, 0.5729724004709823, 0.13700450224754487], "isController": false}, {"data": ["HTTP Request home_page", 48, 0, 0.0, 434.0208333333333, 388, 928, 522.8, 637.3499999999997, 928.0, 0.028613650738202382, 0.5101649994247464, 0.01187578277708595], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["500\/Internal Server Error", 8, 100.0, 0.7386888273314867], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1083, 8, "500\/Internal Server Error", 8, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": ["HTTP Request add new userr180", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr187", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr188", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr195", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr154", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr158", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr169", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr171", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
