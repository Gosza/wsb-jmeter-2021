/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 72.0, "KoPercent": 28.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.605, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "HTTP Request add new userr140"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr141"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr142"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr143"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr144"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr145"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr146"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr147"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr148"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr149"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr150"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr151"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr152"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr153"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr154"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr155"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr156"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr157"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr158"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr159"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr120"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr121"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr122"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr123"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr124"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr125"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr126"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr127"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr128"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr129"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr130"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr131"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr132"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr133"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr134"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr135"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr136"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr137"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr138"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr139"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr180"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr181"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr182"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr183"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr184"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr185"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr186"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr187"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr100"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr188"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr101"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr189"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr102"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr103"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr104"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr105"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr106"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr107"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr108"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr109"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr190"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr191"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr192"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr193"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr194"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr195"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr196"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr197"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr110"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr198"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr111"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr199"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr112"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr113"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr114"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr115"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr116"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr117"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr118"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr119"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr160"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr161"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr162"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr163"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr164"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr165"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr166"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr167"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr168"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr169"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr170"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr171"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr172"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr173"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr174"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr175"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr176"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr177"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr178"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr179"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 100, 28, 28.0, 517.5099999999999, 329, 1236, 696.8, 860.1499999999992, 1235.6, 1.3296634621777228, 1.7743735623013817, 0.6933987195340859], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["HTTP Request add new userr140", 1, 0, 0.0, 492.0, 492, 492, 492.0, 492.0, 492.0, 2.032520325203252, 3.1758130081300813, 1.0599275914634148], "isController": false}, {"data": ["HTTP Request add new userr141", 1, 0, 0.0, 1236.0, 1236, 1236, 1236.0, 1236.0, 1236.0, 0.8090614886731392, 1.26415857605178, 0.42191292475728154], "isController": false}, {"data": ["HTTP Request add new userr142", 1, 1, 100.0, 358.0, 358, 358, 358.0, 358.0, 358.0, 2.793296089385475, 2.089516410614525, 1.4566602653631286], "isController": false}, {"data": ["HTTP Request add new userr143", 1, 1, 100.0, 377.0, 377, 377, 377.0, 377.0, 377.0, 2.6525198938992043, 1.9842092175066313, 1.383247679045093], "isController": false}, {"data": ["HTTP Request add new userr144", 1, 1, 100.0, 396.0, 396, 396, 396.0, 396.0, 396.0, 2.5252525252525255, 1.88900726010101, 1.3168797348484849], "isController": false}, {"data": ["HTTP Request add new userr145", 1, 0, 0.0, 468.0, 468, 468, 468.0, 468.0, 468.0, 2.136752136752137, 3.3386752136752134, 1.1142828525641024], "isController": false}, {"data": ["HTTP Request add new userr146", 1, 0, 0.0, 462.0, 462, 462, 462.0, 462.0, 462.0, 2.1645021645021645, 3.3820346320346317, 1.1287540584415583], "isController": false}, {"data": ["HTTP Request add new userr147", 1, 0, 0.0, 1196.0, 1196, 1196, 1196.0, 1196.0, 1196.0, 0.8361204013377926, 1.3064381270903012, 0.43602372491638797], "isController": false}, {"data": ["HTTP Request add new userr148", 1, 0, 0.0, 525.0, 525, 525, 525.0, 525.0, 525.0, 1.9047619047619047, 2.9761904761904763, 0.9933035714285714], "isController": false}, {"data": ["HTTP Request add new userr149", 1, 0, 0.0, 497.0, 497, 497, 497.0, 497.0, 497.0, 2.012072434607646, 3.143863179074447, 1.0492643360160965], "isController": false}, {"data": ["HTTP Request add new userr150", 1, 0, 0.0, 506.0, 506, 506, 506.0, 506.0, 506.0, 1.976284584980237, 3.0879446640316206, 1.0306015316205535], "isController": false}, {"data": ["HTTP Request add new userr151", 1, 0, 0.0, 493.0, 493, 493, 493.0, 493.0, 493.0, 2.028397565922921, 3.169371196754564, 1.0577776369168357], "isController": false}, {"data": ["HTTP Request add new userr152", 1, 0, 0.0, 620.0, 620, 620, 620.0, 620.0, 620.0, 1.6129032258064515, 2.5201612903225805, 0.8411038306451613], "isController": false}, {"data": ["HTTP Request add new userr153", 1, 0, 0.0, 493.0, 493, 493, 493.0, 493.0, 493.0, 2.028397565922921, 3.169371196754564, 1.0577776369168357], "isController": false}, {"data": ["HTTP Request add new userr154", 1, 1, 100.0, 398.0, 398, 398, 398.0, 398.0, 398.0, 2.512562814070352, 1.8795147613065326, 1.3102622487437185], "isController": false}, {"data": ["HTTP Request add new userr155", 1, 0, 0.0, 478.0, 478, 478, 478.0, 478.0, 478.0, 2.092050209205021, 3.2688284518828454, 1.0909714958158996], "isController": false}, {"data": ["HTTP Request add new userr156", 1, 1, 100.0, 369.0, 369, 369, 369.0, 369.0, 369.0, 2.710027100271003, 2.027227303523035, 1.4132367886178863], "isController": false}, {"data": ["HTTP Request add new userr157", 1, 0, 0.0, 462.0, 462, 462, 462.0, 462.0, 462.0, 2.1645021645021645, 3.3820346320346317, 1.1287540584415583], "isController": false}, {"data": ["HTTP Request add new userr158", 1, 1, 100.0, 539.0, 539, 539, 539.0, 539.0, 539.0, 1.8552875695732838, 1.38784206864564, 0.9675034786641928], "isController": false}, {"data": ["HTTP Request add new userr159", 1, 1, 100.0, 329.0, 329, 329, 329.0, 329.0, 329.0, 3.0395136778115504, 2.2736987082066866, 1.5850588905775076], "isController": false}, {"data": ["HTTP Request add new userr120", 1, 1, 100.0, 351.0, 351, 351, 351.0, 351.0, 351.0, 2.849002849002849, 2.131187678062678, 1.4857104700854702], "isController": false}, {"data": ["HTTP Request add new userr121", 1, 0, 0.0, 559.0, 559, 559, 559.0, 559.0, 559.0, 1.7889087656529516, 2.7951699463327366, 0.9328879695885509], "isController": false}, {"data": ["HTTP Request add new userr122", 1, 0, 0.0, 493.0, 493, 493, 493.0, 493.0, 493.0, 2.028397565922921, 3.169371196754564, 1.0577776369168357], "isController": false}, {"data": ["HTTP Request add new userr123", 1, 1, 100.0, 864.0, 864, 864, 864.0, 864.0, 864.0, 1.1574074074074074, 0.865794994212963, 0.6035698784722222], "isController": false}, {"data": ["HTTP Request add new userr124", 1, 0, 0.0, 995.0, 995, 995, 995.0, 995.0, 995.0, 1.0050251256281408, 1.57035175879397, 0.5241048994974874], "isController": false}, {"data": ["HTTP Request add new userr125", 1, 0, 0.0, 785.0, 785, 785, 785.0, 785.0, 785.0, 1.2738853503184713, 1.9904458598726114, 0.6643113057324841], "isController": false}, {"data": ["HTTP Request add new userr126", 1, 0, 0.0, 637.0, 637, 637, 637.0, 637.0, 637.0, 1.5698587127158556, 2.4529042386185242, 0.8186567896389325], "isController": false}, {"data": ["HTTP Request add new userr127", 1, 1, 100.0, 1100.0, 1100, 1100, 1100.0, 1100.0, 1100.0, 0.9090909090909091, 0.6800426136363635, 0.47407670454545453], "isController": false}, {"data": ["HTTP Request add new userr128", 1, 0, 0.0, 496.0, 496, 496, 496.0, 496.0, 496.0, 2.0161290322580645, 3.150201612903226, 1.0513797883064515], "isController": false}, {"data": ["HTTP Request add new userr129", 1, 1, 100.0, 697.0, 697, 697, 697.0, 697.0, 697.0, 1.4347202295552368, 1.0732379842180775, 0.7481841822094693], "isController": false}, {"data": ["HTTP Request add new userr130", 1, 1, 100.0, 413.0, 413, 413, 413.0, 413.0, 413.0, 2.4213075060532687, 1.8112515133171914, 1.2626740314769977], "isController": false}, {"data": ["HTTP Request add new userr131", 1, 1, 100.0, 354.0, 354, 354, 354.0, 354.0, 354.0, 2.824858757062147, 2.113126765536723, 1.4731197033898307], "isController": false}, {"data": ["HTTP Request add new userr132", 1, 0, 0.0, 518.0, 518, 518, 518.0, 518.0, 518.0, 1.9305019305019306, 3.016409266409266, 1.0067265926640927], "isController": false}, {"data": ["HTTP Request add new userr133", 1, 0, 0.0, 468.0, 468, 468, 468.0, 468.0, 468.0, 2.136752136752137, 3.3386752136752134, 1.1142828525641024], "isController": false}, {"data": ["HTTP Request add new userr134", 1, 0, 0.0, 459.0, 459, 459, 459.0, 459.0, 459.0, 2.1786492374727673, 3.4041394335511983, 1.1361315359477124], "isController": false}, {"data": ["HTTP Request add new userr135", 1, 0, 0.0, 476.0, 476, 476, 476.0, 476.0, 476.0, 2.100840336134454, 3.2825630252100844, 1.0955554096638656], "isController": false}, {"data": ["HTTP Request add new userr136", 1, 1, 100.0, 558.0, 558, 558, 558.0, 558.0, 558.0, 1.7921146953405018, 1.3405857974910393, 0.9345598118279569], "isController": false}, {"data": ["HTTP Request add new userr137", 1, 0, 0.0, 527.0, 527, 527, 527.0, 527.0, 527.0, 1.8975332068311195, 2.9648956356736242, 0.9895339184060721], "isController": false}, {"data": ["HTTP Request add new userr138", 1, 0, 0.0, 491.0, 491, 491, 491.0, 491.0, 491.0, 2.0366598778004072, 3.1822810590631363, 1.0620863034623218], "isController": false}, {"data": ["HTTP Request add new userr139", 1, 1, 100.0, 424.0, 424, 424, 424.0, 424.0, 424.0, 2.3584905660377355, 1.7642614976415094, 1.229915978773585], "isController": false}, {"data": ["HTTP Request add new userr180", 1, 0, 0.0, 563.0, 563, 563, 563.0, 563.0, 563.0, 1.7761989342806395, 2.7753108348134994, 0.9262599911190054], "isController": false}, {"data": ["HTTP Request add new userr181", 1, 0, 0.0, 478.0, 478, 478, 478.0, 478.0, 478.0, 2.092050209205021, 3.2688284518828454, 1.0909714958158996], "isController": false}, {"data": ["HTTP Request add new userr182", 1, 0, 0.0, 462.0, 462, 462, 462.0, 462.0, 462.0, 2.1645021645021645, 3.3820346320346317, 1.1287540584415583], "isController": false}, {"data": ["HTTP Request add new userr183", 1, 0, 0.0, 605.0, 605, 605, 605.0, 605.0, 605.0, 1.6528925619834711, 2.5826446280991737, 0.8619576446280992], "isController": false}, {"data": ["HTTP Request add new userr184", 1, 0, 0.0, 554.0, 554, 554, 554.0, 554.0, 554.0, 1.8050541516245489, 2.820397111913357, 0.941307536101083], "isController": false}, {"data": ["HTTP Request add new userr185", 1, 1, 100.0, 409.0, 409, 409, 409.0, 409.0, 409.0, 2.444987775061125, 1.8289654645476774, 1.2750229217603912], "isController": false}, {"data": ["HTTP Request add new userr186", 1, 0, 0.0, 492.0, 492, 492, 492.0, 492.0, 492.0, 2.032520325203252, 3.1758130081300813, 1.0599275914634148], "isController": false}, {"data": ["HTTP Request add new userr187", 1, 0, 0.0, 467.0, 467, 467, 467.0, 467.0, 467.0, 2.1413276231263385, 3.3458244111349034, 1.116668897216274], "isController": false}, {"data": ["HTTP Request add new userr100", 1, 0, 0.0, 489.0, 489, 489, 489.0, 489.0, 489.0, 2.044989775051125, 3.1952965235173827, 1.0664302147239264], "isController": false}, {"data": ["HTTP Request add new userr188", 1, 0, 0.0, 483.0, 483, 483, 483.0, 483.0, 483.0, 2.070393374741201, 3.2349896480331264, 1.0796777950310559], "isController": false}, {"data": ["HTTP Request add new userr101", 1, 0, 0.0, 480.0, 480, 480, 480.0, 480.0, 480.0, 2.0833333333333335, 3.2552083333333335, 1.08642578125], "isController": false}, {"data": ["HTTP Request add new userr189", 1, 0, 0.0, 453.0, 453, 453, 453.0, 453.0, 453.0, 2.207505518763797, 3.4492273730684326, 1.1511796357615893], "isController": false}, {"data": ["HTTP Request add new userr102", 1, 0, 0.0, 471.0, 471, 471, 471.0, 471.0, 471.0, 2.1231422505307855, 3.3174097664543525, 1.10718550955414], "isController": false}, {"data": ["HTTP Request add new userr103", 1, 1, 100.0, 368.0, 368, 368, 368.0, 368.0, 368.0, 2.717391304347826, 2.0327360733695654, 1.417077105978261], "isController": false}, {"data": ["HTTP Request add new userr104", 1, 1, 100.0, 337.0, 337, 337, 337.0, 337.0, 337.0, 2.967359050445104, 2.219723664688427, 1.5474313798219583], "isController": false}, {"data": ["HTTP Request add new userr105", 1, 0, 0.0, 494.0, 494, 494, 494.0, 494.0, 494.0, 2.0242914979757085, 3.1629554655870447, 1.055636386639676], "isController": false}, {"data": ["HTTP Request add new userr106", 1, 0, 0.0, 767.0, 767, 767, 767.0, 767.0, 767.0, 1.303780964797914, 2.0371577574967406, 0.6799014015645372], "isController": false}, {"data": ["HTTP Request add new userr107", 1, 0, 0.0, 775.0, 775, 775, 775.0, 775.0, 775.0, 1.2903225806451613, 2.0161290322580645, 0.672883064516129], "isController": false}, {"data": ["HTTP Request add new userr108", 1, 0, 0.0, 562.0, 562, 562, 562.0, 562.0, 562.0, 1.779359430604982, 2.7802491103202844, 0.927908140569395], "isController": false}, {"data": ["HTTP Request add new userr109", 1, 0, 0.0, 459.0, 459, 459, 459.0, 459.0, 459.0, 2.1786492374727673, 3.4041394335511983, 1.1361315359477124], "isController": false}, {"data": ["HTTP Request add new userr190", 1, 0, 0.0, 476.0, 476, 476, 476.0, 476.0, 476.0, 2.100840336134454, 3.2825630252100844, 1.0955554096638656], "isController": false}, {"data": ["HTTP Request add new userr191", 1, 1, 100.0, 366.0, 366, 366, 366.0, 366.0, 366.0, 2.73224043715847, 2.043843920765027, 1.4248206967213115], "isController": false}, {"data": ["HTTP Request add new userr192", 1, 0, 0.0, 446.0, 446, 446, 446.0, 446.0, 446.0, 2.242152466367713, 3.5033632286995515, 1.1692474775784754], "isController": false}, {"data": ["HTTP Request add new userr193", 1, 0, 0.0, 496.0, 496, 496, 496.0, 496.0, 496.0, 2.0161290322580645, 3.150201612903226, 1.0513797883064515], "isController": false}, {"data": ["HTTP Request add new userr194", 1, 1, 100.0, 391.0, 391, 391, 391.0, 391.0, 391.0, 2.557544757033248, 1.9131633631713554, 1.3337196291560103], "isController": false}, {"data": ["HTTP Request add new userr195", 1, 0, 0.0, 470.0, 470, 470, 470.0, 470.0, 470.0, 2.127659574468085, 3.3244680851063833, 1.1095412234042554], "isController": false}, {"data": ["HTTP Request add new userr196", 1, 1, 100.0, 350.0, 350, 350, 350.0, 350.0, 350.0, 2.857142857142857, 2.137276785714286, 1.4899553571428572], "isController": false}, {"data": ["HTTP Request add new userr197", 1, 1, 100.0, 357.0, 357, 357, 357.0, 357.0, 357.0, 2.8011204481792715, 2.0953693977591037, 1.4607405462184875], "isController": false}, {"data": ["HTTP Request add new userr110", 1, 1, 100.0, 384.0, 384, 384, 384.0, 384.0, 384.0, 2.6041666666666665, 1.9480387369791665, 1.3580322265625], "isController": false}, {"data": ["HTTP Request add new userr198", 1, 1, 100.0, 347.0, 347, 347, 347.0, 347.0, 347.0, 2.881844380403458, 2.1557546829971184, 1.5028368155619598], "isController": false}, {"data": ["HTTP Request add new userr111", 1, 0, 0.0, 479.0, 479, 479, 479.0, 479.0, 479.0, 2.08768267223382, 3.2620041753653446, 1.0886938935281838], "isController": false}, {"data": ["HTTP Request add new userr199", 1, 0, 0.0, 472.0, 472, 472, 472.0, 472.0, 472.0, 2.1186440677966103, 3.3103813559322037, 1.1048397775423728], "isController": false}, {"data": ["HTTP Request add new userr112", 1, 1, 100.0, 621.0, 621, 621, 621.0, 621.0, 621.0, 1.6103059581320451, 1.2045843397745573, 0.8397493961352657], "isController": false}, {"data": ["HTTP Request add new userr113", 1, 0, 0.0, 499.0, 499, 499, 499.0, 499.0, 499.0, 2.004008016032064, 3.1312625250501003, 1.0450588677354709], "isController": false}, {"data": ["HTTP Request add new userr114", 1, 0, 0.0, 447.0, 447, 447, 447.0, 447.0, 447.0, 2.237136465324385, 3.495525727069351, 1.166631711409396], "isController": false}, {"data": ["HTTP Request add new userr115", 1, 0, 0.0, 492.0, 492, 492, 492.0, 492.0, 492.0, 2.032520325203252, 3.1758130081300813, 1.0599275914634148], "isController": false}, {"data": ["HTTP Request add new userr116", 1, 0, 0.0, 618.0, 618, 618, 618.0, 618.0, 618.0, 1.6181229773462784, 2.52831715210356, 0.8438258495145631], "isController": false}, {"data": ["HTTP Request add new userr117", 1, 0, 0.0, 787.0, 787, 787, 787.0, 787.0, 787.0, 1.2706480304955527, 1.985387547649301, 0.6626230940279543], "isController": false}, {"data": ["HTTP Request add new userr118", 1, 0, 0.0, 462.0, 462, 462, 462.0, 462.0, 462.0, 2.1645021645021645, 3.3820346320346317, 1.1287540584415583], "isController": false}, {"data": ["HTTP Request add new userr119", 1, 0, 0.0, 695.0, 695, 695, 695.0, 695.0, 695.0, 1.4388489208633093, 2.248201438848921, 0.7503372302158274], "isController": false}, {"data": ["HTTP Request add new userr160", 1, 0, 0.0, 445.0, 445, 445, 445.0, 445.0, 445.0, 2.247191011235955, 3.5112359550561796, 1.171875], "isController": false}, {"data": ["HTTP Request add new userr161", 1, 1, 100.0, 365.0, 365, 365, 365.0, 365.0, 365.0, 2.73972602739726, 2.049443493150685, 1.4287243150684932], "isController": false}, {"data": ["HTTP Request add new userr162", 1, 0, 0.0, 493.0, 493, 493, 493.0, 493.0, 493.0, 2.028397565922921, 3.169371196754564, 1.0577776369168357], "isController": false}, {"data": ["HTTP Request add new userr163", 1, 0, 0.0, 472.0, 472, 472, 472.0, 472.0, 472.0, 2.1186440677966103, 3.3103813559322037, 1.1048397775423728], "isController": false}, {"data": ["HTTP Request add new userr164", 1, 0, 0.0, 481.0, 481, 481, 481.0, 481.0, 481.0, 2.079002079002079, 3.2484407484407485, 1.0841670997920998], "isController": false}, {"data": ["HTTP Request add new userr165", 1, 0, 0.0, 518.0, 518, 518, 518.0, 518.0, 518.0, 1.9305019305019306, 3.016409266409266, 1.0067265926640927], "isController": false}, {"data": ["HTTP Request add new userr166", 1, 0, 0.0, 694.0, 694, 694, 694.0, 694.0, 694.0, 1.440922190201729, 2.251440922190202, 0.7514184077809799], "isController": false}, {"data": ["HTTP Request add new userr167", 1, 0, 0.0, 486.0, 486, 486, 486.0, 486.0, 486.0, 2.05761316872428, 3.2150205761316872, 1.0730131172839505], "isController": false}, {"data": ["HTTP Request add new userr168", 1, 1, 100.0, 386.0, 386, 386, 386.0, 386.0, 386.0, 2.5906735751295336, 1.9379452720207253, 1.3509957901554404], "isController": false}, {"data": ["HTTP Request add new userr169", 1, 0, 0.0, 655.0, 655, 655, 655.0, 655.0, 655.0, 1.5267175572519083, 2.3854961832061066, 0.7961593511450381], "isController": false}, {"data": ["HTTP Request add new userr170", 1, 0, 0.0, 481.0, 481, 481, 481.0, 481.0, 481.0, 2.079002079002079, 3.2484407484407485, 1.0841670997920998], "isController": false}, {"data": ["HTTP Request add new userr171", 1, 1, 100.0, 339.0, 339, 339, 339.0, 339.0, 339.0, 2.949852507374631, 2.2066279498525074, 1.5383019911504423], "isController": false}, {"data": ["HTTP Request add new userr172", 1, 0, 0.0, 475.0, 475, 475, 475.0, 475.0, 475.0, 2.1052631578947367, 3.2894736842105265, 1.0978618421052633], "isController": false}, {"data": ["HTTP Request add new userr173", 1, 0, 0.0, 442.0, 442, 442, 442.0, 442.0, 442.0, 2.2624434389140275, 3.5350678733031673, 1.179828902714932], "isController": false}, {"data": ["HTTP Request add new userr174", 1, 0, 0.0, 477.0, 477, 477, 477.0, 477.0, 477.0, 2.0964360587002098, 3.2756813417190775, 1.0932586477987423], "isController": false}, {"data": ["HTTP Request add new userr175", 1, 0, 0.0, 498.0, 498, 498, 498.0, 498.0, 498.0, 2.008032128514056, 3.137550200803213, 1.0471573795180722], "isController": false}, {"data": ["HTTP Request add new userr176", 1, 0, 0.0, 468.0, 468, 468, 468.0, 468.0, 468.0, 2.136752136752137, 3.3386752136752134, 1.1142828525641024], "isController": false}, {"data": ["HTTP Request add new userr177", 1, 0, 0.0, 494.0, 494, 494, 494.0, 494.0, 494.0, 2.0242914979757085, 3.1629554655870447, 1.055636386639676], "isController": false}, {"data": ["HTTP Request add new userr178", 1, 0, 0.0, 454.0, 454, 454, 454.0, 454.0, 454.0, 2.2026431718061676, 3.4416299559471364, 1.1486439977973568], "isController": false}, {"data": ["HTTP Request add new userr179", 1, 0, 0.0, 446.0, 446, 446, 446.0, 446.0, 446.0, 2.242152466367713, 3.5033632286995515, 1.1692474775784754], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["500\/Internal Server Error", 28, 100.0, 28.0], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 100, 28, "500\/Internal Server Error", 28, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr142", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr143", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr144", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr154", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr156", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr158", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr159", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr120", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr123", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr127", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr129", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr130", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr131", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr136", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr139", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr185", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr103", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr104", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr191", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr194", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr196", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr197", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr110", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr198", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr112", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr161", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr168", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr171", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
