/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 85.0, "KoPercent": 15.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.045, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.0, 500, 1500, "HTTP Request add new userr140"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr141"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr142"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr143"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr144"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr145"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr146"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr147"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr148"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr149"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr150"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr151"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr152"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr153"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr154"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr155"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr156"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr157"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr158"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr159"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr120"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr121"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr122"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr123"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr124"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr125"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr126"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr127"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr128"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr129"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr130"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr131"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr132"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr133"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr134"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr135"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr136"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr137"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr138"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr139"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr180"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr181"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr182"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr183"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr184"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr185"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr186"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr187"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr188"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr100"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr189"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr101"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr102"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr103"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr104"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr105"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr106"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr107"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr108"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr109"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr190"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr191"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr192"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr193"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr194"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr195"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr196"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr197"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr110"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr198"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr199"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr111"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr112"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr113"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr114"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr115"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr116"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr117"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr118"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr119"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr160"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr161"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr162"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr163"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr164"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr165"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr166"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr167"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr168"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr169"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr170"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr171"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr172"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr173"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr174"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr175"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr176"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr177"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr178"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr179"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 100, 15, 15.0, 6465.93, 581, 10713, 10129.2, 10408.55, 10711.98, 3.9411973357506014, 5.676632764158751, 2.0552728293855673], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["HTTP Request add new userr140", 1, 0, 0.0, 10381.0, 10381, 10381, 10381.0, 10381.0, 10381.0, 0.09632983334938831, 0.15051536460841922, 0.050234502938059915], "isController": false}, {"data": ["HTTP Request add new userr141", 1, 0, 0.0, 4269.0, 4269, 4269, 4269.0, 4269.0, 4269.0, 0.23424689622862496, 0.3660107753572265, 0.12215609627547434], "isController": false}, {"data": ["HTTP Request add new userr142", 1, 0, 0.0, 8075.0, 8075, 8075, 8075.0, 8075.0, 8075.0, 0.1238390092879257, 0.19349845201238391, 0.06458010835913314], "isController": false}, {"data": ["HTTP Request add new userr143", 1, 0, 0.0, 9814.0, 9814, 9814, 9814.0, 9814.0, 9814.0, 0.10189525168127164, 0.15921133075198696, 0.05313678163847565], "isController": false}, {"data": ["HTTP Request add new userr144", 1, 1, 100.0, 7778.0, 7778, 7778, 7778.0, 7778.0, 7778.0, 0.1285677552069941, 0.09617470750835691, 0.06704607546927231], "isController": false}, {"data": ["HTTP Request add new userr145", 1, 0, 0.0, 3957.0, 3957, 3957, 3957.0, 3957.0, 3957.0, 0.25271670457417234, 0.3948698508971443, 0.13178781273692192], "isController": false}, {"data": ["HTTP Request add new userr146", 1, 0, 0.0, 5808.0, 5808, 5808, 5808.0, 5808.0, 5808.0, 0.17217630853994492, 0.2690254820936639, 0.08978725464876033], "isController": false}, {"data": ["HTTP Request add new userr147", 1, 0, 0.0, 8015.0, 8015, 8015, 8015.0, 8015.0, 8015.0, 0.12476606363069247, 0.19494697442295694, 0.06506355271366188], "isController": false}, {"data": ["HTTP Request add new userr148", 1, 0, 0.0, 698.0, 698, 698, 698.0, 698.0, 698.0, 1.4326647564469914, 2.238538681948424, 0.7471122851002866], "isController": false}, {"data": ["HTTP Request add new userr149", 1, 0, 0.0, 8267.0, 8267, 8267, 8267.0, 8267.0, 8267.0, 0.120962864400629, 0.18900447562598283, 0.06308024374017177], "isController": false}, {"data": ["HTTP Request add new userr150", 1, 1, 100.0, 10410.0, 10410, 10410, 10410.0, 10410.0, 10410.0, 0.09606147934678194, 0.07185848943323728, 0.05009456051873199], "isController": false}, {"data": ["HTTP Request add new userr151", 1, 0, 0.0, 10235.0, 10235, 10235, 10235.0, 10235.0, 10235.0, 0.09770395701025891, 0.15266243282852957, 0.05095108695652174], "isController": false}, {"data": ["HTTP Request add new userr152", 1, 0, 0.0, 8503.0, 8503, 8503, 8503.0, 8503.0, 8503.0, 0.11760555098200635, 0.18375867340938493, 0.06132945725038222], "isController": false}, {"data": ["HTTP Request add new userr153", 1, 0, 0.0, 5020.0, 5020, 5020, 5020.0, 5020.0, 5020.0, 0.19920318725099603, 0.3112549800796813, 0.10388134960159363], "isController": false}, {"data": ["HTTP Request add new userr154", 1, 0, 0.0, 2680.0, 2680, 2680, 2680.0, 2680.0, 2680.0, 0.373134328358209, 0.5830223880597014, 0.19458372201492535], "isController": false}, {"data": ["HTTP Request add new userr155", 1, 0, 0.0, 1366.0, 1366, 1366, 1366.0, 1366.0, 1366.0, 0.7320644216691069, 1.1438506588579793, 0.3817601573938506], "isController": false}, {"data": ["HTTP Request add new userr156", 1, 0, 0.0, 5337.0, 5337, 5337, 5337.0, 5337.0, 5337.0, 0.18737118231216038, 0.29276747236275064, 0.09771114390106803], "isController": false}, {"data": ["HTTP Request add new userr157", 1, 0, 0.0, 8339.0, 8339, 8339, 8339.0, 8339.0, 8339.0, 0.1199184554502938, 0.18737258664108405, 0.06253560079146181], "isController": false}, {"data": ["HTTP Request add new userr158", 1, 0, 0.0, 9689.0, 9689, 9689, 9689.0, 9689.0, 9689.0, 0.10320982557539478, 0.16126535246155435, 0.05382231138404376], "isController": false}, {"data": ["HTTP Request add new userr159", 1, 0, 0.0, 7543.0, 7543, 7543, 7543.0, 7543.0, 7543.0, 0.13257324671881213, 0.20714569799814397, 0.06913487670688055], "isController": false}, {"data": ["HTTP Request add new userr120", 1, 0, 0.0, 954.0, 954, 954, 954.0, 954.0, 954.0, 1.0482180293501049, 1.6378406708595388, 0.5466293238993711], "isController": false}, {"data": ["HTTP Request add new userr121", 1, 0, 0.0, 5946.0, 5946, 5946, 5946.0, 5946.0, 5946.0, 0.16818028927009754, 0.26278170198452744, 0.08770339303733603], "isController": false}, {"data": ["HTTP Request add new userr122", 1, 0, 0.0, 4235.0, 4235, 4235, 4235.0, 4235.0, 4235.0, 0.2361275088547816, 0.3689492325855962, 0.12313680637544273], "isController": false}, {"data": ["HTTP Request add new userr123", 1, 0, 0.0, 6725.0, 6725, 6725, 6725.0, 6725.0, 6725.0, 0.14869888475836432, 0.23234200743494424, 0.07754414498141264], "isController": false}, {"data": ["HTTP Request add new userr124", 1, 1, 100.0, 8251.0, 8251, 8251, 8251.0, 8251.0, 8251.0, 0.12119743061447097, 0.09066135922918435, 0.06320256635559326], "isController": false}, {"data": ["HTTP Request add new userr125", 1, 0, 0.0, 2784.0, 2784, 2784, 2784.0, 2784.0, 2784.0, 0.35919540229885055, 0.561242816091954, 0.18731478987068967], "isController": false}, {"data": ["HTTP Request add new userr126", 1, 1, 100.0, 7423.0, 7423, 7423, 7423.0, 7423.0, 7423.0, 0.1347164219318335, 0.1007741984372895, 0.07025250909335848], "isController": false}, {"data": ["HTTP Request add new userr127", 1, 0, 0.0, 7215.0, 7215, 7215, 7215.0, 7215.0, 7215.0, 0.1386001386001386, 0.21656271656271656, 0.07227780665280666], "isController": false}, {"data": ["HTTP Request add new userr128", 1, 0, 0.0, 7866.0, 7866, 7866, 7866.0, 7866.0, 7866.0, 0.12712941774726672, 0.19863971523010426, 0.0662960049580473], "isController": false}, {"data": ["HTTP Request add new userr129", 1, 0, 0.0, 6995.0, 6995, 6995, 6995.0, 6995.0, 6995.0, 0.14295925661186562, 0.22337383845604003, 0.07455101858470335], "isController": false}, {"data": ["HTTP Request add new userr130", 1, 1, 100.0, 8148.0, 8148, 8148, 8148.0, 8148.0, 8148.0, 0.12272950417280314, 0.09180742206676486, 0.06400151877761415], "isController": false}, {"data": ["HTTP Request add new userr131", 1, 1, 100.0, 1573.0, 1573, 1573, 1573.0, 1573.0, 1573.0, 0.6357279084551812, 0.4755542752701844, 0.3315221710108074], "isController": false}, {"data": ["HTTP Request add new userr132", 1, 0, 0.0, 8399.0, 8399, 8399, 8399.0, 8399.0, 8399.0, 0.11906179307060365, 0.18603405167281822, 0.06208886474580308], "isController": false}, {"data": ["HTTP Request add new userr133", 1, 0, 0.0, 7124.0, 7124, 7124, 7124.0, 7124.0, 7124.0, 0.1403705783267827, 0.219329028635598, 0.07320106330713083], "isController": false}, {"data": ["HTTP Request add new userr134", 1, 0, 0.0, 7054.0, 7054, 7054, 7054.0, 7054.0, 7054.0, 0.14176353841791892, 0.22150552877799828, 0.07392747022965693], "isController": false}, {"data": ["HTTP Request add new userr135", 1, 1, 100.0, 8054.0, 8054, 8054, 8054.0, 8054.0, 8054.0, 0.12416190712689348, 0.09287892662031288, 0.06474849453687609], "isController": false}, {"data": ["HTTP Request add new userr136", 1, 0, 0.0, 9925.0, 9925, 9925, 9925.0, 9925.0, 9925.0, 0.10075566750629723, 0.1574307304785894, 0.052542506297229216], "isController": false}, {"data": ["HTTP Request add new userr137", 1, 0, 0.0, 1225.0, 1225, 1225, 1225.0, 1225.0, 1225.0, 0.8163265306122449, 1.2755102040816326, 0.42570153061224486], "isController": false}, {"data": ["HTTP Request add new userr138", 1, 0, 0.0, 10464.0, 10464, 10464, 10464.0, 10464.0, 10464.0, 0.095565749235474, 0.14932148318042812, 0.049836045011467885], "isController": false}, {"data": ["HTTP Request add new userr139", 1, 0, 0.0, 10133.0, 10133, 10133, 10133.0, 10133.0, 10133.0, 0.09868745682423764, 0.15419915128787132, 0.051463966742327054], "isController": false}, {"data": ["HTTP Request add new userr180", 1, 0, 0.0, 8506.0, 8506, 8506, 8506.0, 8506.0, 8506.0, 0.11756407241946862, 0.1836938631554197, 0.061307826828121324], "isController": false}, {"data": ["HTTP Request add new userr181", 1, 0, 0.0, 7244.0, 7244, 7244, 7244.0, 7244.0, 7244.0, 0.13804527885146328, 0.21569574820541138, 0.07198845596355605], "isController": false}, {"data": ["HTTP Request add new userr182", 1, 0, 0.0, 7284.0, 7284, 7284, 7284.0, 7284.0, 7284.0, 0.1372872048325096, 0.21451125755079628, 0.07159313220757825], "isController": false}, {"data": ["HTTP Request add new userr183", 1, 0, 0.0, 2914.0, 2914, 2914, 2914.0, 2914.0, 2914.0, 0.34317089910775567, 0.5362045298558682, 0.178958261839396], "isController": false}, {"data": ["HTTP Request add new userr184", 1, 1, 100.0, 6857.0, 6857, 6857, 6857.0, 6857.0, 6857.0, 0.14583637159107482, 0.10909244203004229, 0.07605138909143941], "isController": false}, {"data": ["HTTP Request add new userr185", 1, 0, 0.0, 6300.0, 6300, 6300, 6300.0, 6300.0, 6300.0, 0.15873015873015872, 0.24801587301587302, 0.08277529761904762], "isController": false}, {"data": ["HTTP Request add new userr186", 1, 0, 0.0, 8485.0, 8485, 8485, 8485.0, 8485.0, 8485.0, 0.11785503830288745, 0.18414849734826166, 0.06145956098998233], "isController": false}, {"data": ["HTTP Request add new userr187", 1, 0, 0.0, 1118.0, 1118, 1118, 1118.0, 1118.0, 1118.0, 0.8944543828264758, 1.3975849731663683, 0.46644398479427546], "isController": false}, {"data": ["HTTP Request add new userr188", 1, 0, 0.0, 7422.0, 7422, 7422, 7422.0, 7422.0, 7422.0, 0.13473457289140392, 0.21052277014281864, 0.07026197453516572], "isController": false}, {"data": ["HTTP Request add new userr100", 1, 0, 0.0, 8374.0, 8374, 8374, 8374.0, 8374.0, 8374.0, 0.11941724385001194, 0.18658944351564366, 0.06227422677334607], "isController": false}, {"data": ["HTTP Request add new userr189", 1, 0, 0.0, 1422.0, 1422, 1422, 1422.0, 1422.0, 1422.0, 0.7032348804500703, 1.098804500703235, 0.3667260021097047], "isController": false}, {"data": ["HTTP Request add new userr101", 1, 0, 0.0, 9743.0, 9743, 9743, 9743.0, 9743.0, 9743.0, 0.10263779123473263, 0.16037154880426974, 0.05352400441342502], "isController": false}, {"data": ["HTTP Request add new userr102", 1, 0, 0.0, 3124.0, 3124, 3124, 3124.0, 3124.0, 3124.0, 0.3201024327784891, 0.5001600512163892, 0.1669284170934699], "isController": false}, {"data": ["HTTP Request add new userr103", 1, 0, 0.0, 7630.0, 7630, 7630, 7630.0, 7630.0, 7630.0, 0.1310615989515072, 0.20478374836173002, 0.0683465760157274], "isController": false}, {"data": ["HTTP Request add new userr104", 1, 0, 0.0, 5950.0, 5950, 5950, 5950.0, 5950.0, 5950.0, 0.16806722689075632, 0.26260504201680673, 0.08764443277310924], "isController": false}, {"data": ["HTTP Request add new userr105", 1, 0, 0.0, 7880.0, 7880, 7880, 7880.0, 7880.0, 7880.0, 0.1269035532994924, 0.19828680203045684, 0.06617822017766498], "isController": false}, {"data": ["HTTP Request add new userr106", 1, 0, 0.0, 6875.0, 6875, 6875, 6875.0, 6875.0, 6875.0, 0.14545454545454545, 0.22727272727272727, 0.07585227272727273], "isController": false}, {"data": ["HTTP Request add new userr107", 1, 0, 0.0, 1046.0, 1046, 1046, 1046.0, 1046.0, 1046.0, 0.9560229445506692, 1.4937858508604205, 0.49855102772466536], "isController": false}, {"data": ["HTTP Request add new userr108", 1, 0, 0.0, 581.0, 581, 581, 581.0, 581.0, 581.0, 1.721170395869191, 2.689328743545611, 0.8975634681583478], "isController": false}, {"data": ["HTTP Request add new userr109", 1, 0, 0.0, 7823.0, 7823, 7823, 7823.0, 7823.0, 7823.0, 0.1278281989006775, 0.19973156078230855, 0.06666040841109548], "isController": false}, {"data": ["HTTP Request add new userr190", 1, 0, 0.0, 9834.0, 9834, 9834, 9834.0, 9834.0, 9834.0, 0.10168802115110841, 0.1588875330486069, 0.053028714154972546], "isController": false}, {"data": ["HTTP Request add new userr191", 1, 0, 0.0, 6692.0, 6692, 6692, 6692.0, 6692.0, 6692.0, 0.14943215780035862, 0.23348774656306037, 0.0779265354154214], "isController": false}, {"data": ["HTTP Request add new userr192", 1, 0, 0.0, 7682.0, 7682, 7682, 7682.0, 7682.0, 7682.0, 0.13017443374121324, 0.20339755272064566, 0.06788393322051549], "isController": false}, {"data": ["HTTP Request add new userr193", 1, 0, 0.0, 5422.0, 5422, 5422, 5422.0, 5422.0, 5422.0, 0.18443378827001108, 0.2881777941718923, 0.09617933880486906], "isController": false}, {"data": ["HTTP Request add new userr194", 1, 0, 0.0, 8291.0, 8291, 8291, 8291.0, 8291.0, 8291.0, 0.12061271257990593, 0.188457363406103, 0.06289764503678688], "isController": false}, {"data": ["HTTP Request add new userr195", 1, 0, 0.0, 8894.0, 8894, 8894, 8894.0, 8894.0, 8894.0, 0.11243534967393749, 0.1756802338655273, 0.058633278052619746], "isController": false}, {"data": ["HTTP Request add new userr196", 1, 0, 0.0, 5841.0, 5841, 5841, 5841.0, 5841.0, 5841.0, 0.1712035610340695, 0.2675055641157336, 0.0892799820236261], "isController": false}, {"data": ["HTTP Request add new userr197", 1, 0, 0.0, 3810.0, 3810, 3810, 3810.0, 3810.0, 3810.0, 0.26246719160104987, 0.41010498687664043, 0.13687253937007873], "isController": false}, {"data": ["HTTP Request add new userr110", 1, 0, 0.0, 6591.0, 6591, 6591, 6591.0, 6591.0, 6591.0, 0.15172204521316945, 0.2370656956455773, 0.07912067592171142], "isController": false}, {"data": ["HTTP Request add new userr198", 1, 0, 0.0, 7087.0, 7087, 7087, 7087.0, 7087.0, 7087.0, 0.14110342881332016, 0.22047410752081276, 0.07358323338507126], "isController": false}, {"data": ["HTTP Request add new userr199", 1, 0, 0.0, 2616.0, 2616, 2616, 2616.0, 2616.0, 2616.0, 0.382262996941896, 0.5972859327217125, 0.19934418004587154], "isController": false}, {"data": ["HTTP Request add new userr111", 1, 0, 0.0, 3310.0, 3310, 3310, 3310.0, 3310.0, 3310.0, 0.3021148036253776, 0.47205438066465255, 0.1575481495468278], "isController": false}, {"data": ["HTTP Request add new userr112", 1, 0, 0.0, 3334.0, 3334, 3334, 3334.0, 3334.0, 3334.0, 0.29994001199760045, 0.4686562687462507, 0.15641402969406118], "isController": false}, {"data": ["HTTP Request add new userr113", 1, 0, 0.0, 9329.0, 9329, 9329, 9329.0, 9329.0, 9329.0, 0.10719262514738985, 0.16748847679279663, 0.05589927912959588], "isController": false}, {"data": ["HTTP Request add new userr114", 1, 0, 0.0, 6647.0, 6647, 6647, 6647.0, 6647.0, 6647.0, 0.15044380923724987, 0.23506845193320294, 0.07845409583270647], "isController": false}, {"data": ["HTTP Request add new userr115", 1, 0, 0.0, 8751.0, 8751, 8751, 8751.0, 8751.0, 8751.0, 0.11427265455376528, 0.17855102274025828, 0.0595914038395612], "isController": false}, {"data": ["HTTP Request add new userr116", 1, 0, 0.0, 10498.0, 10498, 10498, 10498.0, 10498.0, 10498.0, 0.09525623928367309, 0.14883787388073919, 0.04967464040769671], "isController": false}, {"data": ["HTTP Request add new userr117", 1, 0, 0.0, 4071.0, 4071, 4071, 4071.0, 4071.0, 4071.0, 0.24563989191844754, 0.38381233112257435, 0.1280973655121592], "isController": false}, {"data": ["HTTP Request add new userr118", 1, 0, 0.0, 10611.0, 10611, 10611, 10611.0, 10611.0, 10611.0, 0.09424182452172274, 0.14725285081519177, 0.049145638959570255], "isController": false}, {"data": ["HTTP Request add new userr119", 1, 1, 100.0, 8787.0, 8787, 8787, 8787.0, 8787.0, 8787.0, 0.11380448389666553, 0.08513108853988846, 0.059347260157050184], "isController": false}, {"data": ["HTTP Request add new userr160", 1, 0, 0.0, 10713.0, 10713, 10713, 10713.0, 10713.0, 10713.0, 0.09334453467749462, 0.14585083543358537, 0.04867771632595912], "isController": false}, {"data": ["HTTP Request add new userr161", 1, 0, 0.0, 2232.0, 2232, 2232, 2232.0, 2232.0, 2232.0, 0.44802867383512546, 0.7000448028673835, 0.23363995295698922], "isController": false}, {"data": ["HTTP Request add new userr162", 1, 1, 100.0, 1848.0, 1848, 1848, 1848.0, 1848.0, 1848.0, 0.5411255411255411, 0.404787270021645, 0.2821885146103896], "isController": false}, {"data": ["HTTP Request add new userr163", 1, 0, 0.0, 9227.0, 9227, 9227, 9227.0, 9227.0, 9227.0, 0.10837758751490192, 0.16933998049203425, 0.056517218489216425], "isController": false}, {"data": ["HTTP Request add new userr164", 1, 0, 0.0, 5226.0, 5226, 5226, 5226.0, 5226.0, 5226.0, 0.19135093761959435, 0.2989858400306162, 0.09978652411021814], "isController": false}, {"data": ["HTTP Request add new userr165", 1, 0, 0.0, 2626.0, 2626, 2626, 2626.0, 2626.0, 2626.0, 0.38080731150038083, 0.595011424219345, 0.1985850628332064], "isController": false}, {"data": ["HTTP Request add new userr166", 1, 1, 100.0, 9726.0, 9726, 9726, 9726.0, 9726.0, 9726.0, 0.10281719103434094, 0.07691207844951675, 0.053617558605798886], "isController": false}, {"data": ["HTTP Request add new userr167", 1, 1, 100.0, 8637.0, 8637, 8637, 8637.0, 8637.0, 8637.0, 0.1157809424568716, 0.08660957218941762, 0.06037795241403265], "isController": false}, {"data": ["HTTP Request add new userr168", 1, 0, 0.0, 2529.0, 2529, 2529, 2529.0, 2529.0, 2529.0, 0.39541320680110714, 0.6178331356267299, 0.20620180901542112], "isController": false}, {"data": ["HTTP Request add new userr169", 1, 0, 0.0, 2846.0, 2846, 2846, 2846.0, 2846.0, 2846.0, 0.35137034434293746, 0.5490161630358398, 0.1832341444132115], "isController": false}, {"data": ["HTTP Request add new userr170", 1, 0, 0.0, 9880.0, 9880, 9880, 9880.0, 9880.0, 9880.0, 0.10121457489878542, 0.15814777327935223, 0.0527818193319838], "isController": false}, {"data": ["HTTP Request add new userr171", 1, 0, 0.0, 7812.0, 7812, 7812, 7812.0, 7812.0, 7812.0, 0.12800819252432155, 0.20001280081925243, 0.0667542722734255], "isController": false}, {"data": ["HTTP Request add new userr172", 1, 0, 0.0, 4806.0, 4806, 4806, 4806.0, 4806.0, 4806.0, 0.20807324178110695, 0.3251144402829796, 0.10850694444444445], "isController": false}, {"data": ["HTTP Request add new userr173", 1, 0, 0.0, 10154.0, 10154, 10154, 10154.0, 10154.0, 10154.0, 0.09848335631278314, 0.15388024423872365, 0.051357531514674024], "isController": false}, {"data": ["HTTP Request add new userr174", 1, 1, 100.0, 10272.0, 10272, 10272, 10272.0, 10272.0, 10272.0, 0.09735202492211838, 0.07282387801791278, 0.050767559871495324], "isController": false}, {"data": ["HTTP Request add new userr175", 1, 0, 0.0, 3034.0, 3034, 3034, 3034.0, 3034.0, 3034.0, 0.3295978905735003, 0.5149967040210943, 0.17188014996704024], "isController": false}, {"data": ["HTTP Request add new userr176", 1, 0, 0.0, 8413.0, 8413, 8413, 8413.0, 8413.0, 8413.0, 0.11886366337810532, 0.18572447402828954, 0.061985543206941635], "isController": false}, {"data": ["HTTP Request add new userr177", 1, 1, 100.0, 10095.0, 10095, 10095, 10095.0, 10095.0, 10095.0, 0.09905894006934125, 0.07410073055968301, 0.05165768945022288], "isController": false}, {"data": ["HTTP Request add new userr178", 1, 1, 100.0, 3846.0, 3846, 3846, 3846.0, 3846.0, 3846.0, 0.26001040041601664, 0.19449996749869994, 0.13559136115444617], "isController": false}, {"data": ["HTTP Request add new userr179", 1, 0, 0.0, 1318.0, 1318, 1318, 1318.0, 1318.0, 1318.0, 0.7587253414264037, 1.1855083459787557, 0.3956634104704097], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["500\/Internal Server Error", 15, 100.0, 15.0], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 100, 15, "500\/Internal Server Error", 15, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr144", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr150", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr124", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr126", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr130", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr131", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr135", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr184", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr119", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr162", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr166", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr167", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr174", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr177", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": ["HTTP Request add new userr178", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
