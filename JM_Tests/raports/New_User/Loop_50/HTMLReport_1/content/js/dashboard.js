/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 90.0, "KoPercent": 10.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.44, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.5, 500, 1500, "HTTP Request add new userr140"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr141"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr142"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr143"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr144"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr100"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr145"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr101"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr146"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr102"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr103"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr147"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr104"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr148"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr149"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr105"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr106"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr107"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr108"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr109"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr110"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr111"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr112"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr113"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr114"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr115"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr116"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr117"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr118"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr119"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr120"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr121"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr122"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr123"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr124"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr125"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr126"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr127"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr128"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr129"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr130"], "isController": false}, {"data": [1.0, 500, 1500, "HTTP Request add new userr131"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr132"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr133"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr134"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr135"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr136"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr137"], "isController": false}, {"data": [0.0, 500, 1500, "HTTP Request add new userr138"], "isController": false}, {"data": [0.5, 500, 1500, "HTTP Request add new userr139"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 50, 5, 10.0, 966.52, 425, 1918, 1695.1999999999998, 1783.9499999999996, 1918.0, 3.1709791983764584, 4.696393605720446, 1.6536161054033485], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["HTTP Request add new userr140", 1, 0, 0.0, 777.0, 777, 777, 777.0, 777.0, 777.0, 1.287001287001287, 2.0109395109395107, 0.6711510617760618], "isController": false}, {"data": ["HTTP Request add new userr141", 1, 0, 0.0, 1029.0, 1029, 1029, 1029.0, 1029.0, 1029.0, 0.9718172983479105, 1.5184645286686105, 0.5067875364431488], "isController": false}, {"data": ["HTTP Request add new userr142", 1, 1, 100.0, 698.0, 698, 698, 698.0, 698.0, 698.0, 1.4326647564469914, 1.071700393982808, 0.7471122851002866], "isController": false}, {"data": ["HTTP Request add new userr143", 1, 0, 0.0, 1004.0, 1004, 1004, 1004.0, 1004.0, 1004.0, 0.9960159362549801, 1.5562749003984064, 0.5194067480079682], "isController": false}, {"data": ["HTTP Request add new userr144", 1, 0, 0.0, 960.0, 960, 960, 960.0, 960.0, 960.0, 1.0416666666666667, 1.6276041666666667, 0.543212890625], "isController": false}, {"data": ["HTTP Request add new userr100", 1, 0, 0.0, 853.0, 853, 853, 853.0, 853.0, 853.0, 1.1723329425556857, 1.831770222743259, 0.6113533118405627], "isController": false}, {"data": ["HTTP Request add new userr145", 1, 0, 0.0, 728.0, 728, 728, 728.0, 728.0, 728.0, 1.3736263736263736, 2.146291208791209, 0.7163246909340659], "isController": false}, {"data": ["HTTP Request add new userr101", 1, 0, 0.0, 929.0, 929, 929, 929.0, 929.0, 929.0, 1.0764262648008611, 1.6819160387513454, 0.5613394779332616], "isController": false}, {"data": ["HTTP Request add new userr146", 1, 0, 0.0, 548.0, 548, 548, 548.0, 548.0, 548.0, 1.8248175182481752, 2.8512773722627736, 0.9516138229927007], "isController": false}, {"data": ["HTTP Request add new userr102", 1, 0, 0.0, 461.0, 461, 461, 461.0, 461.0, 461.0, 2.1691973969631237, 3.3893709327548804, 1.1312025488069415], "isController": false}, {"data": ["HTTP Request add new userr103", 1, 0, 0.0, 1136.0, 1136, 1136, 1136.0, 1136.0, 1136.0, 0.8802816901408451, 1.3754401408450705, 0.4590531470070423], "isController": false}, {"data": ["HTTP Request add new userr147", 1, 0, 0.0, 1269.0, 1269, 1269, 1269.0, 1269.0, 1269.0, 0.7880220646178093, 1.2312844759653272, 0.4109411938534279], "isController": false}, {"data": ["HTTP Request add new userr104", 1, 0, 0.0, 565.0, 565, 565, 565.0, 565.0, 565.0, 1.7699115044247788, 2.7654867256637172, 0.9229811946902656], "isController": false}, {"data": ["HTTP Request add new userr148", 1, 0, 0.0, 1752.0, 1752, 1752, 1752.0, 1752.0, 1752.0, 0.5707762557077625, 0.891837899543379, 0.2976508989726027], "isController": false}, {"data": ["HTTP Request add new userr149", 1, 0, 0.0, 956.0, 956, 956, 956.0, 956.0, 956.0, 1.0460251046025104, 1.6344142259414227, 0.5454857479079498], "isController": false}, {"data": ["HTTP Request add new userr105", 1, 0, 0.0, 1703.0, 1703, 1703, 1703.0, 1703.0, 1703.0, 0.5871990604815032, 0.9174985320023488, 0.3062151350557839], "isController": false}, {"data": ["HTTP Request add new userr106", 1, 0, 0.0, 1733.0, 1733, 1733, 1733.0, 1733.0, 1733.0, 0.5770340450086555, 0.9016156953260241, 0.3009142383150606], "isController": false}, {"data": ["HTTP Request add new userr107", 1, 0, 0.0, 1188.0, 1188, 1188, 1188.0, 1188.0, 1188.0, 0.8417508417508417, 1.3152356902356903, 0.43895991161616166], "isController": false}, {"data": ["HTTP Request add new userr108", 1, 1, 100.0, 1357.0, 1357, 1357, 1357.0, 1357.0, 1357.0, 0.7369196757553427, 0.5512504605747973, 0.3842920965364775], "isController": false}, {"data": ["HTTP Request add new userr109", 1, 0, 0.0, 954.0, 954, 954, 954.0, 954.0, 954.0, 1.0482180293501049, 1.6378406708595388, 0.5466293238993711], "isController": false}, {"data": ["HTTP Request add new userr110", 1, 0, 0.0, 1278.0, 1278, 1278, 1278.0, 1278.0, 1278.0, 0.7824726134585289, 1.2226134585289514, 0.40804724178403756], "isController": false}, {"data": ["HTTP Request add new userr111", 1, 0, 0.0, 1057.0, 1057, 1057, 1057.0, 1057.0, 1057.0, 0.9460737937559129, 1.4782403027436142, 0.4933627010406812], "isController": false}, {"data": ["HTTP Request add new userr112", 1, 0, 0.0, 499.0, 499, 499, 499.0, 499.0, 499.0, 2.004008016032064, 3.1312625250501003, 1.0450588677354709], "isController": false}, {"data": ["HTTP Request add new userr113", 1, 1, 100.0, 694.0, 694, 694, 694.0, 694.0, 694.0, 1.440922190201729, 1.0778773414985592, 0.7514184077809799], "isController": false}, {"data": ["HTTP Request add new userr114", 1, 0, 0.0, 1205.0, 1205, 1205, 1205.0, 1205.0, 1205.0, 0.8298755186721991, 1.296680497925311, 0.4327671161825726], "isController": false}, {"data": ["HTTP Request add new userr115", 1, 0, 0.0, 1248.0, 1248, 1248, 1248.0, 1248.0, 1248.0, 0.8012820512820512, 1.252003205128205, 0.41785606971153844], "isController": false}, {"data": ["HTTP Request add new userr116", 1, 0, 0.0, 485.0, 485, 485, 485.0, 485.0, 485.0, 2.061855670103093, 3.2216494845360826, 1.0752255154639176], "isController": false}, {"data": ["HTTP Request add new userr117", 1, 0, 0.0, 1823.0, 1823, 1823, 1823.0, 1823.0, 1823.0, 0.5485463521667581, 0.8571036752605595, 0.28605835161821175], "isController": false}, {"data": ["HTTP Request add new userr118", 1, 0, 0.0, 625.0, 625, 625, 625.0, 625.0, 625.0, 1.6, 2.5, 0.834375], "isController": false}, {"data": ["HTTP Request add new userr119", 1, 1, 100.0, 930.0, 930, 930, 930.0, 930.0, 930.0, 1.075268817204301, 0.8043514784946236, 0.5607358870967741], "isController": false}, {"data": ["HTTP Request add new userr120", 1, 0, 0.0, 539.0, 539, 539, 539.0, 539.0, 539.0, 1.8552875695732838, 2.8988868274582558, 0.9675034786641928], "isController": false}, {"data": ["HTTP Request add new userr121", 1, 0, 0.0, 1190.0, 1190, 1190, 1190.0, 1190.0, 1190.0, 0.8403361344537815, 1.3130252100840336, 0.43822216386554624], "isController": false}, {"data": ["HTTP Request add new userr122", 1, 0, 0.0, 1918.0, 1918, 1918, 1918.0, 1918.0, 1918.0, 0.5213764337851929, 0.814650677789364, 0.2718896637122002], "isController": false}, {"data": ["HTTP Request add new userr123", 1, 0, 0.0, 579.0, 579, 579, 579.0, 579.0, 579.0, 1.7271157167530224, 2.6986183074265977, 0.900663860103627], "isController": false}, {"data": ["HTTP Request add new userr124", 1, 0, 0.0, 1177.0, 1177, 1177, 1177.0, 1177.0, 1177.0, 0.8496176720475787, 1.3275276125743416, 0.4430623406966865], "isController": false}, {"data": ["HTTP Request add new userr125", 1, 0, 0.0, 828.0, 828, 828, 828.0, 828.0, 828.0, 1.2077294685990339, 1.8870772946859904, 0.6298120471014493], "isController": false}, {"data": ["HTTP Request add new userr126", 1, 1, 100.0, 425.0, 425, 425, 425.0, 425.0, 425.0, 2.352941176470588, 1.7601102941176472, 1.2270220588235294], "isController": false}, {"data": ["HTTP Request add new userr127", 1, 0, 0.0, 970.0, 970, 970, 970.0, 970.0, 970.0, 1.0309278350515465, 1.6108247422680413, 0.5376127577319588], "isController": false}, {"data": ["HTTP Request add new userr128", 1, 0, 0.0, 451.0, 451, 451, 451.0, 451.0, 451.0, 2.2172949002217295, 3.464523281596452, 1.156284645232816], "isController": false}, {"data": ["HTTP Request add new userr129", 1, 0, 0.0, 593.0, 593, 593, 593.0, 593.0, 593.0, 1.6863406408094435, 2.634907251264756, 0.8794002951096122], "isController": false}, {"data": ["HTTP Request add new userr130", 1, 0, 0.0, 856.0, 856, 856, 856.0, 856.0, 856.0, 1.1682242990654206, 1.8253504672897196, 0.609210718457944], "isController": false}, {"data": ["HTTP Request add new userr131", 1, 0, 0.0, 464.0, 464, 464, 464.0, 464.0, 464.0, 2.155172413793103, 3.3674568965517238, 1.123888739224138], "isController": false}, {"data": ["HTTP Request add new userr132", 1, 0, 0.0, 1407.0, 1407, 1407, 1407.0, 1407.0, 1407.0, 0.7107320540156361, 1.1105188343994314, 0.37063566098081024], "isController": false}, {"data": ["HTTP Request add new userr133", 1, 0, 0.0, 744.0, 744, 744, 744.0, 744.0, 744.0, 1.3440860215053765, 2.1001344086021505, 0.7009198588709677], "isController": false}, {"data": ["HTTP Request add new userr134", 1, 0, 0.0, 1087.0, 1087, 1087, 1087.0, 1087.0, 1087.0, 0.9199632014719411, 1.437442502299908, 0.4797464351425943], "isController": false}, {"data": ["HTTP Request add new userr135", 1, 0, 0.0, 788.0, 788, 788, 788.0, 788.0, 788.0, 1.2690355329949237, 1.9828680203045685, 0.6617822017766497], "isController": false}, {"data": ["HTTP Request add new userr136", 1, 0, 0.0, 825.0, 825, 825, 825.0, 825.0, 825.0, 1.2121212121212122, 1.893939393939394, 0.6321022727272727], "isController": false}, {"data": ["HTTP Request add new userr137", 1, 0, 0.0, 560.0, 560, 560, 560.0, 560.0, 560.0, 1.7857142857142856, 2.790178571428571, 0.9312220982142856], "isController": false}, {"data": ["HTTP Request add new userr138", 1, 0, 0.0, 1625.0, 1625, 1625, 1625.0, 1625.0, 1625.0, 0.6153846153846154, 0.9615384615384616, 0.32091346153846156], "isController": false}, {"data": ["HTTP Request add new userr139", 1, 0, 0.0, 856.0, 856, 856, 856.0, 856.0, 856.0, 1.1682242990654206, 1.8253504672897196, 0.609210718457944], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": [{"data": ["500\/Internal Server Error", 5, 100.0, 10.0], "isController": false}]}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 50, 5, "500\/Internal Server Error", 5, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr142", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr108", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr113", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr119", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": ["HTTP Request add new userr126", 1, 1, "500\/Internal Server Error", 1, null, null, null, null, null, null, null, null], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
