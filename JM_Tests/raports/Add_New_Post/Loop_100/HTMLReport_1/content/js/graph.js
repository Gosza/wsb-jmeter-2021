/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 7271.0, "minX": 0.0, "maxY": 51588.0, "series": [{"data": [[0.0, 7271.0], [0.1, 7271.0], [0.2, 7271.0], [0.3, 7271.0], [0.4, 7271.0], [0.5, 7271.0], [0.6, 7271.0], [0.7, 7271.0], [0.8, 7271.0], [0.9, 7271.0], [1.0, 7344.0], [1.1, 7344.0], [1.2, 7344.0], [1.3, 7344.0], [1.4, 7344.0], [1.5, 7344.0], [1.6, 7344.0], [1.7, 7344.0], [1.8, 7344.0], [1.9, 7344.0], [2.0, 7349.0], [2.1, 7349.0], [2.2, 7349.0], [2.3, 7349.0], [2.4, 7349.0], [2.5, 7349.0], [2.6, 7349.0], [2.7, 7349.0], [2.8, 7349.0], [2.9, 7349.0], [3.0, 8344.0], [3.1, 8344.0], [3.2, 8344.0], [3.3, 8344.0], [3.4, 8344.0], [3.5, 8344.0], [3.6, 8344.0], [3.7, 8344.0], [3.8, 8344.0], [3.9, 8344.0], [4.0, 8488.0], [4.1, 8488.0], [4.2, 8488.0], [4.3, 8488.0], [4.4, 8488.0], [4.5, 8488.0], [4.6, 8488.0], [4.7, 8488.0], [4.8, 8488.0], [4.9, 8488.0], [5.0, 9663.0], [5.1, 9663.0], [5.2, 9663.0], [5.3, 9663.0], [5.4, 9663.0], [5.5, 9663.0], [5.6, 9663.0], [5.7, 9663.0], [5.8, 9663.0], [5.9, 9663.0], [6.0, 13284.0], [6.1, 13284.0], [6.2, 13284.0], [6.3, 13284.0], [6.4, 13284.0], [6.5, 13284.0], [6.6, 13284.0], [6.7, 13284.0], [6.8, 13284.0], [6.9, 13284.0], [7.0, 17026.0], [7.1, 17026.0], [7.2, 17026.0], [7.3, 17026.0], [7.4, 17026.0], [7.5, 17026.0], [7.6, 17026.0], [7.7, 17026.0], [7.8, 17026.0], [7.9, 17026.0], [8.0, 19154.0], [8.1, 19154.0], [8.2, 19154.0], [8.3, 19154.0], [8.4, 19154.0], [8.5, 19154.0], [8.6, 19154.0], [8.7, 19154.0], [8.8, 19154.0], [8.9, 19154.0], [9.0, 22354.0], [9.1, 22354.0], [9.2, 22354.0], [9.3, 22354.0], [9.4, 22354.0], [9.5, 22354.0], [9.6, 22354.0], [9.7, 22354.0], [9.8, 22354.0], [9.9, 22354.0], [10.0, 23422.0], [10.1, 23422.0], [10.2, 23422.0], [10.3, 23422.0], [10.4, 23422.0], [10.5, 23422.0], [10.6, 23422.0], [10.7, 23422.0], [10.8, 23422.0], [10.9, 23422.0], [11.0, 32555.0], [11.1, 32555.0], [11.2, 32555.0], [11.3, 32555.0], [11.4, 32555.0], [11.5, 32555.0], [11.6, 32555.0], [11.7, 32555.0], [11.8, 32555.0], [11.9, 32555.0], [12.0, 40624.0], [12.1, 40624.0], [12.2, 40624.0], [12.3, 40624.0], [12.4, 40624.0], [12.5, 40624.0], [12.6, 40624.0], [12.7, 40624.0], [12.8, 40624.0], [12.9, 40624.0], [13.0, 40698.0], [13.1, 40698.0], [13.2, 40698.0], [13.3, 40698.0], [13.4, 40698.0], [13.5, 40698.0], [13.6, 40698.0], [13.7, 40698.0], [13.8, 40698.0], [13.9, 40698.0], [14.0, 40702.0], [14.1, 40702.0], [14.2, 40702.0], [14.3, 40702.0], [14.4, 40702.0], [14.5, 40702.0], [14.6, 40702.0], [14.7, 40702.0], [14.8, 40702.0], [14.9, 40702.0], [15.0, 40790.0], [15.1, 40790.0], [15.2, 40790.0], [15.3, 40790.0], [15.4, 40790.0], [15.5, 40790.0], [15.6, 40790.0], [15.7, 40790.0], [15.8, 40790.0], [15.9, 40790.0], [16.0, 40950.0], [16.1, 40950.0], [16.2, 40950.0], [16.3, 40950.0], [16.4, 40950.0], [16.5, 40950.0], [16.6, 40950.0], [16.7, 40950.0], [16.8, 40950.0], [16.9, 40950.0], [17.0, 41206.0], [17.1, 41206.0], [17.2, 41206.0], [17.3, 41206.0], [17.4, 41206.0], [17.5, 41206.0], [17.6, 41206.0], [17.7, 41206.0], [17.8, 41206.0], [17.9, 41206.0], [18.0, 41613.0], [18.1, 41613.0], [18.2, 41613.0], [18.3, 41613.0], [18.4, 41613.0], [18.5, 41613.0], [18.6, 41613.0], [18.7, 41613.0], [18.8, 41613.0], [18.9, 41613.0], [19.0, 42034.0], [19.1, 42034.0], [19.2, 42034.0], [19.3, 42034.0], [19.4, 42034.0], [19.5, 42034.0], [19.6, 42034.0], [19.7, 42034.0], [19.8, 42034.0], [19.9, 42034.0], [20.0, 42100.0], [20.1, 42100.0], [20.2, 42100.0], [20.3, 42100.0], [20.4, 42100.0], [20.5, 42100.0], [20.6, 42100.0], [20.7, 42100.0], [20.8, 42100.0], [20.9, 42100.0], [21.0, 42162.0], [21.1, 42162.0], [21.2, 42162.0], [21.3, 42162.0], [21.4, 42162.0], [21.5, 42162.0], [21.6, 42162.0], [21.7, 42162.0], [21.8, 42162.0], [21.9, 42162.0], [22.0, 42346.0], [22.1, 42346.0], [22.2, 42346.0], [22.3, 42346.0], [22.4, 42346.0], [22.5, 42346.0], [22.6, 42346.0], [22.7, 42346.0], [22.8, 42346.0], [22.9, 42346.0], [23.0, 44113.0], [23.1, 44113.0], [23.2, 44113.0], [23.3, 44113.0], [23.4, 44113.0], [23.5, 44113.0], [23.6, 44113.0], [23.7, 44113.0], [23.8, 44113.0], [23.9, 44113.0], [24.0, 47141.0], [24.1, 47141.0], [24.2, 47141.0], [24.3, 47141.0], [24.4, 47141.0], [24.5, 47141.0], [24.6, 47141.0], [24.7, 47141.0], [24.8, 47141.0], [24.9, 47141.0], [25.0, 47269.0], [25.1, 47269.0], [25.2, 47269.0], [25.3, 47269.0], [25.4, 47269.0], [25.5, 47269.0], [25.6, 47269.0], [25.7, 47269.0], [25.8, 47269.0], [25.9, 47269.0], [26.0, 47571.0], [26.1, 47571.0], [26.2, 47571.0], [26.3, 47571.0], [26.4, 47571.0], [26.5, 47571.0], [26.6, 47571.0], [26.7, 47571.0], [26.8, 47571.0], [26.9, 47571.0], [27.0, 47948.0], [27.1, 47948.0], [27.2, 47948.0], [27.3, 47948.0], [27.4, 47948.0], [27.5, 47948.0], [27.6, 47948.0], [27.7, 47948.0], [27.8, 47948.0], [27.9, 47948.0], [28.0, 48006.0], [28.1, 48006.0], [28.2, 48006.0], [28.3, 48006.0], [28.4, 48006.0], [28.5, 48006.0], [28.6, 48006.0], [28.7, 48006.0], [28.8, 48006.0], [28.9, 48006.0], [29.0, 48083.0], [29.1, 48083.0], [29.2, 48083.0], [29.3, 48083.0], [29.4, 48083.0], [29.5, 48083.0], [29.6, 48083.0], [29.7, 48083.0], [29.8, 48083.0], [29.9, 48083.0], [30.0, 48215.0], [30.1, 48215.0], [30.2, 48215.0], [30.3, 48215.0], [30.4, 48215.0], [30.5, 48215.0], [30.6, 48215.0], [30.7, 48215.0], [30.8, 48215.0], [30.9, 48215.0], [31.0, 48375.0], [31.1, 48375.0], [31.2, 48375.0], [31.3, 48375.0], [31.4, 48375.0], [31.5, 48375.0], [31.6, 48375.0], [31.7, 48375.0], [31.8, 48375.0], [31.9, 48375.0], [32.0, 48417.0], [32.1, 48417.0], [32.2, 48417.0], [32.3, 48417.0], [32.4, 48417.0], [32.5, 48417.0], [32.6, 48417.0], [32.7, 48417.0], [32.8, 48417.0], [32.9, 48417.0], [33.0, 48423.0], [33.1, 48423.0], [33.2, 48423.0], [33.3, 48423.0], [33.4, 48423.0], [33.5, 48423.0], [33.6, 48423.0], [33.7, 48423.0], [33.8, 48423.0], [33.9, 48423.0], [34.0, 48514.0], [34.1, 48514.0], [34.2, 48514.0], [34.3, 48514.0], [34.4, 48514.0], [34.5, 48514.0], [34.6, 48514.0], [34.7, 48514.0], [34.8, 48514.0], [34.9, 48514.0], [35.0, 49040.0], [35.1, 49040.0], [35.2, 49040.0], [35.3, 49040.0], [35.4, 49040.0], [35.5, 49040.0], [35.6, 49040.0], [35.7, 49040.0], [35.8, 49040.0], [35.9, 49040.0], [36.0, 49215.0], [36.1, 49215.0], [36.2, 49215.0], [36.3, 49215.0], [36.4, 49215.0], [36.5, 49215.0], [36.6, 49215.0], [36.7, 49215.0], [36.8, 49215.0], [36.9, 49215.0], [37.0, 49521.0], [37.1, 49521.0], [37.2, 49521.0], [37.3, 49521.0], [37.4, 49521.0], [37.5, 49521.0], [37.6, 49521.0], [37.7, 49521.0], [37.8, 49521.0], [37.9, 49521.0], [38.0, 49597.0], [38.1, 49597.0], [38.2, 49597.0], [38.3, 49597.0], [38.4, 49597.0], [38.5, 49597.0], [38.6, 49597.0], [38.7, 49597.0], [38.8, 49597.0], [38.9, 49597.0], [39.0, 49617.0], [39.1, 49617.0], [39.2, 49617.0], [39.3, 49617.0], [39.4, 49617.0], [39.5, 49617.0], [39.6, 49617.0], [39.7, 49617.0], [39.8, 49617.0], [39.9, 49617.0], [40.0, 49621.0], [40.1, 49621.0], [40.2, 49621.0], [40.3, 49621.0], [40.4, 49621.0], [40.5, 49621.0], [40.6, 49621.0], [40.7, 49621.0], [40.8, 49621.0], [40.9, 49621.0], [41.0, 49630.0], [41.1, 49630.0], [41.2, 49630.0], [41.3, 49630.0], [41.4, 49630.0], [41.5, 49630.0], [41.6, 49630.0], [41.7, 49630.0], [41.8, 49630.0], [41.9, 49630.0], [42.0, 50012.0], [42.1, 50012.0], [42.2, 50012.0], [42.3, 50012.0], [42.4, 50012.0], [42.5, 50012.0], [42.6, 50012.0], [42.7, 50012.0], [42.8, 50012.0], [42.9, 50012.0], [43.0, 50059.0], [43.1, 50059.0], [43.2, 50059.0], [43.3, 50059.0], [43.4, 50059.0], [43.5, 50059.0], [43.6, 50059.0], [43.7, 50059.0], [43.8, 50059.0], [43.9, 50059.0], [44.0, 50143.0], [44.1, 50143.0], [44.2, 50143.0], [44.3, 50143.0], [44.4, 50143.0], [44.5, 50143.0], [44.6, 50143.0], [44.7, 50143.0], [44.8, 50143.0], [44.9, 50143.0], [45.0, 50189.0], [45.1, 50189.0], [45.2, 50189.0], [45.3, 50189.0], [45.4, 50189.0], [45.5, 50189.0], [45.6, 50189.0], [45.7, 50189.0], [45.8, 50189.0], [45.9, 50189.0], [46.0, 50342.0], [46.1, 50342.0], [46.2, 50342.0], [46.3, 50342.0], [46.4, 50342.0], [46.5, 50342.0], [46.6, 50342.0], [46.7, 50342.0], [46.8, 50342.0], [46.9, 50342.0], [47.0, 50364.0], [47.1, 50364.0], [47.2, 50364.0], [47.3, 50364.0], [47.4, 50364.0], [47.5, 50364.0], [47.6, 50364.0], [47.7, 50364.0], [47.8, 50364.0], [47.9, 50364.0], [48.0, 50382.0], [48.1, 50382.0], [48.2, 50382.0], [48.3, 50382.0], [48.4, 50382.0], [48.5, 50382.0], [48.6, 50382.0], [48.7, 50382.0], [48.8, 50382.0], [48.9, 50382.0], [49.0, 50435.0], [49.1, 50435.0], [49.2, 50435.0], [49.3, 50435.0], [49.4, 50435.0], [49.5, 50435.0], [49.6, 50435.0], [49.7, 50435.0], [49.8, 50435.0], [49.9, 50435.0], [50.0, 50462.0], [50.1, 50462.0], [50.2, 50462.0], [50.3, 50462.0], [50.4, 50462.0], [50.5, 50462.0], [50.6, 50462.0], [50.7, 50462.0], [50.8, 50462.0], [50.9, 50462.0], [51.0, 50487.0], [51.1, 50487.0], [51.2, 50487.0], [51.3, 50487.0], [51.4, 50487.0], [51.5, 50487.0], [51.6, 50487.0], [51.7, 50487.0], [51.8, 50487.0], [51.9, 50487.0], [52.0, 50533.0], [52.1, 50533.0], [52.2, 50533.0], [52.3, 50533.0], [52.4, 50533.0], [52.5, 50533.0], [52.6, 50533.0], [52.7, 50533.0], [52.8, 50533.0], [52.9, 50533.0], [53.0, 50597.0], [53.1, 50597.0], [53.2, 50597.0], [53.3, 50597.0], [53.4, 50597.0], [53.5, 50597.0], [53.6, 50597.0], [53.7, 50597.0], [53.8, 50597.0], [53.9, 50597.0], [54.0, 50621.0], [54.1, 50621.0], [54.2, 50621.0], [54.3, 50621.0], [54.4, 50621.0], [54.5, 50621.0], [54.6, 50621.0], [54.7, 50621.0], [54.8, 50621.0], [54.9, 50621.0], [55.0, 50639.0], [55.1, 50639.0], [55.2, 50639.0], [55.3, 50639.0], [55.4, 50639.0], [55.5, 50639.0], [55.6, 50639.0], [55.7, 50639.0], [55.8, 50639.0], [55.9, 50639.0], [56.0, 50686.0], [56.1, 50686.0], [56.2, 50686.0], [56.3, 50686.0], [56.4, 50686.0], [56.5, 50686.0], [56.6, 50686.0], [56.7, 50686.0], [56.8, 50686.0], [56.9, 50686.0], [57.0, 50691.0], [57.1, 50691.0], [57.2, 50691.0], [57.3, 50691.0], [57.4, 50691.0], [57.5, 50691.0], [57.6, 50691.0], [57.7, 50691.0], [57.8, 50691.0], [57.9, 50691.0], [58.0, 50698.0], [58.1, 50698.0], [58.2, 50698.0], [58.3, 50698.0], [58.4, 50698.0], [58.5, 50698.0], [58.6, 50698.0], [58.7, 50698.0], [58.8, 50698.0], [58.9, 50698.0], [59.0, 50732.0], [59.1, 50732.0], [59.2, 50732.0], [59.3, 50732.0], [59.4, 50732.0], [59.5, 50732.0], [59.6, 50732.0], [59.7, 50732.0], [59.8, 50732.0], [59.9, 50732.0], [60.0, 50760.0], [60.1, 50760.0], [60.2, 50760.0], [60.3, 50760.0], [60.4, 50760.0], [60.5, 50760.0], [60.6, 50760.0], [60.7, 50760.0], [60.8, 50760.0], [60.9, 50760.0], [61.0, 50785.0], [61.1, 50785.0], [61.2, 50785.0], [61.3, 50785.0], [61.4, 50785.0], [61.5, 50785.0], [61.6, 50785.0], [61.7, 50785.0], [61.8, 50785.0], [61.9, 50785.0], [62.0, 50801.0], [62.1, 50801.0], [62.2, 50801.0], [62.3, 50801.0], [62.4, 50801.0], [62.5, 50801.0], [62.6, 50801.0], [62.7, 50801.0], [62.8, 50801.0], [62.9, 50801.0], [63.0, 50810.0], [63.1, 50810.0], [63.2, 50810.0], [63.3, 50810.0], [63.4, 50810.0], [63.5, 50810.0], [63.6, 50810.0], [63.7, 50810.0], [63.8, 50810.0], [63.9, 50810.0], [64.0, 50872.0], [64.1, 50872.0], [64.2, 50872.0], [64.3, 50872.0], [64.4, 50872.0], [64.5, 50872.0], [64.6, 50872.0], [64.7, 50872.0], [64.8, 50872.0], [64.9, 50872.0], [65.0, 50890.0], [65.1, 50890.0], [65.2, 50890.0], [65.3, 50890.0], [65.4, 50890.0], [65.5, 50890.0], [65.6, 50890.0], [65.7, 50890.0], [65.8, 50890.0], [65.9, 50890.0], [66.0, 50894.0], [66.1, 50894.0], [66.2, 50894.0], [66.3, 50894.0], [66.4, 50894.0], [66.5, 50894.0], [66.6, 50894.0], [66.7, 50894.0], [66.8, 50894.0], [66.9, 50894.0], [67.0, 50894.0], [67.1, 50894.0], [67.2, 50894.0], [67.3, 50894.0], [67.4, 50894.0], [67.5, 50894.0], [67.6, 50894.0], [67.7, 50894.0], [67.8, 50894.0], [67.9, 50894.0], [68.0, 50906.0], [68.1, 50906.0], [68.2, 50906.0], [68.3, 50906.0], [68.4, 50906.0], [68.5, 50906.0], [68.6, 50906.0], [68.7, 50906.0], [68.8, 50906.0], [68.9, 50906.0], [69.0, 50928.0], [69.1, 50928.0], [69.2, 50928.0], [69.3, 50928.0], [69.4, 50928.0], [69.5, 50928.0], [69.6, 50928.0], [69.7, 50928.0], [69.8, 50928.0], [69.9, 50928.0], [70.0, 50945.0], [70.1, 50945.0], [70.2, 50945.0], [70.3, 50945.0], [70.4, 50945.0], [70.5, 50945.0], [70.6, 50945.0], [70.7, 50945.0], [70.8, 50945.0], [70.9, 50945.0], [71.0, 50967.0], [71.1, 50967.0], [71.2, 50967.0], [71.3, 50967.0], [71.4, 50967.0], [71.5, 50967.0], [71.6, 50967.0], [71.7, 50967.0], [71.8, 50967.0], [71.9, 50967.0], [72.0, 50969.0], [72.1, 50969.0], [72.2, 50969.0], [72.3, 50969.0], [72.4, 50969.0], [72.5, 50969.0], [72.6, 50969.0], [72.7, 50969.0], [72.8, 50969.0], [72.9, 50969.0], [73.0, 50976.0], [73.1, 50976.0], [73.2, 50976.0], [73.3, 50976.0], [73.4, 50976.0], [73.5, 50976.0], [73.6, 50976.0], [73.7, 50976.0], [73.8, 50976.0], [73.9, 50976.0], [74.0, 51002.0], [74.1, 51002.0], [74.2, 51002.0], [74.3, 51002.0], [74.4, 51002.0], [74.5, 51002.0], [74.6, 51002.0], [74.7, 51002.0], [74.8, 51002.0], [74.9, 51002.0], [75.0, 51019.0], [75.1, 51019.0], [75.2, 51019.0], [75.3, 51019.0], [75.4, 51019.0], [75.5, 51019.0], [75.6, 51019.0], [75.7, 51019.0], [75.8, 51019.0], [75.9, 51019.0], [76.0, 51036.0], [76.1, 51036.0], [76.2, 51036.0], [76.3, 51036.0], [76.4, 51036.0], [76.5, 51036.0], [76.6, 51036.0], [76.7, 51036.0], [76.8, 51036.0], [76.9, 51036.0], [77.0, 51054.0], [77.1, 51054.0], [77.2, 51054.0], [77.3, 51054.0], [77.4, 51054.0], [77.5, 51054.0], [77.6, 51054.0], [77.7, 51054.0], [77.8, 51054.0], [77.9, 51054.0], [78.0, 51067.0], [78.1, 51067.0], [78.2, 51067.0], [78.3, 51067.0], [78.4, 51067.0], [78.5, 51067.0], [78.6, 51067.0], [78.7, 51067.0], [78.8, 51067.0], [78.9, 51067.0], [79.0, 51075.0], [79.1, 51075.0], [79.2, 51075.0], [79.3, 51075.0], [79.4, 51075.0], [79.5, 51075.0], [79.6, 51075.0], [79.7, 51075.0], [79.8, 51075.0], [79.9, 51075.0], [80.0, 51093.0], [80.1, 51093.0], [80.2, 51093.0], [80.3, 51093.0], [80.4, 51093.0], [80.5, 51093.0], [80.6, 51093.0], [80.7, 51093.0], [80.8, 51093.0], [80.9, 51093.0], [81.0, 51106.0], [81.1, 51106.0], [81.2, 51106.0], [81.3, 51106.0], [81.4, 51106.0], [81.5, 51106.0], [81.6, 51106.0], [81.7, 51106.0], [81.8, 51106.0], [81.9, 51106.0], [82.0, 51133.0], [82.1, 51133.0], [82.2, 51133.0], [82.3, 51133.0], [82.4, 51133.0], [82.5, 51133.0], [82.6, 51133.0], [82.7, 51133.0], [82.8, 51133.0], [82.9, 51133.0], [83.0, 51145.0], [83.1, 51145.0], [83.2, 51145.0], [83.3, 51145.0], [83.4, 51145.0], [83.5, 51145.0], [83.6, 51145.0], [83.7, 51145.0], [83.8, 51145.0], [83.9, 51145.0], [84.0, 51146.0], [84.1, 51146.0], [84.2, 51146.0], [84.3, 51146.0], [84.4, 51146.0], [84.5, 51146.0], [84.6, 51146.0], [84.7, 51146.0], [84.8, 51146.0], [84.9, 51146.0], [85.0, 51146.0], [85.1, 51146.0], [85.2, 51146.0], [85.3, 51146.0], [85.4, 51146.0], [85.5, 51146.0], [85.6, 51146.0], [85.7, 51146.0], [85.8, 51146.0], [85.9, 51146.0], [86.0, 51151.0], [86.1, 51151.0], [86.2, 51151.0], [86.3, 51151.0], [86.4, 51151.0], [86.5, 51151.0], [86.6, 51151.0], [86.7, 51151.0], [86.8, 51151.0], [86.9, 51151.0], [87.0, 51162.0], [87.1, 51162.0], [87.2, 51162.0], [87.3, 51162.0], [87.4, 51162.0], [87.5, 51162.0], [87.6, 51162.0], [87.7, 51162.0], [87.8, 51162.0], [87.9, 51162.0], [88.0, 51192.0], [88.1, 51192.0], [88.2, 51192.0], [88.3, 51192.0], [88.4, 51192.0], [88.5, 51192.0], [88.6, 51192.0], [88.7, 51192.0], [88.8, 51192.0], [88.9, 51192.0], [89.0, 51200.0], [89.1, 51200.0], [89.2, 51200.0], [89.3, 51200.0], [89.4, 51200.0], [89.5, 51200.0], [89.6, 51200.0], [89.7, 51200.0], [89.8, 51200.0], [89.9, 51200.0], [90.0, 51207.0], [90.1, 51207.0], [90.2, 51207.0], [90.3, 51207.0], [90.4, 51207.0], [90.5, 51207.0], [90.6, 51207.0], [90.7, 51207.0], [90.8, 51207.0], [90.9, 51207.0], [91.0, 51233.0], [91.1, 51233.0], [91.2, 51233.0], [91.3, 51233.0], [91.4, 51233.0], [91.5, 51233.0], [91.6, 51233.0], [91.7, 51233.0], [91.8, 51233.0], [91.9, 51233.0], [92.0, 51234.0], [92.1, 51234.0], [92.2, 51234.0], [92.3, 51234.0], [92.4, 51234.0], [92.5, 51234.0], [92.6, 51234.0], [92.7, 51234.0], [92.8, 51234.0], [92.9, 51234.0], [93.0, 51247.0], [93.1, 51247.0], [93.2, 51247.0], [93.3, 51247.0], [93.4, 51247.0], [93.5, 51247.0], [93.6, 51247.0], [93.7, 51247.0], [93.8, 51247.0], [93.9, 51247.0], [94.0, 51265.0], [94.1, 51265.0], [94.2, 51265.0], [94.3, 51265.0], [94.4, 51265.0], [94.5, 51265.0], [94.6, 51265.0], [94.7, 51265.0], [94.8, 51265.0], [94.9, 51265.0], [95.0, 51311.0], [95.1, 51311.0], [95.2, 51311.0], [95.3, 51311.0], [95.4, 51311.0], [95.5, 51311.0], [95.6, 51311.0], [95.7, 51311.0], [95.8, 51311.0], [95.9, 51311.0], [96.0, 51349.0], [96.1, 51349.0], [96.2, 51349.0], [96.3, 51349.0], [96.4, 51349.0], [96.5, 51349.0], [96.6, 51349.0], [96.7, 51349.0], [96.8, 51349.0], [96.9, 51349.0], [97.0, 51404.0], [97.1, 51404.0], [97.2, 51404.0], [97.3, 51404.0], [97.4, 51404.0], [97.5, 51404.0], [97.6, 51404.0], [97.7, 51404.0], [97.8, 51404.0], [97.9, 51404.0], [98.0, 51519.0], [98.1, 51519.0], [98.2, 51519.0], [98.3, 51519.0], [98.4, 51519.0], [98.5, 51519.0], [98.6, 51519.0], [98.7, 51519.0], [98.8, 51519.0], [98.9, 51519.0], [99.0, 51588.0], [99.1, 51588.0], [99.2, 51588.0], [99.3, 51588.0], [99.4, 51588.0], [99.5, 51588.0], [99.6, 51588.0], [99.7, 51588.0], [99.8, 51588.0], [99.9, 51588.0]], "isOverall": false, "label": "HTTP Request create post", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 7200.0, "maxY": 8.0, "series": [{"data": [[8300.0, 1.0], [8400.0, 1.0], [9600.0, 1.0], [13200.0, 1.0], [17000.0, 1.0], [19100.0, 1.0], [22300.0, 1.0], [23400.0, 1.0], [32500.0, 1.0], [40600.0, 2.0], [40700.0, 2.0], [40900.0, 1.0], [41200.0, 1.0], [41600.0, 1.0], [42000.0, 1.0], [42100.0, 2.0], [42300.0, 1.0], [44100.0, 1.0], [47100.0, 1.0], [47500.0, 1.0], [47200.0, 1.0], [48300.0, 1.0], [48500.0, 1.0], [48000.0, 2.0], [47900.0, 1.0], [48200.0, 1.0], [48400.0, 2.0], [49000.0, 1.0], [50700.0, 3.0], [49500.0, 2.0], [49600.0, 3.0], [50800.0, 6.0], [50900.0, 6.0], [51000.0, 7.0], [51100.0, 8.0], [50000.0, 2.0], [49200.0, 1.0], [50100.0, 2.0], [50300.0, 3.0], [50400.0, 3.0], [50500.0, 2.0], [50600.0, 5.0], [51200.0, 6.0], [51300.0, 2.0], [51400.0, 1.0], [51500.0, 2.0], [7300.0, 2.0], [7200.0, 1.0]], "isOverall": false, "label": "HTTP Request create post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 51500.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 100.0, "minX": 2.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 100.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [[2.0, 100.0]], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 2.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 50.51000000000001, "minX": 1.6229223E12, "maxY": 50.51000000000001, "series": [{"data": [[1.6229223E12, 50.51000000000001]], "isOverall": false, "label": "User_Thread Group", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229223E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 7271.0, "minX": 1.0, "maxY": 51588.0, "series": [{"data": [[2.0, 51200.0], [3.0, 51192.0], [4.0, 51247.0], [5.0, 51145.0], [6.0, 51151.0], [7.0, 51162.0], [8.0, 51146.0], [9.0, 51146.0], [10.0, 51075.0], [11.0, 51133.0], [13.0, 51080.0], [14.0, 51093.0], [15.0, 51019.0], [16.0, 50969.0], [17.0, 51002.0], [18.0, 51036.0], [19.0, 50928.0], [20.0, 51067.0], [21.0, 50976.0], [22.0, 50945.0], [23.0, 50967.0], [24.0, 50890.0], [25.0, 50906.0], [26.0, 50894.0], [27.0, 50810.0], [28.0, 51588.0], [29.0, 50801.0], [30.0, 50698.0], [31.0, 50686.0], [33.0, 50691.0], [32.0, 50760.0], [35.0, 51404.0], [34.0, 51519.0], [37.0, 50597.0], [36.0, 50621.0], [39.0, 51349.0], [38.0, 50435.0], [41.0, 51207.0], [40.0, 51311.0], [43.0, 51265.0], [42.0, 51234.0], [45.0, 50894.0], [44.0, 50872.0], [47.0, 50732.0], [46.0, 50785.0], [49.0, 49630.0], [48.0, 50639.0], [51.0, 50487.0], [50.0, 50533.0], [53.0, 50462.0], [52.0, 50382.0], [55.0, 50342.0], [54.0, 50364.0], [57.0, 50189.0], [56.0, 50143.0], [59.0, 50012.0], [58.0, 49215.0], [61.0, 49597.0], [60.0, 50059.0], [63.0, 49621.0], [62.0, 49040.0], [67.0, 48423.0], [66.0, 48417.0], [65.0, 49521.0], [64.0, 49617.0], [71.0, 48083.0], [70.0, 47948.0], [69.0, 48006.0], [68.0, 48215.0], [75.0, 47571.0], [74.0, 47269.0], [73.0, 48375.0], [72.0, 48514.0], [79.0, 42162.0], [78.0, 42346.0], [77.0, 44113.0], [76.0, 47141.0], [83.0, 41206.0], [82.0, 41613.0], [81.0, 42034.0], [80.0, 42100.0], [87.0, 40702.0], [86.0, 40698.0], [85.0, 40790.0], [84.0, 40950.0], [91.0, 22354.0], [90.0, 23422.0], [89.0, 32555.0], [88.0, 40624.0], [95.0, 9663.0], [94.0, 13284.0], [93.0, 17026.0], [92.0, 19154.0], [99.0, 7271.0], [98.0, 7349.0], [97.0, 8344.0], [96.0, 8488.0], [100.0, 7344.0], [1.0, 51233.0]], "isOverall": false, "label": "HTTP Request create post", "isController": false}, {"data": [[50.51000000000001, 45013.48]], "isOverall": false, "label": "HTTP Request create post-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 820.0, "minX": 1.6229223E12, "maxY": 5764.9, "series": [{"data": [[1.6229223E12, 5764.9]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.6229223E12, 820.0]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229223E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 45013.48, "minX": 1.6229223E12, "maxY": 45013.48, "series": [{"data": [[1.6229223E12, 45013.48]], "isOverall": false, "label": "HTTP Request create post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6229223E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 45013.42000000001, "minX": 1.6229223E12, "maxY": 45013.42000000001, "series": [{"data": [[1.6229223E12, 45013.42000000001]], "isOverall": false, "label": "HTTP Request create post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6229223E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 2.9700000000000015, "minX": 1.6229223E12, "maxY": 2.9700000000000015, "series": [{"data": [[1.6229223E12, 2.9700000000000015]], "isOverall": false, "label": "HTTP Request create post", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6229223E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 7271.0, "minX": 1.6229223E12, "maxY": 51588.0, "series": [{"data": [[1.6229223E12, 51588.0]], "isOverall": false, "label": "Max", "isController": false}, {"data": [[1.6229223E12, 7271.0]], "isOverall": false, "label": "Min", "isController": false}, {"data": [[1.6229223E12, 51206.3]], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [[1.6229223E12, 51587.31]], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [[1.6229223E12, 51308.7]], "isOverall": false, "label": "95th percentile", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229223E12, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 8416.0, "minX": 1.0, "maxY": 51075.0, "series": [{"data": [[2.0, 8416.0], [1.0, 22354.0], [4.0, 45055.0], [8.0, 48731.5], [22.0, 50841.0], [3.0, 27245.0], [6.0, 40746.0], [25.0, 51075.0], [14.0, 50265.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 25.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 8415.5, "minX": 1.0, "maxY": 51075.0, "series": [{"data": [[2.0, 8415.5], [1.0, 22354.0], [4.0, 45055.0], [8.0, 48731.5], [22.0, 50841.0], [3.0, 27245.0], [6.0, 40746.0], [25.0, 51075.0], [14.0, 50265.5]], "isOverall": false, "label": "Successes", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 25.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.6229223E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.6229223E12, 1.6666666666666667]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229223E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.6229223E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.6229223E12, 1.6666666666666667]], "isOverall": false, "label": "201", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.6229223E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.6229223E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.6229223E12, 1.6666666666666667]], "isOverall": false, "label": "HTTP Request create post-success", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6229223E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 1.6666666666666667, "minX": 1.6229223E12, "maxY": 1.6666666666666667, "series": [{"data": [[1.6229223E12, 1.6666666666666667]], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.6229223E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, 7200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

